<?php echo "<?php \$this->Html->addCrumb('{$modelClass}', '/admin/{$pluralVar}'); \$paginationVariables = \$this->Paginator->params();?>\n"; ?>
<?php echo "<div style='padding-bottom:10px;'><?php echo \$this->Session->flash(); ?></div>\n"; ?>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?>
                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                        <?php echo "<?php echo \$this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('{$pluralHumanName}'), 'action' => 'index', 'admin' => true))); ?>\n"; ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <?php echo "<?php echo \$this->Html->link(__('New " . $singularHumanName . " <i class=\"fa fa-plus\"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?>"; ?> 
                        </div>
                        <button class="btn default"  onclick="window.location.href='<?php echo "<?php echo \$this->webroot.'admin/{$pluralHumanName}/export'; ?>"; ?>'">Export to CSV <i class="fa fa-download"></i> </button>
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <?php foreach ($fields as $field):if ( $field != $primaryKey && $field!='created' && $field!= 'modified'  && $field!= 'password') { ?><th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th><?php }endforeach; echo "\n";?>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php echo "<?php  if(isset(\${$pluralVar}) && sizeof(\${$pluralVar})>0) {?>\n" ?>
                  <tbody>
                  <?php	echo "<?php \$slno=0; foreach (\${$pluralVar} as \${$singularVar}): \$slno++?>\n"; ?>
                  <tr>
                     <td><?php echo "<?php echo \$slno+\$paginationVariables['limit']*(\$paginationVariables['page']-1); ?>"?></td>
        					<?php  foreach ($fields as $field) {
        						$isKey = false;
        						if (!empty($associations['belongsTo'])) {
        							foreach ($associations['belongsTo'] as $alias => $details) {
        								if ($field === $details['foreignKey']) {
        									$isKey = true;
        									echo "<td>\n\t\t\t\t\t\t\t\t\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
        									break;
        								}
        							}
        						}
  						if($schema[$field]['type'] == 'boolean'){
  							echo "\t\t<td><?php echo \$toggle_html[\${$singularVar}['{$modelClass}']['{$field}']]; ?>";
  							echo "\t\t<?php echo \$this->Form->input('{$field}', array('class' => 'ts-toogle-input', 'label' => false,  'required' => false,'type' => 'hidden','rel' => '{$modelClass}', 'id' => \${$singularVar}['{$modelClass}']['id'], 'value' => '{$field}'));?>\n";
  							echo "</td>\n";
  						}
  						else if ($isKey !== true && $field!=$primaryKey  && $field!='created' && $field!= 'modified'  && $field!= 'password') {
  							echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
  						}
  					}
  					echo "\t\t\t\t\t\t\t\t\t\t\t<td>\n";
  					echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<?php echo \$this->Html->link(__('<i class=\"fa fa-edit\"></i> Edit'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>\n";
  					echo "\t\t\t\t\t\t\t\t\t\t\t</td>\n";
  					echo "\t\t\t\t\t\t\t\t\t\t\t<td>\n";
  					echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<?php echo \$this->Form->postLink(__('<i class=\"fa fa-trash-o\"></i> Delete'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>\n";
  					echo "\t\t\t\t\t\t\t\t\t\t\t</td>\n";
  					echo "\t\t\t\t\t\t\t\t\t\t</tr>\n";
  					echo "\t\t\t\t\t\t\t\t\t<?php endforeach; ?>\n";
  				?>
                  <?php echo " <?php } else {?>\n"?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php echo "<?php }?>\n"?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo "<?php echo \$this->Form->create('', array( 'url' => array('controller' => strtolower('{$pluralHumanName}'), 'action' => 'index'))); ?>\n"; ?>
                            <label>Show 
                          <?php
	                          echo "\t\t\t<?php echo \$this->Form->input('limit', array('name' => 'data[{$singularHumanName}][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>\$default_limit_dropdown, 'default' => \$limit, 'div' => false));?>\n";
                           ?>
                            records
                            </label>
                            <?php echo "<?php echo \$this->Form->end(); ?>\n" ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php 
                                echo "<?php \$totalItem = \$this->Paginator->counter('{:count}')?>";
                                echo "<?php \$currentItem = \$this->Paginator->counter('{:current}')?>"; 
                                echo "<?php if(\$totalItem>\$currentItem) {?>\n";
                            ?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                <?php
                                    echo "\t\t\t";
                                    echo "<?php echo \$this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>\n";
                                    echo "\t\n"; 
                                ?>
                                </li>
                                <li>
                                <?php
                                    echo "\t\t\t<?php";
                                    echo "\t\techo \$this->Paginator->numbers(array('separator' => ''));"; 
                                    echo "\t?>\n";
                                    ?>
                                </li>
                                <li class="next disabled">
                                <?php
                                    echo "\t\t\t<?php";
                                    echo "\t\techo \$this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));";
                                    echo "\t?>\n";
                                ?>
                                </li>
                            </ul>
                      <?php echo "<?php }?>\n"?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
