<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php /*?><div class="<?php echo $pluralVar; ?> form">
<?php echo "<?php echo \$this->Form->create('{$modelClass}'); ?>\n"; ?>
	<fieldset>
		<legend><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></legend>
<?php
		echo "\t<?php\n";
		foreach ($fields as $field) {
			if (strpos($action, 'add') !== false && $field == $primaryKey) {
				continue;
			} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
				echo "\t\techo \$this->Form->input('{$field}');\n";
			}
		}
		if (!empty($associations['hasAndBelongsToMany'])) {
			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
				echo "\t\techo \$this->Form->input('{$assocName}');\n";
			}
		}
		echo "\t?>\n";
?>
	</fieldset>
<?php
	echo "<?php echo \$this->Form->end(__('Submit')); ?>\n";
?>
</div>
<div class="actions">
	<h3><?php echo "<?php echo __('Actions'); ?>"; ?></h3>
	<ul>

<?php if (strpos($action, 'add') === false): ?>
		<li><?php echo "<?php echo \$this->Form->postLink(__('Delete'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), null, __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>"; ?></li>
<?php endif; ?>
		<li><?php echo "<?php echo \$this->Html->link(__('List " . $pluralHumanName . "'), array('action' => 'index')); ?>"; ?></li>
<?php
		$done = array();
		foreach ($associations as $type => $data) {
			foreach ($data as $alias => $details) {
				if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
					echo "\t\t<li><?php echo \$this->Html->link(__('List " . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index')); ?> </li>\n";
					echo "\t\t<li><?php echo \$this->Html->link(__('New " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add')); ?> </li>\n";
					$done[] = $details['controller'];
				}
			}
		}
?>
	</ul>
</div><?php */?>
<?php echo "<?php \$this->Html->addCrumb('{$modelClass}', '/admin/{$pluralVar}'); ?>\n"; ?>
<?php echo "<?php \$this->Html->addCrumb('".Inflector::humanize(str_replace("admin_", "", $action))."', ''); ?>\n"; ?>
<?php echo "<div style='padding-bottom:10px;'><?php echo \$this->Session->flash(); ?></div>\n"; ?>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#<?php echo $singularHumanName.str_replace(" ", "",Inflector::humanize($action));?>Form').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		<?php 
			foreach($fields as $field){
				if (strpos($action, 'add') !== false && $field == $primaryKey) {
					continue;
				} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
					echo '"data['.$singularHumanName.']['.$field.']" : {required : true},'."\n";
				}
			}
		?>
		},
		messages:{
		<?php 
		foreach($fields as $field){
			if (strpos($action, 'add') !== false && $field == $primaryKey) {
				continue;
			} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
			if($schema[$field]['type'] == 'boolean')
				echo '"data['.$singularHumanName.']['.$field.']" : {required :""},'."\n";
			else
				echo '"data['.$singularHumanName.']['.$field.']" : {required :"Please enter '.$field.'."},'."\n";
			}
		}
		?>
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize(str_replace("admin_", "", $action)), $singularHumanName); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form <?php echo $pluralVar; ?>">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo "<?php echo \$this->Form->create('{$modelClass}', array('class' => 'form-horizontal')); ?>\n"; ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <?php
                            foreach ($fields as $field) {
                                if (strpos($action, 'add') !== false && $field == $primaryKey) {
                                    continue;
                                } elseif (!in_array($field, array('created', 'modified', 'updated'))) {
                                echo '<div class="form-group">'."\n";
                                echo "\t\t\t\t\t\t\t <?php echo \$this->Form->label('{$field}<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>\n";
                                echo "\t\t\t\t\t\t\t<div class='col-md-4'>";
                                if($schema[$field]['type'] == 'boolean'){ 
									/*echo "\t\t\t\t\t\t".'<label class="checkbox-inline">'."\n";
									echo "\t\t\t\t\t\t<?php echo \$this->Form->input('{$field}', array('class' => 'form-control', 'label' => false,  'required' => false,'hiddenField' => false));?>";
									echo "\n\t\t\t\t\t\t".'</label>';*/
									$default_val = strpos($action, 'add') !== false? ", 'default' => 1": "";
									$bool = $field == 'status'? '$boolean_values_status':'$boolean_values';
									echo "\n".'<div class="radio-list">';
									echo "\t\t\t\t\t\t<?php echo \$this->Form->input('{$field}', array('label' => false,  'required' => false,'hiddenField' => false, 'type' => 'radio', 'options' => {$bool}, 'legend' => false ,'div' => false, 'before' => '<label class=\"radio-inline\">', 'after' => '</label>', 'separator' => '</label><label class=\"radio-inline\">' {$default_val}));?>";
									echo "\n".'</div>';
								} else{
                                echo "\n\t\t\t\t\t\t\t\t<?php";
                                echo " echo \$this->Form->input('{$field}', array('class' => 'form-control', 'label' => false, 'required' => false));?>\n";
                           		 }
                                
                                echo "\t\t\t\t\t\t\t</div>\n";
                                echo "\t\t\t\t\t\t</div>\n";
                                }
                            }
                            if (!empty($associations['hasAndBelongsToMany'])) {
                                
                                foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                                    echo "\t\techo \$this->Form->input('{$assocName}');\n";
                                }
                                
                            }
                        ?>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo "<?php echo \$this->webroot.'admin/{$pluralVar}'; ?>"; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php
                        
                            echo "<?php echo \$this->Form->end(); ?>\n";
                        ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>