<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php echo "<?php \$this->Html->addCrumb('{$modelClass}', '/admin/{$pluralVar}'); ?>\n"; ?>
<?php echo "<?php \$this->Html->addCrumb('".Inflector::humanize(str_replace("admin_", "", $action))."', ''); ?>\n"; ?>
<?php echo "<div style='padding-bottom:10px;'><?php echo \$this->Session->flash(); ?></div>\n"; ?>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize(str_replace("admin_", "", $action)), $singularHumanName); ?>
                        <?php echo "\t\t\t\t</div>\n"; ?>
	                        <div class="tools">
	                            <a href="javascript:;" class="collapse"></a>
	                        </div>
	                    </div>
                    	<div class="portlet-body form <?php echo $pluralVar; ?>">
		                    <div class="alert alert-danger display-hide">
		                        <button data-close="alert" class="close"></button>
		                        You have some form errors. Please check below.
		                    </div>
		                    <!-- BEGIN FORM-->
		                    <?php echo "<?php echo \$this->Form->create('{$modelClass}', array('class' => 'form-horizontal')); ?>\n"; ?>
		                    <div class="form-body">                      
		                        <?php
		                            foreach ($fields as $field) {
		                                if (strpos($action, 'add') !== false && $field == $primaryKey) {
		                                    continue;
		                                } elseif (!in_array($field, array('created', 'modified', 'updated','id'))) {
		                                echo "\t\t\t\t\t\t<div class='form-group'>"."\n";
		                                echo "\t\t\t\t\t\t\t <?php echo \$this->Form->label('{$field}:' ,null, array('class' => 'col-md-3 control-label')); ?>\n";
		                                echo "\t\t\t\t\t\t\t<div class='col-md-4'>";
		                                /*if($schema[$field]['type'] == 'boolean')
		                                    echo '<label class="checkbox-inline">';*/
		                                echo '';
		                                echo "\n\t\t\t\t\t\t\t\t<p class=\"form-control-static\"><?php";
		                                if($schema[$field]['type'] == 'boolean'){
		                                   echo " echo \$boolean_values_status[\${$singularVar}['{$modelClass}']['{$field}']];?></p>\n";
		                               }else{
		                                echo " echo h(\${$singularVar}['{$modelClass}']['{$field}']);?></p>\n";
		                                }
		                                echo "\t\t\t\t\t\t\t</div>\n";
		                                echo "\t\t\t\t\t\t</div>\n";
		                                }
		                            }
		                            if (!empty($associations['hasAndBelongsToMany'])) {
		                                
		                                foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
		                                    echo "\t\techo \$this->Form->input('{$assocName}');\n";
		                                }
		                                
		                            }
		                        ?>
		                            <div class="form-actions fluid">
		                                <div class="col-md-offset-3 col-md-9">
		                                    <button type="button" class="btn blue" onclick="window.location='<?php echo "<?php echo \$this->webroot.'admin/{$pluralVar}'; ?>"; ?>'">Go Back</button>
		                                </div>
		                            </div>
		                        </div>
		                        <?php
		                        
		                            echo "<?php echo \$this->Form->end(); ?>\n";
		                        ?>
		                        <!-- END FORM-->
		                    </div>
                	</div>
            </div>
        </div>
    </div>
</div>