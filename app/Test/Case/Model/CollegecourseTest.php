<?php
App::uses('Collegecourse', 'Model');

/**
 * Collegecourse Test Case
 *
 */
class CollegecourseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.collegecourse',
		'app.college',
		'app.country',
		'app.university',
		'app.course'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Collegecourse = ClassRegistry::init('Collegecourse');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Collegecourse);

		parent::tearDown();
	}

}
