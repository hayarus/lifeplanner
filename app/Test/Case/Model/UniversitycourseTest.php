<?php
App::uses('Universitycourse', 'Model');

/**
 * Universitycourse Test Case
 *
 */
class UniversitycourseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.universitycourse',
		'app.university',
		'app.country',
		'app.college',
		'app.collegecourse',
		'app.course'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Universitycourse = ClassRegistry::init('Universitycourse');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Universitycourse);

		parent::tearDown();
	}

}
