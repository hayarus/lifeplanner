<?php
App::uses('College', 'Model');

/**
 * College Test Case
 *
 */
class CollegeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.college',
		'app.country',
		'app.university',
		'app.collegecourse'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->College = ClassRegistry::init('College');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->College);

		parent::tearDown();
	}

}
