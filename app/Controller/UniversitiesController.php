<?php
App::uses('AppController', 'Controller');
/**
 * Universities Controller
 *
 * @property University $University
 * @property PaginatorComponent $Paginator
 */
class UniversitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut','Session');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('universitydetails','contact_us','book_an_appointment','exploreresult','ajax_fetchuniversities','ajax_universitydetails','blog','blogdetails','medicallist','nursingingermany','studyinpoland','collegelist','collegedetails','videogalleries','registration','getemail','login','logout', 'blogdiscussion' ,'thankyou');
	}

	public function getemail(){
		$this->autoRender = false;
		$this->loadmodel('User');
		if ($this->request->is(['post'])) {
        $email =$this->request->data('email');                                        
        $query = $this->User->find('all',array('conditions'=>array('User.email '=>$email)));
        if(!empty($query)){
             echo 1;
        }
    	}
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['University']['limit'])){
            	$limit = $this->data['University']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'University.title LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'University.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->University->recursive = 0;
		$this->set('universities', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->University->exists($id)) {
			throw new NotFoundException(__('Invalid university'));
		}
		$options = array('conditions' => array('University.' . $this->University->primaryKey => $id));
		$this->set('university', $this->University->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
public function admin_add() {
		$universityImagePath = $this->universityImagePath;
		if ($this->request->is('post')) {//pr($this->request->data);//exit;
			$this->request->data['University']['collegecourseids']=implode(',',$this->request->data['University']['collegecourseids']);
			$this->request->data['University']['status']=1;
			$this->University->create();
			if(isset($this->request->data['University']['image']['name']) && $this->request->data['University']['image']['name'] != NULL){
				$galleryImage= $this->request->data['University']['image']['name'];
				$galleryImage = 'img-'.rand(0,9999999).'-'.$galleryImage;
				$trimImageName= str_replace(" ", "", $galleryImage);
				if(move_uploaded_file($this->request->data['University']['image']['tmp_name'], $universityImagePath.$trimImageName)){
					$this->ImageCakeCut->resize($universityImagePath.$trimImageName, $universityImagePath.$trimImageName,'width', 263);												
				}
			}else{
				$this->request->data['University']['image'] = '';
			}
			$this->request->data['University']['image']=$trimImageName;
			if ($this->University->save($this->request->data)) {
						$universityId = $this->University->id;
						if(!empty($this->request->data['University']['collegecourseids'])){
								$this->loadModel('Universitycourse');
								$courseids=explode(',',$this->request->data['University']['collegecourseids']);//pr($courseids);exit;
								for($i=0;$i < sizeof($courseids);$i++){
								$this->Universitycourse->create();//pr($universityId);exit;
								$this->Universitycourse->set('university_id',$universityId);
								$this->Universitycourse->set('course_id',$courseids[$i]);
								$this->Universitycourse->save();
								}
							}
				$this->Session->setFlash('The university has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The university could not be added. Please, try again.','flash_failure');
			}
		}
		$this->loadModel('Course');
		$countries = $this->University->Country->find('list');
		$courses = $this->Course->find('list');
		$this->set(compact('countries','courses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	    	Configure::write('debug', 0);
		$universityImagePath = $this->universityImagePath;
		if (!$this->University->exists($id)) {
			throw new NotFoundException(__('Invalid university'));
		}
		if ($this->request->is(array('post', 'put'))) {//pr($this->request->data);exit;
			$this->request->data['University']['collegecourseids']=implode(',',$this->request->data['University']['collegecourseids']);
			 
			if(isset($this->request->data['University']['image']['name']) && $this->request->data['University']['image']['name'] != NULL){
				$crntImgdetails = $this->University->find('first',array('conditions'=>array('University.id'=>$id)));
				$crntImg = $crntImgdetails['University']['image'];
				@unlink($universityImagePath.$crntImg);

				$galleryImage= $this->request->data['University']['image']['name'];
				$galleryImage = 'img-'.rand(0,9999999).'-'.$galleryImage;
			//	pr($galleryImage);exit;
				$trimImageName= str_replace(" ", "", $galleryImage);
			
				if(move_uploaded_file($this->request->data['University']['image']['tmp_name'], $universityImagePath.$trimImageName)){

					$this->ImageCakeCut->resize($universityImagePath.$trimImageName, $universityImagePath.$trimImageName,'width', 263);												
				}
				$this->request->data['University']['image']=$trimImageName;
			}else{
				$options = array('conditions' => array('University.' . $this->University->primaryKey => $id));
				$galleryimage = $this->University->find('first', $options);
				$this->request->data['University']['image'] = $galleryimage['University']['image'];
			}
			$this->request->data['University']['status']=1;
			if ($this->University->save($this->request->data)) {//pr($this->request->data);exit;
						$universityId = $id;
						if(!empty($this->request->data['University']['collegecourseids'])){//pr($this->request->data);exit;
								
								$this->loadModel('Universitycourse');
								$this->Universitycourse->deleteAll(array('Universitycourse.university_id' => $universityId), false);
								$courseids=explode(',',$this->request->data['University']['collegecourseids']);
								//pr($courseids);exit;
								for($i=0;$i<sizeof($courseids);$i++){
								$this->Universitycourse->create();
								$this->Universitycourse->set('university_id',$universityId);
								$this->Universitycourse->set('course_id',$courseids[$i]);
								$this->Universitycourse->save();
								}
							}
				$this->Session->setFlash('The university has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The university could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('University.' . $this->University->primaryKey => $id));
			$this->request->data = $this->University->find('first', $options);
		}
		$this->loadModel('Course');
		$countries = $this->University->Country->find('list');
		$courses = $this->Course->find('list');
		$this->set(compact('countries','courses'));
	}


/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->University->id = $id;
		if (!$this->University->exists()) {
			throw new NotFoundException(__('Invalid university'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->University->delete()) {
			$this->Session->setFlash('The university has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The university could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->University->find('all');
	    $this->response->download('Crowdfunding-Export-'.'universitys-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('University ID', 'Status', 'Created');
	    $_extract = array('University.id', 'University.status', 'University.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
	 
	/*public function universitydetails() {
	  	$this->layout='details_layout';
	 }*/

	  public function contact_us() {
	  	$this->layout='contact_layout';
	    if($this->request->is('post')){//pr($this->request->data);exit;
	    $name=$this->request->data['name'];
	    $email= $this->request->data['email'];
	    $message= $this->request->data['message'];
	    $admin_email='careers@hayarus.com';
	    if($name!='' && $email!=''){
	        $fromemail=$this->request->data['email'];
									                         $to = $admin_email;
						                                     $subject = "My Lifeplanner - Enquiry Details";
                                                            $txt = '<html><body>';
		$txt .="
		
		Hi Admin,
<br/><br/>

   <table> 
    <tr><td>Name: $name</td></tr>
	<tr><td>Email     : $email</td></tr>
	<tr><td>Message   : $message</td></tr>
    </table>
</br></br><br/><br style='clear:both'/>
    Best Regards,<br/>".$name . "
    
    ";
		$txt .= "</body></html>";
        $to = $admin_email;
		$subject = 'Enquiry from lifeplanner';
		$headers = "From: ".$name." <" . $fromemail. ">\r\n";
		$headers .= "Reply-To: ".$fromemail."\r\n";
		
		//$headers .='X-Mailer: PHP/' . phpversion();
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
		

		if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
		{
		    $this->Session->setFlash('Thank you for contacting us. The enquiry has been received. We will get back you soon.','success');
				return $this->redirect(array('controller'=>'universities','action' => 'thankyou'));
		}else{
		    $this->Session->setFlash('Error while sending mail.','fail_flash');
	         return $this->redirect(array('controller'=>'universities','action' => 'thankyou'));
		}
	    }else{
	        $this->Session->setFlash('Something happend. Please try again.','fail_flash');
	         return $this->redirect(array('controller'=>'universities','action' => 'thankyou'));
	    }
	         
	    }
	 }
	 
	public function universitydetails() {
	  	$this->layout='details_layout';
        $this->loadModel('Country');
        $this->loadModel('University');
		$countries = $this->Country->find('list');
		$universities = $this->University->find('list');
		$this->set(compact('countries','universities'));
	 }
	 
	 public function exploreresult() {
	  	$this->layout='details_layout';
	  	if ($this->request->is('post')) {
        	$countryid=$this->request->data['cid'];
        	$levelid=$this->request->data['pid'];
        	$areaofstudyid=$this->request->data['id'];

        	$this->loadModel('Course');
            $coursesArray=$this->Course->find('all',array('conditions'=>array('Course.areaofstudy_id'=>$areaofstudyid),'recursive'=>-1));
            $selectedids=array();//pr($coursesArray);exit;
            if(!empty($coursesArray)){
            	foreach($coursesArray as $key=>$value){
            		$selectedids[]=$value['Course']['id'];
            	}
            $this->loadModel('Universitycourse');
            $universityArray=$this->Universitycourse->find('all',array('contain'=>array('Course'),'conditions'=>array('Universitycourse.course_id'=>$selectedids)));	
            //pr($universityArray);exit;
            if(!empty($universityArray)){
            	foreach($universityArray as $key=>$value){
            		$selectedids1[]=$value['Universitycourse']['university_id'];
            	}
            }//pr($selectedids1);
            if(empty($selectedids1)){$selectedids1='';}
            $limit=5;
            $this->paginate  = array(
				'limit' => $limit, 
				'order' => 'University.id DESC',
				'conditions' => array('University.id'=>$selectedids1,'University.country_id'=>$countryid,'University.category'=>$levelid)
			);//pr($this->paginate);exit;
			$this->University->recursive = 0;
			$this->set('universities', $this->Paginator->paginate());//pr($universities);exit;
			$this->set('limit', $limit);
			$this->set('universityArray', $universityArray);

            }
        }else{
            $this->Session->setFlash('The bookappointment could not be added. Please, try again.','flash_failure');
        }
    }
    
    /*********Function used to fetch universities based on the selected country*****************/
	public function ajax_fetchuniversities($countryId=null) {
        $this->loadModel('University');
		$this->autoRender = false;
		if($countryId!='') {
			//Fetch state based on countryId 
			$Universitydetails = $this->University->find('list',array('fields' => array('University.id','University.title'),'conditions'=>array('University.country_id'=>$countryId),'recursive'=>-1));
			
			if(!empty($Universitydetails)) {
				$universityId = array_keys($Universitydetails);
					$html ='<div id="unversity"><select id="university_id" name="university_id" class="inputbox required ">';
                
					$html.= '<option value="">Select University</option>';
					foreach($Universitydetails as $key=>$value){ 
						$html.= '<option value="'.$key.'" id="UniversityId">'.$value.'</option>';
					}
					$html.= '</select></div>';               
			}else {
					$html='<div id="unversity"><select id="university_id" name="university_id" class="inputbox required "><option value="">No records available</option></select></div>';
			}echo $html;
 		}
 	}
    
    /*********Function used to fetch university details based on the selected university id*****************/
	public function ajax_universitydetails($uniqId=null) {
        $this->loadModel('University');
		$this->autoRender = false;
		if($uniqId!='') {
			//Fetch state based on id 
			$Universitydetails = $this->University->find('first',array('conditions'=>array('University.id'=>$uniqId),'recursive'=>-1));
			
			if(!empty($Universitydetails)) {
				
					$html ='<div id="search_result">';
					$html.= '<div class="dep-content">
                            <div class="prdetail">
                            <div class="prheading"><strong>'.$Universitydetails['University']['title'].'</strong></div>
                            <p><strong>1. Overview of the institution</strong></p>
                            <p>'.$Universitydetails['University']['description'].'.</p></div></div>';
					
					$html.= '</div>';               
			}else {
					$html='<div id="unversity"><select id="university_id" name="university_id" class="inputbox required "><option value="">No records available</option></select></div>';
			}echo $html;
 		}
 	}
 	
 	public function thankyou() {
	  	$this->layout='contact_layout';
        
	 }
	
	 public function blog(){
		
		$this->layout='details_layout';
	 	$this->loadModel('Blog');
	 	$this->loadModel('Blogcategory');
	 	$this->loadModel('Blogdiscussion');
	 	$this->loadModel('Subcriber');

	 	$blogs=$this->Blog->find('all',array('order' => 'Blog.id DESC', 'conditions' =>array('Blog.status'=>1)));
	 	$popularblogs=$this->Blog->find('all',array('limit'=> 10 ,'order' => 'Blog.id DESC', 'conditions' =>array('Blog.status'=>1)));
	 	$blogcategories=$this->Blogcategory->find('all',array('conditions' =>array('Blogcategory.status'=>1)));
	 	
	 	$blogcategory_count=count($blogcategories); 
	 	$n=0;
	 	foreach ($blogcategories as $key => $blogcategory) {
	 		$categories[$n] = $blogcategory['Blogcategory']['id'];
	 		$n++;
	 	}

	 	for($i=0;$i<$blogcategory_count;$i++){
	 		$activeblogs=$this->Blog->find('all',array('conditions' => array('Blog.blogcategory_id' => $categories[$i], 'Blog.status' => 1)));
	 		$totalblogcategories[$i]=count($activeblogs);
	 	}
	 	
	 	$m=0;
	 	foreach ($blogs as $key => $blog) {
	 		$blogId=$blog['Blog']['id'];
	 		$blogdiscussions=$this->Blogdiscussion->find('all',array('conditions' => array('Blogdiscussion.blog_id' => $blogId, 'Blogdiscussion.status' => 1)));
	 		$numComments[$m]=count($blogdiscussions);
	 		$m++;
	 	}
	 	// pr($totalblogcategories );exit;
	 	
	 	$this->set(compact('blogs','popularblogs','blogcategories','totalblogcategories','numComments'));
	 	if($this->request->is('post')){
	 		$name=$this->request->data['txt_name'];
	 		$fromemail=$this->request->data['txt_email'];
	 		// pr($name.$fromemail);exit;
	 		$this->request->data['Subcriber']['name']=$name;
	 		$this->request->data['Subcriber']['email']=$fromemail;

	 		$this->Subcriber->create();
	 		if ($this->Subcriber->save($this->request->data)) {
	 		$txt = '<html><body>';
		$txt .="
		Hi Admin,
		<br/><br/>
		<table> 
		<tr><td>Name    : $name</td></tr>
		<tr><td>Email             : $fromemail</td></tr>
		
		</table>
		</br></br><br/><br style='clear:both'/>
		Best Regards,<br/>".$name . "

		";
		$txt .= "</body></html>";
        $to = 'info@lifeplanneruniversal.com';
		$subject = "Subscription";
		$headers = "From: ".$name." <" . $fromemail. ">\r\n";
		$headers .= "Reply-To: ".$fromemail."\r\n";
		
		//$headers .='X-Mailer: PHP/' . phpversion();
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
		
		if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
		{
		    $this->Session->setFlash('Mail sent successfully.','success_notif');
		    return $this->redirect(array('controller' =>'universities' , 'action'=>'blog'));
		}else{
		    $this->Session->setFlash('Error while sending mail.','failure_notif');
		    return $this->redirect(array('controller' =>'universities' , 'action'=>'blog'));
		}

	 	}
	 }
	}
	public function blogdetails($id){

		$this->layout='details_layout';
	 	$this->loadModel('Blog');
	 	$this->loadModel('Blogimage');
	 	$this->loadModel('Blogvideo');
	 	$this->loadModel('Blogcategory');
	 	$this->loadModel('Blogdiscussion');
	 	$this->loadModel('Subcriber');
	 	
	 	try{
		 	$blogs=$this->Blog->find('all',array('conditions' => array('Blog.id' => $id)));
		 	$blogimages=$this->Blogimage->find('all',array('conditions' => array('Blog.id' => $id)));
		 	$blogvideos=$this->Blogvideo->find('all',array('conditions' => array('Blog.id' => $id)));
		 	$blogdiscussions = $this->Blogdiscussion->find('all',array('conditions' => array('Blogdiscussion.blog_id' => $id ,'Blogdiscussion.status' =>1 )));
		 	$popularblogs=$this->Blog->find('all',array('limit'=> 10 ,'order' => 'Blog.id DESC', 'conditions' =>array('Blog.status'=>1)));
		 	$blogcategories=$this->Blogcategory->find('all',array('conditions' =>array('Blogcategory.status'=>1)));

		 	
		 	$blogcategory_count=count($blogcategories); 
		 	$n=0;
		 	foreach ($blogcategories as $key => $blogcategory) {
		 		$categories[$n] = $blogcategory['Blogcategory']['id'];
		 		$n++;
		 	}

		 	for($i=0;$i<$blogcategory_count;$i++){
		 		$activeblogs=$this->Blog->find('all',array('conditions' => array('Blog.blogcategory_id' => $categories[$i], 'Blog.status' => 1)));
		 		$totalblogcategories[$i]=count($activeblogs);
		 	}

		 
		 	$this->set(compact('blogs','blogimages','blogvideos','popularblogs','blogcategories','totalblogcategories','blogdiscussions'));

		  }catch(Exception $e){
	        echo 'Error:'.$e->getMessage();
        	return;
	      }	

	 	if($this->request->is('post')){
	 		if(!empty($this->request->data['txt_name']) && !empty($this->request->data['txt_email'])){
	 		$name=$this->request->data['txt_name'];
	 		$fromemail=$this->request->data['txt_email'];
	 		// pr($name.$fromemail);exit;
	 		$this->request->data['Subcriber']['name']=$name;
	 		$this->request->data['Subcriber']['email']=$fromemail;

	 		$this->Subcriber->create();
	 		if ($this->Subcriber->save($this->request->data)) {
	 		$txt = '<html><body>';
			$txt .="
			Hi Admin,
			<br/><br/>
			<table> 
			<tr><td>Name    : $name</td></tr>
			<tr><td>Email             : $fromemail</td></tr>
			
			</table>
			</br></br><br/><br style='clear:both'/>
			Best Regards,<br/>".$name . "

			";
			$txt .= "</body></html>";
	        $to = 'info@lifeplanneruniversal.com';
			$subject = "Subscription";
			$headers = "From: ".$name." <" . $fromemail. ">\r\n";
			$headers .= "Reply-To: ".$fromemail."\r\n";
			
			//$headers .='X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
			
			if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
			{
			    $this->Session->setFlash('Mail sent successfully.','success_notif');
			    return $this->redirect(array('controller' =>'universities' , 'action'=>'blog'));
			}else{
			    $this->Session->setFlash('Error while sending mail.','failure_notif');
			    return $this->redirect(array('controller' =>'universities' , 'action'=>'blog'));
			}

		 	}
	 	}
	 }

	}
	public function collegelist($id=null){
	    $universityImagePath=$this->universityImagePath;
	 	$this->layout='details_layout';
	 	$this->loadModel('University');
	 	//$collegelist=$this->University->find('all');
	 	if($id=='UG'){
	 	$collegelist=$this->University->find('all',array('conditions'=>array('University.category'=>2,'University.type'=>0)));
        }else{
        $collegelist=$this->University->find('all',array('conditions'=>array('University.category'=>1,'University.type'=>0)));  
        }
	 	//pr($collegelist);exit;
	 	$this->set(compact('collegelist'));
	 	
	 }

	 public function collegedetails($id){
	 	$this->layout='details_layout';
	 	$this->loadModel('University');
	 	$this->loadModel('Universitycourse');
	 	$this->loadModel('Course');
	 	$collegedetails=$this->University->find('first', array('conditions'=> array('University.id'=>$id)));
	 	$coursearray=array();
	 	//pr($collegedetails);exit;
	 	    for($i=0;$i<sizeof($collegedetails['Universitycourse']);$i++){
	 	    $courseList=$this->Course->find('first', array('conditions'=> array('Course.id'=>$collegedetails['Universitycourse'][$i]['course_id'])));
	 	   
	 	    $coursearray[$i]['Course']['name']=$courseList['Course']['name'];
	 	} //pr($courseList);exit;
	 //	pr($coursearray);exit;
					                                
	 	$this->set(compact('collegedetails','coursearray'));
	 }

	 public function medicallist(){
	 	$this->layout='details_layout';
	 	$universityImagePath=$this->universityImagePath;
	 	// $collegelist=$this->University->find('all',array('conditions'=>array('University.type'=>1),'order'=>'University.sortorder ASC'));
	 	//pr($collegelist);exit;
	 	$polandcollegelist=$this->University->find('all',array('conditions'=>array('University.type'=>1,'University.country_id'=>175)));
	 	$philippinecollegelist=$this->University->find('all',array('conditions'=>array('University.type'=>1,'University.country_id'=>173)));
	 	$moldoacollegelist=$this->University->find('all',array('conditions'=>array('University.type'=>1,'University.country_id'=>144)));
	 	$germancollegelist=$this->University->find('all',array('conditions'=>array('University.type'=>1,'University.country_id'=>82)));
	 	$georgiacollegelist=$this->University->find('all',array('conditions'=>array('University.type'=>1,'University.country_id'=>81)));

		// pr($germancollegelist);exit;
	 	$this->set(compact('collegelist','polandcollegelist','philippinecollegelist','moldoacollegelist','germancollegelist','georgiacollegelist'));
	 	
	 }
	 public function nursingingermany(){
	 	$this->layout='details_layout';
	 }
	 public function studyinpoland(){
	 	$this->layout='details_layout';
	 }
	 
	 public function videogalleries(){
	 	$this->layout='details_layout';
	 	$this->loadModel('Videogallery');
	 	$videos=$this->Videogallery->find('all',array('order' => 'Videogallery.id DESC'));
	 	$this->set(compact('videos'));
	 }
	 public function registration(){
	 	$this->autoRender=false;
	 	$this->loadModel('User');
	 	if($this->request->is('ajax')){
			$this->request->data['User']['first_name']=$this->request->data('reg_firstname');
			$this->request->data['User']['last_name']=$this->request->data('reg_lastname');
			$this->request->data['User']['email']=$this->request->data('reg_email');
			$this->request->data['User']['password']=$this->request->data('reg_password');
			$this->request->data['User']['group_id']="2";
            // $this->request->data['User']['requestresetpaswd']="";
            $email_id=$this->request->data('reg_email');
            $this->User->create();
            $this->User->save($this->request->data);
            $registrationSucess=$this->User->find('all',array('conditions'=>array('User.email'=>$email_id)));
            if (!empty($registrationSucess)){ 
			    echo 1;
			}

		}
	 }
	 public function login(){
		$this->autoRender=false;
		if($this->request->is('ajax')){
			$this->request->data['User']['email']=$this->request->data('log_email');
			$this->request->data['User']['password']=$this->request->data('log_password');
			$this->Session->delete('Auth');
			if($this->Auth->login()){
				$this->Session->delete('sessionUserInfo');
				$userInfo = $this->Auth->user();
				if(isset($userInfo['group_id']) && $userInfo['group_id']==1){
	        		$this->Session->destroy();
	        		$this->Auth->logout();
	        	}else{
				$this->Session->write("sessionUserInfo",$userInfo);	
				$this->User->id = $this->Auth->user('id');
				echo 1;
				}
			}
		}	
		
	}
	public function logout(){
		$this->Session->delete("sessionUserInfo");
		$this->Session->delete("Auth");
		$this->Session->destroy();
		$this->redirect(array('controller' => 'pages','action' => 'displayhome'));
	}

	 public function blogdiscussion(){
	 	$this->autoRender=false;
	 	$this->loadModel('Blogdiscussion');
	 	if($this->request->is('ajax')){
			$this->request->data['Blogdiscussion']['blog_id']=$this->request->data('blog_blogid');
			$this->request->data['Blogdiscussion']['user_id']=$this->request->data('blog_userid');
			$this->request->data['Blogdiscussion']['content']=$this->request->data('blog_content');
			$this->request->data['Blogdiscussion']['status']=1;
			$id=$this->request->data('blog_blogid');
			// pr();exit;
            $this->Blogdiscussion->create();
            
            $this->Blogdiscussion->save($this->request->data);
			    $blogdiscussions=$this->Blogdiscussion->find('all',array('conditions'=>array('Blogdiscussion.blog_id'=>$id ,'Blogdiscussion.status' => 1)));
			     $this->set(compact('blogdiscussions'));
			    $this->render('blogdiscussion', 'ajax');
		}
	 }

}
