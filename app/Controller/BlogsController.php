<?php
App::uses('AppController', 'Controller');
/**
 * Blogs Controller
 *
 * @property Blog $Blog
 * @property PaginatorComponent $Paginator
 */
class BlogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('rejectstatus');
	}
/**
 * admin_index method
 *
 * @return void
 */
public function multipleimage($id = null) {	

		$this->autoRender= false;
		$blogImageLarge = $this->blogImageLarge;
		$blogImageSmall = $this->blogImageSmall;	
		$storeFolder = $blogImageLarge;

		if(isset($_FILES) && !empty($_FILES['file']['tmp_name'])){
		    $imgname= $this->imagename($_FILES['file']['name']);
		   
		    $this->loadModel('Blogimage');
		    $this->Blogimage->create();
		    $this->request->data['Blogimage']['blog_id'] = $id;
		    $this->request->data['Blogimage']['name'] = $imgname;
		    //$this->request->data['Postimage']['thumbnail']=0;
		    $this->request->data['Blogimage']['status']=1;
		    $this->Blogimage->save($this->request->data['Blogimage']);

			$this->ImageCakeCut->setImagePath($blogImageSmall);
			$this->ImageCakeCut->setwidth(425);
			$this->ImageCakeCut->setheight(240);
			$this->ImageCakeCut->make_thumb($_FILES['file']['tmp_name'],$imgname);

			$this->ImageCakeCut->setImagePath($blogImageLarge);
			// $this->ImageCakeCut->setwidth(850);
			// $this->ImageCakeCut->setheight(480);
			$this->ImageCakeCut->make_thumb($_FILES['file']['tmp_name'],$imgname);

		}else {                                                           
		    $result  = array();		 
		    $files = scandir($storeFolder);                 //1
		    if ( false!==$files ) {
		        foreach ( $files as $file ) {
		            if ( '.'!=$file && '..'!=$file) {       //2
		                $obj['name'] = $file;
		                $obj['size'] = filesize($storeFolder.$ds.$file);
		                $result[] = $obj;
		            }
		        }
		    }		     
		    header('Content-type: text/json');              //3
		    header('Content-type: application/json');
		    echo json_encode($result);
		}

		$this->Session->setFlash('images has been added successfully.','flash_success');
		//return $this->redirect(array('action' => 'edit', $id));

	}
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Blog']['limit'])){
            	$limit = $this->data['Blog']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Blog.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Blog.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Blog->recursive = 0;
		$this->set('blogs', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Blog->exists($id)) {
			throw new NotFoundException(__('Invalid blog'));
		}
		$options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
		$this->set('blog', $this->Blog->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Blog->create();
			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash('The blog has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blog->id));
			} else {
				$this->Session->setFlash('The blog could not be added. Please, try again.','flash_failure');
			}
		}
		$blogcategories = $this->Blog->Blogcategory->find('list',array('conditions' => array('Blogcategory.status' => 1 )));
		$this->set(compact('blogcategories'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->loadModel('Blogdiscussion');
		if (!$this->Blog->exists($id)) {
			throw new NotFoundException(__('Invalid blog'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blog->save($this->request->data)) {
				$this->Session->setFlash('The blog has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blog->id));
			} else {
				$this->Session->setFlash('The blog could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Blog.' . $this->Blog->primaryKey => $id));
			$this->request->data = $this->Blog->find('first', $options);
		}
		$this->set('id',$id);
		$blogcategories = $this->Blog->Blogcategory->find('list',array('conditions' => array('Blogcategory.status' => 1 )));
		$blogdiscussions = $this->Blogdiscussion->find('all',array('conditions' => array('Blogdiscussion.blog_id' => $id)));

		// pr($blogdiscussions);exit;
		$this->set(compact('blogcategories','blogdiscussions'));
		
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Blog->id = $id;
		// pr($id);exit;
		if (!$this->Blog->exists()) {
			throw new NotFoundException(__('Invalid blog'));
		}
		$blogvideos=$this->Blog->Blogvideo->find('all',array('conditions' => array('Blogvideo.blog_id'=> $id)));
		// pr($blogvideos);
		$blogimages=$this->Blog->Blogimage->find('all',array('conditions' => array('Blogimage.blog_id'=> $id)));
		// pr($blogimages);exit;
		if((!empty($blogimages)) || (!empty($blogvideos)))
		{
			
			return $this->redirect(array('action' => 'edit', $id));
			
		}
		else{
			$this->request->allowMethod('post', 'delete');
		if ($this->Blog->delete()) {
			$this->Session->setFlash('The blog has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The blog could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));

		}
		
		
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Blog->find('all');
	    $this->response->download('Crowdfunding-Export-'.'blogs-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Blog ID', 'Status', 'Created');
	    $_extract = array('Blog.id', 'Blog.status', 'Blog.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }

	 
	public function admin_blogvideodelete($id = null) {
		$this->autoRender = false;
		$this->loadModel('Blogvideo');
		$this->Blogvideo->id = $id;
		//pr($id);exit;
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogvideo->delete()) {
			echo 1;
		} else {
			echo 0;
		}
		
	}
	public function admin_blogimagedelete($id = null) {

		$blogImageLarge = $this->blogImageLarge;
		$blogImageSmall = $this->blogImageSmall;

		$this->autoRender = false;
		$this->loadModel('Blogimage');
		$this->Blogimage->id = $id;

		// pr($id);exit;

		$options= array('conditions'=>array('Blogimage.id'=>$id));
		$arrImage= $this->Blogimage->find('first',$options);
			
		$fileLarge = new File($blogImageLarge.$arrImage['Blogimage']['name']);
		$fileSmall = new File($blogImageSmall.$arrImage['Blogimage']['name']);

		$fileLarge->delete();
		$fileSmall->delete();

		$this->request->allowMethod('post', 'delete');

		if ($this->Blogimage->delete()) {
			echo 1;
		} else {
			echo 0;
		}
	}
	public function admin_rejectstatus($id = null) {
		$this->loadModel('Blogdiscussion');
		$this->autoRender = false;
		$this->Blogdiscussion->id = $id;

			if ($this->request->is(array('post', 'put'))) {
				$this->request->data['Blogdiscussion']['status']=0;
				if ($this->Blogdiscussion->save($this->request->data)) {
					echo 1;
				}else{
					echo 0;
				}

			}
	}
	public function admin_approvestatus($id = null) {
		$this->loadModel('Blogdiscussion');
		$this->autoRender = false;
		$this->Blogdiscussion->id = $id;

			if ($this->request->is(array('post', 'put'))) {
				$this->request->data['Blogdiscussion']['status']=1;
				if ($this->Blogdiscussion->save($this->request->data)) {
					echo 1;
				}else{
					echo 0;
				}

			}
	}

}
