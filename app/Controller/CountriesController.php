<?php
App::uses('AppController', 'Controller');
/**
 * Countries Controller
 *
 * @property Country $Country
 * @property PaginatorComponent $Paginator
 */
class CountriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('englishspeakingcountries','europeancountries','canada','australia');
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Country']['limit'])){
            	$limit = $this->data['Country']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Country.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Country.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Country->recursive = 0;
		$this->set('countries', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
		$this->set('country', $this->Country->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Country->create();
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash('The country has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Country->id));
			} else {
				$this->Session->setFlash('The country could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash('The country has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Country->id));
			} else {
				$this->Session->setFlash('The country could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
			$this->request->data = $this->Country->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid country'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Country->delete()) {
			$this->Session->setFlash('The country has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The country could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Country->find('all');
	    $this->response->download('Crowdfunding-Export-'.'countrys-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Country ID', 'Status', 'Created');
	    $_extract = array('Country.id', 'Country.status', 'Country.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
	 public function englishspeakingcountries(){
	 	$this->layout='details_layout';
	 	
	 	$this->set(compact('collegelist'));
	 }
	 public function europeancountries(){
	 	$this->layout='details_layout';
	 	if ($this->request->is('post')) {
			$name=$this->request->data["name"]; 
			$phone=$this->request->data["phone"]; 
			$email=$this->request->data["email"]; 
			$qualification=$this->request->data["qualification"]; 
			$percentage=$this->request->data["percent"]; 
			$yearofpassing=$this->request->data["yearofpassing"]; 

			$fromemail=$email;  
			$txt = '<html><body>';
			$txt .="
			Hi Admin,
			<br/><br/>
			<table> 

			<tr><td>Name    		  : $name</td></tr>
			<tr><td>Phone             : $phone</td></tr>
			<tr><td>Email             : $email</td></tr>
			<tr><td>Qualification     : $qualification</td></tr>
			<tr><td>Percentage     	  : $percentage</td></tr>
			<tr><td>Year of passing   : $yearofpassing</td></tr>

			</table>
			</br></br><br/><br style='clear:both'/>
			Best Regards,<br/>".$name . " 

			";
			$txt .= "</body></html>";
			$to = 'info@lifeplanneruniversal.com';
			$subject = "Application";
			$headers = "From: ".$name." <" . $fromemail. ">\r\n";
			$headers .= "Reply-To: ".$fromemail."\r\n";

			//$headers .='X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
		
		if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
		{
		    $this->Session->setFlash('Thank you.','success_notif');
		    return $this->redirect(array('controller'=>'countries','action' => 'europeancountries'));
		}else{
		    $this->Session->setFlash('Error while sending mail.','failure_notif');
	         return $this->redirect(array('controller' =>'countries' , 'action' => 'europeancountries'));
		}
	 	}

	 }
	 public function canada(){
	 	$this->layout='details_layout';	
	 	if($this->request->is('post')) {
	 		if(!empty($this->request->data["firstname"])){
				 $firstname=$this->request->data["firstname"]; 
				 $lastname=$this->request->data["lastname"]; 
				 $dob=$this->request->data["dob"]; 
				 $mobile=$this->request->data["mobile"]; 
				 $land=$this->request->data["land"]; 
				 $email=$this->request->data["email"]; 
				 
				 $qualific=$this->request->data["qualific"]; 
				 $completion=$this->request->data["completion"]; 
				 $percentage=$this->request->data["percentage"]; 
				 $residential=$this->request->data["residential"]; 
				 $city=$this->request->data["city"]; 
				 $street=$this->request->data["street"]; 
				 $pin=$this->request->data["pin"]; 
				 $country=$this->request->data["country"]; 
				 $state=$this->request->data["state"];
				
				 $employer=$this->request->data["employer"]; 
				 $designation=$this->request->data["designation"]; 
				 $work_from=$this->request->data["work_from"]; 
				 $work_to=$this->request->data["work_to"]; 
				 $test=$this->request->data["test"]; 
				 $test_taken=$this->request->data["test_taken"]; 
				 $valid_from=$this->request->data["valid_from"];
				 $valid_to=$this->request->data["valid_to"];
				 $fromemail=$email;  
			 	$txt = '<html><body>';
				$txt .="
				Hi Admin,
				<br/><br/>
				<table> 
				
				<tr><td>Name    		  : $firstname  $lastname</td></tr>
				<tr><td>Date of Birth     : $dob</td></tr>
				<tr><td>Mobile            : $mobile</td></tr>
				<tr><td>Land number       : $land</td></tr>
				<tr><td>Email             : $email</td></tr>
				<tr><td>Qualification             : $qualific</td></tr>
				<tr><td>Completion Year      : $completion</td></tr>
				<tr><td>Percentage     : $percentage</td></tr>
				<tr><td>Residential    : $residential</td></tr>
				<tr><td>city    	   : $city</td></tr>
				<tr><td>street         : $street</td></tr>
				<tr><td>Pin    : $pin</td></tr>
				<tr><td>Country    : $country</td></tr>
				<tr><td>State    : $state</td></tr>
				<tr><td>Employer    : $employer</td></tr>
				<tr><td>Designation    : $designation</td></tr>
				<tr><td>Work From    : $work_from</td></tr>
				<tr><td>Work To    : $work_to</td></tr>
				<tr><td>Test    : $test</td></tr>
				<tr><td>Test Taken    : $test_taken</td></tr>
				<tr><td>Valid From    : $valid_from</td></tr>
				<tr><td>Valid To    : $valid_to</td></tr>


				
				</table>
				</br></br><br/><br style='clear:both'/>
				Best Regards,<br/>".$firstname . "&nbsp;" . $lastname." 

				";
				$txt .= "</body></html>";
		        $to = 'info@lifeplanneruniversal.com';
				$subject = "Application";
				$headers = "From: ".$firstname." <" . $fromemail. ">\r\n";
				$headers .= "Reply-To: ".$fromemail."\r\n";
				
				//$headers .='X-Mailer: PHP/' . phpversion();
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
				
				if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
				{
				    $this->Session->setFlash('Thank you.','success_notif');
				    return $this->redirect(array('controller'=>'countries','action' => 'canada'));
				}else{
				    $this->Session->setFlash('Error while sending mail.','failure_notif');
			         return $this->redirect(array('controller'=>'countries','action' => 'canada'));
				}
		    } 
		    if(!empty($this->request->data["name"])){
		    	
			     $name=$this->request->data["name"]; 
				 $phone=$this->request->data["phone"]; 
				 $emailid=$this->request->data["emailid"]; 
				 $jobtitle=$this->request->data["jobtitle"]; 
				 $yearofexp=$this->request->data["yearofexp"]; 

				  $fromemail=$emailid;  
				 	$txt = '<html><body>';
					$txt .="
					Hi Admin,
					<br/><br/>
					<table> 
					<tr><td>Name    		  : $name  </td></tr>
					<tr><td>Phone    : $phone</td></tr>
					<tr><td>Job title            : $jobtitle</td></tr>
					<tr><td>year of experience       : $yearofexp</td></tr>
					<tr><td>Email             : $emailid</td></tr>
					</table>
					</br></br><br/><br style='clear:both'/>
					Best Regards,<br/>".$name . "&nbsp;" ." 

					";
					$txt .= "</body></html>";
			        $to = 'info@lifeplanneruniversal.com';
					$subject = "Application";
					$headers = "From: ".$name." <" . $fromemail. ">\r\n";
					$headers .= "Reply-To: ".$fromemail."\r\n";
					
					//$headers .='X-Mailer: PHP/' . phpversion();
					$headers .= "MIME-Version: 1.0\r\n";
					$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
					
					if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
					{
					    $this->Session->setFlash('Thank you.','success_notif');
					    return $this->redirect(array('controller'=>'countries','action' => 'canada'));
					}else{
					    $this->Session->setFlash('Error while sending mail.','failure_notif');
				         return $this->redirect(array('controller'=>'countries','action' => 'canada'));
					}
				 
		    }
	    }

	 }
	  public function australia(){
	 	$this->layout='details_layout';	
	 }
}
