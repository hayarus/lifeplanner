<?php
App::uses('AppController', 'Controller');
/**
 * Galleries Controller
 *
 * @property Gallery $Gallery
 * @property PaginatorComponent $Paginator
 */
class GalleriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut','Session',);

public function beforeFilter() {
// 		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('admin_add');
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Gallery']['limit'])){
            	$limit = $this->data['Gallery']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Gallery.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Gallery.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Gallery->recursive = 0;
		$this->set('galleries', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Gallery->exists($id)) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		$options = array('conditions' => array('Gallery.' . $this->Gallery->primaryKey => $id));
		$this->set('gallery', $this->Gallery->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
	
        $galleryImagePath = $this->galleryImagePath;    
		if ($this->request->is('post')) { 
			$this->autoRender = false;
			$this->Gallery->create();
			if(isset($this->request->data['Gallery']['image']['name']) && $this->request->data['Gallery']['image']['name'] != NULL){
				$galleryImage= $this->request->data['Gallery']['image']['name'];
				$galleryImage = 'img-'.rand(0,9999999).'-'.$galleryImage;
				if(move_uploaded_file($this->request->data['Gallery']['image']['tmp_name'], $galleryImagePath.$galleryImage)){
						
					$this->ImageCakeCut->resize($galleryImagePath.$galleryImage, $galleryImagePath.$galleryImage,'width', 500);												
				}
			}else{
				$this->request->data['Gallery']['image'] = '';
			}
			$this->request->data['Gallery']['image']=$galleryImage;
			if ($this->Gallery->save($this->request->data)) { 
				$this->Session->setFlash('The gallery has been added successfully.','flash_success');
				return $this->redirect(array('controller'=> 'galleries','action' => 'add'));
			} else {
				$this->Session->setFlash('The gallery could not be added. Please, try again.','flash_failure');
			}
			
		}else{
		    if(!empty($this->data)){
				if(isset($this->data['Gallery']['limit'])){
	            	$limit = $this->data['Gallery']['limit'];
					$this->Session->write('default_limit', $limit);
				}
			}else{
				if($this->Session->check('default_limit'))
					$limit = $this->Session->read('default_limit');
				else
					$limit = $this->default_limit;
			}
			$search_conditions = array();
			$conditions = array();
			$this->set("search_string", "");
			if(isset($this->params->query['search'])){
				$this->set("search_string", $this->params->query['search']);
				$conditions = array('OR' => array(
				'Gallery.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
			}
			$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Gallery.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
			$this->Gallery->recursive = 0;
			$this->set('galleries', $this->Paginator->paginate());
			
		}
		$this->set('default_limit_dropdown', $this->default_limit_dropdown);
		 $countries = $this->Gallery->Country->find('list');
		 $this->set('limit', $limit);
		 $this->set('countries', $countries);
}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	    
		$galleryImagePath = $this->galleryImagePath;   
		if (!$this->Gallery->exists($id)) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		if ($this->request->is(array('post', 'put'))) {
		    $this->Gallery->id=$id;
			if(isset($this->request->data['Gallery']['image']['name']) && $this->request->data['Gallery']['image']['name'] != NULL){
				$galleryImage= $this->request->data['Gallery']['image']['name'];
				$galleryImage = 'img-'.rand(0,9999999).'-'.$galleryImage;
				if(move_uploaded_file($this->request->data['Gallery']['image']['tmp_name'], $galleryImagePath.$galleryImage)){
						
					$this->ImageCakeCut->resize($galleryImagePath.$galleryImage, $galleryImagePath.$galleryImage,'width', 500);												
				}
				$this->request->data['Gallery']['image']=$galleryImage;
			}else{
				$options = array('conditions' => array('Gallery.' . $this->Gallery->primaryKey => $id));
				$galleryimage = $this->Gallery->find('first', $options);
				$this->request->data['Gallery']['image'] = $galleryimage['Gallery']['image'];
			}
			if ($this->Gallery->save($this->request->data)) {
				$this->Session->setFlash('The gallery has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash('The gallery could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Gallery.' . $this->Gallery->primaryKey => $id));
			$this->request->data = $this->Gallery->find('first', $options);
			$gallery=$this->request->data;
			$this->set('gallery', $gallery);
		}
		$countries = $this->Gallery->Country->find('list');
		$this->set(compact('countries','galleryImagePath'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Gallery->id = $id;
		$options = array('conditions' => array('Gallery.id' => $id));
		$this->request->data = $this->Gallery->find('first', $options);
		$galleryImagePath = $this->galleryImagePath;
		$this->request->allowMethod('post', 'delete');
		if ($this->Gallery->delete()) {
			unlink($galleryImagePath.$this->request->data['Gallery']['image']);
			$this->Session->setFlash('The gallery has been deleted successfully.','flash_success');
			return $this->redirect(array('action' => 'add'));
		} else {
			$this->Session->setFlash('The gallery could not be deleted. Please, try again.','flash_failure');
			return $this->redirect(array('action' => 'add'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Gallery->find('all');
	    $this->response->download('Crowdfunding-Export-'.'gallerys-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Gallery ID', 'Status', 'Created');
	    $_extract = array('Gallery.id', 'Gallery.status', 'Gallery.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
