<?php
App::uses('AppController', 'Controller');
/**
 * Subcribers Controller
 *
 * @property Subcriber $Subcriber
 * @property PaginatorComponent $Paginator
 */
class SubcribersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Subcriber']['limit'])){
            	$limit = $this->data['Subcriber']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Subcriber.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Subcriber.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Subcriber->recursive = 0;
		$this->set('subcribers', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Subcriber->exists($id)) {
			throw new NotFoundException(__('Invalid subcriber'));
		}
		$options = array('conditions' => array('Subcriber.' . $this->Subcriber->primaryKey => $id));
		$this->set('subcriber', $this->Subcriber->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Subcriber->create();
			if ($this->Subcriber->save($this->request->data)) {
				$this->Session->setFlash('The subcriber has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Subcriber->id));
			} else {
				$this->Session->setFlash('The subcriber could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Subcriber->exists($id)) {
			throw new NotFoundException(__('Invalid subcriber'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Subcriber->save($this->request->data)) {
				$this->Session->setFlash('The subcriber has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Subcriber->id));
			} else {
				$this->Session->setFlash('The subcriber could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Subcriber.' . $this->Subcriber->primaryKey => $id));
			$this->request->data = $this->Subcriber->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Subcriber->id = $id;
		if (!$this->Subcriber->exists()) {
			throw new NotFoundException(__('Invalid subcriber'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Subcriber->delete()) {
			$this->Session->setFlash('The subcriber has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The subcriber could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Subcriber->find('all');
	    $this->response->download('Crowdfunding-Export-'.'subcribers-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Subcriber ID', 'Status', 'Created');
	    $_extract = array('Subcriber.id', 'Subcriber.status', 'Subcriber.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
