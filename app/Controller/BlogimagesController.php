<?php
App::uses('AppController', 'Controller');
/**
 * Blogimages Controller
 *
 * @property Blogimage $Blogimage
 * @property PaginatorComponent $Paginator
 */
class BlogimagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Blogimage']['limit'])){
            	$limit = $this->data['Blogimage']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Blogimage.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Blogimage.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Blogimage->recursive = 0;
		$this->set('blogimages', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Blogimage->exists($id)) {
			throw new NotFoundException(__('Invalid blogimage'));
		}
		$options = array('conditions' => array('Blogimage.' . $this->Blogimage->primaryKey => $id));
		$this->set('blogimage', $this->Blogimage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Blogimage->create();
			if ($this->Blogimage->save($this->request->data)) {
				$this->Session->setFlash('The blogimage has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogimage->id));
			} else {
				$this->Session->setFlash('The blogimage could not be added. Please, try again.','flash_failure');
			}
		}
		$blogs = $this->Blogimage->Blog->find('list');
		$this->set(compact('blogs'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Blogimage->exists($id)) {
			throw new NotFoundException(__('Invalid blogimage'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blogimage->save($this->request->data)) {
				$this->Session->setFlash('The blogimage has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogimage->id));
			} else {
				$this->Session->setFlash('The blogimage could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Blogimage.' . $this->Blogimage->primaryKey => $id));
			$this->request->data = $this->Blogimage->find('first', $options);
		}
		$blogs = $this->Blogimage->Blog->find('list');
		$this->set(compact('blogs'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Blogimage->id = $id;
		if (!$this->Blogimage->exists()) {
			throw new NotFoundException(__('Invalid blogimage'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogimage->delete()) {
			$this->Session->setFlash('The blogimage has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The blogimage could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Blogimage->find('all');
	    $this->response->download('Crowdfunding-Export-'.'blogimages-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Blogimage ID', 'Status', 'Created');
	    $_extract = array('Blogimage.id', 'Blogimage.status', 'Blogimage.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
