<?php
App::uses('AppController', 'Controller');
/**
 * Universitycourses Controller
 *
 * @property Universitycourse $Universitycourse
 * @property PaginatorComponent $Paginator
 */
class UniversitycoursesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Universitycourse']['limit'])){
            	$limit = $this->data['Universitycourse']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Universitycourse.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Universitycourse.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Universitycourse->recursive = 0;
		$this->set('universitycourses', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Universitycourse->exists($id)) {
			throw new NotFoundException(__('Invalid universitycourse'));
		}
		$options = array('conditions' => array('Universitycourse.' . $this->Universitycourse->primaryKey => $id));
		$this->set('universitycourse', $this->Universitycourse->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Universitycourse->create();
			if ($this->Universitycourse->save($this->request->data)) {
				$this->Session->setFlash('The universitycourse has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Universitycourse->id));
			} else {
				$this->Session->setFlash('The universitycourse could not be added. Please, try again.','flash_failure');
			}
		}
		$universities = $this->Universitycourse->University->find('list');
		$courses = $this->Universitycourse->Course->find('list');
		$this->set(compact('universities', 'courses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Universitycourse->exists($id)) {
			throw new NotFoundException(__('Invalid universitycourse'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Universitycourse->save($this->request->data)) {
				$this->Session->setFlash('The universitycourse has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Universitycourse->id));
			} else {
				$this->Session->setFlash('The universitycourse could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Universitycourse.' . $this->Universitycourse->primaryKey => $id));
			$this->request->data = $this->Universitycourse->find('first', $options);
		}
		$universities = $this->Universitycourse->University->find('list');
		$courses = $this->Universitycourse->Course->find('list');
		$this->set(compact('universities', 'courses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Universitycourse->id = $id;
		if (!$this->Universitycourse->exists()) {
			throw new NotFoundException(__('Invalid universitycourse'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Universitycourse->delete()) {
			$this->Session->setFlash('The universitycourse has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The universitycourse could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Universitycourse->find('all');
	    $this->response->download('Crowdfunding-Export-'.'universitycourses-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Universitycourse ID', 'Status', 'Created');
	    $_extract = array('Universitycourse.id', 'Universitycourse.status', 'Universitycourse.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
