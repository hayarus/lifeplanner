<?php
App::uses('AppController', 'Controller');
/**
 * Photogalleries Controller
 *
 * @property PhotoPhotogallery $PhotoPhotogallery
 * @property PaginatorComponent $Paginator
 */
class PhotogalleriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut','Session');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$limit=10;
		if (!empty($this->data)){
			if(isset($this->data['Photogallery']['limit'])){
            	$limit = $this->data['Photogallery']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Photogallery.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Photogallery.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Photogallery->recursive = 0;
		$this->set('photogalleries', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PhotoPhotogallery->exists($id)) {
			throw new NotFoundException(__('Invalid photoPhotogallery'));
		}
		$options = array('conditions' => array('PhotoPhotogallery.' . $this->PhotoPhotogallery->primaryKey => $id));
		$this->set('photoPhotogallery', $this->PhotoPhotogallery->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$limit = $this->default_limit;
		$PhotogalleryImagePath = $this->PhotogalleryImagePath;    //pr($PhotogalleryImagePath);exit;
		if ($this->request->is('post')) {// pr($this->request->data);exit;
		if (empty($this->request->data['Photogallery']['limit'])) {
			$this->autoRender = false;
			$this->Photogallery->create();
			if(isset($this->request->data['Photogallery']['image']['name']) && $this->request->data['Photogallery']['image']['name'] != NULL){
				$PhotogalleryImage= $this->request->data['Photogallery']['image']['name'];
				$description=$this->request->data['Photogallery']['description'];
				$title=$this->request->data['Photogallery']['title'];
				$PhotogalleryImage = 'img-'.rand(0,9999999).'-'.$PhotogalleryImage;
				if(move_uploaded_file($this->request->data['Photogallery']['image']['tmp_name'], $PhotogalleryImagePath.$PhotogalleryImage)){
						
					$this->ImageCakeCut->resize($PhotogalleryImagePath.$PhotogalleryImage, $PhotogalleryImagePath.$PhotogalleryImage,'width', 500);												
				}
			}else{
				$this->request->data['Photogallery']['image'] = '';
			}
			$this->request->data['Photogallery']['image']=$PhotogalleryImage;
			$this->request->data['Photogallery']['description']=$description;
			$this->request->data['Photogallery']['title']=$title;
			//pr($this->request->data);
			if ($this->Photogallery->save($this->request->data)) { //pr($this->request->data);exit;
				$this->Session->setFlash('The Photogallery has been added successfully.','flash_success');
				return $this->redirect(array('controller'=> 'photogalleries','action' => 'add'));
			} else {
				$this->Session->setFlash('The Photogallery could not be added. Please, try again.','flash_failure');
			}
			
		}else{
		    if(!empty($this->data)){
				if(isset($this->data['Photogallery']['limit'])){
	            	$limit = $this->data['Photogallery']['limit'];
					$this->Session->write('default_limit', $limit);
				}
			}else{
				if($this->Session->check('default_limit'))
					$limit = $this->Session->read('default_limit');
				else
					$limit = $this->default_limit;
			}
			$search_conditions = array();
			$conditions = array();
			$this->set("search_string", "");
			if(isset($this->params->query['search'])){
				$this->set("search_string", $this->params->query['search']);
				$conditions = array('OR' => array(
				'Photogallery.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
			}
			$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Photogallery.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
			$this->Photogallery->recursive = 0;
			$this->set('photogalleries', $this->Paginator->paginate());
		    
		}}else{//echo "string";exit;
		    if(!empty($this->data)){
				if(isset($this->data['Photogallery']['limit'])){
	            	$limit = $this->data['Photogallery']['limit'];
					$this->Session->write('default_limit', $limit);
				}
			}else{
				if($this->Session->check('default_limit'))
					$limit = $this->Session->read('default_limit');
				else
					$limit = $this->default_limit;
			}
			$search_conditions = array();
			$conditions = array();
			$this->set("search_string", "");
			if(isset($this->params->query['search'])){
				$this->set("search_string", $this->params->query['search']);
				$conditions = array('OR' => array(
				'Photogallery.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
			}
			$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Photogallery.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
			$this->Photogallery->recursive = 0;
			$this->set('photogalleries', $this->Paginator->paginate());
			
		}
		 $this->set('default_limit_dropdown', $this->default_limit_dropdown);
		 $this->set('limit', $limit);
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	    $PhotogalleryImagePath = $this->PhotogalleryImagePath; 
		//$galleryImagePath = $this->galleryImagePath;   
		if (!$this->Photogallery->exists($id)) {
			throw new NotFoundException(__('Invalid gallery'));
		}
		if ($this->request->is(array('post', 'put'))) {//pr($this->request->data);exit;
		    $this->Photogallery->id=$id;
			if(isset($this->request->data['Photogallery']['image']['name']) && $this->request->data['Photogallery']['image']['name'] != NULL){
				$galleryImage= $this->request->data['Photogallery']['image']['name'];
				$galleryImage = 'img-'.rand(0,9999999).'-'.$galleryImage;
				if(move_uploaded_file($this->request->data['Photogallery']['image']['tmp_name'], $PhotogalleryImagePath.$galleryImage)){
						
					$this->ImageCakeCut->resize($PhotogalleryImagePath.$galleryImage, $PhotogalleryImagePath.$galleryImage,'width', 500);												
				}
				$this->request->data['Photogallery']['image']=$galleryImage;
			}else{
				$options = array('conditions' => array('Photogallery.' . $this->Photogallery->primaryKey => $id));
				$galleryimage = $this->Photogallery->find('first', $options);
				$this->request->data['Photogallery']['title'] = $this->request->data['Photogallery']['title'];
				$this->request->data['Photogallery']['image'] = $galleryimage['Photogallery']['image'];
				$this->request->data['Photogallery']['description'] = $this->request->data['Photogallery']['description'];
			}
			if ($this->Photogallery->save($this->request->data)) {
				$this->Session->setFlash('The gallery has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash('The gallery could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Photogallery.' . $this->Photogallery->primaryKey => $id));
			$this->request->data = $this->Photogallery->find('first', $options);
			$gallery=$this->request->data;
			$this->set('gallery', $gallery);
		}
		//$countries = $this->Gallery->Country->find('list');
		$this->set(compact('countries','galleryImagePath'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Photogallery->id = $id;
		$options = array('conditions' => array('Photogallery.id' => $id));
		$this->request->data = $this->Photogallery->find('first', $options);
		$galleryImagePath = $this->photogalleryImagePath;
		$this->request->allowMethod('post', 'delete');
		if ($this->Photogallery->delete()) {
			unlink($galleryImagePath.$this->request->data['Photogallery']['image']);
			$this->Session->setFlash('The gallery has been deleted successfully.','flash_success');
			return $this->redirect(array('action' => 'add'));
		} else {
			$this->Session->setFlash('The gallery could not be deleted. Please, try again.','flash_failure');
			return $this->redirect(array('action' => 'add'));
		}
		return $this->redirect(array('action' => 'index'));
	}



}
