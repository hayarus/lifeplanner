<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
        $this->Auth->allow('admin_forgotpassword','admin_resetforgottenpassword','admin_login','admin_add','checkEmail','admin_logout','admin_profile');
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['User']['limit'])){
            	$limit = $this->data['User']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'User.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'User.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->User->id));
			} else {
				$this->Session->setFlash('The user could not be added. Please, try again.','flash_failure');
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('The user has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->User->id));
			} else {
				$this->Session->setFlash('The user could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash('The user has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The user could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->User->find('all');
	    $this->response->download('Crowdfunding-Export-'.'users-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('User ID', 'Status', 'Created');
	    $_extract = array('User.id', 'User.status', 'User.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
    
    
    /* Function for super admin login*/
	 public function admin_login(){
		$this->layout="admin_login";
		if($this->request->is('post')){//pr($this->request->data);
			$this->request->data['User']['email']=$this->request->data['User']['email'];
			$this->request->data['User']['password']=$this->request->data['User']['password'];
			$this->Session->delete('Auth');
			if($this->Auth->login()){
				//echo "here";exit;
				$this->Session->delete('sessionUserInfo');
				$userInfo = $this->Auth->user();
				$this->Session->write("sessionUserInfo",$userInfo);	
				$this->User->id = $this->Auth->user('id');
				$this->redirect($this->Auth->redirectUrl());
			}else{
				$this->Session->setFlash('Incorrect email or password, try again.', 'flash_failure');
				return $this->redirect($this->Auth->redirect());
			}
			//pr($this->request->data);exit;
		}	
		
	}

	public function admin_logout(){
		$this->Session->delete("sessionUserInfo");
		$this->Session->delete("Auth");
		$this->Session->destroy();
		$this->Session->setFlash('You have successfully logged out.', 'flash_success');
		//pr($this->Session->read());exit;
		$this->redirect($this->Auth->logout());
		
	}

	/**
*  for admin profile
*/
	public function admin_profile() {
		$this->layout = 'admin_default';
		$userAdmin = $this->Session->read('Auth.User'); 
		$user = $this->User->find('first',array('conditions'=>array('User.group_id'=>$userAdmin['group_id'])));//pr($user);exit;
		$adminId = $user['User']['id'];
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->User->set($this->data['User']);
			if($this->User->validates()) {
				$this->User->id = $adminId;
				$this->User->save();
				$updatedInfo = $this->User->find('first',array('conditions' => array('User.id' => $adminId)));
				$this->Session->write('sessionAdminInfo',$updatedInfo['User']);				
				$this->Session->setFlash('Profile updated successfully.','flash_success');
				$this->redirect(array('controller' => 'Users', 'action' => 'profile'));
			}
		}
		$this->set(compact('user'));	
	}

	public function checkEmail($name) { //echo $name; exit;
		$this->autoRender= false;
		$arrEmail = $this->User->find('first',array('conditions' => array('User.email' => $name)));	
		//pr($arrEmail);
		if(!empty($arrEmail)){
			echo 1;
		}else{
			echo 0;
		}
	}


/**
*  for change password
*/
	public function admin_changepassword() {
	$this->layout = 'admin_default';
	/*$adminDetails = $this->Session->read('sessionAdminInfo');
	$adminId = $adminDetails['id'];*/
	$user = $this->User->find('first',array('conditions'=>array('User.group_id'=>1)));//pr($user);exit;
	$adminId = $user['User']['id'];
	if($this->request->is('post') || $this->request->is('put')) {	
			$this->User->id = $adminId;
			$this->User->set($this->data['User']);
			//Password validation		
				if($this->User->validates()) {//pr($this->request->data);exit;
				if(!empty($this->data['User']['oldpassword']) && AuthComponent::password($this->data['User']['oldpassword']) != $this->User->field('password')){
					$this->Session->setFlash('The old password you gave is incorrect. Please try again.','flash_failure');
				}
				else if($this->data['User']['newpassword'] != $this->data['User']['confirmpwd']) {
					$this->Session->setFlash('The new password and confirm new password does not match. Please try again.','flash_failure');
				}
				else {
					$this->request->data['User']['password'] = $this->data['User']['newpassword'];
					if ($this->User->save($this->data)) 
						$this->Session->setFlash('The password has been updated successfully.', 'flash_success');
					else
						$this->Session->setFlash('The password could not be updated. Please try again.', 'flash_failure'); 
				}
				$this->redirect(array('controller' => 'users', 'action' => 'profile'));
				}
				else {
				$this->render('admin_profile');
				
				}
		
		}
		
	}

	/**
*  for forgotpassword
*/	
	public function admin_forgotpassword(){
		if (!empty($this->data)) {	
			$this->User->set($this->data);
			if (filter_var($this->request->data['User']['email'], FILTER_VALIDATE_EMAIL)) {
                $email = $this->request->data['User']['email'];//pr($email);exit;
				$conditions = array("User.email" => $email,"User.group_id" => 1);
				$this->request->data = $this->User->find('first', array('conditions' => $conditions));
				if(!empty($this->request->data)){
					$id = $this->request->data['User']['id'];
					$this->User->id = $id;	
					//////////format of mail content/////////////////////
					$fromemail='shyamalakshmi.wst@gmail.com';
					$username=$this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'];
	 				$to = $email;
	 				$subject='Reset Password';
	 				$txt="<table width='100%'><tbody><tr><td align='center' style='padding-top:40px;padding-bottom:40px;font-family:Trebuchet MS;font-size:14px;background-color:rgb(255,255,255)'><p style='width:80%;text-align:left;line-height:18px;font-family:Trebuchet MS;font-size:14px;margin-right:auto;margin-left:auto'><strong>Hi ".$username."</strong>,<br><br><span style='text-align:center'>Please click on the link below to reset your password.</span></p><table width='80%' style='padding-top:4px;padding-bottom:4px;font-family:Trebuchet MS;margin-right:auto;margin-left:auto;background-color: #6ca0b1' border='0' cellspacing='0' cellpadding='0'><tbody><tr><td align='center' valign='middle' style='font-family:Trebuchet MS;font-size:14px'><a style='text-decoration:none' href='".Configure::read('App.siteurl')."admin/users/resetforgottenpassword/".urlencode(base64_encode($email))."' target='_blank'><span style='color:rgb(255,255,255);font-size:27px;font-weight:bold'>Reset your Password</span></a></td></tr></tbody></table>";
                    
					//Send mail to admin with the new password					
	                $ok = $this->emailTemplate($fromemail,$to, $subject, $txt);
	                //pr($ok);exit; 
	                if($ok){
							$this->User->saveField('requestresetpaswd', date('Y-m-d H:i:s'));
							$this->Session->setFlash('Please check your mail inbox to reset password.', 'flash_success');
							$this->redirect(array('action' => 'login'));
						}else {
							$this->Session->setFlash('Error: Mail function.<script>$(document).ready(function(){ jQuery(".login-form").hide();
		            			jQuery(".forget-form").show(); });</script>', 'flash_failure');
						     $this->redirect(array('action' => 'login'));
						}

					}else{
						$this->Session->setFlash('Email address not registered.', 'flash_failure');
						$this->redirect(array('action' => 'login'));
					}
				}else {
					$this->Session->setFlash('Please enter a valid email id.', 'flash_failure');
					$this->redirect(array('action' => 'login'));
				}
			}

	}

/**
*  for resetforgottenpassword
*/	
	public function admin_resetforgottenpassword($hashedEmail){
		$this->layout = 'admin_login';
		$email = urldecode(base64_decode($hashedEmail));
		$conditions = array("User.email" => $email,"User.group_id" => 1);
		$user = $this->User->find('first', array('conditions' => $conditions, 'recursive' => -1));
		if(!empty($user) && $user['User']['requestresetpaswd']!= "0000-00-00 00:00:00"){
			$id = $user['User']['id'];
			//Post
			if ($this->request->is('post') || $this->request->is('put')) {
				//pr($this->data);exit;
				if(($this->request->data['User']['newpassword'] == ($this->request->data['User']['confirmpassword'])) && ($this->request->data['User']['newpassword'])!=NULL){
						$this->request->data['User']['id'] = $id;
						$new_password = $this->request->data['User']['newpassword']; 
						$this->request->data['User']['password'] = $new_password;
						$this->request->data['User']['requestresetpaswd'] = "0000-00-00 00:00:00";
					if ($this->User->save($this->request->data)) {
						$this->Session->setFlash('Password updated successfully.<br /> Please login below.', 'flash_success');
						$this->redirect(array('controller' => 'users', 'action' => 'login'));
					}else{
						$this->Session->setFlash('Password could not be updated. Please, try again!', 'flash_failure');
					}
				}else if($this->request->data['User']['newpassword']==NULL){
					$this->Session->setFlash('Please enter a new password.', 'flash_failure');
				}else{
					$this->Session->setFlash('New Passwords do not match.<br />Please, try again!', 'flash_failure');
				}
			}
		}else{
			$this->Session->setFlash('Invalid request.', 'flash_failure');
			$this->redirect(array('controller' => 'users', 'action' => 'login'));
		}
	}
	

}
