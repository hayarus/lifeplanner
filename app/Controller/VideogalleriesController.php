<?php
App::uses('AppController', 'Controller');
/**
 * Videogalleries Controller
 *
 * @property Videogallery $Videogallery
 * @property PaginatorComponent $Paginator
 */
class VideogalleriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Videogallery']['limit'])){
            	$limit = $this->data['Videogallery']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Videogallery.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Videogallery.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Videogallery->recursive = 0;
		$this->set('videogalleries', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Videogallery->exists($id)) {
			throw new NotFoundException(__('Invalid videogallery'));
		}
		$options = array('conditions' => array('Videogallery.' . $this->Videogallery->primaryKey => $id));
		$this->set('videogallery', $this->Videogallery->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Videogallery->create();
			if ($this->Videogallery->save($this->request->data)) {
				$this->Session->setFlash('The videogallery has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Videogallery->id));
			} else {
				$this->Session->setFlash('The videogallery could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Videogallery->exists($id)) {
			throw new NotFoundException(__('Invalid videogallery'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Videogallery->save($this->request->data)) {
				$this->Session->setFlash('The videogallery has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Videogallery->id));
			} else {
				$this->Session->setFlash('The videogallery could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Videogallery.' . $this->Videogallery->primaryKey => $id));
			$this->request->data = $this->Videogallery->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Videogallery->id = $id;
		if (!$this->Videogallery->exists()) {
			throw new NotFoundException(__('Invalid videogallery'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Videogallery->delete()) {
			$this->Session->setFlash('The videogallery has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The videogallery could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Videogallery->find('all');
	    $this->response->download('Crowdfunding-Export-'.'videogallerys-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Videogallery ID', 'Status', 'Created');
	    $_extract = array('Videogallery.id', 'Videogallery.status', 'Videogallery.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
