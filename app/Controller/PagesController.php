<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $components = array('Paginator');

	public function beforeFilter() {
		parent::beforeFilter();
		//$this->layout='admin_default';
		$this->Auth->allow('displayhome');
	}

/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	//Home page for listing products
	
public function displayhome() {
		$this->layout='home_layout';
        $this->loadModel('Country');
        $this->loadModel('Areaofstudy');
        $this->loadModel('Event');
		$countries = $this->Country->find('list',array('conditions'=>array('status'=>1)));
		$areaofstudy = $this->Areaofstudy->find('list');
		$events = $this->Event->find('all',array('limit'=> 25 ,'order' => 'Event.eventdate DESC','conditions'=>array('status'=>1)));
		// pr($events);exit;
		$this->set(compact('countries','areaofstudy','events'));
	}
    
	
	

	 /* Ajax function to generate sefurl */

	public function generate_sefurl($replace = '_',$remove_words = true,$words_array = array()){// echo "string";exit;
		$this->autoRender = false;
		if(!empty($this->data)){//pr($this->data);exit;
			$input = $this->data['input'];
			//make it lowercase, remove punctuation, remove multiple/leading/ending spaces
	  		$return = trim(ereg_replace(' +',' ',preg_replace('/[^a-zA-Z0-9\s]/','',strtolower($input))));
			//remove words, if not helpful to seo
			if($remove_words) { $return = $this->remove_words($return,$replace,$words_array); }
			//convert the spaces to whatever the user wants
			$sefurl = str_replace(' ',$replace,$return);//
			$sef_url = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $sefurl);
			//echo $sefurl;exit;
			$dataUnique = $this->Product->findAllBySefUrl($sef_url);//pr($dataUnique);exit;
			if (empty($dataUnique)) {
				return $sefurl;
			}else{
				return $sefurl.'-'.rand(0,555555555555);
			}
		}
		return false;
	}

	function remove_words($input,$replace,$words_array = array(),$unique_words = true){
	  //separate all words based on spaces
	  $input_array = explode(' ',$input);
	  $return = array();
	  //loops through words, remove bad words, keep good ones
	  foreach($input_array as $word)
	  {//if it's a word we should add...
		if(!in_array($word,$words_array) && ($unique_words ? !in_array($word,$return) : true))
		{$return[] = $word;
		}
	  }
	  //return good words separated by dashes
	  return implode($replace,$return);
	}

	/**********************************For setting customer status***************************/
}
