<?php
App::uses('AppController', 'Controller');
/**
 * Blogdiscussions Controller
 *
 * @property Blogdiscussion $Blogdiscussion
 * @property PaginatorComponent $Paginator
 */
class BlogdiscussionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Blogdiscussion']['limit'])){
            	$limit = $this->data['Blogdiscussion']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Blogdiscussion.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Blogdiscussion.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Blogdiscussion->recursive = 0;
		$this->set('blogdiscussions', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Blogdiscussion->exists($id)) {
			throw new NotFoundException(__('Invalid blogdiscussion'));
		}
		$options = array('conditions' => array('Blogdiscussion.' . $this->Blogdiscussion->primaryKey => $id));
		$this->set('blogdiscussion', $this->Blogdiscussion->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Blogdiscussion->create();
			if ($this->Blogdiscussion->save($this->request->data)) {
				$this->Session->setFlash('The blogdiscussion has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogdiscussion->id));
			} else {
				$this->Session->setFlash('The blogdiscussion could not be added. Please, try again.','flash_failure');
			}
		}
		$blogs = $this->Blogdiscussion->Blog->find('list');
		$users = $this->Blogdiscussion->User->find('list');
		$this->set(compact('blogs', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Blogdiscussion->exists($id)) {
			throw new NotFoundException(__('Invalid blogdiscussion'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blogdiscussion->save($this->request->data)) {
				$this->Session->setFlash('The blogdiscussion has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogdiscussion->id));
			} else {
				$this->Session->setFlash('The blogdiscussion could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Blogdiscussion.' . $this->Blogdiscussion->primaryKey => $id));
			$this->request->data = $this->Blogdiscussion->find('first', $options);
		}
		$blogs = $this->Blogdiscussion->Blog->find('list');
		$users = $this->Blogdiscussion->User->find('list');
		$this->set(compact('blogs', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Blogdiscussion->id = $id;
		if (!$this->Blogdiscussion->exists()) {
			throw new NotFoundException(__('Invalid blogdiscussion'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogdiscussion->delete()) {
			$this->Session->setFlash('The blogdiscussion has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The blogdiscussion could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Blogdiscussion->find('all');
	    $this->response->download('Crowdfunding-Export-'.'blogdiscussions-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Blogdiscussion ID', 'Status', 'Created');
	    $_extract = array('Blogdiscussion.id', 'Blogdiscussion.status', 'Blogdiscussion.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
