<?php
App::uses('AppController', 'Controller');
/**
 * Bookappointments Controller
 *
 * @property Bookappointment $Bookappointment
 * @property PaginatorComponent $Paginator
 */
class BookappointmentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('bookappointment');
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Bookappointment']['limit'])){
            	$limit = $this->data['Bookappointment']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Bookappointment.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Bookappointment.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Bookappointment->recursive = 0;
		$this->set('bookappointments', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Bookappointment->exists($id)) {
			throw new NotFoundException(__('Invalid bookappointment'));
		}
		$options = array('conditions' => array('Bookappointment.' . $this->Bookappointment->primaryKey => $id));
		$this->set('bookappointment', $this->Bookappointment->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Bookappointment->create();
			if ($this->Bookappointment->save($this->request->data)) {
				$this->Session->setFlash('The bookappointment has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Bookappointment->id));
			} else {
				$this->Session->setFlash('The bookappointment could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Bookappointment->exists($id)) {
			throw new NotFoundException(__('Invalid bookappointment'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Bookappointment->save($this->request->data)) {
				$this->Session->setFlash('The bookappointment has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Bookappointment->id));
			} else {
				$this->Session->setFlash('The bookappointment could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Bookappointment.' . $this->Bookappointment->primaryKey => $id));
			$this->request->data = $this->Bookappointment->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Bookappointment->id = $id;
		if (!$this->Bookappointment->exists()) {
			throw new NotFoundException(__('Invalid bookappointment'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Bookappointment->delete()) {
			$this->Session->setFlash('The bookappointment has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The bookappointment could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Bookappointment->find('all');
	    $this->response->download('Crowdfunding-Export-'.'bookappointments-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Bookappointment ID', 'Status', 'Created');
	    $_extract = array('Bookappointment.id', 'Bookappointment.status', 'Bookappointment.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }

	 public function bookappointment() {
	   $this->layout='details_layout';
     $currentyear=date("Y");
     $prefcountries=$this->prefcountries;
        if ($this->request->is('post')) {
        	$this->request->data['Bookappointment']['firstname']=$this->request->data['txt_fname'];
        	$this->request->data['Bookappointment']['lastname']=$this->request->data['txt_lname'];
        	$this->request->data['Bookappointment']['mobile']=$this->request->data['txt_mobile'];
        	$this->request->data['Bookappointment']['email']=$this->request->data['txt_email'];
        	$this->request->data['Bookappointment']['city']=$this->request->data['txt_city'];
        	$this->request->data['Bookappointment']['nearcity']=$this->request->data['txt_nearest_branch'];
        	$this->request->data['Bookappointment']['dob']=$this->request->data['txt_dob'];
        	$this->request->data['Bookappointment']['intake']=$this->request->data['txt_intake'];
        	$this->request->data['Bookappointment']['level']=$this->request->data['txt_level'];
        	$this->request->data['Bookappointment']['areaofstudy']=$this->request->data['txt_studyarea'];
        	$this->request->data['Bookappointment']['countrypref1']=$this->request->data['txt_country_pref_1'];
        	$this->request->data['Bookappointment']['countrypref2']=$this->request->data['txt_country_pref_2'];
        	$this->request->data['Bookappointment']['helpyoumessage']=$this->request->data['txt_helpyou'];
        	$this->request->data['Bookappointment']['capcha']=$this->request->data['security_code'];
        	$this->request->data['Bookappointment']['accept']=1;
			$this->Bookappointment->create();
			if ($this->Bookappointment->save($this->request->data)) {
                $todaysdate=date("d-m-Y");

                $name=$this->request->data['Bookappointment']['firstname'].''.$this->request->data['Bookappointment']['lastname'];
                $frommail=$this->request->data['Bookappointment']['email'];
				/******************************************************************************/

            $subject     = "Appointment Request"; // pr($subject);  exit;
            $txt         = "";
            $txt="<table style='border:2px solid #8f32db;width:100%;font-family:Arial;color:black; padding:20px;' summary='' cellspacing='0' cellpadding='0' border='0'>
          <tbody>
            
            <tr style='background-color:#fff;color:#000'>
              <td style='padding:20px 10px 5px 10px'><font size='2' face='arial'>Dear <b>Admin</b>,<br>
                
                <br>
                <p style='font-weight:normal'>The details of appointment request given below:</p></font></td>
            </tr>
            <tr>
              <td style='padding:5px 10px 5px 10px'><table style='width:100%' summary='' cellspacing='0' cellpadding='2' border='0'>
                  <tbody>
                    <tr>
                      <td><table cellspacing='0' cellpadding='2' border='0'>
                          <tbody>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Candidate Name</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:&nbsp;&nbsp;</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>".$name."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Requseted Date</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>".$todaysdate."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Mobile</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>".$this->request->data['Bookappointment']['mobile']."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Email</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>".$this->request->data['Bookappointment']['email']."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Residing City</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>".$this->request->data['Bookappointment']['city']."</font></td>
                            </tr>
                            
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Nearest City</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>".$this->request->data['Bookappointment']['nearcity']."</font></td>
                            </tr>

                             <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>DOB </font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>". $this->request->data['Bookappointment']['dob']."</font></td>
                            </tr>

                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Selected Intake </font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>". $this->request->data['Bookappointment']['intake']."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Selected Level</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>". $this->request->data['Bookappointment']['level']."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Area of Study</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>". $this->request->data['Bookappointment']['areaofstudy']."</font></td>
                            </tr>
                            <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Country preference 1</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>". $this->request->data['Bookappointment']['countrypref1']."</font></td>
                            </tr>
                             <tr>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b><font size='2' face='arial'>Country preference 2</font></b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><b>:</b></td>
                              <td style='border-bottom:#eee solid 1px;padding:10px 0;'><font size='2' face='arial'>". $this->request->data['Bookappointment']['countrypref2']."</font></td>
                            </tr>
                             

                          </tbody>
                        </table></td>
                    </tr>
                 </tbody>
                </table></td>
            </tr>
             
            <tr>
              <td style='padding:5px 10px 5px 10px'><table style='width:100%' summary='' cellspacing='0' cellpadding='2' border='0'>
                  
                </table></td>
            </tr><tr>
              <td style='padding:10px 10px 5px 10px'><table style='width:100%' summary='' cellspacing='0' cellpadding='0' border='0'>
                 
                </table></td>     
            </tr>
            <tr>
              <td style='padding:5px 10px 5px 10px'>
                <p><font size='2' face='arial'>
        Sincerly,<br>
                  <b>".$name."</b><br>
                  <br>
                  </font></p></td>
            </tr>
          </tbody>
        </table>";

                        
                        $from=$frommail;
                        $url = 'https://api.elasticemail.com/v2/email/send';
                        try{
                        $post =  array('from' =>$from,
                        'to' =>'info@lifeplanneruniversal.com',
                        'fromName' => 'Lifeplanner',
                        'apikey' => '2cc7f3e2-9440-4b36-9fbc-a794b98d3b12',
                        'subject' =>  $subject,
                        'bodyHtml' => $txt,
                        'bodyText' => $txt,
                        'isTransactional' => false);
                        //'file_1' => new CurlFile($file_name_with_full_path, $filetype, $filename));
                               $ch = curl_init();
                               curl_setopt_array($ch, array(
                                   CURLOPT_URL => $url,
                                   CURLOPT_POST => true,
                                   CURLOPT_POSTFIELDS => $post,
                                   CURLOPT_RETURNTRANSFER => true,
                                   CURLOPT_HEADER => false,
                                   CURLOPT_SSL_VERIFYPEER => false
                               ));
                               
                               $result=curl_exec ($ch);
                               curl_close ($ch);
                               $ok=1;
                             
                        }
                        catch(Exception $ex){  
                        }
				/******************************************************************************/
				
				return $this->redirect(array('controller'=>'universities','action' => 'thankyou'));
			} else {
				$this->Session->setFlash('The bookappointment could not be added. Please, try again.','flash_failure');
			}
		}
		$this->loadModel('Country');
		$countries = $this->Country->find('list');
		$this->loadModel('Areaofstudy');
		$areaofstudy = $this->Areaofstudy->find('list');
		$this->set(compact('countries','areaofstudy','currentyear','prefcountries'));
	 }
}
