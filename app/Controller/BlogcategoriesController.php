<?php
App::uses('AppController', 'Controller');
/**
 * Blogcategories Controller
 *
 * @property Blogcategory $Blogcategory
 * @property PaginatorComponent $Paginator
 */
class BlogcategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Blogcategory']['limit'])){
            	$limit = $this->data['Blogcategory']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Blogcategory.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Blogcategory.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Blogcategory->recursive = 0;
		$this->set('blogcategories', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Blogcategory->exists($id)) {
			throw new NotFoundException(__('Invalid blogcategory'));
		}
		$options = array('conditions' => array('Blogcategory.' . $this->Blogcategory->primaryKey => $id));
		$this->set('blogcategory', $this->Blogcategory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Blogcategory->create();
			if ($this->Blogcategory->save($this->request->data)) {
				$this->Session->setFlash('The blog category has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogcategory->id));
			} else {
				$this->Session->setFlash('The blog category could not be added. Please, try again.','flash_failure');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Blogcategory->exists($id)) {
			throw new NotFoundException(__('Invalid blogcategory'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blogcategory->save($this->request->data)) {
				$this->Session->setFlash('The blog category has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogcategory->id));
			} else {
				$this->Session->setFlash('The blog category could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Blogcategory.' . $this->Blogcategory->primaryKey => $id));
			$this->request->data = $this->Blogcategory->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Blogcategory->id = $id;
		if (!$this->Blogcategory->exists()) {
			throw new NotFoundException(__('Invalid blogcategory'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogcategory->delete()) {
			$this->Session->setFlash('The blog category has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The blog category could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Blogcategory->find('all');
	    $this->response->download('Crowdfunding-Export-'.'blogcategorys-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Blogcategory ID', 'Status', 'Created');
	    $_extract = array('Blogcategory.id', 'Blogcategory.status', 'Blogcategory.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
