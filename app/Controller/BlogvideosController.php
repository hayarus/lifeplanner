<?php
App::uses('AppController', 'Controller');
/**
 * Blogvideos Controller
 *
 * @property Blogvideo $Blogvideo
 * @property PaginatorComponent $Paginator
 */
class BlogvideosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Blogvideo']['limit'])){
            	$limit = $this->data['Blogvideo']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Blogvideo.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Blogvideo.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Blogvideo->recursive = 0;
		$this->set('blogvideos', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Blogvideo->exists($id)) {
			throw new NotFoundException(__('Invalid blogvideo'));
		}
		$options = array('conditions' => array('Blogvideo.' . $this->Blogvideo->primaryKey => $id));
		$this->set('blogvideo', $this->Blogvideo->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($id = null) {
		$this->autoRender = false;
		if ($this->request->is('post')) { 
			// pr($id);exit;
			if(!empty($this->request->data['Blogvideo']['youtube_link'])){

				foreach ($this->request->data['Blogvideo']['youtube_link'] as $key => $videos) {
					$this->Blogvideo->create();
					$blogVideoLink['Blogvideo']['blog_id'] = $id;
					$blogVideoLink['Blogvideo']['youtube_link'] = $this->request->data['Blogvideo']['youtube_link'][$key];
					$blogVideoLink['Blogvideo']['status'] = 1;
					//pr($postvideoData);exit;
					$this->Blogvideo->save($blogVideoLink);
				}

				
				$this->Session->setFlash('videos have been added successfully.','flash_success');
				return $this->redirect(array('controller' => 'blogs','action' => 'edit', $id));
			}else {
				$this->Flash->error(__('The blogvideo could not be saved. Please, try again.'));
			}
				
		}
		$blogs = $this->Blogvideo->Blog->find('list');
		$this->set(compact('blogs'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Blogvideo->exists($id)) {
			throw new NotFoundException(__('Invalid blogvideo'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Blogvideo->save($this->request->data)) {
				$this->Session->setFlash('The blogvideo has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Blogvideo->id));
			} else {
				$this->Session->setFlash('The blogvideo could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Blogvideo.' . $this->Blogvideo->primaryKey => $id));
			$this->request->data = $this->Blogvideo->find('first', $options);
		}
		$blogs = $this->Blogvideo->Blog->find('list');
		$this->set(compact('blogs'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Blogvideo->id = $id;
		if (!$this->Blogvideo->exists()) {
			throw new NotFoundException(__('Invalid blogvideo'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Blogvideo->delete()) {
			$this->Session->setFlash('The blogvideo has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The blogvideo could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Blogvideo->find('all');
	    $this->response->download('Crowdfunding-Export-'.'blogvideos-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Blogvideo ID', 'Status', 'Created');
	    $_extract = array('Blogvideo.id', 'Blogvideo.status', 'Blogvideo.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
