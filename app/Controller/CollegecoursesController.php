<?php
App::uses('AppController', 'Controller');
/**
 * Collegecourses Controller
 *
 * @property Collegecourse $Collegecourse
 * @property PaginatorComponent $Paginator
 */
class CollegecoursesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Collegecourse']['limit'])){
            	$limit = $this->data['Collegecourse']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Collegecourse.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Collegecourse.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Collegecourse->recursive = 0;
		$this->set('collegecourses', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Collegecourse->exists($id)) {
			throw new NotFoundException(__('Invalid collegecourse'));
		}
		$options = array('conditions' => array('Collegecourse.' . $this->Collegecourse->primaryKey => $id));
		$this->set('collegecourse', $this->Collegecourse->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Collegecourse->create();
			if ($this->Collegecourse->save($this->request->data)) {
				$this->Session->setFlash('The collegecourse has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Collegecourse->id));
			} else {
				$this->Session->setFlash('The collegecourse could not be added. Please, try again.','flash_failure');
			}
		}
		$colleges = $this->Collegecourse->College->find('list');
		$courses = $this->Collegecourse->Course->find('list');
		$this->set(compact('colleges', 'courses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Collegecourse->exists($id)) {
			throw new NotFoundException(__('Invalid collegecourse'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Collegecourse->save($this->request->data)) {
				$this->Session->setFlash('The collegecourse has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Collegecourse->id));
			} else {
				$this->Session->setFlash('The collegecourse could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('Collegecourse.' . $this->Collegecourse->primaryKey => $id));
			$this->request->data = $this->Collegecourse->find('first', $options);
		}
		$colleges = $this->Collegecourse->College->find('list');
		$courses = $this->Collegecourse->Course->find('list');
		$this->set(compact('colleges', 'courses'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Collegecourse->id = $id;
		if (!$this->Collegecourse->exists()) {
			throw new NotFoundException(__('Invalid collegecourse'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Collegecourse->delete()) {
			$this->Session->setFlash('The collegecourse has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The collegecourse could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Collegecourse->find('all');
	    $this->response->download('Crowdfunding-Export-'.'collegecourses-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Collegecourse ID', 'Status', 'Created');
	    $_extract = array('Collegecourse.id', 'Collegecourse.status', 'Collegecourse.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
