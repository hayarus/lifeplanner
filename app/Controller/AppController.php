<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $helpers = array('Html', 'Form', 'Session');
	
	 public $components = array(
	    'Auth' => array(
	        'loginAction' => array(
	            'controller' => 'users',
	            'action' => 'login'
	        ),
	        'authError' => 'Did you really think you are allowed to see that?',
	        'authenticate' => array(
	            'Form' => array(
	                'fields' => array(
	                  'username' => 'email', //Default is 'username' in the userModel
	                  'password' => 'password'  //Default is 'password' in the userModel
	                )
	            )
	        )
	    ),'Session','RequestHandler',
	);
	
	
	public $categories=array('1'=>'Post Graduate','2'=>'Graduate');
	public $universitytype=array('0'=>'Other','1'=>'Medical');
	public $positions=array('Top Position'=>'Top Position','Bottom Position'=>'Bottom Position');
	public $default_limit = 5;
	public $gallery_limit = 8;

	public $default_limit_dropdown = array("5" => 5, "10" => 10, "20" => 20, "50" => 50, "1000000" => "All");
	public $toggle_status = array("0" => '<a class="btn btn-xs yellow ts-toggle sts"> <i class="fa fa-clock-o"></i> Pending</a>', "1" => '<a class="btn btn-xs green ts-toggle sts"><i class="fa fa-check-square-o"></i> Approved </a>', "2" => '<a class="btn btn-xs red ts-toggle sts"><i class="fa fa-minus-square"></i> Hold </a>');
	
	public $boolean_values_status = array(1 => "Active", "0" => "Inactive");
	public $boolean_values_event_status = array(1 => "New", "0" => "Ended");
	public $prefcountries=array(0 =>'Canada', 1 => 'Australia' , 2 => 'New-Zealand',3 => 'USA' , 4 => 'UK', 5 => 'Ireland' , 6 => 'Germany', 7 => 'Poland', 8 => 'Italy' , 9 => 'Sweden' , 10 => 'France', 11 => 'Spain', 12 => 'Moldova' );
	
	public $galleryImagePath = "uploads/images/large/";
	public $PhotogalleryImagePath="uploads/photo/";
	public $galleryThumbImagePath = "uploads/images/thumb/";
	public $universityImagePath = "uploads/universities/large/";
	public $universityThumbImagePath = "uploads/universities/thumb/";
	public $blogImageLarge= "uploads/blogs/large/";
	public $blogImageSmall= "uploads/blogs/small/";
	public $eventImagePath= "uploads/events/";
	
	public function beforeFilter(){
		$this->Auth->loginAction = array('controller' => 'dashboard', 'action' => 'index','admin'=>'true');
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login','admin'=>'true');
		$this->Auth->loginRedirect = array('controller' => 'dashboard', 'action' => 'index','admin'=>'true');	

		$this->set('categories',$this->categories);
		$this->set('universitytype',$this->universitytype);
		$this->set('positions',$this->positions);
		$this->set('galleryImagePath',$this->galleryImagePath);
		$this->set('galleryThumbImagePath',$this->galleryThumbImagePath);
		$this->set('PhotogalleryImagePath',$this->PhotogalleryImagePath);
		$this->set('universityImagePath',$this->universityImagePath);
		$this->set('universityThumbImagePath',$this->universityThumbImagePath);
		$this->set('default_limit',$this->default_limit);
		$this->set('gallery_limit',$this->gallery_limit);
		$this->set('default_limit_dropdown', $this->default_limit_dropdown);
		$this->set('toggle_status', $this->toggle_status);
		$this->set('boolean_values_status', $this->boolean_values_status);
		$this->set('boolean_values_event_status', $this->boolean_values_event_status);
		$this->set('blogImageLarge', $this->blogImageLarge);
		$this->set('blogImageSmall', $this->blogImageSmall);
		$this->set('eventImagePath', $this->eventImagePath);
		$this->set('prefcountries',$this->prefcountries);
	}

	/////Image name formatter
	public function imagename($name){
	      if($name != ''){
	          $newName=rand(0,9999999).'_'.$name;
	          $newName= preg_replace('#[ -]+#', '-', preg_replace('/[%()]/', '', preg_replace('/[^a-zA-Z0-9.]+/', ' ', $newName)));
	          return $newName;
	      }
	}

	public function dateToDb($date = null){
    if(!empty($date)){
            $date = date("Y-m-d", strtotime($date));
            return $date;
        }else{
            return null;
        }
    }

    public function dateFromDb($date = null){
        if(!empty($date)){
            $date = date("d-m-Y", strtotime($date));
            return $date;
        }else{
            return null;
        }
    }
}
