<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 * @property PaginatorComponent $Paginator
 */
class EventsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','ImageCakeCut');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['Event']['limit'])){
            	$limit = $this->data['Event']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Event.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Event.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Event->recursive = 0;
		$this->set('events', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}
		$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
		$this->set('event', $this->Event->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$eventImagePath = $this->eventImagePath;    
		if ($this->request->is('post')) {
			$this->autoRender = false;
			if(isset($this->request->data['Event']['image']['name']) && $this->request->data['Event']['image']['name'] != NULL){
				$eventImage= $this->request->data['Event']['image']['name'];
				$eventImage = 'img-'.rand(0,9999999).'-'.$eventImage;
				if(move_uploaded_file($this->request->data['Event']['image']['tmp_name'], $eventImagePath.$eventImage)){
						
					$this->ImageCakeCut->resize($eventImagePath.$eventImage, $eventImagePath.$eventImage,'width', 539);												
				}
			}else{
				$this->request->data['Event']['image'] = '';
			}
			$this->request->data['Event']['image']=$eventImage;

			if(!empty($this->request->data['Event']['eventdate']))
               $this->request->data['Event']['eventdate'] = $this->dateToDb($this->request->data['Event']['eventdate']);

			$this->Event->create();
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash('The event details has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->Event->id));
			} else {
				$this->Session->setFlash('The event details could not be added. Please, try again.','flash_failure');
			}
		}else{
			if (!empty($this->data)){
			if(isset($this->data['Event']['limit'])){
            	$limit = $this->data['Event']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'Event.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'Event.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->Event->recursive = 0;
		$this->set('events', $this->Paginator->paginate());
		$this->set('limit', $limit);

		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$eventImagePath = $this->eventImagePath;    
		if (!$this->Event->exists($id)) {
			throw new NotFoundException(__('Invalid event'));
		}

		if ($this->request->is(array('post', 'put'))) {

			if(!empty($this->request->data['Event']['eventdate']))
               $this->request->data['Event']['eventdate'] = $this->dateToDb($this->request->data['Event']['eventdate']);

			if(isset($this->request->data['Event']['image']['name']) && $this->request->data['Event']['image']['name'] != NULL){

				$crntImgdetails = $this->Event->find('first',array('conditions'=>array('Event.id'=>$id)));
				$crntImg = $crntImgdetails['Event']['image'];
				@unlink($eventImagePath.$crntImg);

				$eventImage= $this->request->data['Event']['image']['name'];
				$eventImage = 'img-'.rand(0,9999999).'-'.$eventImage;
				if(move_uploaded_file($this->request->data['Event']['image']['tmp_name'], $eventImagePath.$eventImage)){
						
					$this->ImageCakeCut->resize($eventImagePath.$eventImage, $eventImagePath.$eventImage,'width', 539);												
				}
				$this->request->data['Event']['image']=$eventImage;
				}else{
				$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
				$eventImage = $this->Event->find('first', $options);
				$this->request->data['Event']['image'] = $eventImage['Event']['image'];
			}
			
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash('The event details has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'add'));
			} else {
				$this->Session->setFlash('The event details could not be updated. Please, try again.','flash_failure');
			}
		} else {
			
			$options = array('conditions' => array('Event.' . $this->Event->primaryKey => $id));
			$this->request->data = $this->Event->find('first', $options);
			$event=$this->request->data;
			$this->set('event', $event);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Event->delete()) {
			$this->Session->setFlash('The event has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The event could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->Event->find('all');
	    $this->response->download('Crowdfunding-Export-'.'events-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('Event ID', 'Status', 'Created');
	    $_extract = array('Event.id', 'Event.status', 'Event.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
}
