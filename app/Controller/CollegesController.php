<?php
App::uses('AppController', 'Controller');
/**
 * Colleges Controller
 *
 * @property College $College
 * @property PaginatorComponent $Paginator
 */
class CollegesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function beforeFilter() {
		parent::beforeFilter();
		$this->layout='admin_default';
		$this->Auth->allow('photogalleries','bookappointment','aboutus','gallery','testimonials','languagecoaching','events','privacypolicy');
	}
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		if (!empty($this->data)){
			if(isset($this->data['College']['limit'])){
            	$limit = $this->data['College']['limit'];
				$this->Session->write('default_limit', $limit);
			}
		}else{
			if($this->Session->check('default_limit'))
				$limit = $this->Session->read('default_limit');
			else
				$limit = $this->default_limit;
		}
		$search_conditions = array();
		$conditions = array();
		$this->set("search_string", "");
		if(isset($this->params->query['search'])){
			$this->set("search_string", $this->params->query['search']);
			$conditions = array('OR' => array(
			'College.name LIKE "%'.trim(addslashes($this->params->query['search'])).'%"')); 
		}
		$this->paginate  = array(
				'limit' => $limit, 
				'order' => 'College.id DESC',
				'conditions' => array(array_merge($conditions,$search_conditions))
			);
		$this->College->recursive = 0;
		$this->set('colleges', $this->Paginator->paginate());
		$this->set('limit', $limit);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->College->exists($id)) {
			throw new NotFoundException(__('Invalid college'));
		}
		$options = array('conditions' => array('College.' . $this->College->primaryKey => $id));
		$this->set('college', $this->College->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->College->create();
			if ($this->College->save($this->request->data)) {
				$this->Session->setFlash('The college has been added successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->College->id));
			} else {
				$this->Session->setFlash('The college could not be added. Please, try again.','flash_failure');
			}
		}
		$countries = $this->College->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->College->exists($id)) {
			throw new NotFoundException(__('Invalid college'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->College->save($this->request->data)) {
				$this->Session->setFlash('The college has been updated successfully.','flash_success');
				return $this->redirect(array('action' => 'edit', $this->College->id));
			} else {
				$this->Session->setFlash('The college could not be updated. Please, try again.','flash_failure');
			}
		} else {
			$options = array('conditions' => array('College.' . $this->College->primaryKey => $id));
			$this->request->data = $this->College->find('first', $options);
		}
		$countries = $this->College->Country->find('list');
		$this->set(compact('countries'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->College->id = $id;
		if (!$this->College->exists()) {
			throw new NotFoundException(__('Invalid college'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->College->delete()) {
			$this->Session->setFlash('The college has been deleted successfully.','flash_success');
		} else {
			$this->Session->setFlash('The college could not be deleted. Please, try again.','flash_failure');
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
 * admin_export method
 *
 * @throws NotFoundException
 * @return csv
 */ 
 
	 public function admin_export() {
	    $results = $this->College->find('all');
	    $this->response->download('Crowdfunding-Export-'.'colleges-'.date('d-m-Y').'.csv');
	    $_serialize = 'results';
	    $_header = array('College ID', 'Status', 'Created');
	    $_extract = array('College.id', 'College.status', 'College.created');
		$this->viewClass = 'CsvView.Csv';
	    $this->set(compact('results', '_serialize', '_header', '_extract'));
	 }
	 public function bookappointment(){
	    
	 	$this->layout='details_layout';
	 	// $this->loadModel('Bookappointment');
	    if($this->request->is('post')){
	        // pr($this->request->data);exit;
	    $this->request->data['Enquire']['name']=$this->request->data['Name'];
	    $this->request->data['Enquire']['email']=$this->request->data['Email'];
	    $this->request->data['Enquire']['mobile']=$this->request->data['Phone'];
	    $this->request->data['Enquire']['message']=$this->request->data['Message'];
	    $name=$this->request->data['Enquire']['name'];
	    $message= $this->request->data['Enquire']['message'];
	    $admin_email='aswini.wst@gmail.com';
	    if($this->Enquire->save($this->request->data)){
	        $fromemail=$this->request->data['Enquire']['email'];
									                         $to = 'aswini.wst@gmail.com';
						                                     $subject = "My Truecraft - Enquiry Details";
                                                            $txt = '<html><body>';
		$txt .="
		
		Hi Admin,
		<br/><br/>

		<table> 
			<tr><td>Name: $name</td></tr>
			<tr><td>Email     : $fromemail</td></tr>
			<tr><td>Message   : $message</td></tr>
			</table>
		</br></br><br/><br style='clear:both'/>
			Best Regards,<br/>".$name . "
			
			";
		$txt .= "</body></html>";
        $to = $admin_email;
		$subject = 'Enquiry from truecraft';
		$headers = "From: ".$name." <" . $fromemail. ">\r\n";
		$headers .= "Reply-To: ".$fromemail."\r\n";
		
		//$headers .='X-Mailer: PHP/' . phpversion();
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
		

		if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
		{
		    $this->Session->setFlash('Mail sent successfully.','fail_flash');
		    return $this->redirect(array('action'=> 'index'));
		}else{
		    $this->Session->setFlash('Error while sending mail.','fail_flash');
	         return $this->redirect(array('action'=> 'index'));
		}
	    }else{
	        $this->Session->setFlash('Something happend. Please try again.','fail_flash');
	         return $this->redirect(array('action'=> 'index'));
	    }
	         
	    }
	 	
	 }
	 public function aboutus(){
	 	$this->layout='details_layout';
	 }
	 
	 public function gallery(){
		$this->layout='details_layout';
		$limit = $this->gallery_limit;
	 	$galleryImagePath=$this->galleryImagePath;
	 	$this->loadModel('Gallery');
	 	//$galleries=$this->Gallery->find('all',array('order' => 'Gallery.id DESC'));
	 	//$this->set(compact('galleries'));


		$this->paginate  = array(
			'limit' => $limit, 
			'order' => 'Gallery.id DESC'
		);

		$this->Gallery->recursive = 0;
		$this->set('galleries', $this->Paginator->paginate('Gallery'));
		$this->set('limit', $limit);

	 	$countries = $this->Gallery->Country->find('list');
	 	$this->set('countries', $countries);
	 }
	 
	 public function testimonials(){
	 	$this->layout='details_layout';
	 	$this->loadModel('Testimonial');
	 	$testimonials=$this->Testimonial->find('all',array('order' => 'Testimonial.id DESC'));
	 	$this->set(compact('testimonials'));
	 }
	 
	 public function lifeatgurukul(){
	 	$this->layout='details_layout';
	 }
	  public function languagecoaching(){
	 	$this->layout='details_layout';
	 }
	 public function photogalleries(){
	 	$this->layout='details_layout';
	 	$this->loadModel('Photogallery');
	 	$PhotogalleryImagePath=$this->PhotogalleryImagePath;
	 	$photos=$this->Photogallery->find('all',array('order' => 'Photogallery.id DESC'));
	 	$this->set(compact('photos'));
	 }
	 public function registration(){
	$this->layout='details_layout';
		if($this->request->is('post')){
			$cadidateid = 'cn-'.rand(0,9999999);
			// pr($cadidateid);
			$name=$this->request->data['txt_fname'];
			$class=$this->request->data['txt_class'];
			$stream=$this->request->data['txt_stream'];
			$dob=$this->request->data['txt_dob'];
			$guardian=$this->request->data['txt_parent'];
			$mob=$this->request->data['txt_mobile'];
			$fromemail=$this->request->data['txt_email'];
			$address=$this->request->data['txt_address'];
			$school=$this->request->data['txt_school'];
			
			$txt = '<html><body>';
		$txt .="
		Hi Admin,
		<br/><br/>
		<table> 
		<tr><td>Candidate Id      : $cadidateid</td></tr>
		<tr><td>Candidate Name    : $name</td></tr>
		<tr><td>Class             : $class</td></tr>
		<tr><td>Stream            : $stream</td></tr>
		<tr><td>Date of Birth     : $dob</td></tr>
		<tr><td>Guardian's Name   : $guardian</td></tr>
		<tr><td>Mobile  No        : $mob</td></tr>
		<tr><td>Email             : $fromemail</td></tr>
		<tr><td>Address           : $address</td></tr>
		<tr><td>Name Of School    : $school</td></tr>
		
		</table>
		</br></br><br/><br style='clear:both'/>
		Best Regards,<br/>".$name . "

		";
		$txt .= "</body></html>";
        $to = 'info@lifeplanneruniversal.com';
		$subject = "Application";
		$headers = "From: ".$name." <" . $fromemail. ">\r\n";
		$headers .= "Reply-To: ".$fromemail."\r\n";
		
		//$headers .='X-Mailer: PHP/' . phpversion();
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1\r\n"; 			
		
		if ((mail($to, $subject, $txt, $headers,'-f'.$fromemail))) 
		{
		    
		    return $this->redirect(array('controller'=>'universities','action' => 'thankyou'));
		}else{
		    $this->Session->setFlash('Error while sending mail.','fail_flash');
	         return $this->redirect(array('controller' =>'pages'));
		}
	    }

	}
	public function events(){
		$this->layout='events_layout';
		$this->loadModel('Event');
		$events = $this->Event->find('all',array('order' => 'Event.id DESC','conditions'=>array('status'=>1)));
		$this->set(compact('events'));
	}

	public function privacypolicy(){

		$this->layout='details_layout';
	}
}
