<?php $this->Html->addCrumb('Course', '/admin/courses'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#CourseAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Course][areaofstudy_id]" : {required : true},
"data[Course][department]" : {required : true},
"data[Course][duration]" : {required : true},
"data[Course][name]" : {required : true},
		},
		messages:{
		"data[Course][areaofstudy_id]" : {required :"Please enter areaofstudy_id."},
"data[Course][department]" : {required :"Please enter department."},
"data[Course][duration]" : {required :"Please enter duration."},
"data[Course][name]" : {required :"Please enter name."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Course'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form courses">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Course', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('area of study<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('areaofstudy_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('department<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('department', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('duration<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('duration', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/courses'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>