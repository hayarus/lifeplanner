<!--<script src="https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js"></script>-->
<script src="<?php echo $this->webroot; ?>client/js/jquery-1.12.4.min.js"></script>
<div class="bannercontainer"> <img src="<?php echo $this->webroot; ?>client/images/banner.jpg" class="img-responsive" alt=""> </div>
<div class="contentcontainer"> 
 
    <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h2>Our Products</h2>
      </div>
    </div>
    <div class="row" id="listingContainer">
    	<?php
				if(isset($productDetails) && !empty($productDetails)){ //pr($productDetails);//exit;
				foreach($productDetails as $key => $product){ $i=0;?>			
				      <div class="col-sm-4 listingList">
				        <div class="productbox">
				          <div class="imgarea"> <img src="<?php echo  $this->webroot.$ImageMain.$product['Product']['image_name'];?>" class="img-responsive" alt=""> </div>
				          <div class="description"><?php echo $product['Product']['product_name'];?><br/>
				            <span><?php echo '$'.$product['Product']['price'];?></span> </div>
				          <div class="viewdetails"><img src="<?php echo $this->webroot; ?>client/images/but_viewdetails.png"  alt=""></div>
				        </div>
				      </div>
      			<?php $i++; echo $this->Paginator->next(''); } } else { ?>
				<div class="welllisttop">
					<div class="row">
						<div class="col-md-12 text-center">
							<span class="span">
								Sorry! No more products to load.
							</span>
						</div>
					</div>
				</div>
				<?php
			} ?>
    </div>
    <!-- <div class="clearfix"></div> -->
  </div>
</div>
<!---infinate Page Scroll Start  -->
<script type="text/javascript" src="<?php echo $this->webroot; ?>client/js/jquery.infinitescroll.min.js"></script>
  <script type="text/javascript"> 
   $container = $("#listingContainer");// alert('hi');
   $container.infinitescroll({
      navSelector  : '.next',    // selector for the paged navigation 
      nextSelector : '.next a',  // selector for the NEXT link (to page 2)
      itemSelector : '.listingList',     // selector for all items you'll retrieve
      debug         : false,
      dataType      : 'html',

      loading: {
                finishedMsg: ' <div class="clearfix" style="margin:20px auto; width:300px; height:100px; text-align:center;"><span class="no-found">No more products to load.<span></div>',
                  img: '<?php echo $this->webroot; ?>img/preloader.gif',
              }
    });
</script>
<!---infinate Page Scroll End  -->