<style type="text/css">
	.content_block .cols .col:first-child {
    margin-left: 0;
}
.content_block .cols .col {
    width: 31.3%;
    margin-left: 3%;
    float: left;
    position: relative;
}
.search_box {
    display: block;
    background: #9400D3;
    padding: 23px 6%;
    color: #fff;
    min-height: 392px;
}
.cb:after {
    content: "";
    width: 100%;
    height: 0;
    clear: both;
    display: block;
}
#main h4 {
    font-weight: 600;
    margin: 0 0 5px;
}

.search_box h4 {
    font-size: 24px;
    line-height: 26px;
    margin-bottom: 10px;
    font-weight: 400;
}
.wrapper p {
    margin: 0 0 15px;
    text-align: justify;
}

.search_box p {
    font-size: 16px;
    margin-bottom: 15px;
    font-weight: 300;
}
select {
    padding: 5px;
    border: 1px solid #c2c2c2;
    max-width: 100%;
    width: 100%;
    color: #666;
}
option {
    font-weight: normal;
    display: block;
    white-space: pre;
    min-height: 1.2em;
    padding: 0px 2px 1px;
}
.search_box input[type="submit"] {
    cursor: pointer;
    display: block;
    height: 51px;
    background: #fe670e url(../lifeplanner/images/parallax/search_icon.png) no-repeat 98% center;
    color: #fff;
    padding-left: 3%;
    font-size: 18px;
    text-align: left;
    width: 100%;
    border: none;
    text-indent: 0;
    transition: all .5s ease-in 0;
    -webkit-transition: all .5s ease-in 0;
}
.sbHolder {
    background-color: #fff;
    font-size: 15px;
    font-weight: normal;
    height: 56px;
    border-radius: 3px;
    position: relative;
    width: 100%;
    display: block;
    color: #3b4a5e;
    margin-bottom: 10px;
}
.sbToggle {
    background: url(../lifeplanner/images/parallax/down_arrrow.png) no-repeat center center;
    display: block;
    height: 56px;
    outline: none;
    position: absolute;
    right: 0;
    top: 0;
    width: 50px;
}
.sbSelector:link, .sbSelector:visited, .sbSelector:hover {
    color: #3b4a5e;
    outline: none;
    text-decoration: none;
}

.sbSelector {
    display: block;
    height: 56px;
    left: 0;
    line-height: 56px;
    outline: none;
    overflow: hidden;
    position: absolute;
    text-indent: 10px;
    top: 0;
    width: 100%;
}
.home_top.content_block .cols .col ul {
    margin: 0;
}
.sbOptions {
    background-color: #fff;
    list-style: none;
    left: 0px;
    margin: 0;
    padding: 0;
    position: absolute;
    top: 100%;
    width: 100%;
    z-index: 1;
    overflow-y: auto;
    margin: -2px 0 0!important;
    border-radius: 0px 0px 3px 3px;
}
.home_top.content_block .cols .col ul li {
    margin-bottom: 0;
    min-height: 147px;
    text-align: left;
}

.home_top.content_block .cols .sbOptions li {
    min-height: 1px!important;
}
.sbOptions li {
    padding: 0 7px;
}

.sbOptions li {
    margin: 0;
}
.sbOptions li:last-child>a {
    border-bottom: none;
    margin-bottom: 5px;
}

.sbOptions li.last a {
    border-bottom: none;
}
.sbOptions a:hover, .sbOptions a:focus, .sbOptions a.sbFocus {
    color: #fe670e;
}
.sbOptions a:link, .sbOptions a:visited {
    color: #3b4a5e;
    text-decoration: none;
}
.sbOptions a {
    border-bottom: dotted 1px #0c4da2;
    display: block;
    outline: none;
    padding: 7px 0 7px 3px;
    color: #3b4a5e;
}
</style>

<section class="flat-row padding-small">
            <div class="container">
                <div class="flat-choose-us">
                    <div class="row">
                <div class="col-md-4 col-sm-4">
                	<div class="name-toggle">
                        <h2 class="title">Global Education Services</h2>
                    </div>
                	<div class="search_box cb"><h4>Explore your Study Options</h4>
                		<p>Select your preferred destination and your fields of study</p>
                		<form id="couseform" name="couseform" action="/study-abroad.html" method="POST" class="has-validation-callback"><div> 
                		<select name="cid" class="sbHolder" id="cid" data-validation="required" data-validation-error-msg="Please select your preferred destinations" sb="" >
                		    <option value="">Your Preferred destinations</option>
                		    <option value="1">Australia</option>
                		    <option value="26">Bulgaria</option>
                		    <option value="3">Canada</option>
                		    <option value="4">China</option>
                		    <option value="23">Dubai</option>
                		    <option value="21">Estonia</option>
                		    <option value="15">France</option>
                		    <option value="16">Germany</option>
                		    <option value="24">Grenada</option>
                		    <option value="17">Hong Kong</option>
                		    <option value="6">India</option>
                		    <option value="7">Ireland</option>
                		    <option value="2">Italy</option>
                		    <option value="8">Malaysia</option>
                		    <option value="22">Mauritius</option>
                		    <option value="25">Netherlands</option>
                		    <option value="9">New Zealand</option>
                		    <option value="10">Singapore</option>
                		    <option value="11">Spain</option>
                		    <option value="20">Sweden</option>
                		    <option value="12">Switzerland</option>
                		    <option value="13">United Arab Emirates</option>
                		    <option value="5">United Kingdom</option>
                		    <option value="14">USA</option> 
                		    </select>
                		    </div><div> <select name="pid" class="sbHolder" id="pid" data-validation="required" data-validation-error-msg="Please select level of course looking for"  sb="" style="">
                		        <option value="">Level of course looking for</option>
                		        <option value="Postgraduate">Postgraduate</option>
                		        <option value="Undergraduate">Undergraduate</option> 
                		        </select>
                		        </div>
                		        <div id=""> 
                		        <select name="id" class="sbHolder" id="id" data-validation="required" data-validation-error-msg="Please select your area of interest" sb="90192809" >
                		            <option value="">Your area of interest</option> 
                		            </select>
                		           </div> 
                		           <input type="submit" value="Search">
                		           </form>
                	</div>
                </div>

                        <div class="col-md-8 col-sm-8">
                            <div class="flat-blog">
                                <div class="section-header">
                                    <div class="name-blog">
                                        <h2 class="title">Details</h2>
                                        <a class="" href="#"> <i class=""></i></a>
                                    </div>
                                </div>    
                                
                                <div class="section-body">
                                    <div class="row">
                                        <div class="col-md-12 shortcode-blog-item">
                                        	 <div class="col-md-6">
                                        	 	     <div class="content-pad">
                                                <div class="post-item ">
                                                    <div class="col-md-12">
                                                    	<div class="row">
                                                    		<div class="content-pad">
                                                            <div class="item-thumbnail">
                                                                <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                    <img src="<?php echo $this->webroot; ?>images/about/index1/blog/1.jpg" alt="image" style="height: 222px;">
                                                                    <span class="thumbnail-overlay">June 3, 2014</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    	</div>
                                                    	<div class="row">
                                                    		   <div class="content-pad">
                                                            <div class="item-content">
                                                                <h3 class="item-title">
                                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus" title="Your Career Starts Here" class="main-color-1-hover">Your Career Starts Here</a>
                                                                </h3>
                                                                <div class="shortcode-blog-excerpt">On the other hand we denounce with righteous indignation and dislike men who are so beguiled and demoralized.</div>
                                                                <div class="item-meta">
                                                                <a class="flat-button" href="http://techynab.com/lifeplanner/colleges/aboutus" title="Your Career Starts Here">DETAILS <i class="fa fa-angle-right"></i></a>
                                                                <a href="http://techynab.com/lifeplanner/colleges/aboutus" class="main-color-1-hover" title="View comments">0 COMMENTS</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    	</div>

                                                        
                                                    </div>
                                                    
                                                </div><!--/post-item-->
                                            </div>
                                        	 </div>
                                        <div class="col-md-6">
                                              <div class="content-pad">
                                                <div class="post-item ">
                                                    <div class="col-md-12">
                                                    	<div class="row">
                                                    		 <div class="content-pad">
                                                            <div class="item-thumbnail">
                                                                <a href="#">
                                                                    <img src="<?php echo $this->webroot; ?>images/about/index1/blog/2.jpg" alt="image" style="height: 222px;">
                                                                    <span class="thumbnail-overlay">June 3, 2014</span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    	</div>
                                                    		<div class="row">
                                                    		<div class="content-pad">
                                                            <div class="item-content">
                                                                <h3 class="item-title">
                                                                    <a href="#" title="Spark Of Genius" class="main-color-1-hover">Spark Of Genius</a>
                                                                </h3>
                                                                <div class="shortcode-blog-excerpt">To take a trivial example which of us ever undertakes laborious physical exercise except.</div>
                                                                <div class="item-meta">
                                                                    <a class="flat-button" href="#" title="Spark Of Genius">DETAILS <i class="fa fa-angle-right"></i></a>
                                                                    <a href="#" class="main-color-1-hover" title="View comments">0 COMMENTS</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    	</div>
                                                       
                                                    </div>
                                                </div><!--/post-item-->
                                            </div>
                                        </div>

                                        </div><!--/shortcode-blog-item-->

                                    </div><!--/row-->
                                </div>
                            </div>
                        </div><!--/col-md-6 col-sm-6 -->
                    </div>
                </div>    
            </div>
        </section>

  <div class="page-title parallax parallax4">
            <div class="container">
                <div class="row">
                    <div class="post-wrap">
                        <div class="top-title">
                            <h2 class="title">Services</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                        
                        <div class="posts-carousel">
                            <div class="flat-event">
                                <div class="flat-blog-carousel" data-item="3" data-nav="true" data-dots="false" data-auto="true">
                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus"><img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/1.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->

                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">22</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>Chicago Architecture Foundation River Cruise</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 8:00 am</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/2.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->
                                                
                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">30</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>Good Morning America</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 7:00 am</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/3.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->
                                                
                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">20</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>Student Exchange Program Information Sessions</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 10:00 am</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/4.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->
                                                
                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">22</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>Vegie Garden Wednesday Workshops</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 1:00 pm</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/5.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->
                                                
                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">21</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>The Ecosystem Within Us</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 7:00 am</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/6.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->
                                                
                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">20</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>Science In The New Era</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 4:00 pm</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> 
                                                </div><!-- /event-overlay -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="grid-item">
                                        <div class="grid-item-inner">
                                            <div class="event-item">
                                                <div class="event-thumbnail">
                                                    <a href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/7.jpg" alt="image">
                                                    </a>
                                                </div><!-- /event-thumbnail -->
                                                
                                                <div class="date-block">
                                                    <div class="month">Jun</div>
                                                    <div class="day">19</div>
                                                </div><!-- /date-block -->

                                                <div class="event-overlay">
                                                    <div class="cs-post-header">
                                                        <div class="cs-category-links">
                                                            <a class="overlay-top" href="http://techynab.com/lifeplanner/colleges/aboutus">
                                                                <h4>How To Sell Anything</h4>
                                                            </a>

                                                            <div class="overlay-footer">
                                                                <div>At 9:30 am</div>
                                                                <div>Bentley Campus, Perth, WA</div>
                                                            </div>
                                                        </div>                                
                                                    </div> <!-- /cs-post-header -->
                                                </div><!-- /event-overlay -->
                                            </div><!-- /event-item -->
                                        </div><!-- /grid-item-inner -->
                                    </div><!-- /grid-item -->
                                </div><!-- /flat-blog-carousel -->
                            </div><!-- /flat-event -->
                        </div><!-- /posts-carousel -->
                    </div><!-- /post-wrap -->
                </div><!-- /row -->
            </div><!-- /container -->
        </div><!-- /page-title -->

         <section class="flat-row padding-small-v1">
            <div class="container">
                <div class="row">
                    <div class="flat-blog">
                        <div class="section-header">
                            <div class="name-blog">
                                <h2 class="title">Our Mission</h2>
                                <a class="" href="#"> <i class=""></i></a>
                            </div>
                        </div>    
                        
                        <div class="section-body">
                            <div class="row">
                                <div class="col-md-4 shortcode-blog-item">
                                    <div class="content-pad">
                                        <div class="post-item ">
                                            <div class="col-md-12 ">
                                            	<div class="row">
                                            		<div class="content-pad">
                                                    <div class="item-thumbnail">
                                                        <a href="#">
                                                            <img src="<?php echo $this->webroot; ?>images/about/index1/blog/1.jpg" alt="image">
                                                            <span class="thumbnail-overlay">June 3, 2014</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            	</div>
                                            	<div class="row">
                                            		<div class="content-pad">
                                                    <div class="item-content">
                                                        <h3 class="item-title">
                                                            <a href="#" title="Your Career Starts Here" class="main-color-1-hover">Credibility</a>
                                                        </h3>
                                                        <div class="shortcode-blog-excerpt">Life planner is one of the few educational facilitation organization in India, certified by one ofthe most trustworthy certifications ISO 9001-2015.</div>
                                                        <div class="item-meta">
                                                        <a class="flat-button" href="#" title="Your Career Starts Here">DETAILS <i class="fa fa-angle-right"></i></a>
                                                        <a href="#" class="main-color-1-hover" title="View comments">0 COMMENTS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            	</div>
                                                
                                            </div>

                                        </div><!--/post-item-->
                                    </div>
                                </div><!--/shortcode-blog-item-->

                                <div class="col-md-4 shortcode-blog-item">
                                    <div class="content-pad">
                                        <div class="post-item ">
                                            <div class="col-md-12">
                                            	<div class="row">
                                            		 <div class="content-pad">
                                                    <div class="item-thumbnail">
                                                        <a href="#">
                                                            <img src="<?php echo $this->webroot; ?>images/about/index1/blog/2.jpg" alt="image">
                                                            <span class="thumbnail-overlay">June 3, 2014</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            	</div>
                                            	<div class="row">
                                            		 <div class="content-pad">
                                                    <div class="item-content">
                                                        <h3 class="item-title">
                                                            <a href="#" title="Spark Of Genius" class="main-color-1-hover">Vision</a>
                                                        </h3>
                                                        <div class="shortcode-blog-excerpt">Endeavour to enrich the life of our community through quality education</div>
                                                        <div class="item-meta">
                                                            <a class="flat-button" href="#" title="Spark Of Genius">DETAILS <i class="fa fa-angle-right"></i></a>
                                                            <a href="#" class="main-color-1-hover" title="View comments">0 COMMENTS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            	</div>
                                               
                                            </div>
                                        </div><!--/post-item-->
                                    </div>
                                </div><!--/shortcode-blog-item-->
                                      <div class="col-md-4 shortcode-blog-item">
                                    <div class="content-pad">
                                        <div class="post-item ">
                                            <div class="col-md-12">
                                            	<div class="row">
                                            		 <div class="content-pad">
                                                    <div class="item-thumbnail">
                                                        <a href="#">
                                                            <img src="<?php echo $this->webroot; ?>images/about/index1/blog/3.jpg" alt="image">
                                                            <span class="thumbnail-overlay">June 3, 2014</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            	</div>
                                            	<div class="row">
                                            		 <div class="content-pad">
                                                    <div class="item-content">
                                                        <h3 class="item-title">
                                                            <a href="#" title="Spark Of Genius" class="main-color-1-hover">Mission</a>
                                                        </h3>
                                                        <div class="shortcode-blog-excerpt">Pave the way for our aspiring candidates to soar high in life by providing expert career guidance and mentoring</div>
                                                        <div class="item-meta">
                                                            <a class="flat-button" href="#" title="Spark Of Genius">DETAILS <i class="fa fa-angle-right"></i></a>
                                                            <a href="#" class="main-color-1-hover" title="View comments">0 COMMENTS</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            	</div>
                                               
                                            </div>
                                        </div><!--/post-item-->
                                    </div>
                                </div><!--/shortcode-blog-item-->
                            </div><!--/row-->
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="flat-row color-full">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="flat-business">
                            <h3 class="title">ABOUT LIFE PLANNER</h3>
                            <p>Life Planner from its modest start up in 2012 has become a reputed and well respected
educational facilitator, accredited by an ISO certification within this short span of trustworthy
service. Life Planner had been guiding, supporting and providing professional &amp; educational facilitation for students to choose curriculum, courses, and institutions in academic and professional streams abroad.<a href="<?php echo $this->webroot; ?>colleges/aboutus">Read More..</a></p>

                            <div class="box-icon">
                                <div class="icon-business">
                                    <img src="<?php echo $this->webroot; ?>images/icon/5.png" alt="image">
                                </div>

                                <div class="icon-business">
                                    <img src="<?php echo $this->webroot; ?>images/icon/6.png" alt="image">
                                </div>

                                <div class="icon-business">
                                    <img src="<?php echo $this->webroot; ?>images/icon/7.png" alt="image">
                                </div>
                            </div><!-- /box-icon -->
                        </div><!-- /flat-business -->
                    </div><!-- /col-md-6 -->

                    <div class="col-md-6">
                        <div class="thumb-business">
                            <img src="<?php echo $this->webroot; ?>images/member/3.png" alt="image">
                        </div>
                    </div>
                </div><!-- /row -->
            </div><!-- /container -->
        </section><!-- /flat-row -->

        <section class="flat-row ">

            <div class="container">
                <div class="row">
                	<div class="col-md-12">
                    <div class="col-md-6">
                        <div class="flat-events">
                            <div class="grid-item color-full">
                                <div class="event-item">
                                	 <div style="    width: 100%; height: auto; vertical-align: top;">
                                        <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/main.jpg" width="600" height="300" alt="image">
                                    </div>
                                    <div class="grid-item-content">
                                        <h3 class="title">Upcoming Events</h3>
                                       
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus bibendum sapien lectus, eu sollicitudin nisl vehicula et. Morbi eros diam, sagittis non mollis ac, ornare eget nulla. Ut eu pellentesque nunc. Nullam ac rutrum quam. Pellentesque quis fringilla ipsum, viverra tristique justo. Nam suscipit molestie lacus eget lobortis. Pellentesque faucibus, felis sit amet elementum condimentum, erat leo pretium eros, eu lacinia turpis ipsum non ipsum. Nullam ut nulla est. </p>
                                        
                                    </div>
                                </div>
                            </div>

                        </div>
                        </div>
                        <div class="col-md-6">  
                        	<div class="flat-eventss">
                        <div class="row">  
                        	 
                            <div class="grid-item">
                                <div class="grid-item-inner">
                                    <div class="event-item">
                                        <div class="event-thumbnail">
                                            <a href="#">
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/6.jpg" alt="image">
                                            </a>
                                        </div><!-- /event-thumbnail -->
                                        
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">20</div>
                                        </div><!-- /date-block -->

                                        <div class="event-overlay">
                                            <div class="cs-post-header">
                                                <div class="cs-category-links">
                                                    <a class="overlay-top" href="#">
                                                        <h4>Science In The New Era</h4>
                                                        <span class="price yellow">From £49</span>
                                                    </a>

                                                    <div class="overlay-footer">
                                                        <div>At 4:00 pm</div>
                                                        <div>Bentley Campus, Perth, WA</div>
                                                    </div>
                                                </div>                                
                                            </div> 
                                        </div><!-- /event-overlay -->
                                    </div>
                                </div>
                            </div>

                             <div class="grid-item">
                                <div class="grid-item-inner">
                                    <div class="event-item">
                                        <div class="event-thumbnail">
                                            <a href="#">
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/4.jpg" alt="image">
                                            </a>
                                        </div><!-- /event-thumbnail -->
                                        
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">20</div>
                                        </div><!-- /date-block -->

                                        <div class="event-overlay">
                                            <div class="cs-post-header">
                                                <div class="cs-category-links">
                                                    <a class="overlay-top" href="#">
                                                        <h4>Science In The New Era</h4>
                                                        <span class="price yellow">From £49</span>
                                                    </a>

                                                    <div class="overlay-footer">
                                                        <div>At 4:00 pm</div>
                                                        <div>Bentley Campus, Perth, WA</div>
                                                    </div>
                                                </div>                                
                                            </div> 
                                        </div><!-- /event-overlay -->
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="row">  
                             <div class="grid-item">
                                <div class="grid-item-inner">
                                    <div class="event-item">
                                        <div class="event-thumbnail">
                                            <a href="#">
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/3.jpg" alt="image">
                                            </a>
                                        </div><!-- /event-thumbnail -->
                                        
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">20</div>
                                        </div><!-- /date-block -->

                                        <div class="event-overlay">
                                            <div class="cs-post-header">
                                                <div class="cs-category-links">
                                                    <a class="overlay-top" href="#">
                                                        <h4>Science In The New Era</h4>
                                                        <span class="price yellow">From £49</span>
                                                    </a>

                                                    <div class="overlay-footer">
                                                        <div>At 4:00 pm</div>
                                                        <div>Bentley Campus, Perth, WA</div>
                                                    </div>
                                                </div>                                
                                            </div> 
                                        </div><!-- /event-overlay -->
                                    </div>
                                </div>
                            </div>

                            <div class="grid-item">
                                <div class="grid-item-inner">
                                    <div class="event-item">
                                        <div class="event-thumbnail">
                                            <a href="#">
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/7.jpg" alt="image">
                                            </a>
                                        </div><!-- /event-thumbnail -->
                                        
                                        <div class="date-block">
                                            <div class="month">Jun</div>
                                            <div class="day">19</div>
                                        </div><!-- /date-block -->

                                        <div class="event-overlay">
                                            <div class="cs-post-header">
                                                <div class="cs-category-links">
                                                    <a class="overlay-top" href="#">
                                                        <h4>How To Sell Anything</h4>
                                                        <span class="price yellow">From £99</span>
                                                    </a>

                                                    <div class="overlay-footer">
                                                        <div>At 9:30 am</div>
                                                        <div>Bentley Campus, Perth, WA</div>
                                                    </div>
                                                </div>                                
                                            </div> 
                                        </div><!-- /event-overlay -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>

        

        <section class="flat-row full-color">
            <div class="container">                
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-reviews">
                            <div class="flat-causes">
                                <div class="featured-causes" data-item="3" data-nav="false" 
                                    data-dots="false" data-auto="false">
                                    <div class="item">
                                        <div class="text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit metus vel leo placerat cursus. Nullam rutrum velit eget quam pretium vehicula. Maecenas enim velit.</p>
                                        </div>

                                        <div class="title-testimonial">
                                            <div class="thumb-title">
                                                <img src="<?php echo $this->webroot; ?>images/member/1.jpg" alt="image">
                                            </div>
                                            <div class="post-title">
                                                <h6 class="title-post">John Doe</h6>
                                                <span>Professor</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit metus vel leo placerat cursus. Nullam rutrum velit eget quam pretium vehicula. Maecenas enim velit.</p>
                                        </div>
                                        <div class="title-testimonial">
                                            <div class="thumb-title">
                                                <img src="<?php echo $this->webroot; ?>images/member/2.jpg" alt="image">
                                            </div>
                                            <div class="post-title">
                                                <h6 class="title-post">Mr.John</h6>
                                                <span>Professor</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="item">
                                        <div class="text">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit metus vel leo placerat cursus. Nullam rutrum velit eget quam pretium vehicula. Maecenas enim velit.</p>
                                        </div>
                                        <div class="title-testimonial">
                                            <div class="thumb-title">
                                                <img src="<?php echo $this->webroot; ?>images/member/3.jpg" alt="image">
                                            </div>
                                            <div class="post-title">
                                                <h6 class="title-post">Vicky</h6>
                                                <span>Professor</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>     
                    </div>
                </div>  
            </div>
        </section><!-- /flat-row -->
