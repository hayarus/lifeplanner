<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>


<!---jquery Validation End-->
<style>
.help-block {
    display: block;
    margin-top: 17px;
    font-size: 14px;
    margin-bottom: 17px;
    color: #ff6262;
}
</style>
<script type="text/javascript"> 
$(document).ready(function(){
var error1 = $('.alert-danger');
  $('#couseform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "data[cid]" : {required : true},
    "data[pid]" : {required : true},
    "data[id]" : {required : true},
    //"data[message]" : {required : true},
          
    },
    messages:{
      "data[cid]" : {required :"Please choose your preferred destination."},
      "data[pid]" : {required :"Please choose level of course looking for."},
      "data[id]" : {required :"Please choose your area of interest."},
      //"data[message]" : {required :"Please enter message."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },
  });
});
</script>

<style type="text/css">
	.content_block .cols .col:first-child {
    margin-left: 0;
}
.content_block .cols .col {
    width: 31.3%;
    margin-left: 3%;
    float: left;
    position: relative;
}
.search_box {
    display: block;
    background: #9400D3;
    padding: 23px 6%;
    color: #fff;
    min-height: 392px;
}
.cb:after {
    content: "";
    width: 100%;
    height: 0;
    clear: both;
    display: block;
}
#main h4 {
    font-weight: 600;
    margin: 0 0 5px;
}

.search_box h4 {
    font-size: 24px;
    line-height: 26px;
    margin-bottom: 10px;
    font-weight: 400;
}
.wrapper p {
    margin: 0 0 15px;
    text-align: justify;
}

.search_box p {
    font-size: 16px;
    margin-bottom: 15px;
    font-weight: 300;
}
select {
    padding: 5px;
    border: 1px solid #c2c2c2;
    max-width: 100%;
    width: 100%;
    color: #666;
}
option {
    font-weight: normal;
    display: block;
    white-space: pre;
    min-height: 1.2em;
    padding: 0px 2px 1px;
}
.search_box input[type="submit"] {
    cursor: pointer;
    display: block;
    height: 51px;
    background: #7ABA7A url(../lifeplanner/images/parallax/search_icon.png) no-repeat 98% center;
    color: #fff;
    padding-left: 3%;
    font-size: 18px;
    text-align: left;
    width: 100%;
    border: none;
    text-indent: 0;
    transition: all .5s ease-in 0;
    -webkit-transition: all .5s ease-in 0;
}
.sbHolder {
    background-color: #fff;
    font-size: 15px;
    font-weight: normal;
    height: 56px;
    border-radius: 3px;
    position: relative;
    width: 100%;
    display: block;
    color: #3b4a5e;
    margin-bottom: 10px;
}
.sbToggle {
    background: url(../lifeplanner/images/parallax/down_arrrow.png) no-repeat center center;
    display: block;
    height: 56px;
    outline: none;
    position: absolute;
    right: 0;
    top: 0;
    width: 50px;
}
.sbSelector:link, .sbSelector:visited, .sbSelector:hover {
    color: #3b4a5e;
    outline: none;
    text-decoration: none;
}

.sbSelector {
    display: block;
    height: 56px;
    left: 0;
    line-height: 56px;
    outline: none;
    overflow: hidden;
    position: absolute;
    text-indent: 10px;
    top: 0;
    width: 100%;
}
.home_top.content_block .cols .col ul {
    margin: 0;
}
.sbOptions {
    background-color: #fff;
    list-style: none;
    left: 0px;
    margin: 0;
    padding: 0;
    position: absolute;
    top: 100%;
    width: 100%;
    z-index: 1;
    overflow-y: auto;
    margin: -2px 0 0!important;
    border-radius: 0px 0px 3px 3px;
}
.home_top.content_block .cols .col ul li {
    margin-bottom: 0;
    min-height: 147px;
    text-align: left;
}

.home_top.content_block .cols .sbOptions li {
    min-height: 1px!important;
}
.sbOptions li {
    padding: 0 7px;
}

.sbOptions li {
    margin: 0;
}
.sbOptions li:last-child>a {
    border-bottom: none;
    margin-bottom: 5px;
}

.sbOptions li.last a {
    border-bottom: none;
}
.sbOptions a:hover, .sbOptions a:focus, .sbOptions a.sbFocus {
    color: #fe670e;
}
.sbOptions a:link, .sbOptions a:visited {
    color: #3b4a5e;
    text-decoration: none;
}
.sbOptions a {
    border-bottom: dotted 1px #0c4da2;
    display: block;
    outline: none;
    padding: 7px 0 7px 3px;
    color: #3b4a5e;

}
.blog-pad {
    padding: 0 15px 25px;
    box-shadow: 0 25px 35px -32px rgba(0,0,0,0.2),0 36px 65px 0px rgba(0,0,0,0.19);
    backface-visibility: hidden;
    -webkit-backface-visibility: hidden;
}
.blog-pad .item-content{
    border-color:#fff0; 
}
.shortcode-blog .item-content{
    border-color:#fff0; 
}
</style>

<section class="flat-row full-color-v3" style="padding-top: 40px;padding-bottom: 40px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-clients">
                    <div class="section-body">
                        <div class="row">
                            <div class="col-md-6 shortcode-blog-item ">
                                <div class="content-pad shortcode-blog">
                                    <div class="post-item row">
                                        <div class="col-md-12 " style="top: 19px;">
                                            <div class="content-pad">
                                                <div class="item-content">
                                                    <h3 class="item-title">
                                                         <a href="#" title="Your Career Starts Here" class="main-color-1-hover" style="font-size: 27px;font-style: italic;">ABOUT LIFE PLANNER</a>
                                                    </h3>
                                                    <div class="shortcode-blog-excerpt" style=""> 
                                                        <div class="flat-business"> 
                                                            <div class="box-icon"> 
                                                                <div class="icon-business"> 
                                                                    <img src="<?php echo $this->webroot; ?>images/icon/f1.png" alt="image"> 
                                                                </div> 
                                                                <div class="icon-business"> 
                                                                    <img src="<?php echo $this->webroot; ?>images/icon/f2.png" alt="image"> 
                                                                </div> 
                                                                <div class="icon-business"> 
                                                                    <img src="<?php echo $this->webroot; ?>images/icon/f3.png" alt="image"> 
                                                                </div> 
                                                            </div><!-- /box-icon --> 
                                                        </div> 
                                                        <div class="box-details"> 
                                                            <p style="font-size: 15px;font-style: italic;">Life Planner from its modest start up in 2012 has become a reputed and well respected educational facilitator, accredited by an ISO certification within this short span of trustworthyservice. Life Planner has been guiding, supporting and providing professional &amp; educational facilitation for students to choose curriculum, courses, and institutions in academic and professional streams abroad. </p> 
                                                        </div> 
                                                        <div class="about-btn"> 
                                                            <a class="flat-button"  href="<?php echo $this->webroot; ?>colleges/aboutus"  style="float: right;border: solid 2px #333333;">Read More <i class="fa fa-angle-right"></i></a> 
                                                        </div> 
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>  
                                    </div><!--/post-item-->
                                </div>
                            </div><!--/shortcode-blog-item--> 
                            <div class="col-md-6 shortcode-blog-item"> 
                                <div class="blog-pad"> 
                                    <div class="content-pad"> 
                                        <div class="post-item row"> 
                                            <div class="col-md-12 " style="top: 19px;"> 
                                                <div class="content-pad"> 
                                                    <div class="item-content"> 
                                                        <h3 class="item-title"> 
                                                            <a href="#" title="Your Career Starts Here" class="main-color-1-hover" style="font-size: 27px;font-style: italic;">CHAIRMAN'S MESSAGE</a> 
                                                        </h3> 
                                                        <div class="col-md-4 img-icon" > 
                                                            <div class="content-pad"> 
                                                                <div class="item-thumbnail"> 
                                                                    <a href="#"><img src="<?php echo $this->webroot; ?>images/contact/chairmancopy.jpg" alt="image"></a> 
                                                                </div> 
                                                            </div> 
                                                        </div> 
                                                        <div class="shortcode-blog-excerpt" > 
                                                            <div class="box-details"> 
                                                                <p style="font-size: 15px;font-style: italic;text-align:justify;">Overseas education is a complicated area for students and their parents.This is not because of its nature but because of the reason that it is a field of wider opportunities and choices. Selecting a program, a University and a destination are not always an easy choice for most of the aspirants and there are risks of being misled.An attire for us will be selected in our own size and color preference, the food we eat is based on individual taste preference and similarly the way we think of a particular subject is also determined by different dimensions. </p>
                                                            </div> 
                                                            <div class="about-btn"> 
                                                                <a class="flat-button" id="myBtn" href="#" title="Spark Of Genius" style="float: right;border: solid 2px #333333;">Read More <i class="fa fa-angle-right"></i></a> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!--/post-item--> 
                                    </div> 
                                </div> 
                            </div><!--/shortcode-blog-item--> 
                        </div><!--/row--> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div>
</section>
<div class="page-title parallax parallax4">
    <div class="container">
        <div class="row">
            <div class="post-wrap">
                <div class="top-title">
                    <h2 class="title fancy"><span>SERVICES</span></h2> 
                    <p>Life planner is one of the most trusted overseas education service providers in India</p> 
                </div>
                <div class="posts-carousel"> 
                    <div class="flat-event"> 
                        <div class="flat-blog-carousel" data-item="3" data-nav="true" data-dots="false" data-auto="true">
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/7.jpg" alt="image">
                                            </a>
                                        </div><!-- /event-thumbnail -->
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>CAREER COUNSELING</h4>
                                                    </a>
                                                    <div class="overlay-footer"> 
                                                        <div></div> 
                                                        <div>Keeping in view your individual profile, our team would help you understand your choices.</div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!-- /event-overlay --> 
                                    </div> 
                                </div> 
                            </div><!-- /grid-item --> 
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/2.jpg" alt="image"> 
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>BUILDING A STUDENT PROFILE</h4> 
                                                    </a> 
                                                    <div class="overlay-footer"> 
                                                        <div></div> 
                                                        <div>Universities give value to Letters of Recommendations.</div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!-- /event-overlay --> 
                                    </div> 
                                </div> 
                            </div>
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/3.jpg" alt="image"> 
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>CANDIDATE APPLICATION</h4> 
                                                    </a> 
                                                    <div class="overlay-footer"> 
                                                        <div></div> 
                                                        <div>The application process can be daunting.</div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!-- /event-overlay --> 
                                    </div> 
                                </div> 
                            </div>
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/4.jpg" alt="image"> 
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>SHORLISTING YOUR EDUCATION PLATFORM</h4> 
                                                    </a> 
                                                    <div class="overlay-footer"> 
                                                        <div></div> 
                                                        <div>There is only one University or College, which is the perfect fit for each of us.</div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!-- /event-overlay --> 
                                    </div> 
                                </div> 
                            </div>
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/5.jpg" alt="image">
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>SCHOLARSHIP GUIDANCE</h4>
                                                    </a> 
                                                    <div class="overlay-footer"> 
                                                        <div></div> 
                                                        <div>A limited number of scholarships are available now for students.</div>
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!-- /event-overlay --> 
                                    </div> 
                                </div> 
                            </div>
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/6.jpg" alt="image"> 
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>PRE DEPARTURE BRIEFING</h4> 
                                                    </a> 
                                                    <div class="overlay-footer"> 
                                                        <div></div> 
                                                        <div>Pre departure Orientation and checklist screening.</div> 
                                                    </div> 
                                                </div> 
                                            </div> 
                                        </div><!-- /event-overlay --> 
                                    </div> 
                                </div> 
                            </div>
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/1.jpg" alt="image"> 
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>POST-LANDING ASSISTANCE</h4> 
                                                    </a>
                                                    <div class="overlay-footer"> 
                                                        <div>Smooth transition ensured with Feel At Home service.</div> 
                                                        <div></div> 
                                                    </div> 
                                                </div> 
                                            </div> <!-- /cs-post-header --> 
                                        </div><!-- /event-overlay --> 
                                    </div><!-- /event-item --> 
                                </div><!-- /grid-item-inner --> 
                            </div><!-- /grid-item -->
                            <div class="grid-item"> 
                                <div class="grid-item-inner"> 
                                    <div class="event-item"> 
                                        <div class="event-thumbnail"> 
                                            <a href="#"> 
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/gril/loan.jpg" alt="image"> 
                                            </a> 
                                        </div><!-- /event-thumbnail --> 
                                        <div class="event-overlay"> 
                                            <div class="cs-post-header"> 
                                                <div class="cs-category-links"> 
                                                    <a class="overlay-top" href="#"> 
                                                        <h4>EDUCATIONAL LOAN ASSISTANCE</h4> 
                                                    </a> 
                                                    <div class="overlay-footer"> 
                                                        <div>Financial Concerns - Multiple Options.</div> 
                                                        <div></div> 
                                                    </div> 
                                                </div> 
                                            </div> <!-- /cs-post-header --> 
                                        </div><!-- /event-overlay --> 
                                    </div><!-- /event-item --> 
                                </div><!-- /grid-item-inner --> 
                            </div><!-- /grid-item -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="flat-row padding-small" >
    <div class="container">
        <div class="flat-choose-us">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="globe-edu">
                    <div class="name-toggle">
                        <h2 class="title">Global Education Services</h2>
                    </div>
                    <div class="search_box cb">
                        <h4>Explore your Study Options</h4>
                        <p>Select your preferred destination and your field of study </p>
                        <form id="couseform" name="couseform" action="<?php echo $this->webroot;?>universities/exploreresult" method="POST" class="has-validation-callback">
                            <div>
                             <select name="data[cid]" class="sbHolder" id="cid" data-validation="required" data-validation-error-msg="Please select your preferred destinations" sb="" >
                                <option value="">Your Preferred Destination</option>
                                <?php foreach($countries as $key=>$value){ ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value;?></option><?php }  ?> 
                                </select>
                            </div>
                            <div> 
                                <select name="data[pid]" class="sbHolder" id="pid" data-validation="required" data-validation-error-msg="Please select level of course looking for"  sb="" style="">
                                    <option value="">Level of course looking for</option>
                                    <option value="1">Postgraduate</option>
                                    <option value="2">Undergraduate</option> 
                                </select>
                            </div>
                            <div id=""> 
                                <select name="data[id]" class="sbHolder" id="id" data-validation="required" data-validation-error-msg="Please select your area of interest" sb="90192809" >
                                    <option value="">Your area of interest</option>
                                     <?php foreach($areaofstudy as $key=>$value){ ?><option value=<?php echo $key; ?>><?php echo $value;?></option><?php }  ?> 
                                 </select>
                             </div> 
                             <input type="submit" value="Search">
                         </form>
                     </div>
                 </div>
             </div>
                 <div class="col-md-6 col-sm-6">
                    <div class="globe">
                    <div class="flat-blog">
                        <div class="section-header">
                            <div class="name-blog">
                                <h2 class="title">Upcoming Events</h2>
                                <a class="" href="#"> <i class=""></i></a>
                            </div>
                        </div> 
                        <div class="section-body" id="style-1">
                            <div class="row">
                                <div class="col-md-12 shortcode-blog-item">
                                    <!-- <div class="col-md-12"> </div> -->
                                    <div class="event-content">
                                        
                                        <div class="marquee">
                                       
                                        <?php foreach ($events as $key => $event) { ?>
                                        <div class="post-item ">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="content-pad">
                                                            <div class="item-thumbnail">
                                                                <a href="<?php echo $this->webroot;?>colleges/events">
                                                                    <img src="<?php echo $this->webroot .$eventImagePath.$event['Event']['image']; ?>" alt="image">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="content-pad">
                                                            <div class="item-content">
                                                                <h3 class="item-title"> 
                                                                    <a href="<?php echo $this->webroot;?>colleges/events" title="Your Career Starts Here" class="main-color-1-hover"><?php echo $event['Event']['title']; ?> </a> 
                                       
                                                                    <div  style ="padding: 1%; color: #FFF; background-color: #a047c6; width:20%; text-align: center; border-radius:10%; font-size: 12px;">
                                                                        <?php echo $boolean_values_event_status[$event['Event']['event_status']] ?>
                                                                        
                                                                    </div>
                                                                </h3>
                                                                <div class="shortcode-blog-excerpt"><p><strong>Event Date : </strong> <?php if(!empty($event['Event']['eventdate'])){ echo date("F-d-Y", strtotime($event['Event']['eventdate'])) ; } ?></div>
                                                                <div class="shortcode-blog-excerpt"> <p><strong>Venue : </strong> <?php echo $event['Event']['venue']; ?></p></div>
                                                                <div class="shortcode-blog-excerpt"><a href="#"><?php echo substr($event['Event']['description'],0,250); ?>...</a></div>
                                                                <div class="item-meta">
                                                                    <a class="flat-button" href="<?php echo $this->webroot;?>colleges/events" title="Spark Of Genius">More <i class="fa fa-angle-right"></i></a>
                                                                    <a href="<?php echo $this->webroot;?>colleges/events" class="main-color-1-hover" title="View comments"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- col-md-7 -->
                                                </div>
                                            </div>
                                        </div><!-- post-item -->
                                        <?php } ?>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <!--/row-->
                            </div>
                        </div>
                    </div>
                </div><!--/col-md-6 col-sm-6 -->
            </div>
        </div>
    </div>
</div>
</section>
  

 <section class="flat-row padding-small-v1">
    <div class="container">
        <div class="row">
            <div class="flat-blog">
                <div class="section-header">
                    <div class="name-blog">
                        <h2 class="title fancy"><span>OUR MISSION</span></h2>
                        <!-- <a class="" href="#"> <i class=""></i></a> -->
                    </div>
                </div>    
                
                <div class="section-body">
                    <div class="row">
                        <div class="col-md-4 shortcode-blog-item">
                            <div class="content-pad">
                                <div class="post-item ">
                                    <div class="col-md-12 ">
                                    	<div class="row">
                                    		<div class="content-pad">
                                            <div class="item-thumbnail">
                                                <a href="<?php echo $this->webroot; ?>colleges/aboutus">
                                                    <img src="<?php echo $this->webroot; ?>images/about/index1/blog/right.jpg" alt="image">
                                                    <!--<span class="thumbnail-overlay">June 3, 2014</span>-->
                                                </a>
                                            </div>
                                        </div>
                                    	</div>
                                    	<div class="row">
                                    		<div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title">
                                                    <a href="<?php echo $this->webroot; ?>colleges/aboutus" title="Your Career Starts Here" class="main-color-1-hover">Right Direction</a>
                                                </h3>
                                                <div class="shortcode-blog-excerpt">Doubts and inhibitions have no space as Life Planner has been certified for its services globally by  ISO 9001:2015</div>
                                                <div class="item-meta">
                                                <a class="flat-button" href="<?php echo $this->webroot; ?>colleges/aboutus" title="Your Career Starts Here">DETAILS <i class="fa fa-angle-right"></i></a>
                                                <a href="<?php echo $this->webroot; ?>colleges/aboutus" class="main-color-1-hover" title="View comments"></a>
                                                </div>
                                            </div>
                                        </div>
                                    	</div>
                                        
                                    </div>

                                </div><!--/post-item-->
                            </div>
                        </div><!--/shortcode-blog-item-->

                        <div class="col-md-4 shortcode-blog-item">
                            <div class="content-pad">
                                <div class="post-item ">
                                    <div class="col-md-12">
                                    	<div class="row">
                                    		 <div class="content-pad">
                                            <div class="item-thumbnail">
                                                <a href="#">
                                                    <img src="<?php echo $this->webroot; ?>images/about/index1/blog/subjects.jpg" alt="image">
                                                    <!--<span class="thumbnail-overlay">June 3, 2014</span>-->
                                                </a>
                                            </div>
                                        </div>
                                    	</div>
                                    	<div class="row">
                                    		 <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title">
                                                    <a href="<?php echo $this->webroot;?>bookappointments/bookappointment" title="Spark Of Genius" class="main-color-1-hover">Suitable Subjects</a>
                                                </h3>
                                                <div class="shortcode-blog-excerpt">Entrusting Life Planner enables you to revive the stream which you were born for.</div>
                                                <div class="item-meta">
                                                    <a class="flat-button" href="<?php echo $this->webroot;?>bookappointments/bookappointment" title="Spark Of Genius">DETAILS <i class="fa fa-angle-right"></i></a>
                                                    <a href="#" class="main-color-1-hover" title="View comments"></a>
                                                </div>
                                            </div>
                                        </div>
                                    	</div>
                                       
                                    </div>
                                </div><!--/post-item-->
                            </div>
                        </div><!--/shortcode-blog-item-->
                              <div class="col-md-4 shortcode-blog-item">
                            <div class="content-pad">
                                <div class="post-item ">
                                    <div class="col-md-12">
                                    	<div class="row">
                                    		 <div class="content-pad">
                                            <div class="item-thumbnail">
                                                <a href="<?php echo $this->webroot;?>universities/collegelist">
                                                    <img src="<?php echo $this->webroot; ?>images/about/index1/blog/top.jpg" alt="image">
                                                    <!--<span class="thumbnail-overlay">June 3, 2014</span>-->
                                                </a>
                                            </div>
                                        </div>
                                    	</div>
                                    	<div class="row">
                                    		 <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title">
                                                    <a href="<?php echo $this->webroot;?>universities/collegelist" title="Spark Of Genius" class="main-color-1-hover">Top Universities</a>
                                                </h3>
                                                <div class="shortcode-blog-excerpt">Life Planner promotes ranked universities by focusing on the student's future so that they are being awarded with degrees, which all  accepted and recognized globally.</div>
                                                <div class="item-meta">
                                                    <a class="flat-button" href="<?php echo $this->webroot;?>universities/collegelist" title="Spark Of Genius">DETAILS <i class="fa fa-angle-right"></i></a>
                                                    <a href="<?php echo $this->webroot;?>universities/collegelist" class="main-color-1-hover" title="View comments"></a>
                                                </div>
                                            </div>
                                        </div>
                                    	</div>
                                       
                                    </div>
                                </div><!--/post-item-->
                            </div>
                        </div><!--/shortcode-blog-item-->
                    </div><!--/row-->
                </div>
            </div>
        </div>
    </div>
</section>
<section class="flat-row full-color-v4">
    <div class="container">
            <div class="row">
                <div class="flat-blog">
                <div class="section-header">
                            <div class="name-blog">
                                <h2 class="title fancy"><span> PARTNERING UNIVERSITIES AND COLLEGES </span></h2>
                                <!-- <a class="" href="#"> <i class=""></i></a> -->
                            </div>
                        </div> 
                <div id="carousel-container">
                <div id="carousel-inner">
                    <ul id="carousel-ul">
                        <li></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/montana.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/northernlights.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/nipissing.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/mountsaint.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/coastmountain.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/capilano.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/cumberland.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/greatplains.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/lakeland.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/parkland.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/capebretonuniversity.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/download.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/London-South.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/spokane.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/california.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/rockies.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/selkirk.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/stlaurance.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/vancouver.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/unbc1.png"></li>
                        <li><img src="<?php echo $this->webroot;?>images/logos/yukon.png"></li>
                                          
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 <section class="main-content">
       <div class="container-fluid">
            <div class="row">
                <div id="map" style="width: 100%; height: 500px; "></div> 
            </div>
        </div><!-- /container-fluid -->      
              
</section>
<style type="text/css">

    #carousel-container {
    width:100%;
    margin: 2em auto;
    position: relative;
    /*border: 1px solid #ccc;*/
    overflow: hidden;
    height: 200px;
}

#carousel-inner {
    width: 100%;
    float: right;
    position: relative;
    overflow: hidden;
}

#carousel-ul {
    position:relative;
    right:320px;
    list-style-type: none;
    margin: 0px;
    padding: 0px;
    width:9999px;
 
}

#carousel-ul li{
    float: left;                                    
    width: 200px;  
    padding:0px;
    height:200px;
    margin: 0px 5px 0 5px;
    position: relative;
    /*background: #c0c0c0;*/
    line-height: 186px;
    font-size: 2em;
    text-align: center;
}

</style>


<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h2 style ="color:black">Chairman's Message</h2>
    </div>
    <div class="modal-body">
        <div class="col-md-3 img-icon" >
          <div class="content-pad">
            <div class="item-thumbnail">
                <a href="#"><img src="<?php echo $this->webroot; ?>images/contact/chairmancopy.jpg" alt="image"></a>
                </div>
            </div>
        </div>
        <div class="shortcode-blog-excerpt" > 
            <div class="box-details"> 
                
                <p style="text-align:justify;">Overseas education is a complicated area for students and their parents.This is not because of its nature but because of the reason that it is a field of wider opportunities and choices. Selecting a program, a University and a destination are not always an easy choice for most of the aspirants and there are risks of being misled. 

                An attire for us will be selected in our own size and color preference, the food we eat is based on individual taste preference and similarly the way we think of a particular subject is also determined by different dimensions. In short we have our own choice for each and every aspect that is tailor made to fit best on us. But why is it that we run behind a particular trend in education? Why do we feel the pressure to join a group that opts a particular program, University, college or a country? 

                As stated above, each student has their own dreams, passion, skill and talent. Moreover they will have a vision on what they should become. Hence the role of education must be in enhancing their vision and to fulfill their dreams. 

                Life Planner is well connected with a network of world class universities, colleges and B- Schools across the globe. Our strategy is to provide tailor made options for each and every student according to their personal preferences. This analysis is carried out through our well trained and experienced counselors who will be able to suggest easy legal immigration options for your need. Our team also assures that every student gets uncompromised quality of education and unbiased counseling. 

                As the Chairman of Life Planner, a company that was established with a true vision, I personally visit most of the universities that we promote to ensure its facilities, quality and post-study options. With this assurance I can say that every aspect of your life from the moment you join with us will be planned at Life Planner. 
            </p>
        </div>
    </div>
    </div>
    <div class="modal-footer">
      
    </div>
  </div>

</div>
 <!-- <div class="modal fade" id="overlay">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>

      </div>
      <div class="modal-body">
        <a href="<?php echo $this->webroot;?>colleges/registration">
        <img  src="<?php echo $this->webroot; ?>images/copy.jpg" border="0" alt="" />
        </a>
        <p>Context here</p>
        <button class="btn btn-purpple" type="button"  onclick="window.location='<?php echo $this->webroot.'colleges/registration'; ?>'" >Register Now</button>
      </div>
    </div>
  </div>
</div> -->
<style>
#style-1::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}

#style-1::-webkit-scrollbar
{
    width: 5px;
    background-color: #F5F5F5;
}

#style-1::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
    background-color: #c1c1c1;
}
.section-header{
    text-align: center!important;
}
#style-1{
        /*height: 440px!important;*/
    overflow-x: hidden; 
    overflow-y: scroll; 
    scroll-behavior: smooth;

}

.flat-blog .title {
    float: none;
    text-align: center;
    padding: 10px;
}
.fancy {
  line-height: 0.5;
  text-align: center;
}
.fancy span {
  display: inline-block;
  position: relative;  
  line-height: 25px;
}
.fancy span:before,
.fancy span:after {
  content: "";
  position: absolute;
  height: 5px;
  border-bottom: 3px solid #9400D3;
  top: 10px;
  width: 500px;
}
.fancy span:before {
  right: 100%;
  margin-right: 15px;
}
.fancy span:after {
  left: 100%;
  margin-left: 15px;
}

.globe img{
        height:222px;
    }
/* ---// meadia screen //--- */
@media (min-width: 1200px){
    .blog-pad{
       width: 560px;
       height: 400px;
    }
   
    .globe-edu {
        padding: 50px 0 0;
        width:90%;
    }
    .globe-edu .search_box {
        height: 400px;
    }
    .globe {
        padding: 45px 0 0;
    }
    .globe img{
        height:200px;
    }
    .post-item .content-pad .item-title {
        font-size: 16px;
    }
  	.flat-business .box-icon .icon-business {
  		padding: 0 40px;
  }
  .box-details {
  		padding-top:10px;
  }
  .flat-business .box-icon {
    margin: 0 -35px;
}
 .globe .section-body{
    height: 450px;
 }

}
    @media (min-width: 970px){
    .img-icon{
        /*float: right;*/
        /*margin-top: 50px;*/
    }
    .about-btn a {
        margin-top: 5px;
    }

}

.btn-purpple{
    color: #ffffff;
    background: #9400D3;
    margin-left: 35%;
    margin-top: 10px;
}
.btn-purpple:hover{
     color: #ffffff;
    background: #7ABA7A;
}
.modal{
    margin: 30px auto;
    overflow-y: scroll;
}

.modal-header {
    padding: 24px 22px;
    color: white;
    font-size: 24px;
}
 .about-btn{
        float: left;
    }
/* Modal Body */
.modal-body {padding: 21px 24px;
height: auto!important;}

.modal-content {
    position: relative;
    background-color: #fefefe;
    margin: auto;
    padding: 0;
    border: 1px solid #888;
    width: 80%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    animation-name: animatetop;
    animation-duration: 0.4s
}
.modal-footer {
    padding: 24px 22px;
    background-color: #7ABA7A;
   
}


/* Add Animation */
@keyframes animatetop {
    from {top: -300px; opacity: 0}
    to {top: 0; opacity: 1}
}

@media (min-width:480px)  {

    .modal-body {padding: 21px 24px;height: 250px;
    overflow-y: auto;}
    
    .modal-dialog{
    overflow-y: initial !important
}
}
@media (min-width:320px)  { 
    .modal-content {
        width: 100%;
    }
    .modal-body {padding: 21px 24px;height: 250px;
    overflow-y: auto;}
    
    .modal-dialog{
    overflow-y: initial !important
}
    
}
.event-content{
    /*overflow: hidden;*/
    background: white;
    position: relative;
}

.globe .section-body{
    height: 400px;
 }

</style>
<script>
    // Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var Carousel = new
function() {

    var wrapper = document.getElementById('carousel-container'),
        timer = null;

    var start = function() {
        doCarousel();
    };

    var sliding = function() {
        var item_width = $('#carousel-ul li').outerWidth() + 10;
        var right_indent = parseInt($('#carousel-ul').css('right')) - item_width;
        $('#carousel-ul:not(:animated)').animate({
            'right': right_indent
        }, 2000, 'linear', function() {

           $('#carousel-ul li:first').after($('#carousel-ul li:last'));
                        $('#carousel-ul').css({
                'right': '320px'
            });
        });
    };

    var doCarousel = function() {
        timer = setInterval(sliding, 10);
    };

    var pause = function() {
        clearInterval(timer);
        timer = null;
    };

    var resume = function() {
        doCarousel();
    };

    this.init = function() {
        start();
    };
}();

$(function() {
    Carousel.init();
});

$('#carousel-ul li').mouseover(function(){ 
        //alert("hey");
    //clearInterval(timer);
    //timer = null;
    //pause();
    Carousel.pause();
});
</script>