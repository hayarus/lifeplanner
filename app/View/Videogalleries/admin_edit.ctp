<?php $this->Html->addCrumb('Videogallery', '/admin/videogalleries'); ?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#VideogalleryAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
        "data[Videogallery][name]" : {required : true},
        "data[Videogallery][message]" : {required : true},
        "data[Videogallery][url]" : {required : false},
		},
		messages:{
        "data[Videogallery][name]" : {required :"Please enter name."},
        "data[Videogallery][message]" : {required :"Please enter message."},
        "data[Videogallery][url]" : {required :"Please enter url."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Edit Videogallery'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form Videogallerys">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Videogallery', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div> 
                              <?php
                                echo $this->Form->input('id', array('class' => 'form-control', 'label' => false,  'required' => false,'hiddenField' => false,'type'=>'hidden'));
                                ?>
                          
                            <div class="form-group">
                            <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label', 'for' => '')); ?>
                            <div class="col-md-4">  <?php
                                echo $this->Form->input('name', array('class' => 'form-control', 'label' => false,  'required' => false,'hiddenField' => false,'type'=>'text'));
                                ?>
                            </div>
                        </div>      
                         <div class="form-group">
                            <?php echo $this->Form->label('university' ,null, array('class' => 'col-md-3 control-label', 'for' => '')); ?>
                            <div class="col-md-4">  <?php
                                echo $this->Form->input('university', array('class' => 'form-control', 'label' => false,  'required' => false,'hiddenField' => false,'type'=>'text'));
                                ?>
                            </div>
                        </div>                                       
                       
<div class="form-group">
							 <?php echo $this->Form->label('message<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('message', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('youtubeid<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('youtubeid', array('class' => 'form-control', 'label' => false, 'required' => false,'type'=>'text', 'placeholder' =>'l5KpyMR5q3o'));?>
							</div>
                            <?php if(!empty($video)){ ?>
							<div class="bs-example">
							<a href="#myModal" class="" data-toggle="modal">View Video</a>
							</div>
                            <?php } ?>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/videogalleries'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"> Video</h4>
                </div>
                <div class="modal-body">
                	<?php  if(isset($video)){?>
							<iframe id="tVideo" width="560" height="315" src="//www.youtube.com/embed/<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe>
							<?php } ?>
                    
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
$(document).ready(function(){
    /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#tVideo").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#myModal").on('hide.bs.modal', function(){
        $("#tVideo").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#myModal").on('show.bs.modal', function(){
        $("#tVideo").attr('src', url);
    });
});
</script>

<style type="text/css">
	 .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
</style>