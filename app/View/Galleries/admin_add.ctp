<?php $this->Html->addCrumb('Gallery', '/admin/galleries'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#GalleryAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Gallery][titile]" : {required : true},
"data[Gallery][image]" : {required : true,accept : "png|jpg|jpeg|gif", filesize: 1000000},
		},
    "data[Gallery][course]" : {required : true},
    "data[Gallery][university]" : {required : true},
    "data[Gallery][country_id]" : {required : true},
		messages:{
		"data[Gallery][titile]" : {required :"Please enter name."},
"data[Gallery][image]" : {required :"Please upload image.", accept : "Please upload an image in png, jpg, jpeg or gif format.", filesize: "File size should be less than 1MB."},
		},
    "data[Gallery][course]" : {required :"Please enter course."},
    "data[Gallery][university]" : {required :"Please enter university."},
    "data[Gallery][country_id]" : {required :"Please select country."},


		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
        errorPlacement: function(error, element) {
       if(element.attr("name") =="data[Gallery][image]"){
          $(".imageErrCont1").addClass("help-block");
          $(".imageErrCont1").text($(error).text());       
           // error.insertAfter(".fileupload-new");
        }
        else {
          error.insertAfter(element);
        }
      },
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Gallery'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form galleries">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Gallery', array('class' => 'form-horizontal','controller'=>'galleries','type'=>'file')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('titile', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                            <div class="form-group">
							 <?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
                           <!--<div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 127px; max-height: 127px; line-height: 20px;"></div>
                                <div>
                                  <span class="btn btn-file btn default"><span class="fileupload-new">Select image</span>
                                  <span class="fileupload-exists">Change</span>
                                  <input type="file" id="imgUploadSec" name="data[Gallery][image]" class="default" /></span>
                                  <a href="#" class="btn fileupload-exists btn default" data-dismiss="fileupload">Remove</a>
                                </div>-->
                           
								<?php echo $this->Form->input('image', array('class' => 'form-control', 'label' => false, 'required' => false,'type'=>'file'));?>
						

                            <span class="imageErrCont"></span>
                      </div>
						</div>
          <div class="form-group">
               <?php echo $this->Form->label('course<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('course', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
            <div class="form-group">
               <?php echo $this->Form->label('University<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('university', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
            <div class="form-group">
               <?php echo $this->Form->label('country<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('country_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/galleries'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->Html->addCrumb('Gallery', '/admin/galleries'); $paginationVariables = $this->Paginator->params();?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Galleries'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
               
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('titile','Name'); ?></th><th><?php echo $this->Paginator->sort('course'); ?></th><th><?php echo $this->Paginator->sort('university'); ?></th>
                        <th><?php echo $this->Paginator->sort('country_id'); ?></th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php  if(isset($galleries) && sizeof($galleries)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($galleries as $gallery): $slno++?>
                  <tr><td>
                     <?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        							<td><?php echo h($gallery['Gallery']['titile']); ?>&nbsp;</td>
		<td><?php echo h($gallery['Gallery']['course']); ?>&nbsp;</td><td><?php echo h($gallery['Gallery']['university']); ?>&nbsp;</td>
  <td><?php echo h($gallery['Country']['name']); ?>&nbsp;</td>
											<td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $gallery['Gallery']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $gallery['Gallery']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Galleries'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Gallery][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
