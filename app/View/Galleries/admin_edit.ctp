<?php $this->Html->addCrumb('Gallery', '/admin/galleries'); ?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#GalleryAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Gallery][titile]" : {required : true},
//"data[Gallery][image]" : {required : true,accept : "png|jpg|jpeg|gif", filesize: 1000000},
		},
		"data[Gallery][course]" : {required : true},
		"data[Gallery][university]" : {required : true},
		"data[Gallery][country_id]" : {required : true},
		messages:{
		"data[Gallery][titile]" : {required :"Please enter name."},
//"data[Gallery][image]" : {required :"Please upload image.", accept : "Please upload an image in png, jpg, jpeg or gif format.", filesize: "File size should be less than 1MB."},
		},
		"data[Gallery][course]" : {required :"Please enter course."},
		"data[Gallery][university]" : {required :"Please enter university."},
		"data[Gallery][country_id]" : {required :"Please select country."},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Edit Gallery'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form galleries">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Gallery', array('class' => 'form-horizontal','type'=>'file')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 
							<div class='col-md-4'>
								<?php echo $this->Form->input('id', array('type'=>'hidden','class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('Name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('titile', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
                           
                           
								<?php echo $this->Form->input('image', array('class' => 'form-control', 'label' => false, 'required' => false,'type'=>'file'));?>
								
							
                            <span class="imageErrCont"></span>
                      		</div>
                      		<?php if(!empty($gallery['Gallery']['image'])) {?>
							<div class="bs-example">
							<a href="#myModal" class="" data-toggle="modal"><i class="fa fa-search" style="margin-top: 12px;"></i></a>
							</div>
                            <?php } ?>

						</div>
						 <div class="form-group">
               <?php echo $this->Form->label('course<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('course', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
            <div class="form-group">
               <?php echo $this->Form->label('University<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('university', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
            <div class="form-group">
               <?php echo $this->Form->label('country<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('country_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/galleries/add'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"> Image</h4>
                </div>
                <div class="modal-body">
                	<?php  if(isset($gallery['Gallery']['image'])){?>
							<img src="<?php echo $this->webroot .$galleryImagePath.$gallery['Gallery']['image']; ?>" alt="" />
							<?php } ?>
                    
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
$(document).ready(function(){
    /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#tVideo").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#myModal").on('hide.bs.modal', function(){
        $("#tVideo").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#myModal").on('show.bs.modal', function(){
        $("#tVideo").attr('src', url);
    });
});
</script>

<style type="text/css">
	 .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
</style>