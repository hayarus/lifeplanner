<?php $user = CakeSession::read("Auth.User"); //pr($user);
// header('Content-Type: text/html; charset=ISO-8859-15');


//echo $this->webroot;exit;
//if (substr_count($_SERVER[‘HTTP_ACCEPT_ENCODING’], ‘gzip’)) ob_start(“ob_gzhandler”); else ob_start();
// if( ! ini_get('date.timezone') )
// {
//     date_default_timezone_set('GMT');
// }

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Life Planner');
?>


<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <!-- <title><?php echo $cakeDescription ?> | <?php echo $title_for_layout; ?></title> -->
    <?php
        /*echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');*/
        $this->Html->script('https://maps.google.com/maps/api/js?sensor=true', false);

    /*  echo $this->Html->meta($setting['Setting']['favicon'],$sitefilesPath.$setting['Setting']['favicon'],array('type' => 'icon'));*/
    ?>

    <meta charset="utf-8"/>
    <title>Life Planner Studies | Study abroad consultant | Top universities in Europe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name = "description" content = "We as an overseas education consultant since 2012, created a strong bond with best foreign Universities and Colleges. Study in Canada is possible through us with the help of Canada immigration licensed consultant">
    <meta name="keywords" content="" />
    <meta name="google-site-verification" content="zmNR_Zy-nkPpNnz03K0C3D_HJmTWwHXuN06bH5yF6tE " />
    <meta name="p:domain_verify" content="57b96c35c67c377d4d07038724c3072c"/>
    <!-- Facebook OG Card-->
    <meta property="og:title" content="Life Planner  Studies | Overseas Education consultant in Kerala |">
    <meta property="og:site_name" content="Life Planner Studies & opportunities">
    <meta property="og:url" content="https://www.lifeplanneruniversal.com/">
    <meta property="og:description" content="Life Planner from its modest start up in 2012 has become a reputed and well respected educational facilitator, accredited by an ISO certification within this short span of trustworthy service.">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.facebook.com/lifeplannerstudies/photos/a.215394355267146/584195385053706/?type=1&theater">

    <!-- Twitter Card  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@lifeplannerktm">
    <meta name="twitter:description" content="Life Planner from its modest start-up in 2012 has become a reputed and well respected educational facilitator, accredited by an ISO certification within this short span of trustworthy service.">
    <meta name="twitter:app:name:iphone" content="">
    <meta name="twitter:app:id:iphone" content="">
    <meta name="twitter:app:name:ipad" content="">
    <meta name="twitter:app:id:ipad" content="">
    <meta name="twitter:app:name:googleplay" content="">
    <meta name="twitter:app:id:googleplay" content="">
    <meta name="twitter:app:country" content="">

    
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
  
 <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/responsive.css">
    
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/colors/color1.css" id="colors">
    
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/animate.css">

    <!-- Favicon and touch icons  -->
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link rel="stylesheet" type="text/css" href="<?php $this->webroot; ?>css/style-ext.css">
    <link href="#" rel="shortcut icon">
    <style>
    .popup_msg{
        position:absolute;
        z-index:10;
        width:172px;
        height:102px;
        text-align:center;
        color:#FF0000;
        font: 14px Verdana, Arial, Helvetica, sans-serif;
        background:url(bg_image.gif) bottom right no-repeat;
        display:none;
        }
        

.hameid-loader-overlay {
    width: 100%;
    height: 100%;
    background: url('https://lifeplanneruniversal.com/app/webroot/images/loader.gif') center no-repeat #FFF;
    z-index: 99999;
    position: fixed;
}
  
</style>


<noscript>
    <style>.hameid-loader-overlay { display: none; }</style>
</noscript>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134534356-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134534356-1');
</script>
<!--<script>
        var isChrome = !!window.chrome; 
        
        if( isChrome ) {alert('hi');
        $("#videoId").replaceWith($('<video id="videoId" autoplay loop class="embed-responsive-item"><source src="lplogo.webm" type="video/webm"></video>'));
        }
    </script>-->
</head>
<body class="header-sticky">
    <!--<div class="hameid-loader-overlay"></div>-->
    <div class="boxed">
        <div class="menu-hover">
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->
        </div>
        <!--------------header menu------------>
        
        <!--header menu ends -->
        <!-- Header -->
        <header id="header" class="header">
            <div class="header-wrap">
                <div class="container">
                    <div class="header-wrap clearfix">
                        <div id="logo" class="logo">
                            <a href="<?php echo $this->webroot; ?>" rel="home">
                                <img src="<?php echo $this->webroot; ?>images/logo.png" alt="image">
                            </a>
                        </div><!-- /.logo -->


                        <div class="nav-wrap">

                            <?php 
                               echo $this->element('main-menu'); 
                             ?> 

                        </div><!-- /.nav-wrap -->
                    </div><!-- /.header-wrap -->
                </div><!-- /.container-->
            </div><!-- /.header-wrap-->
        </header><!-- /.header -->
        <!-- END PAGE HEADER-->
        
         <!-- begin banner content -->
            <section class="flat-row padding-60">
                <div class="slider-head slides" >
                    <div class="slider-content">
                        <div class="slider-items"> 
                            <div class="slider-slides" style="" > 
                                <a href="<?php echo $this->webroot;?>universities/nursingingermany"class="banner banner--square"> 
                                    <div class="slider-inner" > 
                                        <img class="" src="<?php echo($this->webroot); ?>images/tiles/nursing.jpg"> 
                                    </div> 
                                    <div class="inner-items"></div> 
                                    <div class="slide-plot"> 
                                        <div class="banner__title heading heading--secondary heading--with-line text--reverse heading--sub-2"> 
                                            <div class="heading--multiline"> 
                                                <p>PAP 3.5</p> 
                                                <p class="text--natural text--italic">Study Nursing in GERMANY @ No FEE</p>
                                            </div> 
                                        </div> 
                                    </div> 
                                </a> 
                            </div> 
                        </div>
                        <div class="slider-items"> 
                            <div class="slider-slides shortcode-blog-item" style=""> 
                                <a href="<?php echo $this->webroot;?>colleges/languagecoaching" class="banner banner--square"> 
                                    <div class="slider-inner" > 
                                        <img src="<?php echo($this->webroot); ?>images/tiles/ielts-guru.jpg"> 
                                    </div> 
                                    <div class="inner-items"></div> 
                                    <div class="slide-plot">
                                        <div class="banner__title heading heading--secondary heading--with-line text--reverse heading--sub-2">
                                            <div class="heading--multiline">
                                                <p>Flag Academy</p>
                                                <p class="text--natural text--italic">IELTS / German in Gurukulam Stay with your Language Guru </p>
                                            </div>
                                        </div> 
                                    </div> 
                                </a> 
                            </div> 
                        </div> 
                        <div class="slider-items"> 
                            <div class="slider-slides" style="">
                                <a href="<?php echo $this->webroot;?>countries/englishspeakingcountries" class="banner banner--square">
                                    <div class="slider-inner" >
                                        <img src="<?php echo($this->webroot); ?>images/tiles/studyincanada.jpg">
                                    </div>
                                    <div class="inner-items background-1" ></div>
                                    <div class="slide-plot">
                                        <div class="banner__title heading heading--secondary heading--with-line text--reverse heading--sub-2">
                                            <div class="heading--multiline">
                                                <p>Study in Canada  </p>
                                                <p class="text--natural text--italic">Post Study Support for settlement through our Immigration Support       office in Canada </p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="slider-items">
                            <div class="slider-slides" style="">
                                <a href="<?php echo $this->webroot;?>countries/canada" class="banner banner--square">
                                    <div class="slider-inner" >
                                        <img src="<?php echo($this->webroot); ?>images/tiles/migratetocanada.jpg">
                                    </div>
                                    <div class="inner-items"></div>
                                    <div class="slide-plot">
                                        <div class="banner__title heading heading--secondary heading--with-line text--reverse heading--sub-2">
                                            <div class="heading--multiline">
                                                <p>Migrate to Canada </p>
                                                <p class="text--natural text--italic">Practical Possibility for PR - Truck Drivers, Painters, Welders and more </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="slider-items">
                            <div class="slider-slides" style="">
                                <a href="<?php echo $this->webroot;?>universities/medicallist" class="banner banner--square">
                                    <div class="slider-inner" >
                                        <img src="<?php echo($this->webroot); ?>images/tiles/Doctor.jpg">
                                    </div>
                                    <div class="inner-items"></div>
                                    <div class="slide-plot">
                                        <div class="banner__title heading heading--secondary heading--with-line text--reverse heading--sub-2">
                                            <div class="heading--multiline">
                                                <p>Collegium Medicum Nicolaus Copernicus University </p>
                                                <p class="text--natural text--italic">World's Top Medical University for Best 24 INDIAN Students </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="slider-items">
                            <div class="slider-slides" style="">
                                <a href="<?php echo $this->webroot;?>universities/studyinpoland" class="banner banner--square">
                                    <div class="slider-inner" >
                                        <img src="<?php echo($this->webroot); ?>images/tiles/tab3.jpg">
                                    </div>
                                    <div class="inner-items"></div>
                                    <div class="slide-plot">
                                        <div class="banner__title heading heading--secondary heading--with-line text--reverse heading--sub-2">
                                            <div class="heading--multiline">
                                                <p>University of Lodz </p>
                                                <p class="text--natural text--italic">Top Ranking University in Central Europe For Arts, Science &   Management @ 2500 euro fee per year </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
         <!-- end banner content -->
        
        
        <!-- Begin page content -->
    <div class="custom"  ><div class="floaters"><a href="<?php echo $this->webroot; ?>bookappointments/bookappointment"><img src="<?php echo $this->webroot; ?>images/book_appointment.png" border="0" alt="Book-your-appointment" /></a><!-- <a href="/discover/events-and-university-visits/inhouse-fair.html"><img src="/templates/home_tpl/images/events.png" border="0" alt="Events" /></a>--></div> </div>
    

        <?php echo $this->fetch('content'); ?>
        
        <!-- end page content-->
        <!-- Begin footer content -->

        <footer class="footer full-color">
           <section id="bottom">
                <div class="section-inner">
                </div>
            </section>
            <div id="bottom-nav">
                <div class="container">
                    <div class="link-center">
                        <a href="#" id="back-to-top" class="btn btn-primary post-scroller-button post-scroller-up back-to-top" title="Back to top">
                            <i class="fa fa-angle-up"></i>
                        </a>
                    </div>
                    <div class="row footer-content">
                        <div class="copyright col-md-4">
                            Lifeplanner Studies & Oppertunities (P) Ltd. <br/>
                            2nd Floor, Thevarolil Building, &nbsp;<br/>
                            Near Darsana IELTS Academy, Sastri Road, Kottayam-1.<br/>
                            Email: info@lifeplanneruniversal.com <br/>Contact: +91 907 2222 911, +91 907 2222 933<br/><!--<a href="http://lifeplanneruniversal.com/">www.lifeplanneruniversal.com</a>-->
                        </div>
                        <div class="hiring col-md-4">
                        </div>
                        <nav class="col-md-4 footer-social">
                            <a href="<?php echo $this->webroot; ?>universities/contact_us" data-toggle="popover" title="we are hiring" >WE ARE HIRING</a>
                            <ul class="social-list">
                                <li>
                                    <a href="https://www.facebook.com/lifeplannerstudies" class="btn btn-default social-icon facebook" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/lifeplannerktm" class="btn btn-default social-icon twitter" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>    
                                    <a href="https://www.instagram.com/lifeplannerstudies/" class="btn btn-default social-icon instagram" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                <
                                <li>
                                    <a href="https://in.pinterest.com/lifeplannerstudies/" class="btn btn-default social-icon pinterest" target="_blank">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCAS3cdbFYwoxk1raYD5Mi4w?view_as=subscriber" class="btn btn-default social-icon youtube" target="_blank">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/lifeplanner/" class="btn btn-default social-icon linkedin" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        
                        <div class="copyright col-md-12">
                            <div class="border-right">
                                © <?php echo date("Y");?> Lifeplanner. All rights reserved. <a href="<?php echo $this->webroot; ?>colleges/privacypolicy">Privacy Policy</a>
                            </div>
                        </div>
                    </div><!--/row-->
                </div><!--/container-->
            </div>
            
        </footer>
        <!-- end footer content -->
 <!-- Javascript -->

        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.min.js"></script>

        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.easing.js"></script>
        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/parallax.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-waypoints.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.tweet.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-validate.js"></script> 

        <!-- Revolution Slider -->
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/slider.js"></script>
        

        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.cookie.js"></script> 
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/main.js"></script>
        <script src="//http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>﻿

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDWml9un8St2k_w9Ha2SJ2B1G90S9sXxo&sensor=false" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/map.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/scroll.js"></script>

    </div>
   

<!-- END JAVASCRIPTS -->
<script type="text/javascript">
$("video").prop('muted', true); //mute
</script>
    
</body>
</html>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
    jQuery(window).load(function(){
        jQuery(".hameid-loader-overlay").fadeOut();
   
    });

$('#overlay').modal('show');

setTimeout(function() {
    $('#overlay').modal('hide');
}, 25000);

</script>
<style>
    .popover-title {
        color:#000;
    }
    .popover-content {
         color:#000;
    }
   
   .tp-banner .revslider-initialised .tp-simpleresponsive{
    transform: none!important;
   }
   .tp-banner{
    /*background:url(<?php echo $this->webroot; ?>images/slides/1.jpg); */
   }
</style>

