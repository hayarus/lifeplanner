<?php $user = CakeSession::read("Auth.User"); //pr($user);
header('Content-Type: text/html; charset=ISO-8859-15');
//echo $this->webroot;exit;
if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('GMT');
}

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Life Planner');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $cakeDescription ?>:<?php echo $title_for_layout; ?></title>
	<?php
		/*echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');*/
		$this->Html->script('http://maps.google.com/maps/api/js?sensor=true', false);
	/*	echo $this->Html->meta($setting['Setting']['favicon'],$sitefilesPath.$setting['Setting']['favicon'],array('type' => 'icon'));*/
	?>

    <meta charset="utf-8"/>
    <title>Life Planner</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

  
 <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/responsive.css">
    
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/colors/color1.css" id="colors">
    
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/animate.css">

    <!-- Favicon and touch icons  -->
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="#" rel="shortcut icon">
</head>
<style>.floaters {
    position: fixed;
    right: 0;
    top: 30%;
    z-index: 99;
    width: 56px;
    text-align: right;
}
	.hameid-loader-overlay {
    width: 100%;
    height: 100%;
    background: url('https://techynab.com/lifeplanner/app/webroot/images/loader.gif') center no-repeat #FFF;
    z-index: 99999;
    position: fixed;
}
</style>
<noscript>
    <style>.hameid-loader-overlay { display: none; } </style>
</noscript>
<body class="header-sticky">
    <div class="hameid-loader-overlay"></div>
    <div class="boxed">
        <div class="menu-hover">
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->
        </div>
        <!--------------header menu------------>
         
        <!--header menu ends -->
        <!-- Header -->
        <header id="header" class="header">
            <div class="header-wrap">
                <div class="container">
                    <div class="header-wrap clearfix">
                        <div id="logo" class="logo">
                            <a href="http://techynab.com/lifeplanner/" rel="home">
                                <img src="<?php echo $this->webroot; ?>images/logo.png" alt="image">
                            </a>
                        </div><!-- /.logo -->


                        <div class="nav-wrap">

                            <?php 
                               echo $this->element('main-menu'); 
                             ?> 

                        </div><!-- /.nav-wrap -->
                    </div><!-- /.header-wrap -->
                </div><!-- /.container-->
            </div><!-- /.header-wrap-->
        </header><!-- /.header -->
        <!-- END PAGE HEADER-->
        <!-- Begin page content -->
        <div class="custom"  ><div class="floaters"><a href="http://techynab.com/lifeplanner/bookappointments/bookappointment"><img src="<?php echo $this->webroot; ?>images/book_appointment.png" border="0" alt="Book-your-appointment" /></a><!-- <a href="/discover/events-and-university-visits/inhouse-fair.html"><img src="/templates/home_tpl/images/events.png" border="0" alt="Events" /></a>--></div></div>
        
        <?php echo $this->fetch('content'); ?>
        <!-- end page content-->
        <!-- Begin footer content -->
        <footer class="footer full-color">
            <section id="bottom">
                <div class="section-inner">
                    <div class="container">
                        <div class="row normal-sidebar">
                            <div class="widget widget-text">
                                <div class="row">
                                   
                            <div class="col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">LAW BUSINESS</h2>
                                    <div class="menu-law-business-container">
                                        <ul id="menu-law-business" class="menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1280">
                                                <a href="#">Accounting Technologies</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1281">
                                                <a href="#">Business Administration</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1282">
                                                <a href="#">Business Law</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1283">
                                                <a href="#">Entrepreneurship</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1284">
                                                <a href="#">Tourism and Hospitality</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1285">
                                                <a href="#">Marketing</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1286">
                                                <a href="#">Public Relations</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">ENGINEERING</h2>
                                    <div class="menu-engineering-container">
                                        <ul id="menu-engineering" class="menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1287">
                                                <a href="#">Information Technology</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1288">
                                                <a href="#">Information System</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1289">
                                                <a href="#">Chemical Engineering</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1290">
                                                <a href="#">Electrical Engineering</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1291">
                                                <a href="#">Mechanical Engineering</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1292">
                                                <a href="#">Software Engineering</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1293">
                                                <a href="#">Applied Geology</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>  
                            </div>

                            <div class=" col-md-3  widget widget-nav-menu">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">HIGHER EDUCATION</h2>
                                    <div class="menu-higher-education-container">
                                        <ul id="menu-higher-education" class="menu">
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1294">
                                                <a href="#">Master Of Taxation</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1295">
                                                <a href="#">Master of IT</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1296">
                                                <a href="#">Master Of Accounting</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1297">
                                                <a href="#">MBA</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1298">
                                                <a href="#">Master of Geology</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1299">
                                                <a href="#">Diploma</a>
                                            </li>
                                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1300">
                                                <a href="#">Other Degrees</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-md-3 border widget widget-text">
                                <div class=" widget-inner">
                                    <h2 class="widget-title maincolor1">CONTACT</h2>
                                    <div class="textwidget">
                                        <p><!--Email: info@lifeplanneruniversal.com<br>Telephone: +91 481-655005,+91 80890 10107<br>
                                        Address:--> 2nd Floor, Thevarolil Building, <br>Near Darsana IELTS Academy,<br> Sastri Road, Kottayam-1.<br><a href="http://lifeplanneruniversal.com/">www.lifeplanneruniversal.com</a></p>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                </div>
                </div>
            </section>

            <div id="bottom-nav">
                <div class="container">
                    <div class="link-center">
                        <div class="line-under"></div>
                        <a class="flat-button go-top-v1 style1" href="#top">TOP</a>
                    </div>
                    <div class="row footer-content">
                        <div class="copyright col-md-6">
                            © 2018 Lifeplanner. All rights reserved.
                        </div>
                        <nav class="col-md-6 footer-social">
                            <ul class="social-list">
                                <li>
                                    <a href="#" class="btn btn-default social-icon">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-default social-icon">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>    
                                    <a href="#" class="btn btn-default social-icon">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-default social-icon">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div><!--/row-->
                </div><!--/container-->
            </div>
        </footer>
        <!-- end footer content -->
 <!-- Javascript -->
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.easing.js"></script>
        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/parallax.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.tweet.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-validate.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-waypoints.js"></script> 
        
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/gmap3.min.js"></script>
        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/main.js"></script>


    </div>

<!-- END JAVASCRIPTS -->
    
</body>
</html>

<script>
    jQuery(window).load(function(){
        jQuery(".hameid-loader-overlay").fadeOut(500);
     
    });
</script>