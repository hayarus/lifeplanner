<?php $user = CakeSession::read("Auth.User"); //pr($user);
header('Content-Type: text/html; charset=ISO-8859-15');
//if (substr_count($_SERVER[‘HTTP_ACCEPT_ENCODING’], ‘gzip’)){ ob_start(“ob_gzhandler”);} else { ob_start();}
//echo $this->webroot;exit;
if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('GMT');
}

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'Life Planner');
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $cakeDescription ?> | <?php echo $title_for_layout; ?></title>
    <?php
        /*echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');*/
        $this->Html->script('http://maps.google.com/maps/api/js?sensor=true', false);
    /*  echo $this->Html->meta($setting['Setting']['favicon'],$sitefilesPath.$setting['Setting']['favicon'],array('type' => 'icon'));*/
    ?>

    <meta charset="utf-8"/>
    <title>Life Planner</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- <meta name="google-site-verification" content="zmNR_Zy-nkPpNnz03K0C3D_HJmTWwHXuN06bH5yF6tE" /> -->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->

  
 <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/responsive.css">
    
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/colors/color1.css" id="colors">
    
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/animate.css">

    <!-- Favicon and touch icons  -->
    <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="#" rel="shortcut icon">
    <style>.floaters {
    position: fixed;
    right: 0;
    top: 30%;
    z-index: 99;
    width: 56px;
    text-align: right;
}</style>
    <style>
        .popup_msg{
        position:absolute;
        z-index:10;
        width:172px;
        height:102px;
        text-align:center;
        color:#FF0000;
        font: 14px Verdana, Arial, Helvetica, sans-serif;
        background:url(bg_image.gif) bottom right no-repeat;
        display:none;
        }
        .hameid-loader-overlay {
    width: 100%;
    height: 100%;
    background: url('https://lifeplanneruniversal.com/app/webroot/images/loader.gif') center no-repeat #FFF;
    z-index: 99999;
    position: fixed;
}
        </style>
<noscript>
    <style>.hameid-loader-overlay { display: none; } </style>
</noscript>
        <script src="<?php echo $this->webroot; ?>js/bootstrap-notify.min.js"></script>
        
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134534356-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134534356-1');
</script>

</head>
<body class="header-sticky">
    <div class="hameid-loader-overlay"></div>
    <div class="boxed">
        <div class="menu-hover">
            <div class="btn-menu">
                <span></span>
            </div><!-- //mobile menu button -->
        </div>
        <!--------------header menu------------>
   
        <!--header menu ends -->
        <!-- Header -->
        <header id="header" class="header">
            <div class="header-wrap">
                <div class="container">
                    <div class="header-wrap clearfix">
                        <div id="logo" class="logo">
                            <a href="<?php echo $this->webroot; ?>" rel="home">
                                <img src="<?php echo $this->webroot; ?>images/logo.png" alt="image">
                            </a>
                        </div><!-- /.logo -->


                        <div class="nav-wrap">
                            <div style='padding-bottom:10px;' ><?php echo $this->Session->flash(); ?></div>

                            <?php 
                               // echo $this->element('main-menu'); 
                             ?> 

                        </div><!-- /.nav-wrap -->
                    </div><!-- /.header-wrap -->
                </div><!-- /.container-->
            </div><!-- /.header-wrap-->
        </header><!-- /.header -->
        <!-- END PAGE HEADER-->
        <!-- Begin page content -->
        </div></div>
        <?php echo $this->fetch('content'); ?>
        <!-- end page content-->
        <!-- Begin footer content -->
        
        <footer class="footer full-color">
           <section id="bottom">
                <div class="section-inner">
                </div>
            </section>
            <div id="bottom-nav">
                <div class="container">
                    <div class="link-center">
                        <a href="#" id="back-to-top" class="btn btn-primary post-scroller-button post-scroller-up back-to-top" title="Back to top">
                            <i class="fa fa-angle-up"></i>
                        </a>
                    </div>
                    <div class="row footer-content">
                        <div class="copyright col-md-4">
                            Lifeplanner Studies & Oppertunities (P) Ltd. <br/>
                            2nd Floor, Thevarolil Building, &nbsp;<br/>
                            Near Darsana IELTS Academy, Sastri Road, Kottayam-1.<br/>
                            Email: info@lifeplanneruniversal.com <br/>Contact: +91 907 2222 911, +91 907 2222 933<br/><!--<a href="http://lifeplanneruniversal.com/">www.lifeplanneruniversal.com</a>-->
                        </div>
                        <div class="hiring col-md-4">
                        </div>
                        <nav class="col-md-4 footer-social">
                            <a href="#" data-toggle="popover" title="Send your CV to" data-content="career@lifeplanneruniversal.com" data-placement="top" >WE ARE HIRING</a>
                            <ul class="social-list">
                                <li>
                                    <a href="https://www.facebook.com/lifeplannerstudies" class="btn btn-default social-icon facebook" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/lifeplannerktm" class="btn btn-default social-icon twitter" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>    
                                    <a href="https://www.instagram.com/lifeplannerstudies/" class="btn btn-default social-icon instagram" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="https://in.pinterest.com/lifeplannerstudies/" class="btn btn-default social-icon pinterest" target="_blank">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCAS3cdbFYwoxk1raYD5Mi4w?view_as=subscriber" class="btn btn-default social-icon youtube" target="_blank">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/lifeplanner/" class="btn btn-default social-icon linkedin" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        
                        <div class="copyright col-md-12">
                            <div class="border-right">
                                © <?php echo date("Y");?> Lifeplanner. All rights reserved. <a href="<?php echo $this->webroot; ?>colleges/privacypolicy">Privacy Policy</a>
                            </div>
                        </div>
                    </div><!--/row-->
                </div><!--/container-->
            </div>        
        </footer>
        <!-- end footer content -->
 <!-- Javascript -->
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.easing.js"></script>
        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/owl.carousel.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/parallax.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.tweet.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.matchHeight-min.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-validate.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-waypoints.js"></script> 
        
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/main.js"></script>
        <script type="text/javascript" src="<?php echo $this->webroot; ?>js/scroll.js"></script>


    </div>

<!-- END JAVASCRIPTS -->
    
</body>
</html>

<script>
    jQuery(window).load(function(){
        jQuery(".hameid-loader-overlay").fadeOut();
        
        
    });
</script>
<style>
    .popover-title {
        color:#000;
    }
    .popover-content {
         color:#000;
    }
</style>