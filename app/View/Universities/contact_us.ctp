
<!---jquery Validation-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<!---jquery Validation End-->

<script type="text/javascript">
     
     $(document).ready(function(){
       var error1 = $('.alert-danger');
  $('#contactform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "data[name]" : {required : true},
    "data[email]" : {required : true,email:true},
    "data[subject]" : {required : true},
    "data[message]" : {required : true},
          
    },
    messages:{
      "data[name]" : {required :"Please enter name."},
      "data[email]" : {required :"Please enter email.",email:"Please enter valid email."},
      "data[subject]" : {required :"Please enter subject."},
      "data[message]" : {required :"Please enter message."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },
  });
});
</script>
<style>
    .help-block{
        color:#b20828!important;
        margin-top: -13px!important;
    }
</style>
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Contact Us</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="#">Home </a></li>
                                <li>\ Contact Us</li>
                            </ul>                   
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row --> 
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <!-- contact posts -->
        <section class="main-content contact-posts">
            <div class="container">
                <div class="row">
                    <div class="post-contact">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="contact-form">
                                     <div class="line-box"></div>
                                    <div class="row">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/chairmancontact.jpg" /></div>
                                        <div class="col-md-7"><strong>Sujin C Cherian</strong></br>Chairman</br><strong>Email</strong>: md@lifeplanneruniversal.com</div>
                                    </div>
                                     <div class="line-box"></div>
                                    <div class="row">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/01.jpg" /></div>
                                        <div class="col-md-7"><strong>Sona Babu</strong></br>CEO</br><strong>Email</strong>: director@lifeplanneruniversal.com</div>
                                    </div>
                                    <div class="line-box"></div>
                                     <div class="row one">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/consultant.jpg" /></div>
                                        <div class="col-md-7"><strong>Mary Ann Go</strong></br>Paralegal/ Regulated Canadian Immigration Consultant </br>
                                         ICCRC ID: R508947 </br>
                                        New Beginnings Legal Services</br>
                                        <strong>Address</strong>:196 Hawkwood Blvd. NW </br>
                                         Calgary, Alberta Canada, T3G2T1, Milton
                                        </div>
                                        </div>
                                     <!--<div class="line-box"></div>-->
                                    <!--<div class="row">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/04.jpg" /></div>
                                        <div class="col-md-7"><strong>Anish Mathew</strong></br>CFO / CAM</br><strong>Contact</strong>: +91 8089095055</br><strong>Email</strong>: cam@lifeplanneruniversal.com</div>
                                    </div>-->
                                    <div class="line-box"></div>
                                    <div class="row">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/jayesh.jpg" /></div>
                                        <div class="col-md-7"><strong>Jayesh Kumar P R</strong></br>Accounts & Administration Manager </br><strong>Contact</strong>: 8089010107  
                                        </br><strong>Email</strong>: accounts@lifeplanneruniversal.com </div>
                                    </div>
                                    <div class="line-box"></div>
                                    <div class="row">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/03.jpg" /></div>
                                        <div class="col-md-7"><strong>Anjana Satheesh</strong></br>Assistant Manager, Kottayam</br><strong>Contact</strong>: +91 9072222911</br><strong>Email</strong>: anjana@lifeplanneruniversal.com</div>
                                    </div>
                                     <div class="line-box"></div>
                                    <div class="row">
                                        <div class="col-md-3"><img src="<?php echo $this->webroot; ?>images/contact/02.jpg" /></div>
                                        <div class="col-md-7"><strong>Milu Elizabeth Cherian</strong></br>Assistant Manager Cochin </br><strong>Contact</strong>: +91 9072222933</br><strong>Email</strong>: milu@lifeplanneruniversal.com</div>
                                    </div>
                                    <div class="line-box"></div>

                                   </div>
                                </div><!-- col-md-6 -->
                                
                                <div class="col-md-4">
                                     <div class="contact-form">
                                        <div class="line-box"></div>
                                        <form action="<?php echo $this->webroot;?>universities/contact_us" method="post" id="contactform" class="comment-form" novalidate="">                         
                                            <fieldset class="style-1 full-name">
                                                <input type="text" id="name" placeholder="Your name" class="tb-my-input" name="data[name]" tabindex="1" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                            </fieldset>

                                            <fieldset class="style-1 email-address">
                                                <input type="email" id="email" placeholder="Your email" class="tb-my-input" name="data[email]" tabindex="2" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                            </fieldset> 

                                            <fieldset class="style-1 subject">
                                                <input type="text" id="subject" placeholder="Subject" class="tb-my-input" name="data[subject]" tabindex="2" value="" size="32" aria-required="true"style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                            </fieldset> 

                                            <fieldset class="message-form">
                                                <textarea id="comment-message" placeholder="Your Message" name="data[message]" rows="8" tabindex="4"style="border: 1px solid #eaeaea; background-color: #eaeaea;"></textarea>
                                            </fieldset>

                                            <div class="submit-wrap">
                                                <button class="flat-button button-style style-v1">Send <i class="fa fa-angle-right"></i></button>
                                            </div>             
                                       </form>
                                    </div><!-- contact-form -->
                                </div>
                                <div class="col-md-3">
                                    <div class="contact-form">
                                        <div class="line-box"></div>
                                        <div class="row" style="margin-left: 1px;">
                                            <strong>Corporate Office </strong></br>
                                            2nd Floor, Thevarolil Building,</br>
                                            Near Darshana Academy,</br>
                                            Sastri Road, Kottayam</br>
                                            Contact person : Anjana Satheesh </br>
                                        Contact no: +91 9072222911 </br>
                                        Mail id: info@lifeplanneruniversal.com</br>
                                        </div>
                                        <div class="line-box"></div>
                                        <div class="row" style="margin-left: 1px;">
                                            <strong>Life Planner Studies &amp; Opportunities, Ernakulum</strong></br>
                                            1st floor, Saniya plaza, </br>
                                            Mahakavi Bharathiyar road,</br>
                                            Oppo. K.S.R.T.C  Bus Stand,</br>
                                            Eranakulam</br>
                                            Contact Person: Milu Elizabeth Cherian</br>
                                        Contact no: +91 9072222933</br>
                                        Mail id: info@lifeplanneruniversal.com</br>
                                        </div>
                                        <div class="line-box"></div>
                                        <div class="row" style="margin-left: 1px;">
                                            <strong>Life Planner Studies &amp; Opportunities, CANADA</strong></br>
                                           New Beginnings Legal Services<br>
                                           196 Hawkwood Blvd. NW Calgary,<br>
                                           Alberta Canada, T3G2T1 <br> 
                                           Milton<br>
                                           Contact Person: Mary Ann Go <br>
                                           Paralegal/ Regulated Canadian Immigration Consultant <br>
                                           ICCRC ID: R508947<br>
                                        </div>
                                        <div class="line-box"></div>
                                        <div class="row" style="margin-left: 1px;">
                                            <strong>Life Planner Studies &amp; Opportunities, UAE</strong>
                                           </br>
                                        Contact no: +91 9946588525</br>
                                        Mail id: info@lifeplanneruniversal.com</br>
                                        </div>
                                        <div class="line-box"></div>
                                        <div class="row" style="margin-left: 1px;">
                                            <strong>Life Planner Studies &amp; Opportunities, MALAYSIA</strong></br>
                                            36 Tkt 1 Jalan Tasek Mutiara, <br>
                                            14120 Simpang Ampat, Pulau Pinang, <br>
                                            SIMPANG AMPAT, PENANG  MALAYSIA <br>
                                        <!-- Contact no: +91 8089030107</br> -->
                                        Whatsapp number : +60174741488 <br>
                                        Mail id: info@lifeplanneruniversal.com</br>
                                        </div>
                                        <div class="line-box"></div>
                                    </div>
                                
                            </div><!-- /row-->
                        </div><!-- /col-md-9 -->
                        <div class="col-md-12">
                        <div class="row">
                           <div class="col-md-12">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div id="map" style="width: 100%; height: 500px; "></div> 
                                    </div>
                                </div><!-- /container-fluid -->
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        


<style type="text/css">

    .one{
        border: 2px solid #d968d9;
        border-radius: 5px;
        padding: 10px 0px 5px;
        margin-right: 0px;
        margin-left: 0px;
        box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19) !important;
    }
     #map_canvas {
    height: 100%;
    width: 100%;
    margin: 0px;
    padding: 0px
}
</style>
