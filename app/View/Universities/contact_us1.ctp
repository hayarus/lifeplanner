         <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Contact Us</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="#">Home </a></li>
                                <li>\ </li>
                            </ul>                   
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <!-- contact posts -->
        <section class="main-content contact-posts">
            <div class="container">
                <div class="row">
                    <div class="post-contact">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="contact-form">
                                        <div class="line-box"></div>
                                        <form action="./contact/contact-process.php" method="post" id="contactform" class="comment-form" novalidate="">                            
                                            <fieldset class="style-1 full-name">
                                                <input type="text" id="name" placeholder="Your name" class="tb-my-input" name="author" tabindex="1" value="" size="32" aria-required="true">
                                            </fieldset>

                                            <fieldset class="style-1 email-address">
                                                <input type="email" id="email" placeholder="Your email" class="tb-my-input" name="email" tabindex="2" value="" size="32" aria-required="true">
                                            </fieldset> 

                                            <fieldset class="style-1 subject">
                                                <input type="text" id="subject" placeholder="Subject" class="tb-my-input" name="subject" tabindex="2" value="" size="32" aria-required="true">
                                            </fieldset> 

                                            <fieldset class="message-form">
                                                <textarea id="comment-message" placeholder="Your Message" name="comment" rows="8" tabindex="4"></textarea>
                                            </fieldset>

                                            <div class="submit-wrap">
                                                <button class="flat-button button-style style-v1">Send <i class="fa fa-angle-right"></i></button>
                                            </div>             
                                        </form>
                                    </div><!-- contact-form -->
                                </div><!-- col-md-6 -->

                                <div class="col-md-6">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div id="map" style="width: 100%; height: 500px; "></div> 
                                        </div>
                                    </div><!-- /container-fluid -->
                                </div>
                            </div><!-- /row-->
                        </div><!-- /col-md-9 -->

                        <div class="col-md-3">
                            <div class="sidebar"><div class="widget widget-nav-menu">
                                    <div class=" widget-inner">
                                        <h2 class="widget-title maincolor2">Main Menu</h2>
                                        <div class="menu-main-navigation-container">
                                            <ul id="menu-main-navigation-1" class="menu">
                                                <li class="menu-item">
                                                    <a href="#"><i class="fa fa-home"></i>  Home</a>
                                                    <ul class="sub-menu">
                                                        <li>
                                                            <a href="#">Homepage V1</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V2</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V3</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V4</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V5</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V6</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V7</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V8</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Homepage V9</a>
                                                        </li>
                                                    </ul>
                                                </li>

                                                <li class="menu-item"><a href="#"><i class="fa fa-bookmark"></i>  Events</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="#">Event List</a></li>
                                                        <li><a href="#">Event Grid</a></li>
                                                        <li><a href="#">Event Carousel</a></li>
                                                        <li><a href="#">Event Calendar</a></li>
                                                        <li><a href="#">Single Event V1</a></li>
                                                    </ul>
                                                </li>
                                                <li class="menu-item"><a href="#"><i class="fa fa-graduation-cap"></i>  Courses</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="#">Course List Classic</a></li>
                                                        <li><a href="#">Course Grid</a></li>
                                                        <li><a href="#">Course List In Table</a></li>
                                                        <li><a href="#">Single Course</a></li>
                                                        <li><a href="#">A LearnDash Course</a></li>
                                                    </ul>
                                                </li>
                                                <li class="sub-menu-left menu-item"><a href="#"><i class="fa fa-file-text"></i>  Pages</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="#">Blog</a></li>
                                                        <li><a href="#">Standard Post</a></li>
                                                        <li><a href="#">Video Post</a></li>
                                                        <li><a href="#">Gallery Post</a></li>
                                                        <li><a href="#">Members</a></li>
                                                        <li><a href="#">Member Details</a></li>
                                                        <li><a href="#">Portfolio</a></li>
                                                        <li><a href="#">Shop</a></li>
                                                        <li><a href="#">Single Product</a></li>
                                                        <li><a href="#">404</a></li>
                                                        <li><a href="#">Contact Us</a></li>
                                                        <li><a href="#">LearnDash Profile</a></li>
                                                    </ul>
                                                </li>
                                                <li class="multi-column menu-item"><a href="#"><i class="fa fa-bolt"></i>  Shortcodes</a>
                                                    <ul class="sub-menu">
                                                        <li><a href="#">Elements</a></li>
                                                        <li><a href="#">Tabs</a></li>
                                                        <li><a href="#">Tour</a></li>
                                                        <li><a href="#">Accordions</a></li>
                                                        <li><a href="#">Toggles</a></li>
                                                        <li><a href="#">Textbox</a></li>
                                                        <li><a href="#">Elements</a></li>
                                                        <li><a href="#">Buttons</a></li>
                                                        <li><a href="#">Headings</a></li>
                                                        <li><a href="#">Testimonials</a></li>
                                                        <li><a href="#">Countdown</a></li>
                                                        <li><a href="#">Media</a></li>
                                                        <li><a href="#">Contents</a></li>
                                                        <li><a href="#">Carousel – Course</a></li>
                                                        <li><a href="#">Carousel – Event</a></li>
                                                        <li><a href="#">Carousel – Post</a></li>
                                                        <li><a href="#">Grid – Course</a></li>
                                                        <li><a href="#">Grid – Event</a></li>
                                                        <li><a href="#">Contents</a></li>
                                                        <li><a href="#">Grid – Post</a></li>
                                                        <li><a href="#">Course List Classic</a></li>
                                                        <li><a href="#">Event list classic</a></li>
                                                        <li><a href="#">Post List Classic</a></li>
                                                        <li><a href="#">Course Table</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="widget widget-search">
                                    <h2 class="widget-title">COURSE SEARCH</h2>
                                    <form role="search" method="get" id="searchform" class="u-course-search-form" action="http://university.cactusthemes.com/">
                                        <div class="input-group">
                                            <div class="input-group-btn u-course-search-dropdown">
                                                <button class="btn btn-default dropdown-toggle u-course-search-dropdown-button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="button-label">All</span> <span class="fa fa-angle-down"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#" data-value="">All</a></li>
                                                    <li>
                                                        <a href="#" data-value="course-autumn-2014">Autumn Courses 2014</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-value="bachelor">Bachelor</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-value="course">Course</a>
                                                    </li>
                                                    <li>
                                                        <a href="#" data-value="study-2">Study</a>
                                                    </li>                
                                                </ul>
                                            </div><!-- /btn-group -->
                                                    
                                            <input type="text" value="" name="s" id="s" placeholder="SEARCH" class="form-control">
                                            <input type="hidden" name="post_type" value="u_course">
                                            <input type="hidden" name="u_course_cat" class="u-course-search-cat" value="">
                                            <span class="input-group-btn">
                                            <button type="submit" id="searchsubmit" class="btn btn-default u-course-search-submit"><i class="fa fa-search"></i></button>
                                            </span>
                                        </div>
                                    </form>
                                </div><!-- /widget-search -->

                                <div class="widget widget-posts">
                                    <div class="blog-box">
                                        <h2 class="widget-title">LATEST POST</h2>
                                        <ul class="recent-posts clearfix">
                                            <li>
                                                <div class="thumb item-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog/widget/1.jpg" alt="image">
                                                        <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                        <div class="thumbnail-hoverlay-cross"></div>
                                                    </a>
                                                </div>
                                                <div class="text">
                                                    <a href="#">Your Career Starts Here</a>
                                                    <p>JUNE 3, 2014</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="thumb item-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog/widget/2.jpg" alt="image">
                                                        <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                        <div class="thumbnail-hoverlay-cross"></div>
                                                    </a>
                                                </div>
                                                <div class="text">
                                                    <a href="#">Spark Of Genius</a>
                                                    <p>JUNE 3, 2014</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="thumb item-thumbnail">
                                                    <a href="#">
                                                        <img src="images/blog/widget/3.jpg" alt="image">
                                                        <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                        <div class="thumbnail-hoverlay-cross"></div>
                                                    </a>
                                                </div>
                                                <div class="text">
                                                    <a href="#">University Ranking</a>
                                                    <p>MAY 29, 2014</p>
                                                </div>
                                            </li>
                                        </ul><!-- /popular-news clearfix -->
                                    </div>
                                </div><!-- /widget-posts -->

                                <div class="widget widget-courses">
                                    <h2 class="widget-title">COURSES LIST</h2>
                                    <ul class="recent-posts clearfix">
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/4.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">Chemical Engineering</a>
                                                <p>DECEMBER 25, 2015</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/5.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">Information System</a>
                                                <p>MARCH 11, 2015</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/6.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">Work-Life Balance</a>
                                                <p>AUGUST 21, 2015</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/7.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">The Theory Of Sport</a>
                                                <p>OCTOBER 31, 2015</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/8.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">Bachelor Of Nursing</a>
                                                <p>JULY 24, 2015</p>
                                            </div>
                                        </li>
                                    </ul><!-- /popular-news clearfix -->
                                </div><!-- /widget-posts -->

                                <div class="widget widget-posts">
                                    <h2 class="widget-title">EVENTS LIST</h2>
                                    <ul class="recent-posts clearfix">
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/9.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">Chicago Architecture Foundation River Cruise</a>
                                                <p>JUNE 22, 2016</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/10.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">Spark Of Genius</a>
                                                <p>JUNE 3, 2014</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/11.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">University Ranking</a>
                                                <p>MAY 29, 2014</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/12.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">University Ranking</a>
                                                <p>MAY 29, 2014</p>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="thumb item-thumbnail">
                                                <a href="#">
                                                    <img src="images/blog/widget/13.jpg" alt="image">
                                                    <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                    <div class="thumbnail-hoverlay-cross"></div>
                                                </a>
                                            </div>
                                            <div class="text">
                                                <a href="#">University Ranking</a>
                                                <p>MAY 29, 2014</p>
                                            </div>
                                        </li>
                                    </ul><!-- /popular-news clearfix -->
                                </div><!-- /widget-posts -->
                            </div><!-- /col-md-9 -->
                        </div><!-- /col-md-3 -->
                    </div>
                </div>
            </div>
        </section>

        