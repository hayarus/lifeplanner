<?php $this->Html->addCrumb('University', '/admin/universities'); $paginationVariables = $this->Paginator->params();
App::uses('CakeText', 'Utility');
?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Universities'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                        <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Universities'), 'action' => 'index', 'admin' => true))); ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New University <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div>
                        <!--<button class="btn default"  onclick="window.location.href='<?php echo $this->webroot.'admin/Universities/export'; ?>'">Export to CSV <i class="fa fa-download"></i> </button>-->
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('category'); ?></th><th><?php echo $this->Paginator->sort('title'); ?></th><th><?php echo $this->Paginator->sort('country_id'); ?></th><!--<th><?php// echo $this->Paginator->sort('description'); ?></th>-->
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php  if(isset($universities) && sizeof($universities)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($universities as $university): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        							<td><?php echo h($categories[$university['University']['category']]); ?>&nbsp;</td>
		<td><?php echo h($university['University']['title']); ?>&nbsp;</td>
<td>
											<?php echo $this->Html->link($university['Country']['name'], array('controller' => 'countries', 'action' => 'view', $university['Country']['id'])); ?>
		</td>
<!--		<td><?php //echo h($university['University']['description'],CakeText::truncate($university['University']['description'], 255, array('html' => true))); ?>&nbsp;</td>-->
		
		<!--<td><?php echo h($university['University']['status']); ?>&nbsp;</td>-->
											<td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $university['University']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $university['University']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Universities'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[University][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
