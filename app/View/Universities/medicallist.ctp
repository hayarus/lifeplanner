<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>css/style-tab.css">
<style>
@media (min-width: 1200px){
.tab-links li {
    width: 40%!important;
}
}
.tab-links a {
    line-height: 40px;
    font-size: 14px;
}
.nav-tabs {
    border-bottom: 1px solid #ddd;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #555;
    cursor: default;
    background-color: #fff;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
}
</style> 
    
     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Study MBBS</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / List of Universities
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#poland"> POLAND </a></li>
                          <li><a data-toggle="tab" href="#moloda">MOLDOVA </a></li>
                          <li><a data-toggle="tab" href="#german">GERMANY </a></li>

                        </ul>
                        <br>
                        <div class="tab-content">
                        <div id="poland" class="tab-pane fade in active">
                            <div class="col-md-12"> 
                                <div class="row"> 
                                    <h4 class="tab-title">Why Choose Poland? </h4>
                                    <p> Poland is the 6th largest economy in the European Union and has the lowest tuition expenses among Schengen visa Countries. Poland is the only country in European Union that have not fell into recession and has showed steady economic growth. Poland became a part of European Union in the year 2004 which resulted in the emigration of a vast majority of Polish population into European states. In addition to this the decreasing birth rate has resulted in reduction of Poland’s population considerably. </p> 

                                    <h4 class="tab-title">Study MBBS in Poland! </h4>
                                    <p>What makes studying MBBS in Poland most attractive is that the tuition fees is less compared to several Indian Medical Colleges and IELTS is not required for admission procedure. It is a hub of top ranking universities for studying MBBS in Europe and opens the gateway for you to become a doctor in Europe! </p>

                                    <h4 class="tab-title">Benefits of MBBS from Poland? </h4>
                                    <p>You will be accepted for PG in Germany and all Schengen visa countries without separate visa requirements after completing MBBS in Poland. You will also be accepted for PG in USA and all English Speaking Countries. The country also has less restrictive immigration options for highly skilled. You will be getting a Temporary Resident Permit during the course after which your options for Permanent Residency are more.</p>

                                    <div class="line-space"></div>
                                </div> 
                            </div>

                            <?php if(!empty($polandcollegelist) ){
                            foreach($polandcollegelist as $polandcollege){
                        ?>
        
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad ">
                                        <div class="item-thumbnail">
                                            <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $polandcollege['University']['id'];?>">
                                                <?php if(!empty($polandcollege['University']['image'])){?>
                                                <img src="<?php echo $this->webroot.$universityImagePath.$polandcollege['University']['image'];?>" alt="image">
                                                <?php } else { ?>
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/list/1.jpg" alt="image">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h3 class="item-title">
                                                <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $polandcollege['University']['id'];?>" class="main-color-1-hover"><?php echo $polandcollege['University']['title'];?></a>
                                            </h3>

                                            <div class="price main-color-1"></div>
                                            <p><a href="<?php echo strip_tags($polandcollege['University']['description']);?>" target="_blank"><?php echo $polandcollege['University']['description'];?></a></p>
                                            <div class="event-time"></div>
                                            <!-- <div class="event-address"><?php echo $polandcollege['Country']['name'];?></div> -->
                                        </div>
                                        <div class="item-meta">
                                            <!-- <a class="flat-button" href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $polandcollege['University']['id'];?>">DETAILS  <i class="fa fa-angle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->

                       <?php } } ?>
                            <ul class="tab-links">
                                <li><a class="link" href="<?php echo $this->webroot;?>countries/europeancountries">KNOW MORE ON <i class="fa fa-angle-right"></i></a></li>
                                <li><a class="link" href="<?php echo $this->webroot;?>universities/studyinpoland">UNIVERSITY OF LODZ <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                         
                        <div id="moloda" class="tab-pane fade">
                            <div class="col-md-12"> 
                                <div class="row"> 
                                    <p></p>
                                    <h4 class="tab-title">Why Choose Moldova? </h4>
                                    <p>Moldova is  located in Eastern Europe. It is a small lower-middle- income European economy. Moldova  has made superb progress in reducing poverty and promoting inclusive growth since the early 2000's.The economy has expanded by an average of 5 percent a year.The official language of Moldova is Romanian, and Russian is also  widely spoken. </p> 

                                    <h4 class="tab-title">Study MBBS in MOLDOVA! </h4>
                                    <p>Modern Medicine is a western science invented and developed in Europe and in other western countries. Therefore, studying medicine in Europe itself will be the best approach for those  who want to excel in the field. In East Europe, Moldova offers opportunity for international students to  pursue degrees in Medicine and Pharmacology at Nicolae  Testemitanue State University. </p>

                                    <h4 class="tab-title">Benefits of MBBS from Moldova  </h4>
                                    <p> European medical education institutions are generally having world class standards. Students from other parts of the world get international  exposure too in Europe. Cost of living in Moldova is not high like other European countries. The study duration for medicine in Moldova is six years and the title is MD.</p>
                                    <div class="line-space"></div>
                                </div> 
                            </div>
                              <?php if(!empty($moldoacollegelist) ){
                            foreach($moldoacollegelist as $moldoacollege){
                        ?>
        
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                            <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $moldoacollege['University']['id'];?>">
                                                <?php if(!empty($moldoacollege['University']['image'])){?> 
                                                <img src="<?php echo $this->webroot.$universityImagePath.$moldoacollege['University']['image'];?>" alt="image">
                                                <?php } else { ?>
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/list/1.jpg" alt="image">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h3 class="item-title">
                                                <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $moldoacollege['University']['id'];?>" class="main-color-1-hover"><?php echo $moldoacollege['University']['title'];?></a>
                                            </h3>

                                            <div class="price main-color-1"></div>
                                            <p><a href="<?php echo strip_tags($moldoacollege['University']['description']);?>" target="_blank"><?php echo $moldoacollege['University']['description'];?></a></p>
                                            <div class="event-time"></div>
                                            <!-- <div class="event-address"><?php echo $moldoacollege['Country']['name'];?></div> -->
                                        </div>
                                        <div class="item-meta">
                                            <!-- <a class="flat-button" href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $moldoacollege['University']['id'];?>">DETAILS  <i class="fa fa-angle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->

                       <?php } } ?>
                            <ul class="tab-links">
                                <li><a class="link" href="<?php echo $this->webroot;?>countries/europeancountries">KNOW MORE ON <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </div>
                         <div id="german" class="tab-pane fade">
                            <div class="col-md-12"> 
                                <div class="row"> 
                                    <p>Germany awaits you for MBBS foreign study!</p>
                                    <h4 class="tab-title">THE SUCCESS STORY OF GERMANY </h4>
                                    <p> Germany is the most economically secure country in Europe and fifth largest economy in the world.Germany is far ahead of other countries in terms of educational institutions with international standards, efficient and skilled based education, high standard of living, economic growth, health care facilities and social service schemes. International students who wish to study in Germany have numerous opportunities to get access to free education, with the right kind of counselling and guidance. Another reason for you to choose Germany is that you can pursue studies while working. </p> 

                                    <h4 class="tab-title">MBBS IN GERMANY </h4>
                                    <p>Germany is a land of opportunity for international students with more than 100 Universities and colleges in the heart of Europe. Graduate Courses, Master’s Degree and Doctoral Degrees are provided at renowned Medical Schools all over Germany. You can become a Doctor through high quality education and even find job there itself. Most of the Universities we recommend are popular for renowned faculty members who are experts in their field, high quality training and practice. The skill based education system makes learning more effective. If you are an MBBS aspirant interested in specializing in a particular field of medicine then Germany is your right choice because of its educational system that focuses on specialization. </p>

                                    <h4 class="tab-title">KNOWLEDGE OF GERMAN LANGUAGE – A NECESSITY </h4>
                                    <p>German is the second most spoken language in Europe. German language proficiency is necessary for studying and working in Germany. Even though the language of English helps you find opportunities, professionals are required to know German. Most of the academic books will be in German hence in order to have access to the high quality of work produced in Germany, you need to learn the language. Even though there are German language schools available in Germany, it is better to learn the language in India itself, that too in an institute like ours which ensures individual attention to students. Learning German is easy if you have a good foundation in English language.</p>

                                    <h4 class="tab-title">ICPF-MED PLUS (FOR DOCTORS AND DENTISTS) </h4>
                                    <p><strong>Eligibility:</strong> Bachelor of Medicine (MBBS) or Medicine/ Surgery Diploma</p>
                                    <p><strong>Course Details:</strong> German language proficiency (C1 Level), Medical Fachsprache Preparation and Approbation Preparation (MAP) Module, B1 Level for those who have studied German at country of origin.</p>
                                    <p><strong>Tuition Fee:</strong> 8500 Euro</p>
                                    <p><strong>Livelihood expenses:</strong> the German Embassy should be convinced that 6480 Euros is deposited in the students Blocked Account for livelihood expenses which includes lodging, food and leisure activities.</p>
                                    <div class="line-space"></div>
                                </div> 
                            </div>
                              <?php if(!empty($germancollegelist) ){
                            foreach($germancollegelist as $germancollege){
                        ?>
        
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                            <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $germancollege['University']['id'];?>">
                                                <?php if(!empty($germancollege['University']['image'])){?>
                                                <img src="<?php echo $this->webroot.$universityImagePath.$germancollege['University']['image'];?>" alt="image">
                                                <?php } else { ?>
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/list/1.jpg" alt="image">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h3 class="item-title">
                                                <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $germancollege['University']['id'];?>" class="main-color-1-hover"><?php echo $germancollege['University']['title'];?></a>
                                            </h3>

                                            <div class="price main-color-1"></div>
                                            <p><a href="<?php echo strip_tags($germancollege['University']['description']);?>" target="_blank"><?php echo $germancollege['University']['description'];?></a></p>
                                            <div class="event-time"></div>
                                            <!-- <div class="event-address"><?php echo $germancollege['Country']['name'];?></div> -->
                                        </div>
                                        <div class="item-meta">
                                            <!-- <a class="flat-button" href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $germancollege['University']['id'];?>">DETAILS  <i class="fa fa-angle-right"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
                       <?php } } ?>

                            <ul class="tab-links">
                                <li><a class="link" href="<?php echo $this->webroot;?>countries/europeancountries">KNOW MORE ON <i class="fa fa-angle-right"></i></a></li>
                                <li><a class="link" href="<?php echo $this->webroot;?>universities/nursingingermany">NURSING IN GERMANY <i class="fa fa-angle-right"></i></a></li>
                            </ul>
                            
                        </div>
                         
                    </div>
                    </div><!-- /col-md-9 -->
<style type="text/css">

  .nav a{
    font-weight: bold;
    font-size: 16px;
  }
  .tab-pane{
    padding: 20px;
  }
  .tab-title , .tab-form-title {
    font-style: normal;
    font-size: 18px;
    color:#656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 10px;
    margin-bottom: 5px;
}
.line-space{
    margin-bottom: 15px;
    padding-top: 15px;
}

</style>