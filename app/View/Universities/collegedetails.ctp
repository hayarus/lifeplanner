<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Details of University</h2>
                        </div>
                        <div class="breadcrumbs v1">
                            <ul>
                                <li class="home"><a href="#">Home </a></li>
                                <li class="home"><a href="#">\ Details of University </a></li>
                            </ul>                   
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="main-content course-single">
            <div class="container">
                <div class="content-course">
                    <div class="row">
                        <div class="col-md-9">
                            <article class="post-course">
                                <div class="row">
                                    <?php if(!empty($collegedetails) ){
                        ?>
                                    <div class="col-md-4 col-sm-5">
                                        <div class="content-pad single-event-meta">
                                            <div class="item-thumbnail">
                                                <?php if(!empty($collegedetails['University']['image'])){?>
                                                <img src="<?php echo $this->webroot.$universityImagePath.$collegedetails['University']['image'];?>" alt="image">
                                                <?php } else { ?>
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/list/1.jpg" alt="image">
                                                <?php } ?>
                                            </div><!--/item-thumbnail-->
                                            
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="content-pad single-course-detail">
                                            <div class="course-detail">
                                                <div class="course-speaker">
                                                    <h4 class="text"></h4>
                                                    <div class="row">   
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="media professor">
                                                                
                                                                <div class="media-body">
                                                                    <h1 class="media-heading main-color-2" style="font-size: 30px!important;"><a class="main-color-2" href="#"><?php echo $collegedetails['University']['title'];?></a></h1>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                                   
                                                       
                                                    </div>
                                                </div><!--/course-speaker-->

                                                <div class="course-info row content-pad">
                                          
                                                </div><!--/course-info-->

                                                <div class="content-content">
                                                    <div class="content-dropcap v1">
                                                        <p><a href="<?php echo strip_tags($collegedetails['University']['description']);?>" target="_blank"><?php echo $collegedetails['University']['description'];?></a></p>
                                                        <p> <h4 style="font-size:20px!important;">Courses</h4></p>
                                                        <p>
                                                            <ul>
                                                                 <?php
                                                        foreach($coursearray as $course){
	 	                                       
                                                        ?>
                                                      
                                                        
                                                                <li><?php echo $course['Course']['name']; ?></li>
                                                                <?php } ?>
                                                            </ul>
                                                        </p>
                                                    </div>


                                                    <!--<div class="content-pad v1">
                                                        <ul class="list-inline social-light">
                                                            <li>
                                                                <a class="btn btn-default btn-lighter social-icon"><i class="fa fa-facebook"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="btn btn-default btn-lighter social-icon"><i class="fa fa-twitter"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="btn btn-default btn-lighter social-icon"><i class="fa fa-google-plus"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="btn btn-default btn-lighter social-icon"><i class="fa fa-pinterest"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="btn btn-default btn-lighter social-icon"><i class="fa fa-vk"></i></a>
                                                            </li>
                                                            <li>
                                                                <a class="btn btn-default btn-lighter social-icon"><i class="fa fa-envelope"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>-->

                                                    <div class="event-more-detail">
                                                        <h4>MORE DETAILS</h4>
                                                        <h6 class="text">Phone</h6>
                                                        <p><a href="#">8589040107 </a></p>
                                                        <h6 class="text">Email</h6>
                                                        <p><a href="#">info@lifeplanneruniversal.com</a></p>
                                                    </div>

                                                                                 
                                                </div><!--/content-content-->
                                            </div><!--/course-detail-->
                                        </div><!--/single-content-detail-->         
                                    </div>
                                    <?php } ?>
                                    
                                </div>
                            </article>
                        </div>

                    