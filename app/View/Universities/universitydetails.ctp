<script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery.min.js"></script>
<style>
    .left_container {
    width: 67%;
    float: left;
    background: url(../images/left_cont_bg.png) no-repeat right 0;
    min-height: 645px;
    padding: 15px 5% 0 0;
    margin-top: -15px;
}
 h1, h2 {
    font-size: 40px;
    line-height: 42px;
    font-weight: 300;
    margin: 0 0 20px;
    color: #4a4a4a;
}
.search_uni {
    margin-bottom: 20px;
}
.search_uni .search_uni_country {
    width: 47%;
    margin: 0 1%;
    display: inline-block;
    color: #034ea2;
}
.search_uni .search_uni_country select, .search_uni .search_uni_university select {
    border-radius: 4px;
    width: 95%;
    padding: 4px 2%;
    border: 1px solid #c2c2c2;
}
select {
    padding: 5px;
    border: 1px solid #c2c2c2;
    max-width: 100%;
    width: 100%;
    color: #666;
}
select {
    padding: 5px;
    border: 1px solid #c2c2c2;
    max-width: 100%;
    width: 100%;
    color: #666;
}
.search_uni .search_uni_university {
    width: 48%;
    margin: 0 1%;
    display: inline-block;
    color: #034ea2;
}
 h4 {
    font-weight: 600;
    margin: 0 0 5px;
}
.search_results.cb {
    position: relative;
}
.cb {
    display: block;
}
* {
    margin: 0;
    padding: 0;
}
.loading {
    display: block;
    margin: 0 auto;
    width: 160px;
    text-align: center;
    font-size: 0;
}
#search_result {
    clear: both;
}
.prdetail .prheading {
    font-size: 12px;
    font-style: italic;
}
.wrapper p {
    margin: 0 0 15px;
    text-align: justify;
}
.left_container p {
    margin-bottom: 10px;
}
strong, b {
    font-weight: 600;
}
#search_result table {
    border-top: 1px solid #ccc;
    border-left: 1px solid #ccc;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.wrapper > section {
    position: relative;
}
.wrapper {
    width: 1140px;
    margin: 0 auto;
}
form {
    display: block;
    margin-top: 0em;
}
.search_uni {
    margin-bottom: 20px;
}
#scholarshipform, .search_uni, .universityfilters form, #searchfilter {
    background: #f1f1f1;
    padding: 2%;
}
#searchfrm ~ .search_results .prdetail .prheading strong {
    font-size: 22px;
}

.prdetail .prheading strong {
    font-weight: 600;
    margin: 0;
    font-size: 16px;
    color: #034ea2;
    font-style: normal;
    display: block;
    margin-bottom: 20px;
}
strong, b {
    font-weight: 600;
}
select {
    -webkit-appearance: menulist;
    box-sizing: border-box;
    align-items: center;
    white-space: pre;
    -webkit-rtl-ordering: logical;
    color: black;
    background-color: white;
    cursor: default;
    border-width: 1px;
    border-style: solid;
    border-color: initial;
    border-image: initial;
}
</style>
 <script>
    $(document).ready(function(){
        $('#search_result').hide();
        $('#country_id').on('change', function() {//alert();
        var country_id = this.value;
        //alert(country_id);
        if(country_id!='') {
            $.ajax({
                        type: "GET",
                        url: "<?php echo $this->webroot?>universities/ajax_fetchuniversities/"+country_id,
                        success: function(html) { //alert(html);
                            if(html.indexOf("+")>=0) {
                                var ids=html.split("+");
                                window.location.replace("<?php echo $this->webroot;?>universities/universitydetails");
                            } else {
                                $("#tdstateDrp").html(html);
                            }                       
                        }
                });
            }
    });
        
        
    $('#tdstateDrp').on('change', function() {
       var university_id = $("#university_id").val();//alert(university_id);
        if(university_id!='') {
            $.ajax({
                        type: "GET",
                        url: "<?php echo $this->webroot?>universities/ajax_universitydetails/"+university_id,
                        success: function(html) { //alert(html);
                            if(html.indexOf("+")>=0) {
                                var ids=html.split("+");
                                window.location.replace("<?php echo $this->webroot;?>universities/universitydetails");
                            } else {
                                $("#tddistrictDrp").html(html);
                            }                       
                        }
                });
            }
    });
 });
    
</script>
 <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">University Information</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?>
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
        <section class="flat-row padding-v1">
            <div class="container">
<div class="left_container"> 
<h1>University Information</h1><form name="searchfrm" id="searchfrm" action="" method="POST">
    <div class="search_uni">
    <div class="search_uni_country"><h4>Countries</h4><div> 
    <select id="country_id" name="country_id" class="inputbox required ">
        <option value="">Select Country</option>
                		    <?php foreach($countries as $key=>$value){ ?>
	                			    <option value=<?php echo $key; ?>><?php echo $value;?></option>
                            <?php }  ?> 
    </select>
    </div>
    </div>
    <div class="search_uni_university"><h4>University</h4>
        <span id="tdstateDrp">
        <div id="unversity">
        <select id="university_id" name="university_id" class="inputbox required ">
        <option value="">Select University</option>
                		    <?php foreach($universities as $key=>$value){ ?>
	                			    <option value=<?php echo $key; ?>><?php echo $value;?></option>
                            <?php }  ?> 
        </select>
        </div>
        </span>
    </div>
</div></form>
    
<div class="search_results cb"> <span class="loading" style="display: none;"> <img src="https://www.thechopras.com/templates/home_tpl/images/loader.png" alt=""> Loading More Listings </span>
<span id="tddistrictDrp">
<div id="search_result">
</div></span></div></div>
