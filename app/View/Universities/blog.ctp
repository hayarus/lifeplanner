<script type="text/javascript">
var i=0;
$(document).ready(function(){
if(i==0)
$("#spopup").show("slow");else $("#spopup").hide("slow");
});
function closeSPopup(){
$('#spopup').hide('slow');
i=1;
}
</script>
<div class="page-title full-color">
    <div class="container">
        <div class="row">
            <div class="col-md-12">                    
                <div class="page-title-heading">
                    <h2 class="title">Blog</h2>
                </div>
                <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Blog
                                      
                </div>                  
            </div><!-- /.col-md-12 -->  
        </div><!-- /.row -->  
    </div><!-- /.container -->                      
</div><!-- /page-title -->
<!-- Blog posts -->
<section class="main-content blog-posts">
    <div class="container">
        <div class="row">
            <div class="post-wrap">
                <div class="col-md-12">
                    <div class="blog-listing">
                        <div class="blog-item">
                            <?php $m=0; foreach($blogs as $blog){ ?>
                            <div class="post-item blog-post-item">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="content-pad">
                                            <div class="blog-thumbnail">
                                                <div class="item-thumbnail-gallery">
                                                    <div class="item-thumbnail">
                                                         <a href="#">
                                                        <?php if (!empty($blog['Blogimage'][0]['name'])){ ?>
                                                            <img src="<?php echo $this->webroot.$blogImageSmall.$blog['Blogimage'][0]['name']; ?>" alt="image" style = "width: 100%">
                                                            <?php }else { ?>
                                                            <img src="<?php echo $this->webroot; ?>image_not_available.png" alt="image" style = "width: 100%">
                                                            <?php } ?>
                                                        <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                        <!-- <div class="thumbnail-hoverlay-cross"></div> -->
                                                        </a>
                                                    </div>
                                                </div>            
                                            </div><!--/blog-thumbnail-->
                                            <div class="thumbnail-overflow">
                                                <div class="comment-block main-color-1-bg dark-div">       
                                                    <a href="#">
                                                        <i class="fa fa-comment"></i><?php echo $numComments[$m]; ?>
                                                    </a>
                                                </div>
                                                <div class="date-block main-color-2-bg dark-div">
                                                    <div class="month"><?php echo gmdate("M", strtotime($blog['Blog']['created'])); ?></div>
                                                    <div class="day"><?php echo gmdate("d", strtotime($blog['Blog']['created'])); ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12">
                                        <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title"><a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $blog['Blog']['id']; ?>"><?php echo $blog['Blog']['title']; ?></a></h3> 
                                                <div class="item-meta blog-item-meta">
                                                    <span>By<span class="sep">|</span> </span>
                                                    <span><a href="#"><?php echo $blog['Blog']['author']; ?></a></span>
                                                </div>
                                                <div class="sub-title">
                                                    <h5>“<?php echo $blog['Blog']['subtitle']; ?>”</h5>
                                                </div>
                                                <div class="item-excerpt blog-item-excerpt">
                                                    <p><a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $blog['Blog']['id']; ?>" target="_blank"><?php echo substr($blog['Blog']['content'], 0,200);?>....</a> </p>
                                                </div>
                                                
                                                <div class="bottom-pad">
                                                    <div class="social-icons" >  
                                                        <ul class="list-inline social-light"> 
                                                            <li><a class="btn btn-default social-icon" href="https://www.shareaholic.com/api/share/?v=1&amp;apitype=1&amp;apikey=8943b7fd64cd8b1770ff5affa9a9437b&amp;service=7&amp;title=<?php echo str_replace('"','',$blog['Blog']['title']);?>&amp;link=https://www.lifeplanneruniversal.com/universities/blogdetails/<?php echo $blog['Blog']['id'];?>&amp;shortener=google&amp;template=Reading: ${title} (${short_link}) by @Lifeplanner&amp;source=Lifeplanneruniversal"><i class="fa fa-twitter"></i></a></li>

                                                            <li><a class="btn btn-default social-icon" href="https://www.shareaholic.com/api/share/?v=1&apitype=1&apikey=8943b7fd64cd8b1770ff5affa9a9437b&service=5&title=<?php echo str_replace('"','',$blog['Blog']['title']);?>&link=https://www.lifeplanneruniversal.com/universities/blogdetails/<?php echo $blog['Blog']['id'];?>&shortener=google&source=Lifeplanneruniversal&" target="_blank" target="_blank"><i class="fa fa-facebook"></i></a></li>

                                                            <li><a class="btn btn-default social-icon" href="https://www.shareaholic.com/api/share/?v=1&apitype=1&apikey=8943b7fd64cd8b1770ff5affa9a9437b&service=88&title=<?php echo str_replace('"','',$blog['Blog']['title']);?>&link=https://www.lifeplanneruniversal.com/universities/blogdetails/<?php echo $blog['Blog']['id'];?>&shortener=google&source=Lifeplanneruniversal&" target="_blank" target="_blank"><i class="fa fa-linkedin"></i></a></li> 
                                                        </ul> 
                                                </div>
                                                <div style="float: left;">
                                                    <div class="about-btn"> 
                                                        <a class="flat-button" href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $blog['Blog']['id']; ?>" style="float: right;border: solid 2px #333333;">Read More <i class="fa fa-angle-right"></i></a> 
                                                    </div>
                                            </div>
                                        </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /blog-item -->
                         <?php $m++; } ?>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="sidebar">
                        <div class="widget widget-posts">
                            <div class="blog-box">
                                <h2 class="widget-title">Popular Blogs</h2>
                                    <ul class="recent-posts clearfix">
                                        <?php foreach ($popularblogs as $key => $popularblog) { ?>
                                        <li>
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    <div class="row">
                                                        <div class="thumb item-thumbnail">
                                                         <a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $popularblog['Blog']['id']; ?>">
                                                            <?php if (!empty($popularblog['Blogimage'][0]['name'])){ ?>
                                                                <img src="<?php echo $this->webroot.$blogImageSmall.$popularblog['Blogimage'][0]['name']; ?>" alt="image">
                                                            <?php }else { ?>
                                                                <img src="<?php echo $this->webroot; ?>image_not_available.png" alt="image" >
                                                            <?php } ?>
                                                           
                                                            <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                        </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="row">
                                                        <div class="text" style="float: left;">
                                                            <a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $popularblog['Blog']['id']; ?>"><?php echo $popularblog['Blog']['title']; ?></a>
                                                            <p><?php echo gmdate("d-M-Y", strtotime($popularblog['Blog']['created'])); ?></p>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul><!-- /popular-news clearfix -->
                        </div><!-- /widget-posts -->
                    </div>

                    
                </div> 
            </div>
            </div>
        </div>
    </div>
</section>
<div class="col-md-8">
    <div class="col-md-12 subscribe">
        <div class="member-single-post">
            <div class="row">
                <div id="spopup" style="display: none;">
                <a style="position:absolute;top:0px;right:10px;color:#555;font-size:10px;font-weight:bold;" href="javascript:void(0);" onclick="return closeSPopup();"><img src="<?php echo $this->webroot;?>assets/img/portlet-remove-icon-white.png"></a>
                <div class="left_container"> 
                    <div class="form">
                        <div class="sub-title"><h5>Subscribe For Lastest Updations</h5></div>
                             <form method="post" id="subscribeform"> 
                                        <p class="form-error"></p> 
                                        <ul class="formcol2" style="list-style:none;">
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <div class="input-group"> 
                                                    <span class="input-group-addon"> <i class="fa fa-user" aria-hidden="true"></i></span> 
                                                    <input class="form-control" placeholder="Your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Name'" name="txt_name" id="txt_name" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" required="true"> 
                                                </div> 
                                                <span for="txt_name" class="help-block"></span> 
                                            </div> 
                                        </div>
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <div class="input-group"> 
                                                    <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> 
                                                    <input placeholder="Email ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID'" class="form-control" type="text" name="txt_email" id="txt_email" maxlength="100" data-validation="email" data-validation-error-msg=" " value="" required="true"> 
                                                </div> 
                                                <span for="txt_email" class="help-block"></span> 
                                            </div>
                                        </div> 
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <input name="btn_submit" type="submit" class="formbtn" value="Subscribe" style="background: none repeat scroll 0 0 #7ABA7A;border: 0 none;color: #fff;cursor: pointer;font-size: 15px;height: 35px; padding: 0 34px;margin-top: 0px;text-transform: uppercase;transition: all .5s ease 0;-webkit-transition: all .5s ease 0; margin-left: 0px;">
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style type="text/css">
.formbtn{
    width: 100%;
}

.item-title{
    text-transform: capitalize;
    font-weight: 400!important;
}
 .new-title {
    font-size: 18px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    line-height: 1.25;
    font-style: italic;
    /*text-transform: capitalize;*/
}

.social-icons{
    float:left;
}

.single-post{
    border-bottom: 2px solid #eaeaea;
     margin-bottom: 20px ;
}

.item-thumbnail{
    margin-bottom: 20px ;
}

.author,.created{
    width:50%;
    float:left;
}

.member-tax{
    border-top: 2px solid #eaeaea00!important;
    margin-top: 0px!important;
    padding-top: 0px!important;
}

.member-single-post .content-pad .item-content .small-text {
    font-size: 14px;
}

.sub-title{
   margin-bottom: 15px;
}

.sub-title h5{
    font-size: 16px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    font-style: italic;
}

.content{
    padding: 20px 0;
}

.bottom-pad{
    width: 100%;
}

.flat-button {
    display: inline-block;
    padding-top: 6px;
    font-size: 13px;
    line-height: 1.42857143;
    padding: 9px 20px;
    margin-right: 16px;
    transition: all .2s;
    border-radius: 3px;
    border: solid 2px #eaeaea;
}

.flat-button:hover {
    color: #fff;
    background-color: #666666;
    border-color: #666666;
}

.blog-item{
    padding: 20px 10px;
}

.side-title{
    text-transform: capitalize;
    font-weight: 500;

}

.blog-item .small-text{
    font-style: italic;
}

.blog-menu{
    padding-bottom: 20px;
}

.input-group-addon {
    border-radius: 4px;
    width: 40px!important;
    font-size: 12px!important;
    font-weight: normal!important;
    line-height: 1!important;
    color: #fff!important;
    text-align: center!important;
    background: #9400D3!important;
    border: 1px solid #9400D3!important;
}

.input-group-addon .fa {
    font-size: 22px;
}

.form-error {
    left: 0;
    position: absolute;
    top: -20px;
}

.help-block{
        color:#fff!important;
        margin-top: 0px!important;
        margin-bottom: 10px!important;
        height: 15px!important
    }

.left_container {
    /*padding: 15px 15px 0 0;*/
    -moz-box-shadow:inset 0 0 3px #fff;
    -webkit-box-shadow:inset 0 0 3px #fff;
    box-shadow:inner 0 0 3px #fff;
    border: 1px solid #e7e5e5;
    background: #000000c7;
    opacity: 1;
}

.form {
    display: block;
    padding: 20px;
} 

.input-group {
    width: 100%;
    position: relative;
    display: table;
    border-collapse: separate;
    /*margin-bottom: 45px;*/
}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: #000;
    opacity:1;
}

::-moz-placeholder { /* Firefox 19+ */
    color: #000;
    opacity:1;
}

:-ms-input-placeholder { /* IE 10+ */
    color: #000;
    opacity:1;
}

:-moz-placeholder { /* Firefox 18- */
    color: #000;
    opacity:1;
}

/** placeholder color change on error occurs **/    
.has-error input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    #a94442;
}

.has-error input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    color:    #a94442;
    opacity:  1;
}

.has-error input::-moz-placeholder { /* Mozilla Firefox 19+ */
    color:    #a94442;
    opacity:  1;
}

.has-error input:-ms-input-placeholder { /* Internet Explorer 10-11 */
    color:    #a94442;
}

.has-success .captcha_section {
    border-left: 1px solid #3C763D;
}

.formbtn {
    background: none repeat scroll 0 0 #f15a22;
    border: 0 none;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 46px;
    padding: 0 34px;
    margin-top: 5px;
    text-transform: uppercase;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}

.subscribe{
    margin-top: 10px;
}

.subscribe .sub-title h5{
    color:#fff;
}

#spopup{

    width: 300px;
    position:fixed;
    bottom:13px;
    right:2px;
    display:none;
    z-index:99;
}
.item-meta {
    font-size: 12px!important;
}
.blog-item-meta {
    margin-bottom: 20px!important;
}
</style>
<script type="text/javascript">
     
$(document).ready(function(){
    var error1 = $('.alert-danger'); 
    $('#subscribeform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "txt_name" : {required : true},
    "txt_email" : {required : true,email:true},
         
    },
    messages:{
     "txt_name" : {required : "Please enter your name"},
     "txt_email" : {required : "Please enter your email id.",email:"Please enter valid email id."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.input-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.input-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.input-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.input-group').removeClass('error');
    },
  });
});
</script>
<script src="<?php echo $this->webroot; ?>js/jquery-ui1.js"></script> 
<script src="<?php echo $this->webroot; ?>js/bootstrap.min1.js"></script> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
