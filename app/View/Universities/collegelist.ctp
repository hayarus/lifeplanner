     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">List of Universities</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / List of Universities
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                        
                        <?php if(!empty($collegelist) ){
                            foreach($collegelist as $college){
                        ?>
        
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                            <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $college['University']['id'];?>">
                                                <?php if(!empty($college['University']['image'])){?>
                                                <img src="<?php echo $this->webroot.$universityImagePath.$college['University']['image'];?>" alt="image">
                                                <?php } else { ?>
                                                <img src="<?php echo $this->webroot; ?>images/about/index1/event/list/1.jpg" alt="image">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h3 class="item-title">
                                                <a href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $college['University']['id'];?>" class="main-color-1-hover"><?php echo $college['University']['title'];?></a>
                                            </h3>

                                            <div class="price main-color-1"></div>
                                            <p><a href="<?php echo strip_tags($college['University']['description']);?>" target="_blank"><?php echo $college['University']['description'];?></a></p>
                                            <div class="event-time"></div>
                                            <div class="price main-color-1"><?php echo $college['Country']['name'];?></div>
                                        </div>
                                        <div class="item-meta">
                                            <a class="flat-button" href="<?php echo $this->webroot; ?>universities/collegedetails/<?php echo $college['University']['id'];?>">DETAILS  <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->

                       <?php } } ?>
                    </div><!-- /col-md-9 -->

                    