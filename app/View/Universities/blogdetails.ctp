<?php 
$seesionData =  $this->Session->read("sessionUserInfo");
?>

<script type="text/javascript">
var i=0; 
var error1 = $('.alert-danger');
$(document).ready(function(){
  
  if(i==0){
    $("#spopup").show("slow"); 
  } 
  else{ 
    $("#spopup").hide("slow"); 
  }

  
$('#subscribeform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "txt_name" : {required : true},
    "txt_email" : {required : true,email:true},
         
    },
    messages:{
     "txt_name" : {required : "Please enter your name"},
     "txt_email" : {required : "Please enter your email id.",email:"Please enter valid email id."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.input-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.input-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.input-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.input-group').removeClass('error');
    },
  });

  $('#blogloginform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "log_email" : {required : true, email:true},
    "log_password" :{required : true}, 
    },
    messages:{
     "log_email" : {required : "Please enter your email id.",email:"Please enter valid email id."},
     "log_password" :{required : "Please enter your password"},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },

    submitHandler: function (form) {
      var formData = $("#blogloginform").serialize();
            $.ajax({
                type: "POST",
                url: '<?= Router::Url(['controller' => 'Universities', 'action' => 'login']); ?>',
                data: formData,
                success: function (data) {
                     if(data){ 
                          window.location.reload(true);
                          $( ".flash" ).html('');
                      }else{  
                        $( ".flash" ).html('');
                        $( ".flash" ).html('<div class="alert alert-danger display-hide"><button data-dismiss="alert" class="close"></button>Login failed! Invalid username or password</div>'); 
                      }
                }
            });

      event.preventDefault();
      return false; // blocks redirect after submission via ajax
    },
  });
 $('#blogregistrationform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "reg_firstname" : {required : true},
    "reg_lastname" : {required : true},
    "reg_email" : {required : true,email:true},
    "reg_password" :{required : true},
         
    },
    messages:{
     "reg_firstname" : {required : "Please enter your first name"},
     "reg_lastname" : {required : "Please enter your last name"},
     "reg_email" : {required : "Please enter your email id.",email:"Please enter valid email id."},
     "reg_password" :{required : "Please enter your password"},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },

    submitHandler: function (form) {
      var formData = $("#blogregistrationform").serialize();
            $.ajax({
                type: "POST",
                url: '<?= Router::Url(['controller' => 'Universities', 'action' => 'registration']); ?>',
                data: formData,
                success: function(data) {
                    if(data){
                        $('#blogregistrationform')[0].reset();
                        $("#Registration").removeClass('active');
                        $("#Login").addClass('active');
                        $("#tab2").removeClass('active');
                        $("#tab1").addClass('active');
                    }else{
                         

                    }
                }
            });

      event.preventDefault();
      return false; // blocks redirect after submission via ajax
    },
  });

 $('#blogdiscussionform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
   
    "blog_content" : {required : true},
         
    },
    messages:{
     "blog_content" : {required : "Please enter your comment"},
     
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },
     submitHandler: function (form) {
      var formData = $("#blogdiscussionform").serialize();
            $.ajax({
                type: "POST",
                url: '<?= Router::Url(['controller' => 'Universities', 'action' => 'blogdiscussion']); ?>',
                data: formData,
                success: function(data) {
                    if(data){
                        $('#contactStatus').html(data);
                        $('#blogregistrationform')[0].reset();
                        $('#blogloginform')[0].reset();
                        $('#blogdiscussionform')[0].reset();
                        $('#myModal').modal('hide'); 
                    }
                }
            });
    },
  });

  	
  $('#write').click(function(){

    $('#myModal').modal('show'); 
    $('#blogregistrationform')[0].reset();
    $('#blogloginform')[0].reset();
    $('#blogdiscussionform')[0].reset();

    $('.form-group').each(function () { $(this).removeClass('has-success'); });
    $('.form-group').each(function () { $(this).removeClass('has-error'); });
    $('.form-group .help-block').each(function () { $(this).remove(); });
   

    var loginData = '<?php echo $seesionData['id']; ?>'; 
    if(loginData!= ""){
      $('#first').hide(); 
      $('#second').show(); 
      $('#blog_userid').val('<?php echo $this->Session->read("sessionUserInfo.id"); ?>'); 
    }else{
      $('#first').show(); 
      $('#second').hide(); 
    }
  });
     
    $('#reg_email').keyup(function(){
      var email = $(this).val(); 
        $.ajax({
          method:'POST', 
          url:'<?php echo Router::url(['controller' => 'Universities', 'action' => 'getemail']); ?>', 
          dataType: "text", 
          data:{email:email}, 
          success: function(data){
              if(data){
              $("#reg_email").closest('.form-group').addClass('has-error'); 
              $( ".result" ).html('<span id="emailspan" style="color:red">Someone Already Has That Email Id. Try another?</span>'); 
            }else{
              $("#reg_email").closest('.form-group').removeClass('has-error'); 
              $( ".result" ).html(''); 
            } 
          } 
        }); 
    });

$( "#txt_dob" ).datepicker();
});

function closeSPopup(){
  $('#spopup').hide('slow');
  i=1;
} 

</script>

     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Blog</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Blog
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
         <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div class="member-single">
                        <div class="col-md-8">
                        <?php foreach($blogs as $blog){ ?>
                        <div class="col-md-12 single-post">
                            <div class="member-single-post">
                                <div class="row"> 
                                    <div class="col-md-12">
                                        <div class="content-pad">
                                            <div class="item-content"> 
                                              <div class="item-thumbnail"> 
                                                <?php if (!empty($blog['Blogimage'][0]['name'])){ ?> 
                                                  <a href="#"><img src="<?php echo $this->webroot.$blogImageLarge.$blog['Blogimage'][0]['name']; ?>" alt="image" style = "width: 100%"></a> 
                                                <?php }else { ?> 
                                                  <a href="#"><img src="<?php echo $this->webroot; ?>image_not_available.png" alt="image"></a> 
                                                <?php } ?> 
                                              </div>
                                              <h3 class="item-title"><a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $blog['Blog']['id']; ?>"><?php echo $blog['Blog']['title']; ?></a></h3> 
                                              <div class="member-tax small-text"> 
                                                <div class="created"> 
                                                  <a  href="" class="cat-link"><?php echo gmdate("d-F-Y", strtotime($blog['Blog']['created'])); ?></a> 
                                                </div> 
                                                <div class="author"> 
                                                  <a  href="" class="cat-link"><?php echo $blog['Blog']['author']; ?></a> 
                                                </div> 
                                              </div> 
                                              <div class="sub-title"> 
                                                <h5>“<?php echo $blog['Blog']['subtitle']; ?>”</h5> 
                                              </div> 
                                              <div class="content"> 
                                                
                                                  <?php echo $blog['Blog']['content'];?>
                                                </div> 
                                                <?php 
                                                if(!empty($blogimages)) { ?> 
                                                  <h3 class="new-title">images</h3> 
                                                  <?php foreach($blogimages as $key => $blogimage){ ?> 
                                                    <div class="col-md-4"> 
                                                      <div class="row" style="float:left; margin-right:10px;text-align: center; margin-bottom: 10px;"> 
                                                        <a  data-toggle="modal" href="#viewimages<?php echo $blogimage['Blogimage']['id']; ?>"><img src="<?php echo $this->webroot.$blogImageSmall.$blogimage['Blogimage']['name']; ?>" ></a> 
                                                      </div> 
                                                      <div class="clearfix"></div>
                                                    </div> 
                                                    <!-- Model --> 
                                                    <div id="viewimages<?php echo $blogimage['Blogimage']['id']; ?>" class="modal fade modal-scroll" tabindex="-1" data-replace="true"> 
                                                      <div class="modal-dialog"> 
                                                        <div class="modal-content"> 
                                                          <div class="modal-header"> 
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button> 
                                                            <h4 class="modal-title">Gallery Images</h4> 
                                                          </div> 
                                                          <div class="modal-body"> 
                                                            <img src="<?php echo $this->webroot.$blogImageLarge.$blogimage['Blogimage']['name']; ?>"> 
                                                          </div> 
                                                          <div class="modal-footer"> 
                                                            <button type="button" data-dismiss="modal" class="btn">Close</button> 
                                                          </div> 
                                                        </div> 
                                                      </div> 
                                                    </div>
                                            <!-- model -->
                                            <?php } }?>


                                            <div style="clear: both;"></div>
                                            <br>

                                           <?php if(!empty($blogvideos)) { ?>
                                            <h3 class="new-title">Videos</h3>
                                            <?php foreach($blogvideos as $key => $blogvideo) { ?>
                                                <iframe width="225" height="225" src="//www.youtube.com/embed/<?php echo $blogvideo['Blogvideo']['youtube_link']; ?>"  frameborder="0" allowfullscreen></iframe> 

                                            <?php }} ?>

                                            </div>
                                        </div><!--/content-pad--> 
                                        <div class="bottom-pad">
                                            <div class="social-icons"> 
                                                <ul class="list-inline social-light"> 
                                                    <li><a class="btn btn-default social-icon" href="https://www.shareaholic.com/api/share/?v=1&amp;apitype=1&amp;apikey=8943b7fd64cd8b1770ff5affa9a9437b&amp;service=7&amp;title=<?php echo str_replace('"','',$blog['Blog']['title']);?>&amp;link=https://www.lifeplanneruniversal.com/universities/blogdetails/<?php echo $blog['Blog']['id'];?>&amp;shortener=google&amp;template=Reading: ${title} (${short_link}) by @Lifeplanner&amp;source=Lifeplanneruniversal"><i class="fa fa-twitter"></i></a></li>

                                                    <li><a class="btn btn-default social-icon" href="https://www.shareaholic.com/api/share/?v=1&apitype=1&apikey=8943b7fd64cd8b1770ff5affa9a9437b&service=5&title=<?php echo str_replace('"','',$blog['Blog']['title']);?>&link=https://www.lifeplanneruniversal.com/universities/blogdetails/<?php echo $blog['Blog']['id'];?>&shortener=google&source=Lifeplanneruniversal&" target="_blank" target="_blank"><i class="fa fa-facebook"></i></a></li>

                                                    <li><a class="btn btn-default social-icon" href="https://www.shareaholic.com/api/share/?v=1&apitype=1&apikey=8943b7fd64cd8b1770ff5affa9a9437b&service=88&title=<?php echo str_replace('"','',$blog['Blog']['title']);?>&link=https://www.lifeplanneruniversal.com/universities/blogdetails/<?php echo $blog['Blog']['id'];?>&shortener=google&source=Lifeplanneruniversal&" target="_blank" target="_blank"><i class="fa fa-linkedin"></i></a></li>

                                                    <!-- <li><a class="btn btn-default social-icon" href="http://www.linkedin.com/shareArticle?mini=true&url=[http://dakshinkalyanaphotography.com/pages/photo_gallery]"><i class="fa fa-linkedin"></i></a></li>--> 
                                                </ul> 
                                            </div>
                                            <div >
                                                <div class="about-btn"> 
                                                    <div class="content-content">
                                                         <div class="course-cta">
                                                            <a class="flat-button" data-toggle="modal" href="#" id="write">
                                                             <?php 
                                                             if(!empty($seesionData)){ echo "ADD YOUR COMMENT"; }else{ echo "LOGIN TO WRITE COMMENT"; }
                                                             ?>
                                                            </a>
                                                         </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div><!--/col-md-12-->
                                </div><!--/member-single-post-->
                            </div>
                        </div><!-- /col-md-8 -->
                        <?php } ?>
                        
                        
                            <div class="flat-project-post">
                            <div class="col-md-12">
                                <!-- <div class="simple-navigation project-navigation">
                                    <div class="row">
                                        <div class="simple-navigation-item col-md-6 col-sm-6 col-xs-6">
                                            <a href="#" rel="prev"><i class="fa fa-angle-left"></i>Previous Project</a>
                                        </div>

                                        <div class="simple-navigation-item col-md-6 col-sm-6 col-xs-6 text-right">
                                            <a href="#" rel="next"><i class="fa fa-angle-right pull-right"></i>Next Project</a>
                                        </div>
                                    </div>
                                </div>
 -->
                                <h2 class="project-title">Comments</h2>
                                <div class="project-content-text">
                                    <!-- <p>Let discuss about the blog</p> -->
                                </div>
                                <div id="contactStatus">
                                <?php foreach($blogdiscussions as $blogdiscussion){ ?>
                                      <div class="event-listing event-listing-classic">
                                          <article class="post-item row event-classic-item">
                                              <div class="col-md-12 col-sm-4">
                                                  <div class="content-pad">
                                                      <div class="item-content">
                                                          <div class="price main-color-1"><?php echo $blogdiscussion['User']['first_name'];?></div>

                                                          <p><?php echo $blogdiscussion['Blogdiscussion']['content']; ?></p>
                                                          
                                                      </div>
                                                  </div>
                                              </div>
                                          </article>
                                      </div>
                                  <?php } ?>
                                  </div>
                                
                            </div><!-- /col-md-12 -->
                        </div>
                        </div>
                        <div class="col-md-3">
                    <div class="sidebar">
                        <div class="widget widget-posts">
                            <div class="blog-box">
                                <h2 class="widget-title">Popular Blogs</h2>
                                    <ul class="recent-posts clearfix">
                                        <?php foreach ($popularblogs as $key => $popularblog) { ?>
                                        <li>
                                            <div class="col-md-12">
                                                <div class="col-md-5">
                                                    <div class="row">
                                                        <div class="thumb item-thumbnail">
                                                         <a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $popularblog['Blog']['id']; ?>">
                                                            <?php if (!empty($popularblog['Blogimage'][0]['name'])){ ?>
                                                                <img src="<?php echo $this->webroot.$blogImageSmall.$popularblog['Blogimage'][0]['name']; ?>" alt="image">
                                                            <?php }else { ?>
                                                                <img src="<?php echo $this->webroot; ?>image_not_available.png" alt="image" >
                                                            <?php } ?>
                                                           
                                                            <div class="thumbnail-hoverlay main-color-1-bg"></div>
                                                        </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="row">
                                                        <div class="text" style="float: left;">
                                                            <a href="<?php echo $this->webroot;?>universities/blogdetails/<?php echo $popularblog['Blog']['id']; ?>"><?php echo $popularblog['Blog']['title']; ?></a>
                                                            <p><?php echo gmdate("d-M-Y", strtotime($popularblog['Blog']['created'])); ?></p>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul><!-- /popular-news clearfix -->
                        </div><!-- /widget-posts -->
                    </div>

                    <div class="widget widget-posts">
                        <div class="blog-box">
                            <h2 class="widget-title">Categories</h2>
                            <ul class="recent-posts clearfix">
                            <?php $i=0;  foreach ($blogcategories as $key => $blogcategory){ ?>
                                <li>
                                    <div class="col-md-12">
                                        <div class="col-md-8">
                                            <div class="thumb ">
                                                <a href="#">
                                                <h3 class="side-title"><i class="fa fa-hand-o-right"></i> <?php echo $blogcategory['Blogcategory']['name']; ?></h3></a> 
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="text">
                                             <a href="#"><?php echo $totalblogcategories[$i]; $i++; ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div> 
              </div> 
          </div> 
      </div> 
</section>
<div class="col-md-8">
    <div class="col-md-12 subscribe">
        <div class="member-single-post">
            <div class="row">
                <div id="spopup" style="display: none;">
                <a style="position:absolute;top:0px;right:10px;color:#555;font-size:10px;font-weight:bold;" href="javascript:void(0);" onclick="return closeSPopup();"><img src="<?php echo $this->webroot;?>assets/img/portlet-remove-icon-white.png"></a>
                <div class="left_container"> 
                    <div class="form">
                        <div class="sub-title"><h5>Subscribe For Lastest Updations</h5></div>
                             <form method="post" id="subscribeform"> 
                                        <p class="form-error"></p> 
                                        <ul class="formcol2" style="list-style:none;">
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <div class="input-group"> 
                                                    <span class="input-group-addon"> <i class="fa fa-user" aria-hidden="true"></i></span> 
                                                    <input class="form-control" placeholder="Your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Name'" name="txt_name" id="txt_name" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" required="true"> 
                                                </div> 
                                                <span for="txt_name" class="help-block"></span> 
                                            </div> 
                                        </div>
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <div class="input-group"> 
                                                    <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> 
                                                    <input placeholder="Email ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID'" class="form-control" type="text" name="txt_email" id="txt_email" maxlength="100" data-validation="email" data-validation-error-msg=" " value="" required="true"> 
                                                </div> 
                                                <span for="txt_email" class="help-block"></span> 
                                            </div>
                                        </div> 
                                        <div class="row"> 
                                            <div class="col-md-12"> 
                                                <input name="btn_submit" type="submit" class="formbtn" value="Subscribe" style="background: none repeat scroll 0 0 #7ABA7A;border: 0 none;color: #fff;cursor: pointer;font-size: 15px;height: 35px; padding: 0 34px;margin-top: 0px;text-transform: uppercase;transition: all .5s ease 0;-webkit-transition: all .5s ease 0; margin-left: 0px;">
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Definitions (tabbed over for <pre>) -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"> 
    <div class="modal-dialog" role="document"> 
        <div class="modal-content"> 
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> 
                <!-- <h4 class="modal-title" id="myModalLabel">Modal title</h4>  -->
            </div> 
            <div class="modal-body"> 
                <!-- Login/Registration -->
                <div id="first" >
                    <ul class="top-tabs"> 
                        <li id="tab1" class="active"><a href="#Login" data-toggle="tab" >Login</a></li> 
                        <li id="tab2"><a href="#Registration" data-toggle="tab">Registration</a></li> 
                    </ul>

                    <div class="tab-content"> 
                        <div class="tab-pane active" id="Login"> 
                            <form role="form" class="form-horizontal" id="blogloginform" method="post" > 
                                
                                    <div class="flash"></div>
                               
                                <div class="form-group"> 
                                    <label for="email" class="col-sm-2 control-label"> Email</label> 
                                    <div class="col-sm-10"> 
                                        <input type="email" class="form-control" id="log_email" name="log_email" placeholder="Email" /> 
                                    </div> 
                                </div> 
                                <div class="form-group"> 
                                    <label for="exampleInputPassword1" class="col-sm-2 control-label"> Password</label> 
                                    <div class="col-sm-10"> 
                                        <input type="password" class="form-control" id="log_password" name="log_password" placeholder="Password" /> 
                                    </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-md-2"></div> 
                                        <div class="col-md-10"> 
                                            <button type="submit" class="col-md-12 form-submit" id="formloginbutton"> Submit</button>
                                        </div> 
                                    </div> 
                                </form> 
                            </div>
                            <div class="tab-pane" id="Registration"> 
                                <form role="form" class="form-horizontal" id="blogregistrationform"> 
                                    <div class="form-group"> 
                                        <label for="email" class="col-sm-2 control-label"> First Name</label> 
                                        <div class="col-sm-10"> 
                                            <input type="text" class="form-control" id="reg_firstname" name="reg_firstname" placeholder="Your first name" /> 
                                        </div> 
                                    </div>
                                    <div class="form-group"> 
                                        <label for="email" class="col-sm-2 control-label"> Last Name</label> 
                                        <div class="col-sm-10"> 
                                            <input type="text" class="form-control" id="reg_lastname" name="reg_lastname" placeholder="Your last name" /> 
                                        </div> 
                                    </div> 
                                    <div class="form-group"> 
                                        <label for="email" class="col-sm-2 control-label"> Email</label> 
                                        <div class="col-sm-10"> 
                                            <input type="email" class="form-control" id="reg_email" name="reg_email" placeholder="Your email" /> 
                                            <div class="result"></div>
                                        </div> 
                                    </div>  
                                    <div class="form-group"> 
                                        <label for="password" class="col-sm-2 control-label"> Password</label> 
                                        <div class="col-sm-10"> 
                                            <input type="password" class="form-control" id="password" name="reg_password" placeholder="Enter a password" /> 
                                        </div> 
                                    </div> 
                                    <div class="row"> 
                                        <div class="col-sm-2"> 
                                        </div> 
                                        <div class="col-sm-10"> 
                                            <button type="submit" class="col-md-12 form-submit" id="formsubmitbutton"> Sign Up</button>
                                            <!-- <button type="button" class="btn btn-default btn-sm"> Cancel</button>  -->
                                        </div> 
                                    </div> 
                                </form> 
                            </div>
                        </div>
                    </div>
                    <div id="second">
                        <div class="heading"> 
                            <h3 class="title"><a href="blog-single.html" class="main-color-1-hover">Add your comment</a></h3>
                        </div>
                            <?php  foreach ($blogs as $key => $blog) {
                                $blog_id=$blog['Blog']['id'];
                            } ?>
                        <form role="form" class="form-horizontal" id="blogdiscussionform" method="post"> 
                            <div class="form-group"> 
                                <label for="blog_userid" class="col-sm-2 control-label"></label> 
                                <div class="col-sm-10"> 
                                    <input type="hidden" class="form-control" id="blog_userid" name="blog_userid" placeholder="" /> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="blog_userid" class="col-sm-2 control-label"></label> 
                                <div class="col-sm-10"> 
                                    <input type="hidden" class="form-control" id="blog_blogid" name="blog_blogid" placeholder="" value="<?php echo $blog_id; ?>" /> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="blog_name" class="col-sm-2 control-label">Name</label> 
                                <div class="col-sm-10"> 
                                    <input type="text" class="form-control" id="blog_name" name="blog_name" placeholder="Your first name" value="<?php echo $seesionData['first_name']; ?>" readonly /> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="blog_name" class="col-sm-2 control-label">Comment</label> 
                                <div class="col-sm-10"> 
                                    <textarea class="form-control" id="blog_content" name="blog_content" placeholder="Your comment"></textarea>
                                </div> 
                            </div>
                            <div class="row"> 
                                <div class="col-sm-2"> 
                                </div> 
                                <div class="col-sm-10"> 
                                    <button type="submit" class="col-md-12 form-submit" id="blogdiscussionformbutton" > Submit</button> 
                                </div> 
                            </div>
                        </form>
                    </div>
                </div> 
            <!-- <div class="modal-footer"> 
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                <button type="button" class="btn btn-primary">Save changes</button> 
            </div> --> 
        </div> 
    </div> 
</div>

<style type="text/css">
.modal-header{
  border-bottom: 1px solid #e5e5e500;
}
.tab-content > .active {
    padding-top: 6em!important;
}
.top-tabs li {
    width: 50%;
    float: left;
    text-align: center;
    list-style: none;
}

.top-tabs li:first-child a {
    border-radius: .25em 0 0 0;
}
 .top-tabs > li > a {
    display: block;
    width: 100%;
    height: 50px;
    line-height: 50px;
    background: #e1e1e1;
    color: #809191;
}
.top-tabs> li.active > a, .top-tabs > li.active > a:hover, .top-tabs > li.active > a:focus{
   background: #FFF;
    color: #505260; 
}
.formbtn{
  width: 100%;
}
.form-submit {
    padding: 4px 0;
    cursor: pointer;
    background: #4ebe51;
    color: #FFF;
    height: 3em;
    text-transform: uppercase;
    border: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    -ms-appearance: none;
    -o-appearance: none;
    appearance: none;
    width: 100%;
}
@media only screen and (min-width: 600px){
.top-tabs a {
    height: 70px;
    line-height: 70px;
}
}
.popup .top-tabs a {
    display: block;
    width: 100%;
    height: 50px;
    line-height: 50px;
    background: #e1e1e1;
    color: #809191;
}
/**/
.new-title {
    font-size: 19px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    line-height: 1.25;
    text-transform: capitalize;
}
.social-title {
    font-size: 16px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    line-height: 1.25;
    font-style: italic;
    
}
.item-title{
    text-transform: capitalize;
}
.social-icons{
    float:left;
}

 .item-title{
    text-transform: capitalize;
}
 .new-title {
    font-size: 18px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    line-height: 1.25;
    font-style: italic;
    /*text-transform: capitalize;*/
}
.social-icons{
    float:left;
}
.single-post{
    border-bottom: 0px solid #eaeaea;
     margin-bottom: 20px ;
}
.item-thumbnail{
    margin-bottom: 20px ;
}
.author,.created{
    width:50%;
    float:left;
}
.member-tax{
    border-top: 2px solid #eaeaea00!important;
    margin-top: 0px!important;
    padding-top: 0px!important;
}
.member-single-post .content-pad .item-content .small-text {
    font-size: 14px;
}
.sub-title{

    padding-top: 20px;

}
.sub-title h5{
    font-size: 16px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    font-style: italic;
}
.content{
    padding: 20px 0;
}
.bottom-pad{
    width: 100%;
}
.flat-button {
    display: inline-block;
    padding-top: 6px;
    font-size: 13px;
    line-height: 1.42857143;
    padding: 9px 20px;
    margin-right: 16px;
    transition: all .2s;
    border-radius: 3px;
    border: solid 2px #872b8e;
    background: #872b8e;
    color: #fff;
    font-weight: 500;
}
.flat-button:hover {
    color: #fff;
    background-color: #7aba79;
    border-color: #7aba79;
}
.blog-item{
    padding: 20px 10px;
}
.side-title{
    text-transform: capitalize;
    font-weight: 500;

}
.blog-item .small-text{
    font-style: italic;
}
.blog-menu{
    padding-bottom: 20px;
}

.input-group-addon {
    border-radius: 4px;
    width: 40px!important;
    font-size: 12px!important;
    font-weight: normal!important;
    line-height: 1!important;
    color: #fff!important;
    text-align: center!important;
    background: #9400D3!important;
    border: 1px solid #9400D3!important;
}

.input-group-addon .fa {
    font-size: 22px;
}

.form-error {
    left: 0;
    position: absolute;
    top: -20px;
}

.help-block{
        color:#fff!important;
        margin-top: 0px!important;
        margin-bottom: 10px!important;
        height: 15px!important
    }
.form-group .help-block {
    color: #a94442!important;
    margin-top: -20px!important;
}
.left_container {
    /*padding: 15px 15px 0 0;*/
    -moz-box-shadow:inset 0 0 3px #fff;
    -webkit-box-shadow:inset 0 0 3px #fff;
    box-shadow:inner 0 0 3px #fff;
    border: 1px solid #e7e5e5;
    background: #000000c7;
    opacity: 1;
}

.form {
    display: block;
    padding: 20px;
} 

.input-group {
    width: 100%;
    position: relative;
    display: table;
    border-collapse: separate;
    /*margin-bottom: 45px;*/
}
.single-course-detail .content-content{
    border:none!important;
    padding-top: 0px!important;
    margin-top: 0px!important;
}
.single-course-detail .content-content .course-cta a{
    padding: 15px 40px!important;
}
.content-align{
    padding-left: -15px!important;
    padding-right: -15px!important;
}
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
    color: #000;
    opacity:1;
}

::-moz-placeholder { /* Firefox 19+ */
    color: #000;
    opacity:1;
}

:-ms-input-placeholder { /* IE 10+ */
    color: #000;
    opacity:1;
}

:-moz-placeholder { /* Firefox 18- */
    color: #000;
    opacity:1;
}

/** placeholder color change on error occurs **/    
.has-error input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
    color:    #a94442;
}

.has-error input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
    color:    #a94442;
    opacity:  1;
}

.has-error input::-moz-placeholder { /* Mozilla Firefox 19+ */
    color:    #a94442;
    opacity:  1;
}

.has-error input:-ms-input-placeholder { /* Internet Explorer 10-11 */
    color:    #a94442;
}

.has-success .captcha_section {
    border-left: 1px solid #3C763D;
}
.formbtn {
    background: none repeat scroll 0 0 #f15a22;
    border: 0 none;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 46px;
    padding: 0 34px;
    margin-top: 5px;
    text-transform: uppercase;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}

.subscribe{
    margin-top: 10px;
}

.subscribe .sub-title h5{
    color:#fff;
}

#spopup{

    width: 300px;
    position:fixed;
    bottom:13px;
    right:2px;
    display:none;
    z-index:99;
}
.modal.fade.in {
    top: 15%!important;
}
.modal-open[style] {
padding-right: 0px !important;
}
body:not(.modal-open){
  padding-right: 0px !important;
}
.flash{
    text-align: center;
    margin: 0px auto;
}
.heading .title{
    font-size: 21px;
    font-weight: 400;
    color: #343434;
    margin-bottom: 10px;
    line-height: 1.25;
}
.flat-project-post .project-content-text {
    border-bottom: solid 1px #eaeaea00;
    margin-bottom: 18px;
}
.price{
  text-transform: capitalize;
}
</style>
<script src="<?php echo $this->webroot; ?>js/jquery-ui1.js"></script> 
<script src="<?php echo $this->webroot; ?>js/bootstrap.min1.js"></script> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">