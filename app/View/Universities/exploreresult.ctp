<style>
    
    #main h4 {
    font-weight: 600;
    margin: 0 0 5px;
}

.college_list li .info h4 {
    color: #373737;
    font-weight: 400;
    font-size: 24px;
    line-height: 26px;
    margin-bottom: 5px;
}
    
    .college_list li .info h5 {
    color: #f15a22;
    font-weight: 400;
    font-size: 15px;
    line-height: 17px;
    margin-bottom: 25px;
}
    
    .left_container {
    width: 67%;
    float: left;
    background: url(../images/left_cont_bg.png) no-repeat right 0;
    min-height: 645px;
    padding: 15px 5% 0 0;
    margin-top: -15px;
}
    .college_list li .btns a {
    display: block;
    margin-bottom: 10px;
}
    .search_results.cb {
    position: relative;
}

.cb {
    display: block;
}
  .college_list li .info {
    width: 60%;
    float: left;
    padding: 0;
    text-align: left;
}

    .college_list li {
    display: block;
    border: 1px solid #d5d5d5;
    box-shadow: 0 0 10px #9a9393;
    padding: 17px;
    overflow: hidden;
    position: relative;
    margin-bottom: 20px;
}

.ul li {
    margin-bottom: 15px;
    text-align: justify;
}
    
    .college_list li .btns {
    position: absolute;
    bottom: 10px;
    right: 20px;
}

.btns {
    display: block;
    clear: both;
    overflow: hidden;
}
.course_search {
    display: block;
    margin-bottom: 15px;
    background: #fff;
}
.course_search .box {
    display: block;
    border: 1px solid #b3b3b3;
    height: 252px;
    margin-bottom: 10px;
    overflow: hidden;
}
.course_search .col {
    width: 50%;
    float: left;
}
.course_search .col h4 {
    padding: 16px 25px;
    background: #ebeced;
    font-size: 18px;
    font-weight: 400;
}
.course_search .col:first-child .options {
    border-right: 1px solid #c5c5c5;
}
.course_search .col .options {
    display: block;
    padding: 20px 5% 30px;
    height: 149px;
    overflow: auto;
}
.mCustomScrollbar {
    -ms-touch-action: none;
    touch-action: none;
}
.mCustomScrollBox {
    position: relative;
    height: 100%;
    max-width: 100%;
    outline: none;
    direction: ltr;
}
.mCSB_inside > .mCSB_container {
    margin-right: 30px;
}
.mCSB_container {
    overflow: hidden;
    width: auto;
    height: auto;
}
#courseSpecialization {
    margin-right: 8px !important;
}
.course_search .col .options label input {
    position: relative;
    display: inline-block;
    margin-right: 5px;
    
}
.refineSearch .list_cnt input[type="radio"], input[type="checkbox"] {
    margin: 9px 5px 0 0;
    float: left;
}
.refineSearch .list input[type="radio"], input[type="checkbox"] {
    margin: 3px!important;
    float: left;
}
input[type="radio"], input[type="checkbox"] {
    margin: 4px 0 0;
    margin-top: 1px \9;
    line-height: normal;
}
input[type="checkbox"], input[type="radio"] {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input {
    line-height: normal;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: inherit;
}
.course_search .col .options label span {
    display: inline-block;
    position: relative;
    margin-left: 15px;
    width: 85%;
}
.course_search .col .options label {
    display: block;
    position: relative;
    color: #666;
    font-size: 16px;
    margin-bottom: 13px;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
}
.mCSB_scrollTools {
    opacity: 0.75;
    filter: "alpha(opacity=75)";
    -ms-filter: "alpha(opacity=75)";
}
.mCSB_scrollTools, .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .mCSB_scrollTools .mCSB_buttonUp, .mCSB_scrollTools .mCSB_buttonDown, .mCSB_scrollTools .mCSB_buttonLeft, .mCSB_scrollTools .mCSB_buttonRight {
    -webkit-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -moz-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -o-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
}
.mCSB_scrollTools {
    position: absolute;
    width: 16px;
    height: auto;
    left: auto;
    top: 0;
    right: 0;
    bottom: 0;
}
.mCSB_scrollTools .mCSB_draggerContainer {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: auto;
}
.mCSB_scrollTools .mCSB_dragger {
    cursor: pointer;
    width: 100%;
    height: 30px;
    z-index: 1;
}
.mCS-dark.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
    background-color: #8d8d8d;
}
.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
    background-color: #fff;
    background-color: rgba(255,255,255,0.75);
    filter: "alpha(opacity=75)";
    -ms-filter: "alpha(opacity=75)";
}
.mCSB_scrollTools, .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .mCSB_scrollTools .mCSB_buttonUp, .mCSB_scrollTools .mCSB_buttonDown, .mCSB_scrollTools .mCSB_buttonLeft, .mCSB_scrollTools .mCSB_buttonRight {
    -webkit-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -moz-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -o-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
}
.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
    position: relative;
    width: 7px;
    height: 100%;
    margin: 0 auto;
    -webkit-border-radius: 16px;
    -moz-border-radius: 16px;
    border-radius: 16px;
    text-align: center;
}
.mCS-dark.mCSB_scrollTools .mCSB_draggerRail {
    background-color: #d0d0d0);
}
.mCSB_scrollTools .mCSB_draggerRail {
    background-color: #d0d0d0;
}
.mCSB_scrollTools .mCSB_draggerRail {
    width: 5px;
    height: 100%;
    margin: 0 auto;
    -webkit-border-radius: 16px;
    -moz-border-radius: 16px;
    border-radius: 16px;
}
.course_search .col {
    width: 50%;
    float: left;
}
.course_search .orange_btn, .course_search .blue_btn {
    margin-left: 5px;
    height: 47px;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer !important;
}
.orange_btn, .press_kit > .press_block > li .fancyboxIframe {
    display: inline-block;
    line-height: 34px!important;
    color: #fff!important;
    font-size: 12px!important;
    font-weight: 600!important;
    background: #7ABA7A!important ;
    border: 1px solid #7ABA7A!important;
    border: none!important;
    font-family: 'Open Sans'!important;
    padding: 0 34px!important;
    cursor: pointer;
    text-transform: uppercase!important;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.fr {
    float: right;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input {
    line-height: normal;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: inherit;
}
.course_search .orange_btn, .course_search .blue_btn {
    margin-left: 5px;
    height: 47px;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer !important;
}
.blue_btn, #btnSubmit, .universityfilters form input[type="button"], .universityfilters form input[type="submit"] {
    display: inline-block!important;
    line-height: 34px!important;
    color: #fff!important;
    font-size: 12px!important;
    font-weight: 600!important;
    background: #9400D3!important;
    border: none!important;
    font-family: 'Open Sans'!important;
    padding: 0 34px!important;
    cursor: pointer;
    text-transform: uppercase!important;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.fr {
    float: right;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input {
    line-height: normal;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: inherit;
}
.blue_btn, #btnSubmit, .universityfilters form input[type="button"], .universityfilters form input[type="submit"] {
    display: inline-block!important;
    line-height: 34px!important;
    color: #fff!important;
    font-size: 12px!important;
    font-weight: 600!important;
    background: #9400D3!important;
    border: none!important;
    font-family: 'Open Sans'!important;
    padding: 0 34px!important;
    cursor: pointer;
    text-transform: uppercase!important;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
#changeCountries {
    /* background: #ECECEC none repeat scroll 0 0; */
    color: #fff!important;
    font-size: 13px!important;
    padding: 7px 24px!important;
    text-transform: uppercase!important;
    top: -61px!important;
    border: solid 1px #ccc!important;
        margin-left: -157px!important;
    margin-top: -6px!important;
}
.refineSearch .list_cnt input[type="radio"], input[type="checkbox"] {
    margin: 9px 5px 0 0;
    float: left;
}
.refineSearch .list input[type="radio"], input[type="checkbox"] {
    margin: 3px!important;
    float: left;
}
input[type="radio"], input[type="checkbox"] {
    margin: 4px 0 0;
    margin-top: 1px \9;
    line-height: normal;
}
input[type="checkbox"], input[type="radio"] {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
</style>

<?php //pr($universityArray);exit;?>

<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">University Information</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?>
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
<section class="flat-row padding-v1">
            <div class="container">
<div class="left_container"> 
    <div class="search_results cb">
	<ul class="college_list cb">
 <?php if(!empty($universities) && !empty($universityArray)){ foreach($universities as $key=>$value){ ?>
        <?php foreach($universityArray as $key=>$university){ ?>
        <?php if($value['University']['id']==$university['Universitycourse']['university_id']){?>
       <li>
			<div class="info">
				<h4><?php echo $value['University']['title'];?></h4>
                
				<h5><?php echo $university['Course']['name'];?></h5>
				<p>Department: <?php echo $university['Course']['department'];?></p>
				<p>Duration: <?php echo $university['Course']['duration'];?></p>
                
			</div>
			<span class="btns">
				<!--
				<a href="/counselor.html?view=pages&amp;layout=counselor&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Graduate%20Diploma%20in%20Arts%20Management" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>				
				-->
				<a href="<?php echo $this->webroot;?>colleges/collegedetails/<?php echo $value['University']['id'];?>" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>
				<a href="<?php echo $this->webroot;?>bookappointments/bookappointment/<?php echo $value['University']['id'];?>" data-fancybox-type="iframe" class="fancyboxIframe orange_btn">Apply Now</a>
			</span> 
        </li>
		
<?php }}}}else { ?>	
        <li>
			<div class="info">
				<h4><?php echo "No records found";?></h4>
                
			</div>
			
        </li>
    <?php } ?>
</ul></div></div>
	

                
	
