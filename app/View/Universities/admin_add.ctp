<?php $this->Html->addCrumb('University', '/admin/universities'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<script type="text/javascript" src="<?php echo $this->webroot; ?>ckeditor/ckeditor.js"></script>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#UniversityAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[University][type]" : {required : true},
		"data[University][category]" : {required : true},
"data[University][title]" : {required : true},
"data[University][country_id]" : {required : true},
//"data[University][description]" : {required : true},
"data[University][collegecourseids][]" : {required : true},
//"data[University][status]" : {required : true},
		},
		messages:{
		 "data[University][type]" : {required : "Please select university type."},
		"data[University][category]" : {required :"Please select category."},
"data[University][title]" : {required :"Please enter title."},
"data[University][country_id]" : {required :"Please enter country_id."},
//"data[University][description]" : {required :"Please enter description."},
"data[University][collegecourseids][]" : {required :"Please select courses."},
//"data[University][status]" : {required :"Please enter status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
       if(element.attr("name") =="data[University][image]"){
          $(".imageErrCont1").addClass("help-block");
          $(".imageErrCont1").text($(error).text());       
           // error.insertAfter(".fileupload-new");
        }
        else {
          error.insertAfter(element);
        }
      },
	});

	//$('#demo').multiselect();
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add University'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form regionvendorgroups">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                   <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('University', array('class' => 'form-horizontal','type'=>'file')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>     
	                        
	                        <div class="form-group">
							 <?php echo $this->Form->label('University Type<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('type', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$universitytype));?>
							</div>
						</div>   
                        <div class="form-group">
							 <?php echo $this->Form->label('category<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('category', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('title<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('select country<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('country_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('description<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('description', array('class' => 'ckeditor form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
<div class="form-group">
							 <?php echo $this->Form->label('select courses<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
							
								<?php echo $this->Form->input('collegecourseids', array('class' => 'form-control select2me','multiple'=>'multiple','label' => false, 'required' => false,'options'=>$courses));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
                           
								<?php echo $this->Form->input('image', array('class' => 'form-control', 'label' => false, 'required' => false,'type'=>'file'));?>
						
								(Please upload 526px X 526px for better quality)
                            <span class="imageErrCont"></span>
                      </div>
						</div>
<!--<div class="form-group">
							 <?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>-->
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/universities'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>