<style>
#thankyou {
    background-color: lightgrey;
    width: 800px;
    border: 10px solid #9400D3;
    padding: 25px;
    margin: 25px;
    font-size: 20px;
}
@media only screen and (max-width: 479px){
    #thankyou { width: 90%; }
}
</style>


<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Thank you</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="#">Home </a></li>
                                <li>\Thank you </li>
                            </ul>                   
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <!-- contact posts -->
        <section class="main-content contact-posts">
            <div class="container">
                <div class="row">
                    <div class="post-contact">
                       <!--<h2 class="title">Thank you for contacting us. The enquiry has been received. We will get back you soon.</h2>-->
                    <div class="col-md-9">
                            
                                   <div id="thankyou">
                            <h2 class="title">Thank you for contacting us. The enquiry has been received. We will get in touch with you soon.</h2>
                        </div>
                               
                        </div><!-- /col-md-9 -->

                        <div class="col-md-3">
                            <div class="sidebar"><div class="widget widget-nav-menu">
                                    <div class=" widget-inner">
                                        <h2 class="widget-title maincolor2">Main Menu</h2>
                                        <div class="menu-main-navigation-container">
                                            <ul id="menu-main-navigation-1" class="menu">
                                                <li class="menu-item">
                                                    <a href="<?php echo $this->webroot; ?>"><i class="fa fa-home"></i>  Home</a>
                                                   
                                                </li>

                                                <li class="menu-item"><a href="<?php echo $this->webroot; ?>colleges/collegelist"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Details of colleges</a>
                                                    
                                                </li>
                                                <li class="menu-item"><a href="<?php echo $this->webroot; ?>universities/universitydetails"><i class="fa fa-graduation-cap"></i>  University information</a>
                                                    
                                                </li>
                                                <li class="sub-menu-left menu-item"><a href="<?php echo $this->webroot; ?>universities/contact_us"><i class="fa fa-file-text"></i>  Contact Us</a>
                                                    
                                                </li>
<!--
                                                <li class="multi-column menu-item"><a href="#"><i class="fa fa-bolt"></i>  Shortcodes</a>
                                                   
                                                </li>
-->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                          
                            </div><!-- /col-md-9 -->
                        </div><!-- /col-md-3 -->
                    </div>
                </div>
            </div>
        </section>

        