
     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">University of Lodz</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / University of Lodz
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                   <div id="content" class="col-md-9">
                        <div class="content-content"> 
                          <!-- <div class="content-dropcap">  -->
                              <h4 class="tab-title">Study in Poland, at the heart of Europe!</h4>
                              <p style="text-align:justify;">Study Bachelors and Post-Graduate Courses in Management at internationally acclaimed university in Poland that beat IIM </p>

                              <h5 class="sub-title"><i class="fa fa-angle-right"></i><span></span> IIM – A Dream</h5>
                              <p style="text-align:justify;"> The Indian Institute of Management (IIM) is the first choice for any Indian student who wishes to study Management. Apart from being a government institution, its academic excellence has been internationally acclaimed than any private universities and B- Schools. One of the reason behind its success is the focus on skill based education rather than by hearting text books, which is a method followed by European universities.</p>

                              <h5 class="sub-title"><i class="fa fa-angle-right"></i><span></span> Why IIM remains a distant dream for most aspirants?</h5>
                              <p style="text-align:justify;">There are only less than 3500 seats available for studying MBA at IIM’s all-around India for which approximately two and a half lakh people apply annually. Same is the case for studying Bachelor’s degree here. The huge education fee of around 15 lakh is also an important issue.
                           </p>

                          <!-- </div> -->

                          <h4 class="tab-title">University of Lodz for aspirants seeking IIM standard education</h4>
                              <p style="text-align:justify;">What if you are granted admission to University of Lodz, established on European standards that focus on skill based education with international ranking and legacy dating older than IIM? Lodz is a step ahead of any IIM in India while comparing both based on International ranking and recognition.  Here are some of the reasons why you should select this top university in Europe for higher education. </p>
                              <ul class="list-ul">
                                <li>Established in the year 1945</li>
                                <li>Poland is a Schengen visa country and is often considered as a gateway to settle in Europe. </li>
                                <li>Public University that is functioning though government funding with excellent infrastructure and facilities.</li>
                                <li>Acclaimed departments for learning Economics, Finance, Marketing, International Relations and Business Management</li>
                                <li>The university and its departments are popular in Europe. Most preferred by huge Multi-National Corporations for consulting and recruitment.</li>
                                <li>Candidates who complete courses at Lodz are eagerly recruited by such MNC’s</li>
                                <li>Lodz students are given high preference even for part time jobs </li>
                                <li>Ranking steadily inside 800 in OS world ranking system</li>
                                <li>One among top 5 universities in Poland</li>
                                <li>Around 2000 international students form over 40 countries are enrolled every year making it an international campus in every sense. </li>
                                <li>Campus spread around in  an area of 3000 acers, that too at the heart of the city</li>
                                <li>Well-designed department buildings that are architectural marvels City of Lodz</li>
                                <li>Third biggest city in Poland</li>
                                <li>Known as the city of lights and is popular for its festivals and celebrations. Favourite destination among European tourists.</li>
                                <li>Excellent urban planning with extraordinary facilities, apartments, buildings, tourist destinations and transportation facilities</li>
                                <li>Only city in central Europe which has an actual forest in the city. Lodz is a role model for world nations to follow sustainable development.</li>
                                <li>European standard of living at a very low expense compared to life in any Indian metro cities. 15000 INR is enough for a month’s living expense at Lodz</li>
                                <li>High quality food products such as organic vegetables, fruits, fish and meat available at low cost.</li>

                              </ul>
                              <p style="text-align:justify;">Such world class facilities available under a fee just 2500 Euro (approximately 2.5 lakh INR) makes it much more attractive than studying in the IIM. Safe accommodation is provided to both boys and girls within the campus with a monthly expense of 100 Euro with facilities including fully furnished kitchen and attached bathroom.</p>
                              <p style="text-align:justify;"><a href="<?php echo $this->webroot;?>">LIFE PLANNER</a>, with an experience of 7 years in the field is the only representative of Lodz in India. There are many fraud agencies that claim to be the representatives of Lodz and cheat students, which has come into the attention of the university. We request you to be cautious and not to fall as a pray for such frauds. Only LIFE PLANNER handles legitimate admission to Lodz and a large number of our students studying there will assure you this fact</p>
                        </div>

                      <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <div class="content-pad single-course-detail">
                                    <div class="course-detail">
                                        <div class="content-content">
                                            <div class="content-pad calendar-import">
                                                  <div style="float: left;">
                                                      <a href="<?php echo $this->webroot;?>countries/europeancountries" class="flat-button"> KNOW MORE ON <i class="fa fa-angle-right"></i></a>
                                                      <a href="<?php echo $this->webroot;?>universities/medicallist" class="flat-button"> MBBS IN POLAND <i class="fa fa-angle-right"></i></a>
                                                  </div>
                                              </div>          
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <div class="clear"></div>      
                          </div> 
                  </div>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 -->
<style type="text/css">
  .tab-title {
    font-style: normal;
    font-size: 18px;
    color: #656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 10px;
    margin-bottom: 5px;
}
.sub-title {
    font-style: normal;
    font-size: 16px;
    color: #656565;
    font-weight: 400;
    font-style: inherit;
    letter-spacing: 1px;
    margin-top: 15px;
    margin-bottom: 15px;
}
.list-ul{
  margin-top: 10px;
  margin-left: 10px;
}
.list-ul li {
  list-style-type: square;
  margin-bottom: 5px;
  margin-left: 10px;
}
.single-course-detail .content-content .content-pad .flat-button {
    background-color: #872b8e;
    border: solid 2px #872b8e;
    color:#fff;
}
.single-course-detail .content-content .content-pad .flat-button:hover {
    background-color: #81c873!important;
    border: solid 2px #81c873!important;
    color:#fff;
}
</style>