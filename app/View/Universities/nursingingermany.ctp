<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>css/style-tab.css">
<style>
#watermark
{
 position:fixed;
 bottom:5px;
 right:5px;
 opacity:0.5;
 z-index:99;
 color:#9400D3;
 font-size:18px;
 margin-right: 1px;
}
@media (min-width: 1200px){
.tab-links li {
    width: 40%!important;
}
}
.tab-links a {
    line-height: 40px;
    font-size: 14px;
}
</style>


<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Nursing in Germany</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Nursing in Germany
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
       <!-- <div id="watermark">“It always seems impossible until it’s done.” – Nelson Mandela</div> -->
        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                        <div class="col-md-12"> 
                                <div class="row"> 
                                    <div class="flat-future">
                                        <div class="post-future">
                                            <h3 class="heading">FREE EDUCATION IN GERMANY!</h3>
                                            <h3 class="heading">Become a German Nurse and settle in Germany!</h3>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <!-- <h4 class="tab-title">Free education in Germany!</h4> -->
                                    <h4 class="tab-title">Germany, mightiest among world nations </h4>
                                    <p>Germany is the fourth biggest economic power in the world and first in Europe. They are giants in exporting technology and countries like USA are dependent on their Robotic science technology. German products are available in India through brands such as BMW, Benz, Volkswagen, Skoda, Bosch, and so on. </p> 

                                    <h4 class="tab-title">A country that provides maximum social security for a safe comfortable life </h4>
                                    <p>Avail benefits such as PF, Pension, Health/Medical Insurance, Social Security and Free Education from the day of joining as a nurse in Germany to ensure a comfortable family life. </p>

                                    <h4 class="tab-title">Health Care Industry  </h4>
                                    <p> Germany gives utmost importance to its health sector and ensure all necessary means to ensure health care to its citizens. This makes them giants in the field of clinical research and clinical ethnology. Nursing Jobs in Germany provide opportunities for migration towards Germany, which comes under the Schengen visa zone.</p>

                                    <h4 class="tab-title">Challenges faced by Germany </h4>
                                    <p>The rising aging population along with low population growth is one of the biggest challenges faced by Germany</p>

                                    <h4 class="tab-title">How is this going to affect Germany?</h4>
                                    <ul class="list-ul">
                                        <li>Decreasing number of youngsters who come under the working age population</li>
                                        <li>Health care industry will be in need of more recruits, especially nurses who are not readily available in Germany.</li>
                                    </ul>

                                    <h4 class="tab-title">Great opportunity for foreign students! </h4>
                                    <p>Germany expects more health care professionals, especially Nurses, from countries like India which has made commendable contributions to health care sector</p>

                                    <h4 class="tab-title">Study Nursing in Germany </h4>
                                    <p>Public universities in Germany including top ones are recruiting foreign students with an aim to give them free training. Studying in Germany is an excellent opportunity for students to learn German and for the government to make their health care sector more efficient.</p>

                                    <div class="line-space"></div>
                                </div> 
                            </div>
                            <div class="event-listing event-listing-classic">
                                    <article class="post-item row event-classic-item">
                                                                      
                                        <div class="col-md-8 col-sm-7">
                                            <div class="content-pad">
                                                <div class="item-content">
                                                    <h3 class="item-title">Pre-Nursing </h3>

                                                    <div class="price main-color-1"></div>
                                                    <p>The government has implemented the Pre-Nursing scheme with collaboration from top German universities.</p>
                                                    <p>The 6 months study in Germany scheme helps to fine tune the skills of our students to that of German students having 13 years of schooling.</p>
                                                    <p>The course also ensures that the candidates learn German terms useful in clinical field and the fee is just 5000 Euro.</p>
                                                </div>
                                                <div class="item-meta">
                                                    <a class="flat-button" href="<?php echo $this->webroot; ?>bookappointments/bookappointment">Know More <i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div> 
                                <div class="event-listing event-listing-classic">
                                    <article class="post-item row event-classic-item">
                                          <div class="col-md-4 col-sm-5">
                                            <div class="content-pad">
                                                <div class="item-thumbnail">
                                                    
                                                </div>
                                            </div>
                                        </div>                           
                                        <div class="col-md-8 col-sm-7">
                                            <div class="content-pad">
                                                <div class="item-content">
                                                    <h3 class="item-title">3 Year Government Licensed Nursing Course </h3>

                                                    <div class="price main-color-1"></div>
                                                    <p>Nursing is under the category of  Free education in Germany.</p>
                                                    <p>The candidates who have successfully completed the Pre-Nursing Course will be admitted to this course.</p>
                                                    <p>You will be studying in reputed colleges and will be allotted a monthly stipend to meet living expenses through hospital practice, which is already a part of the course.</p>
                                                    <p>In short the money spent for learning BSc.</p>
                                                    <p> Nursing in India (approximately 4 lakh) can be spent for learning Pre-Nursing Course in Germany (5000 Euros).</p>
                                                    <p> Join for a 3 year course after that with monthly stipend to meet your living expenses.</p>
                                                    <p> Study, find work and settle in Germany</p>
                                                </div>
                                                <div class="item-meta">
                                                    <a class="flat-button" href="<?php echo $this->webroot; ?>bookappointments/bookappointment">Know More <i class="fa fa-angle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div> 
                                <div class="event-listing event-listing-classic">
                                <article class="post-item row event-classic-item">
                                                                  
                                    <div class="col-md-8 col-sm-7">
                                        <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title">
                                                   PAP 3.5 </h3>( Nursing Program)

                                                <div class="price main-color-1"></div>

                                                <p>The adaptation program named Pre-Nursing is 6 months program. After the completion of the program, the student can enroll for the three year nursing program along with the signed job contract.</p>
                                                <p>You should complete 6 months Pre program (5000€ course fees) and bring B1 level German from your home country.</p>
                                                <p>After the 6 months you can join our 3 year Nursing training Program where the hospital tie ups also do pay you a stipend (approx 800€ first year, 900€ 2nd year and 1200€ 3rd year).</p>
                                                <p><strong>Living Expense </strong>: 4320 Euro (Blocked Account)</p>
                                                
                                            </div>
                                            <div class="item-meta">
                                                <a class="flat-button" href="<?php echo $this->webroot; ?>bookappointments/bookappointment">Know More <i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="event-listing event-listing-classic">
                                <article class="post-item row event-classic-item">
                                </article>
                            </div>
                            <ul class="tab-links">
                                <li><a class="link" href="<?php echo $this->webroot?>countries/europeancountries">KNOW ABOUT GERMANY <i class="fa fa-angle-right"></i></a></li>
                                <li><a class="link" href="<?php echo $this->webroot;?>universities/medicallist">MBBS IN GERMANY <i class="fa fa-angle-right"></i></a></li>

                            </ul>
                            <br>
                    </div><!-- /col-md-9 -->

<style type="text/css">
    .tab-title , .tab-form-title {
    font-style: normal;
    font-size: 18px;
    color:#656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 10px;
    margin-bottom: 5px;
    }
    .heading {
    font-style: normal;
    font-size: 14px;
    color:#656565;
    font-weight: 500;
    /*font-family: 'Bitter', sans-serif;*/
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 10px;
    margin-bottom: 5px;
    }

    .line-space{
        margin-bottom:25px;
        padding-top: 15px;
    }
    .list-ul{
        margin-top: 10px;
        margin-left: 10px;
    }
    .list-ul li {
        list-style-type: square;
        margin-bottom: 5px;
        margin-left: 10px;
    }
    .clear{
    margin-bottom: 10px;
    padding-top: 10px;
}
</style>