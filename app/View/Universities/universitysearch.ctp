<style>
    
    #main h4 {
    font-weight: 600;
    margin: 0 0 5px;
}

.college_list li .info h4 {
    color: #373737;
    font-weight: 400;
    font-size: 24px;
    line-height: 26px;
    margin-bottom: 5px;
}
    
    .college_list li .info h5 {
    color: #f15a22;
    font-weight: 400;
    font-size: 15px;
    line-height: 17px;
    margin-bottom: 25px;
}
    
    .left_container {
    width: 67%;
    float: left;
    background: url(../images/left_cont_bg.png) no-repeat right 0;
    min-height: 645px;
    padding: 15px 5% 0 0;
    margin-top: -15px;
}
    .college_list li .btns a {
    display: block;
    margin-bottom: 10px;
}
    .search_results.cb {
    position: relative;
}

.cb {
    display: block;
}
  .college_list li .info {
    width: 60%;
    float: left;
    padding: 0;
    text-align: left;
}

    .college_list li {
    display: block;
    border: 1px solid #d5d5d5;
    box-shadow: 0 0 10px #9a9393;
    padding: 17px;
    overflow: hidden;
    position: relative;
    margin-bottom: 20px;
}

.ul li {
    margin-bottom: 15px;
    text-align: justify;
}
    
    .college_list li .btns {
    position: absolute;
    bottom: 10px;
    right: 20px;
}

.btns {
    display: block;
    clear: both;
    overflow: hidden;
}
.course_search {
    display: block;
    margin-bottom: 15px;
    background: #fff;
}
.course_search .box {
    display: block;
    border: 1px solid #b3b3b3;
    height: 252px;
    margin-bottom: 10px;
    overflow: hidden;
}
.course_search .col {
    width: 50%;
    float: left;
}
.course_search .col h4 {
    padding: 16px 25px;
    background: #ebeced;
    font-size: 18px;
    font-weight: 400;
}
.course_search .col:first-child .options {
    border-right: 1px solid #c5c5c5;
}
.course_search .col .options {
    display: block;
    padding: 20px 5% 30px;
    height: 149px;
    overflow: auto;
}
.mCustomScrollbar {
    -ms-touch-action: none;
    touch-action: none;
}
.mCustomScrollBox {
    position: relative;
    height: 100%;
    max-width: 100%;
    outline: none;
    direction: ltr;
}
.mCSB_inside > .mCSB_container {
    margin-right: 30px;
}
.mCSB_container {
    overflow: hidden;
    width: auto;
    height: auto;
}
#courseSpecialization {
    margin-right: 8px !important;
}
.course_search .col .options label input {
    position: relative;
    display: inline-block;
    margin-right: 5px;
    
}
.refineSearch .list_cnt input[type="radio"], input[type="checkbox"] {
    margin: 9px 5px 0 0;
    float: left;
}
.refineSearch .list input[type="radio"], input[type="checkbox"] {
    margin: 3px!important;
    float: left;
}
input[type="radio"], input[type="checkbox"] {
    margin: 4px 0 0;
    margin-top: 1px \9;
    line-height: normal;
}
input[type="checkbox"], input[type="radio"] {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input {
    line-height: normal;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: inherit;
}
.course_search .col .options label span {
    display: inline-block;
    position: relative;
    margin-left: 15px;
    width: 85%;
}
.course_search .col .options label {
    display: block;
    position: relative;
    color: #666;
    font-size: 16px;
    margin-bottom: 13px;
}
label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
}
.mCSB_scrollTools {
    opacity: 0.75;
    filter: "alpha(opacity=75)";
    -ms-filter: "alpha(opacity=75)";
}
.mCSB_scrollTools, .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .mCSB_scrollTools .mCSB_buttonUp, .mCSB_scrollTools .mCSB_buttonDown, .mCSB_scrollTools .mCSB_buttonLeft, .mCSB_scrollTools .mCSB_buttonRight {
    -webkit-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -moz-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -o-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
}
.mCSB_scrollTools {
    position: absolute;
    width: 16px;
    height: auto;
    left: auto;
    top: 0;
    right: 0;
    bottom: 0;
}
.mCSB_scrollTools .mCSB_draggerContainer {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    height: auto;
}
.mCSB_scrollTools .mCSB_dragger {
    cursor: pointer;
    width: 100%;
    height: 30px;
    z-index: 1;
}
.mCS-dark.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
    background-color: #8d8d8d;
}
.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
    background-color: #fff;
    background-color: rgba(255,255,255,0.75);
    filter: "alpha(opacity=75)";
    -ms-filter: "alpha(opacity=75)";
}
.mCSB_scrollTools, .mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .mCSB_scrollTools .mCSB_buttonUp, .mCSB_scrollTools .mCSB_buttonDown, .mCSB_scrollTools .mCSB_buttonLeft, .mCSB_scrollTools .mCSB_buttonRight {
    -webkit-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -moz-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    -o-transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
    transition: opacity .2s ease-in-out, background-color .2s ease-in-out;
}
.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar {
    position: relative;
    width: 7px;
    height: 100%;
    margin: 0 auto;
    -webkit-border-radius: 16px;
    -moz-border-radius: 16px;
    border-radius: 16px;
    text-align: center;
}
.mCS-dark.mCSB_scrollTools .mCSB_draggerRail {
    background-color: #d0d0d0);
}
.mCSB_scrollTools .mCSB_draggerRail {
    background-color: #d0d0d0;
}
.mCSB_scrollTools .mCSB_draggerRail {
    width: 5px;
    height: 100%;
    margin: 0 auto;
    -webkit-border-radius: 16px;
    -moz-border-radius: 16px;
    border-radius: 16px;
}
.course_search .col {
    width: 50%;
    float: left;
}
.course_search .orange_btn, .course_search .blue_btn {
    margin-left: 5px;
    height: 47px;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer !important;
}
.orange_btn, .press_kit > .press_block > li .fancyboxIframe {
    display: inline-block;
    line-height: 34px!important;
    color: #fff!important;
    font-size: 12px!important;
    font-weight: 600!important;
    background: #f15a22!important ;
    border: 1px solid #f15a22!important;
    border: none!important;
    font-family: 'Open Sans'!important;
    padding: 0 34px!important;
    cursor: pointer;
    text-transform: uppercase!important;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.fr {
    float: right;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input {
    line-height: normal;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: inherit;
}
.course_search .orange_btn, .course_search .blue_btn {
    margin-left: 5px;
    height: 47px;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer !important;
}
.blue_btn, #btnSubmit, .universityfilters form input[type="button"], .universityfilters form input[type="submit"] {
    display: inline-block!important;
    line-height: 34px!important;
    color: #fff!important;
    font-size: 12px!important;
    font-weight: 600!important;
    background: #034ea2!important;
    border: none!important;
    font-family: 'Open Sans'!important;
    padding: 0 34px!important;
    cursor: pointer;
    text-transform: uppercase!important;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.fr {
    float: right;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
input {
    line-height: normal;
}
button, input, optgroup, select, textarea {
    margin: 0;
    font: inherit;
    color: inherit;
}
.blue_btn, #btnSubmit, .universityfilters form input[type="button"], .universityfilters form input[type="submit"] {
    display: inline-block!important;
    line-height: 34px!important;
    color: #fff!important;
    font-size: 12px!important;
    font-weight: 600!important;
    background: #034ea2!important;
    border: none!important;
    font-family: 'Open Sans'!important;
    padding: 0 34px!important;
    cursor: pointer;
    text-transform: uppercase!important;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
#changeCountries {
    /* background: #ECECEC none repeat scroll 0 0; */
    color: #fff!important;
    font-size: 13px!important;
    padding: 7px 24px!important;
    text-transform: uppercase!important;
    top: -61px!important;
    border: solid 1px #ccc!important;
        margin-left: -157px!important;
    margin-top: -6px!important;
}
.refineSearch .list_cnt input[type="radio"], input[type="checkbox"] {
    margin: 9px 5px 0 0;
    float: left;
}
.refineSearch .list input[type="radio"], input[type="checkbox"] {
    margin: 3px!important;
    float: left;
}
input[type="radio"], input[type="checkbox"] {
    margin: 4px 0 0;
    margin-top: 1px \9;
    line-height: normal;
}
input[type="checkbox"], input[type="radio"] {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    padding: 0;
}
input, button, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
}
</style>


<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">University Information</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?>
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
<section class="flat-row padding-v1">
            <div class="container">
<div class="left_container"> 
<form name="searchfrm" id="searchfrm" action="" method="POST" class="has-validation-callback">
    <div class="search_container  cb">
        <div class="wrap33">
            <div class="course_search cb">
                <div class="box">
                    <div class="col">
                        <h4>Course Specialization</h4>
                        <div class="options mCustomScrollbar _mCS_3 scroll_class" id="cousrse_options">
                            <div id="mCSB_3" class="mCustomScrollBox mCS-dark mCSB_vertical mCSB_inside" tabindex="0">
                                <div id="mCSB_3_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px;" dir="ltr">
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="117"> <span>Aerospace, Automotive and Aviation</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="87"> <span>Business Administration and General Management</span></label><label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="36"> <span>Culture, History and Religious Studies</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="98"> <span>Economics</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="22"> <span>Education</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="55"> <span>Fashion</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="2"> <span>Fine, Performing and Visual Arts</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="99"> <span>Genetics and Disabilities</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="51"> <span>Geography</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="82"> <span>Hospitality, Travel and Tourism</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="30"> <span>Journalism and Media</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="16"> <span>Languages and Literature</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="100"> <span>Liberal Arts</span></label><label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="61"> <span>Music Technology and Practice</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="56"> <span>Political Science and International Relations</span></label>
                                     <label> <input type="checkbox" name="specialization[]" id="courseSpecialization" value="48"> <span>Social</span></label>
                                     </div>
                                     </div></div></div><div class="col"><h4>Countries</h4>
                                                 <div class="options mCustomScrollbar _mCS_4 scroll_class" id="country_options"><div id="mCSB_4" class="mCustomScrollBox mCS-dark mCSB_vertical mCSB_inside" tabindex="0"><div id="mCSB_4_container" class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr"> <label> <input type="checkbox" name="country[]" id="courseCountry" value="1" checked=""> <span>Australia</span></label><label> <input type="checkbox" name="country[]" id="courseCountry" value="26"> <span>Bulgaria</span></label><label> <input type="checkbox" name="country[]" id="courseCountry" value="3"> <span>Canada</span></label><label> <input type="checkbox" name="country[]" id="courseCountry" value="4"> <span>China</span></label>
                                                 <label> <input type="checkbox" name="country[]" id="courseCountry" value="14"> <span>USA</span></label></div>
                                                </div></div></div></div> <input type="hidden" name="Itemid" id="Itemid" value="584"> <input type="hidden" name="prid" id="prid" value="1"> <input type="hidden" name="strid" id="strid" value="2"> <input type="hidden" name="txt_url" id="txt_url" value="Postgraduate_Art and Humanities"> <span id="submit_btn_hide" style="display: inline;"><div id="submit_btn" class="submit_btn"> <input type="submit" class="fr orange_btn" value="Submit"> <input type="button" class="fr blue_btn" value="Reset" onclick="form_reset()"></div> </span><div id="refineSearch" class="refineSearch" style="display:none"><div class=""><div class="container"><div class="small-container"><div class="row"><div class="col-lg-5"><div class="input-group"><span id="basic-addon1" class="input-group-addon"> Education Level </span> <select class="selectpicker" id="StudentTitle" name="StudentTitle" tabindex="1"><option value="">- Select -</option>Undergraduate<option value="2">Undergraduate</option><option selected="" value="1">Postgraduate</option> </select></div></div><div class="col-lg-7"><div class="input-group"><span id="basic-addon1" class="input-group-addon"> Choose field of Interest </span> <select class="selectpicker" id="StudentTitle" name="StudentTitle" tabindex="1" onchange="get_Specialization(this.value);"><option value="">- Select -</option><option value="2" selected="">Art and Humanities</option><option value="6">Business and Social Sciences</option><option value="3">Criminology, Law and Justice</option><option value="4">Engineering and Technology</option><option value="7">Information Technology and Computing</option><option value="5">Medicine and Health</option><option value="8">Pathways</option><option value="1">Sciences</option><option value="9">Skills and Vocational Training</option> </select></div></div></div></div><div class="text-center"><div class="curve"></div></div><div class="clearfix"></div><div class="row skin-color"><div class="col-md-6"><h2>Choose field of Specialization</h2><div class="content mCustomScrollbar shadow-none _mCS_1 mCS-autoHide mCS_no_scrollbar" style="overflow: visible;"><div id="mCSB_1" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr"><div id="specialization"></div></div></div><div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-minimal-dark mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 50px; top: 0px;" oncontextmenu="return false;"><div class="mCSB_dragger_bar" style="line-height: 50px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div><div class="col-md-6"><h2>Choose Country</h2><div class="content mCustomScrollbar shadow-none _mCS_2 mCS-autoHide mCS_no_scrollbar" style="overflow: visible;"><div id="mCSB_2" class="mCustomScrollBox mCS-minimal-dark mCSB_vertical mCSB_outside" tabindex="0"><div id="mCSB_2_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr"><div id="target" class="list2 list_cnt"><li><span><img src="https://www.thechopras.com/country_flags/1.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="1" checked=""> Australia</li><li><span><img src="https://www.thechopras.com/country_flags/26.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="26"> Bulgaria</li><li><span><img src="https://www.thechopras.com/country_flags/3.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="3"> Canada</li><li><span><img src="https://www.thechopras.com/country_flags/4.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="4"> China</li><li><span><img src="https://www.thechopras.com/country_flags/23.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="23"> Dubai</li><li><span><img src="https://www.thechopras.com/country_flags/21.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="21"> Estonia</li><li><span><img src="https://www.thechopras.com/country_flags/15.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="15"> France</li><li><span><img src="https://www.thechopras.com/country_flags/16.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="16"> Germany</li><li><span><img src="https://www.thechopras.com/country_flags/24.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="24"> Grenada</li><li><span><img src="https://www.thechopras.com/country_flags/17.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="17"> Hong Kong</li><li><span><img src="https://www.thechopras.com/country_flags/6.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="6"> India</li><li><span><img src="https://www.thechopras.com/country_flags/7.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="7"> Ireland</li><li><span><img src="https://www.thechopras.com/country_flags/2.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="2"> Italy</li><li><span><img src="https://www.thechopras.com/country_flags/8.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="8"> Malaysia</li><li><span><img src="https://www.thechopras.com/country_flags/22.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="22"> Mauritius</li><li><span><img src="https://www.thechopras.com/country_flags/25.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="25"> Netherlands</li><li><span><img src="https://www.thechopras.com/country_flags/9.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="9"> New Zealand</li><li><span><img src="https://www.thechopras.com/country_flags/10.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="10"> Singapore</li><li><span><img src="https://www.thechopras.com/country_flags/11.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="11"> Spain</li><li><span><img src="https://www.thechopras.com/country_flags/20.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="20"> Sweden</li><li><span><img src="https://www.thechopras.com/country_flags/12.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="12"> Switzerland</li><li><span><img src="https://www.thechopras.com/country_flags/13.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="13"> United Arab Emirates</li><li><span><img src="https://www.thechopras.com/country_flags/5.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="5"> United Kingdom</li><li><span><img src="https://www.thechopras.com/country_flags/14.jpg"></span> <input type="checkbox" name="country[]" id="courseCountry" value="14"> USA</li></div></div></div><div id="mCSB_2_scrollbar_vertical" class="mCSB_scrollTools mCSB_2_scrollbar mCS-minimal-dark mCSB_scrollTools_vertical" style="display: none;"><div class="mCSB_draggerContainer"><div id="mCSB_2_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 50px; top: 0px;" oncontextmenu="return false;"><div class="mCSB_dragger_bar" style="line-height: 50px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div></div><div class="row"> <input type="hidden" name="Itemid" id="Itemid" value="584"> <input type="hidden" name="prid" id="prid" value="1"> <input type="hidden" name="strid" id="strid" value="2"> <input type="hidden" name="txt_url" id="txt_url" value="Postgraduate_Art and Humanities"><div id="submit_btn1" class="submit_btn"> <input type="submit" class="fr hgt orange_btn" value="Submit"> <input type="button" class="fr hgt blue_btn" value="Reset" onclick="form_reset()"></div></div></div></div></div></div><div id="contact-form2" align="center" style="display: block;"> <a href="/study_search_form.php?prid=1&amp;strid=2" class="changeCountries blue_btn" data-fancybox-type="iframe" id="changeCountries">Change Course/Country Preference <i class="fa fa-chevron-down"></i></a></div></div> <span class="toggle">Refine Your Search<a href="#"></a></span></div></form>
    <div class="search_results cb">
	<ul class="college_list cb">

	
		<li>
			<!--
							<figure> <img src="https://www.thechopras.com/uploads/university/1449814445uni_download.php-5.jpeg" alt="" > </figure>
						-->
			<div class="info">
				<h4>Edith Cowan University</h4>
				<h5>Graduate Diploma in Arts Management</h5>
				<p>Department: Education and Arts</p>
				<p>Duration: 1 Year</p>
			</div>
			<span class="btns">
				<!--
				<a href="/counselor.html?view=pages&amp;layout=counselor&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Graduate%20Diploma%20in%20Arts%20Management" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>				
				-->
				<a href="/component/study/?view=list&amp;task=universityInfo&amp;Itemid=582&amp;univid=5" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>
				<a href="/counselor.html?view=pages&amp;layout=apply&amp;url=Postgraduate_Art%20and%20Humanities&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Graduate%20Diploma%20in%20Arts%20Management" data-fancybox-type="iframe" class="fancyboxIframe orange_btn">Apply Now</a>
			</span> 
		</li>
		
		<li>
			<!--
							<figure> <img src="https://www.thechopras.com/uploads/university/1449814445uni_download.php-5.jpeg" alt="" > </figure>
						-->
			<div class="info">
				<h4>Edith Cowan University</h4>
				<h5>Master of Arts Management</h5>
				<p>Department: Education and Arts</p>
				<p>Duration: 2 Years</p>
			</div>
			<span class="btns">
				<!--
				<a href="/counselor.html?view=pages&amp;layout=counselor&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Master%20of%20Arts%20Management" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>				
				-->
				<a href="/component/study/?view=list&amp;task=universityInfo&amp;Itemid=582&amp;univid=5" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>
				<a href="/counselor.html?view=pages&amp;layout=apply&amp;url=Postgraduate_Art%20and%20Humanities&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Master%20of%20Arts%20Management" data-fancybox-type="iframe" class="fancyboxIframe orange_btn">Apply Now</a>
			</span> 
		</li>
		
		<li>
			<!--
							<figure> <img src="https://www.thechopras.com/uploads/university/1449814445uni_download.php-5.jpeg" alt="" > </figure>
						-->
			<div class="info">
				<h4>Edith Cowan University</h4>
				<h5>Master of Design (Display and Events)</h5>
				<p>Department: Communication Arts </p>
				<p>Duration: 2 Years</p>
			</div>
			<span class="btns">
				<!--
				<a href="/counselor.html?view=pages&amp;layout=counselor&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Master%20of%20Design%20(Display%20and%20Events)" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>				
				-->
				<a href="/component/study/?view=list&amp;task=universityInfo&amp;Itemid=582&amp;univid=5" data-fancybox-type="iframe" class="fancyboxIframe blue_btn">Know More</a>
				<a href="/counselor.html?view=pages&amp;layout=apply&amp;url=Postgraduate_Art%20and%20Humanities&amp;type=1&amp;univ=Edith%20Cowan%20University&amp;course=Master%20of%20Design%20(Display%20and%20Events)" data-fancybox-type="iframe" class="fancyboxIframe orange_btn">Apply Now</a>
			</span> 
		</li>
		
		
	
        </ul></div></div>
	

                
	
