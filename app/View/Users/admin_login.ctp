<style>
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#back-btn").click(function(){
		$('.alert-danger').remove();
		// $('#forgotpasswordAdminLoginForm')[0].reset();
		// $(".form-group").removeClass("has-error");
		// $(".help-block").html("");
	});
	var error1 = $('.alert-danger');
	$('#LoginForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		//onfocusout: false,
		rules:{
				"data[User][email]" : {required : true, email:true},
				"data[User][password]" : {required : true}
		},
		messages:{
				"data[User][email]" : {required :"Please enter email address.", email : "Please enter a valid email address."},
				"data[User][password]" : {required : "Please enter password."}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			$("span.error").html('&nbsp;');
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
		if (element.attr("name") == "data[User][email]" )
					error.insertAfter(".class1");
				else if  (element.attr("name") == "data[User][password]" )
					error.insertAfter(".class2");
				else
					error.insertAfter(element);
		},
            
	});
	$('#forgotpasswordLoginForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
				"data[User][email]" : {required : true, email:true}
		},
		messages:{
				"data[User][email]" : {required :"Please enter email address.", email : "Please enter a valid email address."}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			$("span.error").html('&nbsp;');
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
		if (element.attr("name") == "data[User][email]" )
					error.insertAfter(".class3");
				else
					error.insertAfter(element);
		},

	});
});

</script>

<div align="center" style="padding-bottom:5%;"><img src="<?php echo $this->webroot;?>assets/img/log_black.png" alt=""/></div>
    <div id="alert">
    <!-- BEGIN LOGIN FORM -->
	<?php echo $this->Session->flash(); ?>
    </div>
	<?php echo $this->Form->create('User', array('class' => 'login-form','id'=>'LoginForm','role' => 'form', 'url' => array('controller' => 'users', 'action' => 'admin_login'))); ?>
	
		<h3 class="form-title">Login to your account</h3>
		
		<!--<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
				 Enter  username and password.
			</span>
		</div>-->
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="data[User][email]"/>
			</div>
            <div class="class1"></div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="data[User][password]"/>
			</div>
            <div class="class2"></div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn green pull-right">
				Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="forget-password">
			<h4>Forgot your password ?</h4>
			<p>
				 Click <a href="javascript:;" id="forget-password" onclick="$('#alert').remove();">here</a>
				to reset your password.
			</p>
		</div>	
	<?php echo $this->Form->end(); ?>
	<!-- END LOGIN FORM -->
	<!-- BEGIN FORGOT PASSWORD FORM -->
    <?php echo $this->Form->create('forgotpassword', array('class' => 'forget-form','role' => 'form', 'url' => array('controller' => 'users', 'action' => 'forgotpassword'))); ?>
		<h3>Forgot Password ?</h3>
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off"  name="data[User][email]"/>
			</div>
            <div class="class3"></div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
			<button type="submit" class="btn green pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
	</form>
	<!-- END FORGOT PASSWORD FORM -->