<?php $this->Html->addCrumb('Profile', '/admin/users/profile'); ?>
<div id="flashmessage">&nbsp;<?php echo $this->Session->flash(); ?></div>
<div class="alert alert-danger display-hide">
    <button data-close="alert" class="close"></button>
    You have some form errors. Please check below.
</div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#UserAdminProfileForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
				"data[User][first_name]":{required : true, regexname : true,minlength:2, maxlength:20},
				"data[User][last_name]" : {required : true, regexname : true,minlength:1, maxlength:20},
				"data[User][email]" : {required : true,email:true}
		},
		messages:{
				"data[User][first_name]" : {required :"Please enter first name.", regexname : "Name should have only alphabets."},
				"data[User][last_name]" : {required :"Please enter last name.", regexname : "Name should have only alphabets."},
				"data[User][email]" : {required :"Please enter email address.",email:"Please enter a valid email address."}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			$("span.error").html('&nbsp;');
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},

	});
	$('#AdminuserPasswordEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
				"data[User][oldpassword]" : {required : true},
				"data[User][newpassword]" : {required :true, minlength : 5},
				"data[User][confirmpwd]" : {required :true, equalTo: "#UserNewpassword"}
		},
		messages:{
				"data[User][oldpassword]" : {required :"Please enter old password."},
				"data[User][newpassword]" : {required :"Please enter new password.", minlength : "Password too short. Use at least 5 characters."},
				"data[User][confirmpwd]" : {required :"Please confirm new password.", equalTo: "Password mismatch."}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			$("span.error").html('&nbsp;');
			$(".alert-success").remove();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		}

	});


    var crntName =  $("#UserEmail").val();
    $("#btnSubmit").click(function(){
            $('#UserAdminProfileForm').valid();
            var name= $("#UserEmail").val();
            var originalElemVal = $("#UserEmail").attr("data"); 
            if(crntName!=name){
                $.ajax({
                         url: "<?php echo $this->webroot;?>users/checkEmail/"+name,            
                         cache: false,
                          success: function(html){//alert(html);
                                if(html == 1){      

                                    bootbox.alert('Email address already exists.');
                                    $("#UserEmail").val(originalElemVal);
                                    //$("#UserEmail").val('');
                                }else{
                                    $("#UserAdminProfileForm").submit();
                                }
                                
                        }
                });
            }else{
                 $("#UserAdminProfileForm").submit();
            }

    });

});
</script>
<?php ?>
<div class="row">
    <div class="col-md-6" style="height:40px;">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box red">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-user"></i><?php echo __('Update Profile'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form users">                    	
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('User', array( 'class' => 'form-horizontal','url'=>array('controller'=>'users','action'=>'admin_profile'))); ?>
                         <div class='col-md-4'>
                               <!-- <?php// echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false , 'hidden' => true));?>-->
                                    <?php echo $this->Form->input('id', array('type' => 'hidden','class' => 'form-control', 'label' => false, 'required' => false,'value'=>$user['User']['id']));?>

                            </div>
                       <div class="form-body">
                        	<div class="form-group"><label class="col-md-4 control-label" for="">&nbsp;</label>
                                <div class="col-md-6">
                                    <div class="input text">
    									<span class="required" style="color:#F00"> *</span>= Required
                                    </div>
                                </div>
                            </div>                             
							
                            <div class="form-group">
                           	<?php echo $this->Form->label('First Name<span class=required> * </span>' ,null, array('class' => 'col-md-4 control-label', 'for' => '')); ?>
	                            <div class="col-md-7">
    	                        	<?php echo $this->Form->input('first_name', array('class' => 'form-control', 'label' => false, 'required' => false,'hiddenField' => false,'value'=>$user['User']['first_name'])); ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                           	<?php echo $this->Form->label('Last Name<span class=required> * </span>' ,null, array('class' => 'col-md-4 control-label', 'for' => '')); ?>
	                            <div class="col-md-7">
    	                        	<?php echo $this->Form->input('last_name', array('class' => 'form-control', 'label' => false, 'required' => false,'hiddenField' => false,'value'=>$user['User']['last_name'])); ?>
                                </div>
                            </div>
                            
                            <div class="form-group">
                            <?php echo $this->Form->label('Email<span class=required> * </span>' ,null, array('class' => 'col-md-4 control-label', 'for' => '')); ?>
                            	<div class="col-md-7">
                                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'required' => false, 'data' => '','value'=>$user['User']['email']));?>

                                	<?php //echo $this->Form->input('email', array('class' => 'form-control', 'label' => false,  'required' => false,'hiddenField' => false,'value'=>$user['email'])); ?>
                                </div>
                            </div>
                             <div class="form-group">
                           
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-4 col-md-9">
                                    <input type="button" class="btn blue" value="Submit" id="btnSubmit"/>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/users/profile'; ?>'">Cancel</button>
                                </div>
                            </div>
						</div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                   </div>
                </div>
             </div>
           </div>
        </div>

        <div class="col-md-6">
        <div class="tab-content">
                <div class="portlet box red">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-lock"></i><?php echo __('Change Password'); ?></div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form users">                       
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('User', array('class' => 'form-horizontal','id'=>'AdminuserPasswordEditForm','url'=>array('controller'=>'users','action'=>'changepassword'))); ?>
                       <div class="form-body">
                            <div class="form-group"><label class="col-md-5 control-label" for="">&nbsp;</label>
                                <div class="col-md-6">
                                    <div class="input text">
                                        <span class="required" style="color:#F00"> *</span>= Required
                                    </div>
                                </div>
                            </div>                             
                            <?php
                                   // echo $this->Form->input('id', array('class' => 'form-control', 'label' => false,  'required' => false,'hiddenField' => false));
                            ?><div class="form-group">
                            <?php echo $this->Form->label('Old Password<span class=required> * </span>' ,null, array('class' => 'col-md-4 control-label', 'for' => '')); ?>
                            <div class="col-md-7">  <?php
                                    echo $this->Form->input('oldpassword', array('class' => 'form-control', 'type' => 'password', 'label' => false,  'required' => false,'hiddenField' => false));
                            ?></div></div><div class="form-group">
                            <?php echo $this->Form->label('New Password<span class=required> * </span>' ,null, array('class' => 'col-md-4 control-label', 'for' => '')); ?>
                            <div class="col-md-7">  <?php
                                    echo $this->Form->input('newpassword', array('class' => 'form-control', 'label' => false,  'required' => false, 'type' =>'password','hiddenField' => false));
                            ?></div></div>
                            <div class="form-group">
                            <?php echo $this->Form->label('Confirm New Password<span class=required> * </span>' ,null, array('class' => 'col-md-4 control-label', 'for' => '')); ?>
                            <div class="col-md-7">  <?php
                                    echo $this->Form->input('confirmpwd', array('class' => 'form-control', 'label' => false, 'type' =>'password', 'required' => false,'hiddenField' => false));
                            ?></div></div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-4 col-md-9">
                              <input type="submit" class="btn blue" value="Submit"  />
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/users/profile'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                     </div>
                </div>
                </div>
                </div>
      </div>  <br><br>
 
      



