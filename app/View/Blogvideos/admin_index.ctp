<?php $this->Html->addCrumb('Blog Video', '/admin/blogvideos'); $paginationVariables = $this->Paginator->params();?>
<script type="text/javascript">
  function fancyBoxVideo(id){
    //alert(id);
    $("#popupVideo"+id).fancybox();
  }
</script>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Blogvideos'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                        <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Blogvideos'), 'action' => 'index', 'admin' => true))); ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <div class="btn-group pull-right">
                        <!-- <div class="btn-group" style="padding-right:15px;">
	                        <?php echo $this->Html->link(__('New Blogvideo <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?> 
                        </div> -->
                       <!--  <button class="btn default"  onclick="window.location.href='<?php echo $this->webroot.'admin/Blogvideos/export'; ?>'">Export to CSV <i class="fa fa-download"></i> </button> -->
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('blog_id'); ?></th><th><?php echo $this->Paginator->sort('youtube_link'); ?></th><th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php  if(isset($blogvideos) && sizeof($blogvideos)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($blogvideos as $blogvideo): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        					<td>
											<?php echo $this->Html->link($blogvideo['Blog']['title'], array('controller' => 'blogs', 'action' => 'view', $blogvideo['Blog']['id'])); ?>
		</td>
    		<td>
          <img src="<?php echo $this->webroot ?>img/yplayer.png" onclick="javascript:fancyBoxVideo(<?php echo $blogvideo['Blogvideo']['id']; ?>);" id="popupVideo<?php echo $blogvideo['Blogvideo']['id']; ?>" href="#Video<?php echo $blogvideo['Blogvideo']['id']; ?>">  

              <div id="Video<?php echo $blogvideo['Blogvideo']['id']; ?>" style="display: none; width: 444px; height: 341px">
              <iframe width="100%" height="338" src="//www.youtube.com/embed/<?php echo $blogvideo['Blogvideo']['youtube_link']; ?>"  frameborder="0" allowfullscreen></iframe> </div>

            </td>
    		<td><?php echo h($boolean_values_status[$blogvideo['Blogvideo']['status']]); ?>&nbsp;</td>
											<td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $blogvideo['Blogvideo']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $blogvideo['Blogvideo']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Blogvideos'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Blogvideo][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
