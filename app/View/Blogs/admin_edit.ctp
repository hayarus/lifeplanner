<?php $this->Html->addCrumb('Blog', '/admin/blogs'); $paginationVariables = $this->Paginator->params();?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/ckeditor/ckeditor.js"></script>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	CKEDITOR.config.contentsCss = '<?php echo $this->webroot; ?>css/style.css';
	var error1 = $('.alert-danger');
	$('#BlogAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Blog][author]" : {required : true},
		"data[Blog][title]" : {required : true},
		"data[Blog][subtitle]" : {required : true},
		"data[Blog][blogcategory_id]" : {required : true},
		"data[Blog][content]" : {required: function() 
		                    {
		                    CKEDITOR.instances.StaticpageContent.updateElement();
		                    }},
		"data[Blog][status]" : {required : true},
		},
		messages:{
		"data[Blog][author]" : {required :"Please enter author."},
		"data[Blog][title]" : {required :"Please enter title."},
		"data[Blog][subtitle]" : {required :"Please enter subtitle."},
		"data[Blog][blogcategory_id]" : {required :"Please enter blogcategory_id."},
		"data[Blog][content]" : {required :"Please enter content."},
		"data[Blog][status]" : {required :"Please enter status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "data[Blog][content]"){
				$("span.err1").append(error);
			}else {
				error.insertAfter(element);
			}
		},
	});
	<?php foreach($this->request->data['Blogvideo'] as $video){ ?>

		$('#deleteVideo<?php echo $video['id']; ?>').click(function(event){	 
		    $.ajax({
		        type:'POST',
		        url:"<?php echo $this->webroot; ?>admin/blogs/blogvideodelete/<?php echo $video['id']; ?>",
		        success: function(result){ 
		           if(result == 1){
		           	refreshPage();
		           }
		        }
		    })
		});		
		
	<?php } ?>
	<?php foreach($this->request->data['Blogimage'] as $image){ ?>

		$('#deleteImage<?php echo $image['id']; ?>').click(function(event){	 
		    $.ajax({
		        type:'POST',
		        url:"<?php echo $this->webroot; ?>admin/blogs/blogimagedelete/<?php echo $image['id']; ?>",
		        success: function(result){ 
		           if(result == 1){
		           	refreshPage();
		           }
		        }
		    })
		});		

	<?php } ?>
	<?php foreach($blogdiscussions as $blogdiscussion){ ?>

		$('#rejectStatus<?php echo $blogdiscussion['Blogdiscussion']['id']; ?>').click(function(event){	 
		    $.ajax({
		        type:'POST',
		        url:"<?php echo $this->webroot; ?>admin/blogs/rejectstatus/<?php echo $blogdiscussion['Blogdiscussion']['id']; ?>",
		        success: function(result){ 
		           if(result == 1){
		           	refreshPage();
		           }
		        }
		    })
		});		

	<?php } ?>
	<?php foreach($blogdiscussions as $blogdiscussion){ ?>

		$('#approveStatus<?php echo $blogdiscussion['Blogdiscussion']['id']; ?>').click(function(event){	 
		    $.ajax({
		        type:'POST',
		        url:"<?php echo $this->webroot; ?>admin/blogs/approvestatus/<?php echo $blogdiscussion['Blogdiscussion']['id']; ?>",
		        success: function(result){ 
		           if(result == 1){
		           	refreshPage();
		           }
		        }
		    })
		});		

	<?php } ?>

	$('#addNew').click(function(){
    	 //$("#paymentContainer").clone().appendTo("#paymentDiv");
    	 var content = '<div class="form-group"><label class="col-md-3 control-label" for=""></label><div class="col-md-4"><div class="input text"><input name="data[Blogvideo][youtube_link][]" class="form-control" type="text" id="ImageYoutubeLink" placeholder="Enter Video Id. Eg: SiwxNBM_UXc"></div></div></div>';
    		 
    		 $("#newDiv").append(content);
   			 
    });
});
function refreshPage(){
    window.location.reload();
} 

function fancyBoxVideo(id){
    //alert(id);
    $("#popupVideo"+id).fancybox();
  }
</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Edit Blog'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form blogs">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Blog', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('id<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('author<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('author', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('title<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('subtitle<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('subtitle', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('blog category<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('blogcategory_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('content<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-8'>
								<?php echo $this->Form->input('content', array('class' => 'ckeditor form-control', 'label' => false, 'required' => false, 'id' => 'StaticpageContent'));?>
								<span class="err1"></span>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false , 'options' => $boolean_values_status));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/blogs'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</br>
<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Add Videos'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
						<button data-close="alert" class="close"></button>
						You have some form errors. Please check below.
						</div>

						<?php echo $this->Form->create('Blogvideo', array('class' => 'form-horizontal','method'=>'post', 'url' => '/admin/Blogvideos/add/'.$id)); ?>

						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">Youtube Link</label>
								<div class='col-md-4'>
									<?php echo $this->Form->input('youtube_link', array('class' => 'form-control', 'label' => false, 'required' => false, 'name' => 'data[Blogvideo][youtube_link][]', 'placeholder' => 'Enter Video Id. Eg: SiwxNBM_UXc'));?>

								</div>
								<button class="btn default" type="button" id="addNew"><i class="fa fa-plus"></i></button>
															
															
							</div>
							<div id="newDiv"></div>

							<div class="form-group"> 
								<div class="col-md-12">
								
									<?php foreach($this->request->data['Blogvideo'] as $video){ 

									?>	
									<div style="float:left; margin-right:10px;text-align: center;">
										<img src="<?php echo $this->webroot ?>img/yplayer.png" onclick="fancyBoxVideo(<?php echo $video['id']; ?>);" id="popupVideo<?php echo $video['id']; ?>" href="#Video<?php echo $video['id']; ?>">  
							           
							           <div id="Video<?php echo $video['id']; ?>" style="display: none; width: 444px; height: 341px">
							          <iframe width="100%" height="338" src="//www.youtube.com/embed/<?php echo $video['youtube_link']; ?>"  frameborder="0" allowfullscreen></iframe> </div>

										</br><a href="" id="deleteVideo<?php echo $video['id']; ?>" ><img src="<?php echo $this->webroot; ?>img/delete.png" width="12" ></a>
								
									</div>
									<?php } ?>
							</div>
							
						</div>

							<div class="form-actions fluid">
	                            <div class="col-md-offset-3 col-md-9">
	                                <button type="submit" class="btn blue">Submit</button>
	                                <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/blogs'; ?>'">Cancel</button>
	                            </div>
	                        </div>
						</div>
						<?php echo $this->Form->end(); ?>
						

					<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Add Images'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body form users">
						<div class="alert alert-danger display-hide">
							<button data-close="alert" class="close"></button>
							You have some form errors. Please check below.
						</div>
						<?php echo $this->Form->create('Blogimage', array('class' => 'form-horizontal','enctype'=>'multipart/form-data','method'=>'post')); ?>

						<div class="form-body">  
							<div class="form-group">
								<label class="col-md-3 control-label" for="">Blog Images</label>
								<div class="col-md-4">
									<div class="input text">
										<p><a class=" btn blue" data-toggle="modal" href="#uploadpop">Upload  Images</a></p> 
										(Please upload 850px X 480px for better quality)
									</div>
								</div>							
							</div>

							<div class="form-group"> 
								<div class="col-md-12">
								<p>
									<?php foreach($this->request->data['Blogimage'] as $image){ ?>	
									<div style="float:left; margin-right:10px;text-align: center;"><a  data-toggle="modal" href="#viewimages<?php echo $image['id']; ?>"><img src="<?php echo $this->webroot.$blogImageSmall.$image['name']; ?>" width="50" ></a>
										</br><a href="" id="deleteImage<?php echo $image['id']; ?>" ><img src="<?php echo $this->webroot; ?>img/delete.png" width="12" ></a>
								</p>
									</div>
									<?php } ?>
							</div>

							<?php foreach($this->request->data['Blogimage'] as $image){
							 ?>
							<div id="viewimages<?php echo $image['id']; ?>" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Blog Images</h4>
										</div>
										<div class="modal-body">
											
											
											<img src="<?php echo $this->webroot.$blogImageSmall.$image['name']; ?>" width="555" >
											
										</div>
										<div class="modal-footer">
											<button type="button" data-dismiss="modal" class="btn">Close</button>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>


							</div>

							<?php echo $this->Form->end(); ?>
							<div id="uploadpop" class="modal fade modal-scroll" tabindex="-1" data-replace="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="refreshPage()"></button>
										<h4 class="modal-title">Drag and Drop or Click to Upload Multiple Images</h4>
									</div>
									<div class="modal-body">
										<form action="<?php echo $this->webroot; ?>blogs/multipleimage/<?php echo $id; ?>" class="dropzone" id="my-dropzone">

										</form>
										
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn" onClick="refreshPage()">Close</button>
									</div>
								</div>
							</div>
						</div>

					<!-- END FORM-->

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</br>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<div class="tab-pane active" id="tab_0">
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-reorder"></i><?php echo __('Blog Discussions'); ?>                        
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse"></a>
						</div>
					</div>
					<div class="portlet-body"><br />
						<div class="table-toolbar"></div>
						<div class="table-responsive">
		                <table class="table table-striped table-bordered table-hover " id="sample_1">
		                  <thead>
		                      <tr>
		                        <th>#</th>  
		                        <th><?php echo $this->Paginator->sort('blog_id'); ?></th><th><?php echo $this->Paginator->sort('user_id'); ?></th><th><?php echo $this->Paginator->sort('content'); ?></th><th><?php echo $this->Paginator->sort('status'); ?></th><th>Action</th>
		                      </tr>
		                  </thead>
		                  <?php  if(isset($blogdiscussions) && sizeof($blogdiscussions)>0) {?>
		                  <tbody>
		                  <?php $slno=0; foreach ($blogdiscussions as $blogdiscussion): $slno++?>
		                  <tr>
		                     <td><?php echo $slno; ?></td>
		        			 <td><?php echo h($blogdiscussion['Blog']['title']); ?></td>
		        			 <td><?php echo h($blogdiscussion['User']['first_name']." ".$blogdiscussion['User']['last_name']); ?></td>
							 <td><?php echo h($blogdiscussion['Blogdiscussion']['content']); ?>&nbsp;</td>
							 <td><?php echo $boolean_values_status[$blogdiscussion['Blogdiscussion']['status']];?></td>
							 <td><?php  if($blogdiscussion['Blogdiscussion']['status'] == 1){?>
							 	<a href="" id="rejectStatus<?php echo $blogdiscussion['Blogdiscussion']['id']; ?>"  class="btn default btn-xs red">Reject</a><?php
							 	}else{ ?>
							 		<a href="" id="approveStatus<?php echo $blogdiscussion['Blogdiscussion']['id']; ?>" class="btn default btn-xs green">Approve</a><?php
							 	}?>&nbsp;

							 </td>
							</tr> 
						<?php endforeach; ?> 
					<?php } else {?> 
						<tr><td colspan='7' align='center'>No Records Found.</td></tr> 
					<?php }?> 
				</tbody> 
			</table> 
		</div>

					<!-- END FORM-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br/>

