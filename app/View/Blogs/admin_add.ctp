<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/ckeditor/ckeditor.js"></script>
<?php $this->Html->addCrumb('Blog', '/admin/blogs'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	CKEDITOR.config.contentsCss = '<?php echo $this->webroot; ?>css/style.css';
	var error1 = $('.alert-danger');
	$('#BlogAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Blog][author]" : {required : true},
		"data[Blog][title]" : {required : true},
		"data[Blog][subtitle]" : {required : true},
		"data[Blog][blogcategory_id]" : {required : true},
		"data[Blog][content]" : {required: function() 
		                    {
		                    CKEDITOR.instances.StaticpageContent.updateElement();
		                    }},
		"data[Blog][status]" : {required : true},
		},
		messages:{
		"data[Blog][author]" : {required :"Please enter author."},
		"data[Blog][title]" : {required :"Please enter title."},
		"data[Blog][subtitle]" : {required :"Please enter subtitle."},
		"data[Blog][blogcategory_id]" : {required :"Please select an option."},
		"data[Blog][content]" : {required :"Please enter content."},
		"data[Blog][status]" : {required :"Please enter status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "data[Blog][content]"){
				$("span.err1").append(error);
			}else {
				error.insertAfter(element);
			}
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Blog'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form blogs">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Blog', array('class' => 'form-horizontal')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('author<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('author', array('class' => 'form-control', 'label' => false, 'value' => 'admin','required' => false));?>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('title<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => false,  'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('subtitle<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('subtitle', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('blog category<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('blogcategory_id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('content<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-8'>
								<?php echo $this->Form->input('content', array('class' => 'ckeditor form-control', 'label' => false, 'required' => false, 'id' => 'StaticpageContent'));?>
								<span class="err1"></span>
							</div>
						</div>
							<div class="form-group">
							 <?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false ,'options' => $boolean_values_status));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/blogs'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>