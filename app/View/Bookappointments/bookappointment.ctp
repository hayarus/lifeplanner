<style>
    #main h2 {
	color: #034EA2;
	border-bottom: 1px solid;
	padding-bottom: 10px;
	position: relative;
	padding-left: 65px;
}
#main h2:before {
	border-bottom: 4px solid;
	width: 220px;
	bottom: -2px;
	position: absolute;
	left: 0;
}
.input-group-addon {
    border-radius: 4px;
    width: 40px!important;
    /* padding: 4px 12px; */
    font-size: 12px!important;
    font-weight: normal!important;
    line-height: 1!important;
    color: #fff!important;
    text-align: center!important;
    background: #9400D3!important;
    border: 1px solid #9400D3!important;
}
.input-group-addon .fa {
	font-size: 22px;
}
.form-error {
	left: 0;
	position: absolute;
	top: -20px;
}
.formbtn {
    background: none repeat scroll 0 0 #f15a22;
    border: 0 none;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 46px;
    padding: 0 34px;
    margin-top: 5px;
    text-transform: uppercase;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.wd-50 {
	width: 50% !important;
	float: left;
}
.wd-50:last-child {
	width: 50% !important;
	float: left;
	border-left: 0!important;
}
.form {
	background: none;
	border: solid 1px #e7e5e5;
}
.ui-datepicker-trigger {
	position: absolute;
	right: 0;
	top: 5px;
	cursor: pointer;
}
.captcha_section {
	width: 110px;
	margin: 0;
	position: absolute;
	right: 1px;
	top: 1px;
	height: 36px;
	background: #FFFFFF;
	border-left: 1px solid #034EA2;
}
.captcha_section img {
	margin-top: 5px;
}
.has-error .captcha_section {
	border-left: 1px solid #A94442;
}
.has-error .form-control {
	color: #a94442;
}

/** placeholder color change on error occurs **/	
.has-error input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
color:    #a94442;
}
.has-error input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color:    #a94442;
opacity:  1;
}
.has-error input::-moz-placeholder { /* Mozilla Firefox 19+ */
color:    #a94442;
opacity:  1;
}
.has-error input:-ms-input-placeholder { /* Internet Explorer 10-11 */
color:    #a94442;
}
.has-success .captcha_section {
	border-left: 1px solid #3C763D;
}
.captcha_section #the_captcha {
	width: 70% !important;
	border: 0;
}
.formbtn {
	border-radius: 4px;
	font-weight: bold;
}
.input-group .form-control {
	font-size: 11px!important;
	padding: 6px 8px;
}
.form {
	display: block;
	padding: 20px;
}
.left_container {
	padding: 15px 15px 0 0;
}
.has-error .form-control::-webkit-input-placeholder {
color:#a94442 !important;
}
.form-control-agreed {
	line-height: 24px;
	overflow: auto
}
.custom-combobox {
	position: relative;
	display: inline-block;
}
.custom-combobox-toggle {
	position: absolute;
	top: 0;
	bottom: 0;
	margin-left: -1px;
	padding: 0;
}
.custom-combobox-input {
	margin: 0;
	padding: 5px 10px;
}
.custom-combobox-city .custom-combobox {
	display: block;
}
.custom-combobox-city input.custom-combobox-input {
	background-color: #ffffff;
	border: 1px solid #034ea2;
	border-radius: 0 4px 4px 0;
	width: 100%;
	height: 38px;
	font-size: 11px;
}
.custom-combobox-city .ui-button {
	top: 0px;
	right: 0px;
	border: none;
	margin: 2px;
	border: solid 1px #fff;
	outline: solid 1px #707070;
	width: 16px;
	background: #f2f2f2;
	background: -moz-linear-gradient(top, #f2f2f2 0%, #d1d1d1 100%);
	background: -webkit-linear-gradient(top, #f2f2f2 0%, #d1d1d1 100%);
	background: linear-gradient(to bottom, #f2f2f2 0%, #d1d1d1 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f2f2', endColorstr='#d1d1d1', GradientType=0 );
}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
color: #000;
opacity:1;
}
::-moz-placeholder { /* Firefox 19+ */
color: #000;
opacity:1;
}
:-ms-input-placeholder { /* IE 10+ */
color: #000;
opacity:1;
}
:-moz-placeholder { /* Firefox 18- */
color: #000;
opacity:1;
}
.has-error input.custom-combobox-input {
	border-color: #a94442!important;
}
.hilight_box {
	background-color: #f2dede;
	color: #a94442;
	border-color: #a94442
}
.nothilight_box {
  background-color: #dff0d8;
  border-color: #3c763d;
  color: #3c763d;
}
.custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
}
.sidebar{
    display:none;
}
</style>

<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Book Your Appointment</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Book an Appointment
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
         <section class="flat-row padding-v1" id="main">
            <div class="container">
                 <div class="row">
                    <div id="content" class="col-md-9">
                        <section id="" class="cb"><div class="wrapper"><div id="system-message-container"></div><div class="left_container"> 
                            <div class="form">
     
                                <?php echo $this->Form->create('Bookappointment', array('url'=>array('controller'=>'bookappointments','action'=>'bookappointment'),'autocomplete' =>'off')); ?>
                            <p class="form-error"></p>
                            <ul class="formcol2" style="list-style:none;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"> <i class="fa fa-user" aria-hidden="true"></i></span> 
                                            <input class="form-control wd-50" placeholder="First name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" name="txt_fname" id="txt_fname" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" required="true"> 
                                            <input class="form-control wd-50" placeholder="Last name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" name="txt_lname" id="txt_lname" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" >
                                        </div>
                                        <span for="txt_fname" class="help-block"></span>
                                        <span for="txt_lname" class="help-block"></span>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span> 
                                            <input placeholder="Mobile number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile number'" class="form-control" type="text" name="txt_mobile" id="txt_mobile" maxlength="10" data-validation="number length" data-validation-length="10-10" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_mobile" class="help-block"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> 
                                            <input placeholder="Email ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID'" class="form-control" type="text" name="txt_email" id="txt_email" maxlength="100" data-validation="email" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_email" class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-building-o" aria-hidden="true"></i></span> 
                                            <input placeholder="City" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City'" class="form-control" type="text" name="txt_city" id="txt_city" maxlength="100" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_city" class="help-block"></span>
                                    </div>
                                    </div>
                                    <div class="row"><div class="col-md-6"><div class="input-group"> <span class="input-group-addon"><i class="fa fa-university" aria-hidden="true"></i></span> <select class="form-control" name="txt_nearest_branch" id="txt_nearest_branch" data-validation="required" data-validation-error-msg=" ">
    <option value=""> Select Nearest Branch</option>
    <option value="Kottayam">Kottayam</option>
    <option value="Kochi">Kochi</option>
    <option value="UAE">UAE</option>
    <option value="Malaysia">Malaysia</option>
    <option value="Canada">Canada</option>
    </select></div>
    <span for="txt_nearest_branch" class="help-block"></span>
    </div>

    <div class="col-md-6"><div class="input-group"> <span class="input-group-addon" ><i class="fa fa-birthday-cake" aria-hidden="true"></i></span> <input class="form-control" placeholder="Date of Birth" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date of Birth'" type="text"  name="txt_dob" id="txt_dob"   maxlength="100" data-validation="required" data-validation-error-msg=" " value=""   /></div>
     <span for="txt_dob" class="help-block"></span>
    </div>
    </div>
                                        <div class="row">
                                        <div class="col-md-6"><div class="input-group"> 
                                                <span class="input-group-addon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
                                                <select class="form-control" name="txt_intake" id="txt_intake" data-validation="required" data-validation-error-msg=" ">
                                                    <option value="">Select Intake</option>
                                                    <?php $num = 6;  for($i=0; $i<=$num; $i++){ $year= $currentyear+$i;?>
                                                    <option value=<?php echo $year; ?>><?php echo $year; ?></option>
                                                    <?php } ?>
                                                    
                                                </select>
                                        </div>
                                        <span for="txt_intake" class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"> <i class="fa fa-files-o" aria-hidden="true"></i></span> 
                                            <select class="form-control" name="txt_level" id="txt_level" data-validation="required" data-validation-error-msg=" " required="true">
                                                <option value="">Select Level</option>
                                                <option value="UG">Undergraduate</option>
                                                <option value="PG">Post&nbsp;Graduate</option> 
                                            </select>
                                        </div>
                                         <span for="txt_level" class="help-block"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                        <span class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span> 
                                        <select class="form-control" name="txt_studyarea" id="txt_studyarea" data-validation="required" data-validation-error-msg=" " required="true">
                                            <option value="">Select your area of interest</option> 
                                    <?php foreach($areaofstudy as $key=>$value){ ?>
	                			    <option value=<?php echo $value; ?>><?php echo $value;?></option>
                                    <?php }  ?>  
                                        </select>
                                    </div>
                                     <span for="txt_studyarea" class="help-block"></span>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group"> 
                                    <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span> 
                                    <select class="form-control wd-50" name="txt_country_pref_1" id="txt_country_pref_1" data-validation="required" data-validation-error-msg=" " required="true">
                                        <option value="">Select Country Pref1</option>
                                        
                		             <?php foreach($prefcountries as $key=>$value){ ?>
	                			    <option value=<?php echo $value; ?>><?php echo $value;?></option>
                                    <?php }  ?>
                                        <option value="Others">Others</option> 
                                    </select> 
                                    <select class="form-control wd-50" name="txt_country_pref_2" id="txt_country_pref_2" data-validation="required" data-validation-error-msg=" ">
                                        <option value="">Select Country Pref2</option>
                                        
                		             <?php foreach($prefcountries as $key=>$value){ ?>
	                			    <option value=<?php echo $value; ?>><?php echo $value;?></option>
                                    <?php }  ?>
                                        <option value="Others">Others</option> 
                                    </select>
                                </div>
                                <span for="txt_country_pref_1" class="help-block"></span>
                                <span for="txt_country_pref_2" class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group"> 
                                <span class="input-group-addon"><i class="fa fa-question-circle" aria-hidden="true"></i></span> 
                                <select class="form-control" name="txt_helpyou" id="txt_helpyou" data-validation="required" data-validation-error-msg=" " >
                                    <option value="">Select How may we help you</option>
                                    <option value="Personalised Evaluation - Personality and Career Mapping">Personalised Evaluation - Personality and Career Mapping</option>
                                    <option value="Higher Education and Career Management">Higher Education and Career Management</option>
                                    <option value="Global Admissions, Applications, Scholarships, Visas and Post Visa Services">Global Admissions, Applications, Scholarships, Visas and Post Visa Services</option>
                                    <option value="Test Preparation">Test Preparation</option>
                                    <option value="English Language Training">English Language Training</option>
                                    <option value="German Language Training">German Language Training</option>
                                    <option value="Study Skills">Study Skills</option>
                                    <option value="Higher Education Pathways">Higher Education Pathways</option>
                                    <option value="Global Education Events and University Interaction">Global Education Events and University Interaction</option> 
                                </select>
                            </div>
                             
                        </div>
                        <div class="col-md-6">
                            <div class="input-group"> 
                                
                            </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group"> 
                                <span class="input-group-addon"> 
                                <input id="inpu6" type="checkbox" checked="checked" name="accept" value="accept" data-validation="required" data-validation-error-msg=" "> </span>
                                <div class="form-control form-control-agreed"> I would like Lifeplanner to send me regular updates relating to University Admissions via SMS / Email / Call.</div>
                            </div>
                        </div>
                    </div>
                    <div class="row"> 
                    <input type="hidden" name="txt_form_name" value="Book-Your-Appointment"> 
                    <input type="hidden" name="txt_type" value="Book Your Appointment"> 
                    <input type="hidden" name="security_code" value="security_code"> 
                    <input type="hidden" name="txt_url" value= "">
                    <input type="hidden" name="txt_page_type" value="">
                    <div class="col-md-12"> 
                    
                    <input name="btn_submit" type="submit" class="formbtn" value="Submit" style=" background: none repeat scroll 0 0 #7ABA7A;border: 0 none;color: #fff;cursor: pointer;font-size: 15px;height: 46px; padding: 0 34px;margin-top: 5px;text-transform: uppercase;transition: all .5s ease 0;-webkit-transition: all .5s ease 0;"></div></div>  </ul>
           <?php echo $this->Form->end(); ?>
                    </div> 
               </div>
       </section> 
       </div> 
     </section>
</div>

     <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap1.css">
     <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/font-awesome1.css">

    

<!---jquery Validation-->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
 <script src="<?php echo $this->webroot; ?>js/jquery-ui1.js"></script> 
     <script src="<?php echo $this->webroot; ?>js/bootstrap.min1.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<!---jquery Validation End-->

<script type="text/javascript">
     
     $(document).ready(function(){
        
      var error1 = $('.alert-danger');
  $('#BookappointmentBookappointmentForm').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "txt_fname" : {required : true},
    // "txt_lname" : {required : true},
    "txt_email" : {required : true,email:true},
    "txt_mobile" : {required : true, minlength:10,
           maxlength:10},
     "txt_city" : {required : true},
    "txt_nearest_branch" : {required : true},
    "txt_dob" : {required : true},
    "txt_intake" : {required : true},
    "txt_level" : {required : true},
    "txt_studyarea" : {required : true},
    "txt_country_pref_1" : {required : true},
   // "txt_country_pref_2" : {required : true},

          
    },
    messages:{
     "txt_fname" : {required : "Please enter name"},
     // "txt_lname" : {required : "Please enter lastname"},
    "txt_email" : {required : "Please enter email id.",email:"Please enter valid email id."},
    "txt_mobile" : {required : "Please enter mobile number",
           minlength:"Mobile number must need 10 digits",
           maxlength:"Mobile number only need 10 digits"},
     "txt_city" : {required : "Please enter city."},
    "txt_nearest_branch" : {required : "Please select nearest branch."},
    "txt_dob" : {required : "Please enter date of birth."},
    "txt_intake" : {required : "Please select intake."},
    "txt_level" : {required : "Please select level."},
    "txt_studyarea" : {required : "Please select area of study."},
    "txt_country_pref_1" : {required : "Please select country of preference"},
   // "txt_country_pref_2" : {required : "Please select country 2."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },
  });
  $( "#txt_dob" ).datepicker();
});
</script>
 
<style>
    .help-block{
        color:#b20828!important;
        margin-top: -13px!important;
    }
    body {
	font-family: Arial, Helvetica, sans-serif;
}

table {
	font-size: 1em;
}

.ui-draggable, .ui-droppable {
	background-position: top;
}
</style>