<?php $this->Html->addCrumb('Photogallery', '/admin/photogalleries'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	var error1 = $('.alert-danger');
	$('#PhotogalleryAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Photogallery][titile]" : {required : true},
"data[Photogallery][image]" : {required : true,accept : "png|jpg|jpeg|gif", filesize: 1000000},
		},
    "data[Photogallery][description]" : {required : true},
   // "data[Photogallery][university]" : {required : true},
    //"data[Photogallery][country_id]" : {required : true},
		messages:{
		"data[Photogallery][titile]" : {required :"Please enter name."},
"data[Photogallery][image]" : {required :"Please upload image.", accept : "Please upload an image in png, jpg, jpeg or gif format.", filesize: "File size should be less than 1MB."},
		},
    "data[Photogallery][description]" : {required :"Please enter course."},
    //"data[Photogallery][university]" : {required :"Please enter university."},
    //"data[Photogallery][country_id]" : {required :"Please select country."},


		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
        errorPlacement: function(error, element) {
       if(element.attr("name") =="data[Photogallery][image]"){
          $(".imageErrCont1").addClass("help-block");
          $(".imageErrCont1").text($(error).text());       
           // error.insertAfter(".fileupload-new");
        }
        else {
          error.insertAfter(element);
        }
      },
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Photogallery'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form photogalleries">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Photogallery', array('class' => 'form-horizontal','controller'=>'photogalleries','type'=>'file')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('name<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                            <div class="form-group">
							 <?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
                           <!--<div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 127px; max-height: 127px; line-height: 20px;"></div>
                                <div>
                                  <span class="btn btn-file btn default"><span class="fileupload-new">Select image</span>
                                  <span class="fileupload-exists">Change</span>
                                  <input type="file" id="imgUploadSec" name="data[Photogallery][image]" class="default" /></span>
                                  <a href="#" class="btn fileupload-exists btn default" data-dismiss="fileupload">Remove</a>
                                </div>-->
                           
								<?php echo $this->Form->input('image', array('class' => 'form-control', 'label' => false, 'required' => false,'type'=>'file'));?>
						

                            <span class="imageErrCont"></span>
                      </div>
						</div>
          <div class="form-group">
               <?php echo $this->Form->label('description<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
              <div class='col-md-4'>
                <?php echo $this->Form->input('description', array('class' => 'form-control', 'label' => false, 'required' => false));?>
              </div>
            </div>
            
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/photogalleries'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->Html->addCrumb('Photogallery', '/admin/photogalleries'); $paginationVariables = $this->Paginator->params();?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('photogalleries'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
               
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('titile','Name'); ?></th><th><?php echo $this->Paginator->sort('description'); ?></th>
                        <th>Preview</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php //pr($photogalleries);exit;
                  if(isset($photogalleries) && sizeof($photogalleries)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($photogalleries as $Photogallery): $slno++?>
                  <tr><td>
                     <?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        							        <td><?php echo h($Photogallery['Photogallery']['title']); ?>&nbsp;</td><td><?php echo h($Photogallery['Photogallery']['description']); ?>&nbsp;</td>
											
											<td><?php if(!empty($Photogallery['Photogallery']['image'])){ ?><img src="<?php echo $this->webroot .$PhotogalleryImagePath.$Photogallery['Photogallery']['image']; ?>" alt="" width="150" height="150"/> <?php } else{?> No Video Available <?php } ?></td>
											<!--<td>
                							<a href="#myModal" class="" data-toggle="modal"><i class="fa fa-search" style="margin-top: 12px;"></i></a>
                							</td>-->
                							    <div id="myModal" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                            <h4 class="modal-title"> Image</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                        	<?php  if(isset($Photogallery['Photogallery']['image'])){?>
                                            							<img src="<?php echo $this->webroot .$PhotogalleryImagePath.$Photogallery['Photogallery']['image']; ?>" alt="" />
                                            							<?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											<td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $Photogallery['Photogallery']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $Photogallery['Photogallery']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('photogalleries'), 'action' => 'add'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Photogallery][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div>
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>


<script type="text/javascript">
$(document).ready(function(){
    /* Get iframe src attribute value i.e. YouTube video url
    and store it in a variable */
    var url = $("#tVideo").attr('src');
    
    /* Assign empty url value to the iframe src attribute when
    modal hide, which stop the video playing */
    $("#myModal").on('hide.bs.modal', function(){
        $("#tVideo").attr('src', '');
    });
    
    /* Assign the initially stored url back to the iframe src
    attribute when modal is displayed again */
    $("#myModal").on('show.bs.modal', function(){
        $("#tVideo").attr('src', url);
    });
});
</script>

<style type="text/css">
	 .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
</style>
