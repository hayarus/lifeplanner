<?php 
$seesionData =  $this->Session->read("sessionUserInfo");
?>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <nav id="mainnav" class="mainnav">
                                <ul class="menu"> 
                                    <li class="home">
                                        <a href="<?php echo $this->webroot;?>">HOME</a>
                                        
                                    </li>
                                    <li>
                                        <a href="#">ABOUT US</a>
                                        <ul class="submenu">
                                            <li><a href="<?php echo $this->webroot;?>universities/videogalleries">EXPERTS SPEAK</a></li>
                                            <li><a href="<?php echo $this->webroot;?>colleges/photogalleries">ALBUM</a></li>
                                            <li><a href="<?php echo $this->webroot;?>colleges/gallery">OUR STUDENTS</a></li>
                                            <li><a href="<?php echo $this->webroot;?>colleges/testimonials">TESTIMONIALS</a></li>
                                            <!--<li><a href="<?php //echo $this->webroot;?>colleges/lifeatgurukul">LIFE AT GURUKUL</a></li>-->
                                        </ul>
                                       
                                    </li>

                                    <li>
                                        <a href="#">STUDY ABROAD</a>
                                        <ul class="submenu">
                                            <li><a href="<?php echo $this->webroot;?>countries/englishspeakingcountries">ENGLISH SPEAKING COUNTRIES</a></li>
                                            <li><a href="<?php echo $this->webroot;?>countries/europeancountries">EUROPEAN COUNTRIES</a></li>
                                        </ul> 
                                       
                                    </li>
                                   <li>
                                        <a href="#">IMMIGRATION</a>
                                        <ul class="submenu">
                                            <li><a href="<?php echo $this->webroot;?>countries/canada">CANADA</a></li>
                                            <li><a href="<?php echo $this->webroot;?>countries/australia">AUSTRALIA</a></li>
                                            <!--<li><a href="<?php //echo $this->webroot;?>colleges/lifeatgurukul">LIFE AT GURUKUL</a></li>-->
                                        </ul>
                                       
                                    </li>
                                    <!--<li>
                                        <a href="#">STUDIES</a>
                                        <ul class="submenu">
                                            
                                            <li><a href="<?php// echo $this->webroot;?>colleges/studyingermany">STUDY IN GERMANY</a></li>
                                            <li><a href="<?php// echo $this->webroot;?>colleges/medicallist">STUDY MBBS</a></li>
                                            
                                        </ul>
                                       
                                    </li>-->
                                    <!--<li>
                                        <a href="<?php //echo $this->webroot;?>colleges/collegelist">LIST OF UNIVERSITIES</a>
                                       
                                    </li> -->
                                    <li>
                                        <a href="#">UNIVERSITIES</a>
                                        <ul class="submenu">
                                            <li><a href="<?php echo $this->webroot;?>universities/collegelist/UG">GRADUATE COURSES</a></li>
                                            <li><a href="<?php echo $this->webroot;?>universities/collegelist/PG">POSTGRADUATE COURSES</a></li>
                                            <li><a href="<?php echo $this->webroot;?>universities/medicallist">MBBS</a></li>
                                            <li><a href="<?php echo $this->webroot;?>universities/nursingingermany">NURSING IN GERMANY</a></li>
                                            <li><a href="<?php echo $this->webroot;?>universities/studyinpoland">UNIVERSITY OF LODZ </a></li>
                                        </ul>
                                       
                                    </li>
                                    <li><a href="<?php echo $this->webroot;?>colleges/languagecoaching">LANGUAGE COACHING</a></li>
                                    <li>
                                        <a href="<?php echo $this->webroot;?>universities/blog">BLOG</a>
                                       
                                    </li>  
                                    <li>
                                        <a href="<?php echo $this->webroot;?>universities/contact_us">CONTACT US</a>
                                       
                                    </li>
                                    <li>
                                    <a href="tel:+91 9072222911">+91 9072222911</a>
                                       
                                    </li>
                                    <?php if(!empty($seesionData)&& $seesionData['group_id']==2){?>
                                        <li>
                                            <a href="#"><?php echo "Hi,".strtoupper($seesionData['first_name']);?></a>

                                            <ul class="submenu">
                                                <li><a href="<?php echo $this->webroot;?>universities/logout">LOGOUT</a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                   <!--  <li>
                                        <div class ="div" id="google_translate_element"></div>   
                                       
                                    </li>  --> 
                                    <!--<li>
                                       
                                        <a href="#" data-toggle="popover" title="Send your CV to" data-content="career@lifeplanneruniversal.com" data-placement="bottom" data-trigger="hoveer focus">WE ARE HIRING</a>
                                       
                                    </li> -->

                                                                
                                </ul><!-- /.menu -->
                            </nav><!-- /.mainnav -->
