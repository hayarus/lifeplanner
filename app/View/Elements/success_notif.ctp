
<script>
$(document).ready(function(){ 
	Lobibox.notify(
	    'success', {
	    	position: 'center top',
	    	title:'<?php echo $message ?>',
	        msg: 'We will get in touch with you soon.',
	        width:600,
	        sound: false
	});
});
</script>
