<?php $user = $this->Session->read('Auth.User'); 
      $this->set('tabPermission', $this->Session->read("tabPermission"));
?>
<ul class="page-sidebar-menu">
<li class="sidebar-toggler-wrapper">
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                    <div class="sidebar-toggler hidden-phone">
                    </div>
                    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                </li>
                <br>
                
                <li class=" <?php if($this->request->params['controller']=='dashboard' && ($this->request->params['action']=='admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/dashboard">
                    <i class="fa fa-dashboard"></i>
                    <span class="title">
                        Dashboard
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                
                <li class=" <?php if($this->request->params['controller']=='universities' && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?> " >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/universities">
                    <i class="fa fa-barcode"></i>
                    <span class="title">
                        University Management
                    </span> 
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                <li class=" <?php if(($this->request->params['controller']=='courses')||($this->request->params['controller']=='Advertisements') && ($this->request->params['action']=='admin_add' || $this->request->params['action'] == 'admin_edit' || $this->request->params['action'] == 'admin_view' || $this->request->params['action'] == 'admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/courses">
                    <i class="fa fa-user"></i>
                    <span class="title">
                     Course Management <span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
    
                <li class=" <?php if(($this->request->params['controller']=='areaofstudies')||($this->request->params['controller']=='Areaofstudies') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/areaofstudies/index">
                    <i class="fa fa-folder"></i>
                    <span class="title">
                      Area of studies<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if(($this->request->params['controller']=='galleries')||($this->request->params['controller']=='Galleries') && ($this->request->params['action']=='admin_add'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/galleries/add">
                    <i class="fa fa-picture-o"></i>
                    <span class="title">
                      Gallery<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                <li class=" <?php if(($this->request->params['controller']=='testimonials')||($this->request->params['controller']=='Testimonials') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/testimonials/index">
                    <i class="fa fa-comment"></i>
                    <span class="title">
                      Testimonials<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                <li class=" <?php if(($this->request->params['controller']=='photogalleries')||($this->request->params['controller']=='photogalleries') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/photogalleries/add">
                    <i class="fa fa-comment"></i>
                    <span class="title">
                      Photo Gallery<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                
                <li class=" <?php if(($this->request->params['controller']=='videogalleries')||($this->request->params['controller']=='Videogalleries') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/videogalleries/index">
                    <i class="fa fa-comment"></i>
                    <span class="title">
                      Video Gallery<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if(($this->request->params['controller']=='blogcategories')||($this->request->params['controller']=='Blogcategories') && ($this->request->params['action']=='admin_index') && ($this->request->params['action']=='admin_add') && ($this->request->params['action']=='admin_edit') )echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/blogcategories/index">
                    <i class="fa fa-comment"></i>
                    <span class="title">
                      Blog Category<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                 <li class=" <?php if(($this->request->params['controller']=='blogs')||($this->request->params['controller']=='blogs') && ($this->request->params['action']=='admin_index'))echo 'active';?>"  > <a class="loadButton" href="<?php echo $this->webroot; ?>admin/blogs/index"> <i class="fa fa-clipboard"></i>
                    <span class="title">
                     Blog<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                <li class=" <?php if(($this->request->params['controller']=='events')||($this->request->params['controller']=='events') && ($this->request->params['action']=='admin_add'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/events/add">
                    <i class="fa fa-clipboard"></i>
                    <span class="title">
                     Events<span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
                

                 <li class=" <?php if(($this->request->params['controller']=='users')||($this->request->params['controller']=='Users') && ($this->request->params['action']=='admin_profile'))echo 'active';?>"  >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/profile">
                    <i class="fa fa-user"></i>
                    <span class="title">
                      Profile <span class="badge badge-danger"></span>
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>
              
             <li class="" >
                <a class="loadButton" href="<?php echo $this->webroot; ?>admin/users/logout">
                    <!--<i class="fa fa-power-off"></i>-->
                    <i class="fa fa-key"></i>
                    <span class="title">
                        Logout
                    </span>
                    <span class="selected">
                    </span>
                </a>
                </li>   <!--  -->
</ul>                