
     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">LANGUAGE COACHING</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Language Coaching
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                   <div id="content" class="col-md-9">
                      <div class="col-md-12">
                         <div class="col-md-4"><img src="<?php echo $this->webroot; ?>images/flag_logo.jpg" alt="gurukul_image"></div>
                         <div class="col-md-8">FLAG an initiative by Life Planner, offers you the best of what any language trainee would demand. We aim to provide almost everything for every student in our institution. We look forward to training students, not just in terms of academics, but also in terms of mannerism, habits, and lifestyle. We train our students to adopt the culture overseas, to learn table manners and speech etiquettes to learn to groom themselves as per the occasion, to take up challenges and to see the vice and virtues of each culture. We work out an all-around schedule for the development of our students. This ensures that they are immune to uncertainties while overseas. We solely aim to give them an insight about life.</div>
                            <br>
                        </div>

                        <div class="col-md-12">
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#home">GURUKULAM</a></li>
                          <li><a data-toggle="tab" href="#menu1">IELTS  </a></li>
                          <li><a data-toggle="tab" href="#menu2">GERMAN</a></li>
                          <!-- <li><a data-toggle="tab" href="#menu3">IMAGES</a></li> -->
                        </ul>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="tab-content">

                              <div id="home" class="tab-pane fade in active">
                                <div class="content">
                                <div class="content-content"> 
                                  <div class="content-dropcap v1"> 
                                    <p style="text-align:justify;">“The ancient Gurukulam education system focuses on educating the mind by understanding individual capabilities. Education attained from a Gurukulam is refined into practical knowledge through rigorous practice and preaching. Foreign Languages Academy Gurukulam (FLAG) was born out of our trust in the efficiency of Gurukulam mode of education, which we believe is the best method to excel in English proficiency related IELTS, TOEFL, PTE, OET and to gain mastery over languages like FRENCH and GERMAN. The chances of becoming fluent in foreign languages and studying courses like IELTS, GERMAN and so on from nine to five while living in a society which is largely dependent on our mother tongue Malayalam, is a futile attempt. At FLAG, you will experience a new way of learning where the language is taught in an environment where only the language in training is to be used both day and night.</p> 
                                  </div> 
                                </div>

                                <div class="line-box"></div>
                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">OUR CAMPUS</h4>
                                </div>
                                <div class="col-md-7"> 
                                  <p>At the heart of Kottayam, yet away from all the hustles of a city, the Gurukulam is located around 700m from the district Collectorate, in a peaceful and friendly residential area. The academy is equipped with all basic facilities for our students in a 6000 sq.ft. building.</p> 
                                </div> 
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching0.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">CLASSROOMS</h4>
                                </div>
                                
                                <div class="col-md-7">
                                <p>Even though the syllabus is designed based on a Gurukulam mode, the classrooms are equipped with all modern facilities to ensure active participation of all students. Our teacher student ratio is maintained in a manner that all students get individual attention.</p>
                                </div>
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching2.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">LIBRARY</h4>
                                </div>
                                <div class="col-md-7"> 
                                  <p>Apart from the quality study materials distributed in class rooms, the campus has a well maintained library that is equipped with internationally acclaimed books, novels and journals that will suit your needs, and with an intention to open the doorways for an in-depth study about the language and gives you a better exposure to its local words and variants.</p>
                                </div> 
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching3.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">STUDIO THEATRE</h4>
                                </div>
                                
                                <div class="col-md-7">
                                <p>Like any art and talent, language becomes flexible only when the learner starts to appreciate it. The academy is equipped with a studio theater consisting of a big screen television and a mini theater sound system, for watching movies, TV shows and listening to music, in respective foreign languages. This enhances the pronunciation capability of students and introduces them to new vocabulary.</p>
                                </div>
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching9.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">ONLINE TRAINING BY A FOREIGN TRAINNER </h4>
                                </div>
                                <div class="col-md-7"> 
                                   <p>Special sessions are arranged by native speakers and Language Trainers every week through video conference in our high-tech language lab. This not only helps in improving pronunciation and vocabulary but also helps the candidate to gain confidence while confronting a Language Examiner and to overcome exam fear. </p>
                                </div> 
                                 <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching1.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">ACCOMMODATION</h4>
                                </div>
                                
                                <div class="col-md-7">
                                <p>In the campus, accommodation is provided for girls, designed to suit individual care. All rooms are equipped with basic modern facilities that ensure safety and cleanliness.</p>
                                </div>
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching5.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>
                              </div>
                                
                                <div class="content">
                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">SAFETY AND SECURITY</h4>
                                </div>
                                <div class="col-md-7"> 
                                   <p>The campus is secured by a compound wall and all public spaces are under 24 hours CCTV surveillance. In addition, a security officer is also assigned to ensure safety in the campus.</p>

                                </div>
                                 <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching4.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div> 
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">MESS AND CAFETERIA</h4>
                                </div>
                                
                                <div class="col-md-7">
                                <p>Top floor of the Gurukulam is an open kitchen with a clean wide space used as mess/cafeteria. This space can also be used for fun and relaxation, and has an excellent view of the green landscape.</p>
                                </div>
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching0.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">SELF GROOMING</h4>
                                </div>
                                <div class="col-md-7"> 
                                   <p>Presenting oneself is equally important along with language skills while attending an exam or interview as it enhances the confidence of the candidates. Make-up artists who hold years of experience in the film industries are invited to train our students, so that they can independently groom themselves for public appearances.</p>
                                </div> 
                                 <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching6.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                 <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">YOGA AND ZUMBA</h4>
                                </div>
                                
                                <div class="col-md-7">
                               <p>Concentration skill is vital for Gurukulam mode of education.  As the students will be dedicating their entire time for language studies, yoga training would be provided early in the morning to improve concentration and meditation skills. Zumba practice makes the students energetic, ensuring mental and physical health. The same is arranged in the evening, after regular classes.</p>

                                </div>
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching7.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>

                                <div class="row">
                                <div class="col-md-12">
                                <h4 class="tab-title">TABLE ETIQUETTES</h4>
                                </div>
                                <div class="col-md-7"> 
                                   <p>We offer special training by Hotel Management professionals on dining etiquettes. The Students can practice and refine these skills during their stay in the Gurukulam.</p><p>The Gurukulam guides you at every step to gain mastery over foreign languages. We are here to help you get better scores, to fulfill your desire for a job abroad and to prepare you for a new world of possibilities abroad.</p>
                                </div> 
                                <div class="col-md-5">
                                  <img src="<?php echo $this->webroot; ?>images/languagecoaching8.png" alt="Jane" style="min-height:182px; max-height:182px;">
                                </div>
                                </div>
                                <div class="line-box"></div>
                              </div>
                              <div class="pagination">
                              <ol id="pagin">
                                <li><a class="current" href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                </ol>
                              </div>
                              </div>

                              <div id="menu1" class="tab-pane fade">
                                <div class="course-list-table">
                                  <div class="flat-all-course">
                                    <div class="title-list v1">
                                        <!-- <h2 class="title">IETLS</h2> -->
                                    </div><!-- /title-list -->

                                    <div class="courses-list">
                                        <table class="table course-list-table">
                                            <thead >
                                                <tr>
                                                  <th colspan="3" style="text-align: center;color:#000;">IELTS</th>
                                                </tr>
                                                <tr class="main-color-1-bg dark-div">
                                                  <th>LEVELS</th>
                                                  <th>DURATION</th>
                                                  <th>FEE</th>
                                                </tr>
                                            </thead>
                                            <tbody>                          
                                              <tr> 
                                                <td>Gurukulam </td> 
                                                <td>Monthly </td> 
                                                <td>15000 </td>
                                              </tr>
                                              <tr>
                                                <td>Regular  </td>
                                                <td>3 Months </td>
                                                <td>10000 </td>
                                              </tr>
                                              <tr>
                                                <td>Evening  </td>
                                                <td>Monthly </td>
                                                <td>5000 </td>
                                              </tr>
                                              <tr>
                                                <td>Weekends </td>
                                                <td>Monthly</td>
                                                <td>4000  </td>
                                              </tr>
                                              <tr>
                                                <td>Crash Course </td>
                                                <td>One Month </td>
                                                <td>6000 </td>
                                              </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div><!-- /flat-list-table -->
                              </div>
                              </div>

                              <div id="menu2" class="tab-pane fade"> 
                                  <div class="content-content"> 
                                     <div class="content-dropcap"> 
                                        <p style="text-align:justify;"></p> 
                                        <p>FLAG Academy, an authorized German Academy under the guidance and supervision of vitruvius University</p> 
                                        <br> 
                                     </div> 
                                  </div>

                                  <div class="course-list-table">
                                      <div class="flat-all-course">
                                          <div class="title-list v1">
                                              <!-- <h2 class="title">IETLS</h2> -->
                                          </div><!-- /title-list -->
                                          <div class="courses-list">
                                              <table class="table course-list-table">
                                                  <thead > 
                                                    <tr> <th colspan="3" style="text-align: center;color:#000;">GERMAN</th> </tr> 
                                                    <tr class="main-color-1-bg dark-div">
                                                      <th>LEVELS</th>
                                                      <th>DURATION</th>
                                                      <th>FEE</th>
                                                    </tr> 
                                                  </thead> 
                                                  <tbody> 
                                                      <tr>
                                                        <td>A1</td>
                                                        <td>30-45 Days</td>
                                                        <td>10000</td>
                                                      </tr> 
                                                      <tr>
                                                        <td>A2</td>
                                                        <td>30-45 Days</td>
                                                        <td>12000</td>
                                                      </tr>
                                                      <tr>
                                                        <td>B1</td>
                                                        <td>45 Days</td>
                                                        <td>14000</td>
                                                      </tr>
                                                      <tr>
                                                        <td>B2</td>
                                                        <td>45-60 Days</td>
                                                        <td>16000</td>
                                                      </tr>
                                                  </tbody>
                                              </table>
                                          </div>
                                      </div><!-- /flat-list-table -->
                                  </div>
                              </div>

                            </div>
                          </div>
                        </div>
                    </div>
                
<style type="text/css">
  .course-list-table.table > tbody > tr:hover > td {
      background-color: #ddd!important;
      color: #000!important;
  }
  .nav a{
    font-weight: bold;
    font-size: 16px;
  }
  .tab-pane{
    padding: 20px;
  }
  .tab-title {
    font-style: normal;
    font-size: 18px;
    color: #656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 10px;
    margin-bottom: 5px;
}
.table-content{
  padding: 10px;
}
.column {
  float: left;
  width: 33.333%;
  margin-bottom: 16px;
  padding: 0 11px;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}
.line-box {
    border-bottom: 1px solid #eaeaea;
    margin-bottom: 35px;
    padding-top: 30px;
}
#pagin {
    clear: both;
    padding:0;
    width:250px;
    margin:0 auto;
}
#pagin li {
    float:left;
    margin-right:10px;
}
#pagin li a {
    display:block;
    color:#717171;
    font:bold 11px;
    text-shadow:0px 1px white;
    padding:5px 8px;
    -webkit-border-radius:3px;
    -moz-border-radius:3px;
    border-radius:3px;
    -webkit-box-shadow:0px 1px 3px 0px rgba(0,0,0,0.35);
    -moz-box-shadow:0px 1px 3px 0px rgba(0,0,0,0.35);
    box-shadow:0px 1px 3px 0px rgba(0,0,0,0.35);
    background:#f9f9f9;
    background:-webkit-linear-gradient(top,#f9f9f9 0%,#e8e8e8 100%);
    background:-moz-linear-gradient(top,#f9f9f9 0%,#e8e8e8 100%);
    background:-o-linear-gradient(top,#f9f9f9 0%,#e8e8e8 100%);
    background:-ms-linear-gradient(top,#f9f9f9 0%,#e8e8e8 100%);
    background:linear-gradient(top,#f9f9f9 0%,#e8e8e8 100%);
    filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9',endColorstr='#e8e8e8',GradientType=0 );
}
#pagin li a.current {
    color:white;
    text-shadow:0px 1px #3f789f;
    -webkit-box-shadow:0px 1px 2px 0px rgba(0,0,0,0.8);
    -moz-box-shadow:0px 1px 2px 0px rgba(0,0,0,0.8);
    box-shadow:0px 1px 2px 0px rgba(0,0,0,0.8);
    background:#7cb9e5;
    background:-webkit-linear-gradient(top,#7cb9e5 0%,#57a1d8 100%);
    background:-moz-linear-gradient(top,#7cb9e5 0%,#57a1d8 100%);
    background:-o-linear-gradient(top,#7cb9e5 0%,#57a1d8 100%);
    background:-ms-linear-gradient(top,#7cb9e5 0%,#57a1d8 100%);
    background:linear-gradient(top,#7cb9e5 0%,#57a1d8 100%);
    filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#7cb9e5',endColorstr='#57a1d8',GradientType=0 );
}
#pagin li a:hover {
    -webkit-box-shadow:0px 1px 3px 0px rgba(0,0,0,0.55);
    -moz-box-shadow:0px 1px 3px 0px rgba(0,0,0,0.55);
    box-shadow:0px 1px 3px 0px rgba(0,0,0,0.55);
    background:#fff;
    background:-webkit-linear-gradient(top,#fff 0%,#e8e8e8 100%);
    background:-moz-linear-gradient(top,#fff 0%,#e8e8e8 100%);
    background:-o-linear-gradient(top,#fff 0%,#e8e8e8 100%);
    background:-ms-linear-gradient(top,#fff 0%,#e8e8e8 100%);
    background:linear-gradient(top,#fff 0%,#e8e8e8 100%);
    filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff',endColorstr='#e8e8e8',GradientType=0 );
}
#pagin li a:active,#pagin li a.current:active {
    -webkit-box-shadow:inset 0px 1px 3px 0px rgba(0,0,0,0.5),0px 1px 1px 0px rgba(255,255,255,1) !important;
    -moz-box-shadow:inset 0px 1px 3px 0px rgba(0,0,0,0.5),0px 1px 1px 0px rgba(255,255,255,1) !important;
    box-shadow:inset 0px 1px 3px 0px rgba(0,0,0,0.5),0px 1px 1px 0px rgba(255,255,255,1) !important;
}
#pagin li a.current:hover {
    -webkit-box-shadow:0px 1px 2px 0px rgba(0,0,0,0.9);
    -moz-box-shadow:0px 1px 2px 0px rgba(0,0,0,0.9);
    box-shadow:0px 1px 2px 0px rgba(0,0,0,0.9);
    background:#99cefc;
    background:-webkit-linear-gradient(top,#99cefc 0%,#57a1d8 100%);
    background:-moz-linear-gradient(top,#99cefc 0%,#57a1d8 100%);
    background:-o-linear-gradient(top,#99cefc 0%,#57a1d8 100%);
    background:-ms-linear-gradient(top,#99cefc 0%,#57a1d8 100%);
    background:linear-gradient(top,#99cefc 0%,#57a1d8 100%);
    filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#99cefc',endColorstr='#57a1d8',GradientType=0 );
}
</style>
<script type="text/javascript">
  $(document).ready(function(){
    pageSize = 1;
    showPage = function(page) {
    $(".content").hide();
    $(".content").each(function(n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show();
    });        
}
    
showPage(1);

$("#pagin li a").click(function() {
    $("#pagin li a").removeClass("current");
    $(this).addClass("current");
    showPage(parseInt($(this).text())) 
});
    });
  
</script>