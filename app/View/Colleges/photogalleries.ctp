
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Album</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Album
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-small-v1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        
                        
                        <div class="row">
                            <?php if(!empty($photos)) {
                                        foreach($photos as $photo) { ?>
  <div class="column">
    <div class="card">
      <img src="<?php echo $this->webroot .$PhotogalleryImagePath.$photo['Photogallery']['image']; ?>" alt="Jane" style="min-height:202px; max-height:202px;">
      <div style="min-height: 157px;padding-left:10px;">
        <h2 style="font-size: 18px;"><?php echo $photo['Photogallery']['title']; ?></h2>
        <p class="title"><?php echo $photo['Photogallery']['description']; ?></p>
        
        
      </div>
    </div>
  </div> <?php } }  ?></div>
                        
                    </div>
                    
                </div>
                
                
                
            </div>
        </section>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<style>
html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.column {
  float: left;
  width: 25%;
  margin-bottom: 16px;
  padding: 0 11px;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
</style>



