<style>
    #main h2 {
	color: #034EA2;
	border-bottom: 1px solid;
	padding-bottom: 10px;
	position: relative;
	padding-left: 65px;
}
#main h2:before {
	border-bottom: 4px solid;
	width: 220px;
	bottom: -2px;
	position: absolute;
	left: 0;
}
.input-group-addon {
    border-radius: 4px;
    width: 40px!important;
    /* padding: 4px 12px; */
    font-size: 12px!important;
    font-weight: normal!important;
    line-height: 1!important;
    color: #fff!important;
    text-align: center!important;
    background: #9400D3!important;
    border: 1px solid #9400D3!important;
}
.input-group-addon .fa {
	font-size: 22px;
}
.form-error {
	left: 0;
	position: absolute;
	top: -20px;
}
.formbtn {
    background: none repeat scroll 0 0 #f15a22;
    border: 0 none;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 46px;
    padding: 0 34px;
    margin-top: 5px;
    text-transform: uppercase;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.wd-50 {
	width: 50% !important;
	float: left;
}
.wd-50:last-child {
	width: 50% !important;
	float: left;
	border-left: 0!important;
}
.form {
	background: none;
	border: solid 1px #e7e5e5;
}
.ui-datepicker-trigger {
	position: absolute;
	right: 0;
	top: 5px;
	cursor: pointer;
}
.captcha_section {
	width: 110px;
	margin: 0;
	position: absolute;
	right: 1px;
	top: 1px;
	height: 36px;
	background: #FFFFFF;
	border-left: 1px solid #034EA2;
}
.captcha_section img {
	margin-top: 5px;
}
.has-error .captcha_section {
	border-left: 1px solid #A94442;
}
.has-error .form-control {
	color: #a94442;
}

/** placeholder color change on error occurs **/	
.has-error input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
color:    #a94442;
}
.has-error input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color:    #a94442;
opacity:  1;
}
.has-error input::-moz-placeholder { /* Mozilla Firefox 19+ */
color:    #a94442;
opacity:  1;
}
.has-error input:-ms-input-placeholder { /* Internet Explorer 10-11 */
color:    #a94442;
}
.has-success .captcha_section {
	border-left: 1px solid #3C763D;
}
.captcha_section #the_captcha {
	width: 70% !important;
	border: 0;
}
.formbtn {
	border-radius: 4px;
	font-weight: bold;
}
.input-group .form-control {
	font-size: 11px!important;
	padding: 6px 8px;
}
.form {
	display: block;
	padding: 20px;
}
.left_container {
	padding: 15px 15px 0 0;
}
.has-error .form-control::-webkit-input-placeholder {
color:#a94442 !important;
}
.form-control-agreed {
	line-height: 24px;
	overflow: auto
}
.custom-combobox {
	position: relative;
	display: inline-block;
}
.custom-combobox-toggle {
	position: absolute;
	top: 0;
	bottom: 0;
	margin-left: -1px;
	padding: 0;
}
.custom-combobox-input {
	margin: 0;
	padding: 5px 10px;
}
.custom-combobox-city .custom-combobox {
	display: block;
}
.custom-combobox-city input.custom-combobox-input {
	background-color: #ffffff;
	border: 1px solid #034ea2;
	border-radius: 0 4px 4px 0;
	width: 100%;
	height: 38px;
	font-size: 11px;
}
.custom-combobox-city .ui-button {
	top: 0px;
	right: 0px;
	border: none;
	margin: 2px;
	border: solid 1px #fff;
	outline: solid 1px #707070;
	width: 16px;
	background: #f2f2f2;
	background: -moz-linear-gradient(top, #f2f2f2 0%, #d1d1d1 100%);
	background: -webkit-linear-gradient(top, #f2f2f2 0%, #d1d1d1 100%);
	background: linear-gradient(to bottom, #f2f2f2 0%, #d1d1d1 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f2f2', endColorstr='#d1d1d1', GradientType=0 );
}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
color: #000;
opacity:1;
}
::-moz-placeholder { /* Firefox 19+ */
color: #000;
opacity:1;
}
:-ms-input-placeholder { /* IE 10+ */
color: #000;
opacity:1;
}
:-moz-placeholder { /* Firefox 18- */
color: #000;
opacity:1;
}
.has-error input.custom-combobox-input {
	border-color: #a94442!important;
}
.hilight_box {
	background-color: #f2dede;
	color: #a94442;
	border-color: #a94442
}
.nothilight_box {
  background-color: #dff0d8;
  border-color: #3c763d;
  color: #3c763d;
}
.custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
}
.sidebar{
    display:none;
}
.image{
    width: 100%;
   
}
.image img{
    width: 100%;

}
.flat-row.padding-v4 {
    padding: 0px 0;
}
</style>
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Candidate Registration</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Candidate Registration 
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
         <section class="flat-row padding-v4" id="main">
            <div class="image"><img src="<?php echo $this->webroot; ?>images/neetweb.jpg" alt="image"></div>
              <div class="container">
                 <div class="row">
                    <div id="content" class="col-md-9">
                        <section id="" class="cb"><div class="wrapper"><div id="system-message-container">
                           
                        </div><div class="left_container"> 
                            <div class="form">
                               
                                <!-- <?php echo $this->Form->create('Bookappointment', array('url'=>array('controller'=>'bookappointments','action'=>'bookappointment'))); ?> -->
                                <form method="post" id="registrationform">
                            <p class="form-error"></p>
                            <ul class="formcol2" style="list-style:none;">
                                <!-- first row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"> <i class="fa fa-user" aria-hidden="true"></i></span> 
                                            <input class="form-control" placeholder="First name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Candidate Name'" name="txt_fname" id="txt_fname" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" required="true"> 
                                           <!--  <input class="form-control wd-50" placeholder="Last name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Last Name'" name="txt_lname" id="txt_lname" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" > -->
                                        </div>
                                        <span for="txt_fname" class="help-block"></span>
                                        <!-- <span for="txt_lname" class="help-block"></span> -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-building-o" aria-hidden="true"></i></span> 
                                            <input placeholder="Class" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Class'" class="form-control" type="text" name="txt_class" id="txt_class" maxlength="100" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_class" class="help-block"></span>
                                    </div>
                                </div>
                                <!-- first row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"> <i class="fa fa-files-o" aria-hidden="true"></i></span> 
                                            <select class="form-control" name="txt_stream" id="txt_stream" data-validation="required" data-validation-error-msg=" " required="true">
                                                <option value="">Select Stream</option>
                                                <option value="Medical">Medical</option>
                                                <option value="Engineering">Engineering</option> 
                                                <option value="Both">Both</option> 
                                            </select>
                                        </div>
                                         <span for="txt_stream" class="help-block"></span>
                                    </div>
                                    <div class="col-md-6"><div class="input-group"> <span class="input-group-addon" ><i class="fa fa-birthday-cake" aria-hidden="true"></i></span> <input class="form-control" placeholder="Date of Birth" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date of Birth'" type="text"  name="txt_dob" id="txt_dob"   maxlength="100" data-validation="required" data-validation-error-msg=" " value=""   /></div>
                                     <span for="txt_dob" class="help-block"></span>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"> <i class="fa fa-user" aria-hidden="true"></i></span> 
                                            <input class="form-control" placeholder="Parent/Guardian Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Parent/Guardian Name'" name="txt_parent" id="txt_parent" type="text" maxlength="40" data-validation="custom length" data-validation-regexp="^([a-zA-Z ]+)$" data-validation-length="3-40" data-validation-error-msg=" " value="" required="true"> 
                                           
                                        </div>
                                        <span for="txt_parent" class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span> 
                                            <input placeholder="Mobile number" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Mobile number'" class="form-control" type="text" name="txt_mobile" id="txt_mobile" maxlength="10" data-validation="number length" data-validation-length="10-10" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_mobile" class="help-block"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> 
                                            <input placeholder="Email ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email ID'" class="form-control" type="text" name="txt_email" id="txt_email" maxlength="100" data-validation="email" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_email" class="help-block"></span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-building-o" aria-hidden="true"></i></span> 
                                            <input placeholder="Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'" class="form-control" type="text" name="txt_address" id="txt_address" maxlength="100" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_address" class="help-block"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-group"> 
                                            <span class="input-group-addon"><i class="fa fa-building-o" aria-hidden="true"></i></span> 
                                            <input placeholder="Name Of School" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Name Of School'" class="form-control" type="text" name="txt_school" id="txt_school" maxlength="100" data-validation-error-msg=" " value="" required="true">
                                        </div>
                                        <span for="txt_school" class="help-block"></span>
                                    </div>
                                </div>   
                            <div class="row"> 
                            <input name="btn_submit" type="submit" class="formbtn" value="Submit" style=" background: none repeat scroll 0 0 #7ABA7A;border: 0 none;color: #fff;cursor: pointer;font-size: 15px;height: 46px; padding: 0 34px;margin-top: 5px;text-transform: uppercase;transition: all .5s ease 0;-webkit-transition: all .5s ease 0; margin-left: 15px;"></div></div>  </ul>
                   <?php echo $this->Form->end(); ?>
                            </div> 
                </div>
       </section> 
       </div> 
     </section>
</div>

     <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/bootstrap1.css">
     <link rel="stylesheet" href="<?php echo $this->webroot; ?>css/font-awesome1.css">

    

<!---jquery Validation-->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
 <script src="<?php echo $this->webroot; ?>js/jquery-ui1.js"></script> 
     <script src="<?php echo $this->webroot; ?>js/bootstrap.min1.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
<!---jquery Validation End-->

<script type="text/javascript">
     
     $(document).ready(function(){
        
      var error1 = $('.alert-danger');
  $('#registrationform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "txt_fname" : {required : true},
    "txt_class" : {required : true},
    "txt_stream" : {required : true},
    "txt_dob" : {required : true},
    "txt_parent" : {required : true},
    "txt_mobile" : {required : true},
    "txt_email" : {required : true,email:true},
    "txt_address" : {required : true},
    "txt_school" : {required : true},      
    },
    messages:{
     "txt_fname" : {required : "Please enter name"},
     "txt_class" : {required : "Please enter class"},
     "txt_stream" : {required : "Please select stream."},
     "txt_dob" : {required : "Please enter date of birth."},
     "txt_parent" : {required : "Please enter parent or guardian name."},
     "txt_mobile" : {required : "Please enter mobile number."},
     "txt_email" : {required : "Please enter email id.",email:"Please enter valid email id."},
     "txt_address" : {required : "Please enter address."},
     "txt_school" : {required : "Please enter name school."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
      //success1.hide();
      error1.show();
      //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
      $(element)
        .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
      $(element)
        .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
      label
        .closest('.form-group').removeClass('has-error'); // set success class to the control group
      label
        .closest('.form-group').removeClass('error');
    },
  });
  $( "#txt_dob" ).datepicker();
});
</script>
 
<style>
    .help-block{
        color:#b20828!important;
        margin-top: -13px!important;
    }
    body {
	font-family: Arial, Helvetica, sans-serif;
}

table {
	font-size: 1em;
}

.ui-draggable, .ui-droppable {
	background-position: top;
}
</style>