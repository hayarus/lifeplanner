     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">FOREIGN LANGUAGES ACADEMY GURUKULAM – (F.L.A.G)</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Language Coaching
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                   <div id="content" class="col-md-9">
                       <div class="row">
                           <div class="content-content">
        <div class="content-dropcap v1">
            <p style="text-align:justify;">“FLAG  an initiative by Life Planners, offers you the best of what any language trainee would demand. We aim to provide almost everything for every student in our institution. 
               <!--  OUR VISION -->
We look forward to training students, not just in terms of academics, but also in terms of mannerism, habits, and lifestyle. We train our students to adopt the culture overseas, to learn table manners and speech etiquettes to learn to groom themselves as per the occasion, to take up challenges and to see the vice and virtues of each culture. We work out an all-around schedule for the development of our students. This ensures that they are immune to uncertainties while overseas. We solely aim to give them an insight about life. 
                </p></div></div></div>
<div class="row">
   <!-- <img src="<?php echo $this->webroot; ?>images/gurukul_1.png" alt="gurukul_image"  style="margin-left: 164px;height: 207px;"/> -->
         <div class="divimg">
             <img src="<?php echo $this->webroot; ?>images/FLAG_LOGO.png" alt="gurukul_image"style="  padding-left: 30px; padding-top:23px;"/>
 
 </div>
 
 <p style="text-align:justify;">
     <!-- OUR MOTTO -->
Excellence is our aim and we strive along with our students to achieve. The institute gives the students a shot to secure future, which cannot be termed as a good shot or a very good shot. We give them the perfect shot. Nothing less, nothing more. We render our services available for our students at all terms.
competent to face and interact with any foreign nationality with sheer confidence.
<!--   OUR SERVICES -->
We provide coaching for IELTS, GERMAN and other foreign languages. We conduct sessions on Yoga, Dining Etiquettes, basic skills and manners, interview training and much more. We accommodate students in our institution, where they can avail more in terms of language and lifestyle development. 
Just as the name suggests, Foreign Language Academy Gurukulam, our students get an experience of what a ‘Gurukulam’ offers; our students would get around-the-clock attention from the ‘Guru’. There is minimal room for mistakes and an overall development is ensured. </p>

</div>
<div class="row">
   <div class="content-content">
        
        
    </div>
    
</div>
                       

</div>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 -->
<style type="text/css">
  .divimg{
 height: 350px;
width: 700px;
/*margin-left: 30px;*/
  }
@media screen and (max-width: 650px) {
  .divimg{
    width: 100%;
    height: 100%;
    display: block;
    /*margin-left:20px;*/
    margin-left: 10px;
  }
  img{
    width: 90%;
     height: 100%;
  }
}
</style>