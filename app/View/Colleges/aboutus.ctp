     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">About Us</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?>
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                       <h4 class="h4-title">WHY LIFE PALNNER?</h4>
                       <p>Life Planner from its modest start up in 2012 has become a reputed and well respected educational facilitator, accredited by an ISO certification within this short span of trustworthy service. Life Planner offers detailed counselling for each student throughout admission and visa processing after understanding the talent, interest and study skills of individual students. Our specialized counselors extend to provide admission guidance to Medical, Engineering, Paramedical, Nursing, IT, Technical, Management and multiple other academic streams in reputed ranked Universities and institutions worldwide. There are under graduation & post graduation programs, certificate programs and diploma courses offered in almost all Countries. We provide expert training through Gurukulam mode of education at our own Academy to ensure that you get admission with excellent IELTS/German language score. International students are provided help in finding a job after studies with the help of our Alumnus and Immigration Consultant Office. Life Planner is an organisation that provides service to our students even after reaching abroad. We are committed in helping our students settle down upon arrival by providing airport pickup and arrangements for stay. This is made possible through our coordinated action with University/College officials and our own former students who are well settled . Our former students are wholeheartedly part of these efforts because of the excellent service we have provided for them in past. </p><br/>
                        <p><strong>LIFE PLANNER students enrolling benefit from:</strong></p><br/>
                        <ul style="padding-left:10px;list-style-type:disc;">
                            
                            <li> Expert advice to match your interests with the best programs</li>
                            <li> Care and attention in all stages of processing</li>
                            <li> Frequent spot admission/seminar sessions at our offices</li>
                            <li> Scholarship/fee discounts from selected institutions based on criteria</li>
                            <li> Free airport pick up and accommodation assistance</li>
                            <li> Free visa guidance</li>
                            <li> Assistance in obtaining education loan</li>
                            <li> Forex services</li>
                            <li> Air Ticketing at competitive rates</li>
                            <li> Access to references from students who went abroad through us</li>
                            <li> 100% transparent services</li>
                        </ul>
                        <div class="clear"></div>
                    </div><!-- /col-md-9 -->

<style type="text/css">
    .clear{
    margin-bottom: 20px;
    padding-top: 20px;
}

.h4-title {
    font-style: normal;
    font-size: 18px;
    color: #656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-bottom: 15px;
}
</style>