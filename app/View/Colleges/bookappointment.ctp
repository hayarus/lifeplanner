<style>
    #main h2 {
	color: #034EA2;
	border-bottom: 1px solid;
	padding-bottom: 10px;
	position: relative;
	padding-left: 65px;
}
#main h2:before {
	border-bottom: 4px solid;
	width: 220px;
	bottom: -2px;
	position: absolute;
	left: 0;
}
.input-group-addon .fa {
	font-size: 22px;
}
.form-error {
	left: 0;
	position: absolute;
	top: -20px;
}
.formbtn {
    background: none repeat scroll 0 0 #f15a22;
    border: 0 none;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 46px;
    padding: 0 34px;
    margin-top: 5px;
    text-transform: uppercase;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;
}
.wd-50 {
	width: 50% !important;
	float: left;
}
.wd-50:last-child {
	width: 50% !important;
	float: left;
	border-left: 0!important;
}
.form {
	background: none;
	border: solid 1px #e7e5e5;
}
.ui-datepicker-trigger {
	position: absolute;
	right: 0;
	top: 5px;
	cursor: pointer;
}
.captcha_section {
	width: 110px;
	margin: 0;
	position: absolute;
	right: 1px;
	top: 1px;
	height: 36px;
	background: #FFFFFF;
	border-left: 1px solid #034EA2;
}
.captcha_section img {
	margin-top: 5px;
}
.has-error .captcha_section {
	border-left: 1px solid #A94442;
}
.has-error .form-control {
	color: #a94442;
}

/** placeholder color change on error occurs **/	
.has-error input::-webkit-input-placeholder { /* WebKit, Blink, Edge */
color:    #a94442;
}
.has-error input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color:    #a94442;
opacity:  1;
}
.has-error input::-moz-placeholder { /* Mozilla Firefox 19+ */
color:    #a94442;
opacity:  1;
}
.has-error input:-ms-input-placeholder { /* Internet Explorer 10-11 */
color:    #a94442;
}
.has-success .captcha_section {
	border-left: 1px solid #3C763D;
}
.captcha_section #the_captcha {
	width: 70% !important;
	border: 0;
}
.formbtn {
	border-radius: 4px;
	font-weight: bold;
}
.input-group .form-control {
	font-size: 11px!important;
	padding: 6px 8px;
}
.form {
	display: block;
	padding: 20px;
}
.left_container {
	padding: 15px 15px 0 0;
}
.has-error .form-control::-webkit-input-placeholder {
color:#a94442 !important;
}
.form-control-agreed {
	line-height: 24px;
	overflow: auto
}
.custom-combobox {
	position: relative;
	display: inline-block;
}
.custom-combobox-toggle {
	position: absolute;
	top: 0;
	bottom: 0;
	margin-left: -1px;
	padding: 0;
}
.custom-combobox-input {
	margin: 0;
	padding: 5px 10px;
}
.custom-combobox-city .custom-combobox {
	display: block;
}
.custom-combobox-city input.custom-combobox-input {
	background-color: #ffffff;
	border: 1px solid #034ea2;
	border-radius: 0 4px 4px 0;
	width: 100%;
	height: 38px;
	font-size: 11px;
}
.custom-combobox-city .ui-button {
	top: 0px;
	right: 0px;
	border: none;
	margin: 2px;
	border: solid 1px #fff;
	outline: solid 1px #707070;
	width: 16px;
	background: #f2f2f2;
	background: -moz-linear-gradient(top, #f2f2f2 0%, #d1d1d1 100%);
	background: -webkit-linear-gradient(top, #f2f2f2 0%, #d1d1d1 100%);
	background: linear-gradient(to bottom, #f2f2f2 0%, #d1d1d1 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f2f2f2', endColorstr='#d1d1d1', GradientType=0 );
}

::-webkit-input-placeholder { /* Chrome/Opera/Safari */
color: #000;
opacity:1;
}
::-moz-placeholder { /* Firefox 19+ */
color: #000;
opacity:1;
}
:-ms-input-placeholder { /* IE 10+ */
color: #000;
opacity:1;
}
:-moz-placeholder { /* Firefox 18- */
color: #000;
opacity:1;
}
.has-error input.custom-combobox-input {
	border-color: #a94442!important;
}
.hilight_box {
	background-color: #f2dede;
	color: #a94442;
	border-color: #a94442
}
.nothilight_box {
  background-color: #dff0d8;
  border-color: #3c763d;
  color: #3c763d;
}
.custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
}
.sidebar{
    display:none;
}
</style>
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Book Your Appointment</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?>
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
         <section class="flat-row padding-v1" id="main">
            <div class="container">
                 <div class="row">
                    <div id="content" class="col-md-9">
<section id="" class="cb"><div class="wrapper"><div id="system-message-container"></div><div class="left_container"> 
 <link rel="stylesheet" href="https://www.thechopras.com/templates/home_tpl/css/jquery-ui.css"> <script src="https://www.thechopras.com/templates/home_tpl/js/jquery-ui.js"></script> <script src="https://www.thechopras.com/bootstrap/js/bootstrap.min.js"></script> <link rel="stylesheet" href="https://www.thechopras.com/templates/home_tpl/bootstrap/css/bootstrap.css"><link rel="stylesheet" href="https://www.thechopras.com/templates/home_tpl/bootstrap/font-awesome/css/font-awesome.css"><link rel="stylesheet" href="https://www.thechopras.com/templates/home_tpl/autoselect/jquery-ui.css"><link rel="stylesheet" href="https://www.thechopras.com/templates/home_tpl/autoselect/style.css"> <script src="https://www.thechopras.com/templates/home_tpl/autoselect/jquery-ui.js"></script> <script src="https://www.thechopras.com/templates/home_tpl/autoselect/customselect.js"></script> 
 
 <div class="form">
    <?php echo $this->Form->create('Bookappointment', array('url'=>array('controller'=>'colleges','action'=>'bookappointment'))); ?>
         <p class="form-error"></p>
         <ul class="formcol2" style="list-style:none;">
             <div class="row">
                 <div class="col-md-6">
                     <div class="input-group">
                          <span class="input-group-addon"> 
                          <i class="fa fa-user" aria-hidden="true"></i></span> <input class="form-control wd-50" placeholder="First name"  name="firstname" id="firstname" type="text" maxlength="40"  value=""> 
                          <input class="form-control wd-50" placeholder="Last name"  name="lastname" id="lastname" type="text" maxlength="40"  value=""></div>
                          </div>
                          <div class="col-md-6">
                              <div class="input-group"> 
                              <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span> 
                              <input placeholder="Mobile number" onfocus="this.placeholder = ''" class="form-control" type="text" name="mobileno" id="mobileno" maxlength="10"  value=""></div></div></div>
                              <div class="row">
                                  <div class="col-md-6">
                                      <div class="input-group"> <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span> 
                                      <input placeholder="Email ID" onfocus="this.placeholder = ''" class="form-control" type="text" name="email" id="email" maxlength="100"  value=""></div></div>
                                      <div class="col-md-6"><div class="input-group"> <span class="input-group-addon"><i class="fa fa-birthday-cake" aria-hidden="true"></i></span> 
                                                  <input class="form-control hasDatepicker" placeholder="Date of Birth"  type="text" name="dob" id="dob"  maxlength="100"  value=""><img class="ui-datepicker-trigger" src="https://www.thechopras.com/templates/home_tpl/images/Calender-icon.png" alt="calendar" title="calendar"></div></div></div><div class="row"><div class="col-md-6"><div class="input-group"> <span class="input-group-addon"><i class="fa fa-file-text" aria-hidden="true"></i></span> 
                                                  <select class="form-control" name="intake" id="intake"><option value="">Select Intake</option><option value="233">2017</option><option value="246">2018</option><option value="259">2019</option><option value="272">2020</option><option value="285">2021</option><option value="298">2022</option> </select>
                                                  </div></div><div class="col-md-6"><div class="input-group"> <span class="input-group-addon"> <i class="fa fa-files-o" aria-hidden="true"></i></span> 
                                                  <select class="form-control" name="level" id="level" ><option value="">Select Level</option><option value="UG">Undergraduate</option><option value="PG">Post&nbsp;Graduate</option> </select></div></div></div><div class="row"><div class="col-md-6"><div class="input-group"> <span class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span> 
                                                  <select class="form-control" name="studyarea" id="studyarea" ><option value="">Select Area of Study</option><option value="1907">Art and Humanities</option><option value="1911">Business and Social Science</option><option value="1940">Criminology and Law</option><option value="1942">Engineering and Technology</option><option value="1954">Information Technology and Computing</option><option value="1965">Medicine and Health</option><option value="2035">Pathways</option><option value="1990">Sciences</option><option value="2005">Skills and Vocational Training</option> </select>
                                                  </div></div><div class="col-md-6"><div class="input-group"> <span class="input-group-addon"><i class="fa fa-globe" aria-hidden="true"></i></span> <select class="form-control wd-50" name="country_pref_1" id="country_pref_1" ><option value="">Select Country Pref1</option><option value="2">Australia</option><option value="104">Austria</option><option value="112">Barbados</option><option value="91">Belgium</option><option value="4">Canada</option><option value="47">China</option><option value="64">Dubai</option><option value="49">France</option> </select> 
                                                  <select class="form-control wd-50" name="country_pref_2" id="country_pref_2" ><option value="">Select Country Pref2</option><option value="2">Australia</option><option value="104">Austria</option><option value="112">Barbados</option><option value="91">Belgium</option><option value="4">Canada</option><option value="47">China</option><option value="64">Dubai</option><option value="49">France</option><option value="18">Germany</option><option value="57">Hong Kong</option> </select>
                                                  </div></div></div><div class="row"><div class="col-md-6"><div class="input-group"> <span class="input-group-addon"><i class="fa fa-question-circle" aria-hidden="true"></i></span> 
                                                  <select class="form-control" name="helpyou" id="helpyou" ><option value="">Select How may we help you</option><option value="5801">Personalised Evaluation - Personality and Career Mapping</option><option value="5802">Higher Education and Career Management</option><option value="5803">Global Admissions, Applications, Scholarships, Visas and Post Visa Services</option><option value="5804">Test Preparation</option> </select>
                                                  </div></div><div class="col-md-6"><div class="input-group"> </div></div></div></div><div class="row"><div class="col-md-12"><div class="input-group"> <span class="input-group-addon"> <input id="inpu6" type="checkbox" checked="checked" name="accept" value="accept" data-validation="required" data-validation-error-msg=" "> </span><div class="form-control form-control-agreed"> I would like Lifeplanner to send me regular updates relating to University Admissions via SMS / Email / Call.</div></div></div></div><div class="row"> <input type="hidden" name="txt_form_name" value="Book-Your-Appointment"> <input type="hidden" name="txt_type" value="Book Your Appointment"> <input type="hidden" name="txt_query" value="query2"> <input type="hidden" name="txt_url" value=""> <input type="hidden" name="txt_page_type" value=""><div class="col-md-12"> <input name="btn_submit" type="submit" class="formbtn" value="Submit" style=" background: none repeat scroll 0 0 #f15a22;
    border: 0 none;
    color: #fff;
    cursor: pointer;
    font-size: 15px;
    height: 46px;
    padding: 0 34px;
    margin-top: 5px;
    text-transform: uppercase;
    transition: all .5s ease 0;
    -webkit-transition: all .5s ease 0;"></div></div>  </ul><?php echo $this->Form->end(); ?></div> </div></section> </div> </section>
</div>