     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Album</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Album
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->


         <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div class="member-single">
                        <?php foreach($photos as $photo){ ?>
                        <div class="col-md-12">
                            <div class="member-single-post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="item-thumbnail">
                                            <?php if (!empty($photo['Photogallery']['image'])){ ?>
                                            <img src="<?php echo $this->webroot .$PhotogalleryImagePath.$photo['Photogallery']['image']; ?>" alt="Jane" style="width:200px;height:200px;">
                                            <?php }else { ?>
                                            <a href="#"><img src="<?php echo $this->webroot; ?>no_youtube_image.png" alt="image" style = "width: 100%"></a>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title"><?php echo $photo['Photogallery']['title']; ?></h3>
                                                
                                                <hr/>
                                                <p><?php echo $photo['Photogallery']['description']; ?></p>
                                                
                                            </div>
                                        </div><!--/content-pad-->
                                    </div><!--/col-md-8-->
                                </div><!--/row-->
                            </div><!--/member-single-post-->

                            <div class="pure-content">
                                <!-- <div class="content-pad">
                                    <p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.</p>
                                    <p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure.</p>
                                </div> -->
                            </div>

                            
                        </div><!-- /col-md-9 -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>




                    