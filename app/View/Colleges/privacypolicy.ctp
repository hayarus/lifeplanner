     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Privacy Policy</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Privacy Policy
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main"> 
          <div class="container"> <div class="row"> 
            <div id="content" >
            <div class="col-md-12"> 
              <div class="row"> 
                <div class="content-content">  
                    Copyright ©. All rights reserved.  All trademarks, logos and service marks displayed on the Site are our  property. You are not permitted to use these Marks without our prior written consent.

                  <h2 class="tab-title"> Contact Information </h2>
                   <strong> Email: </strong> info@lifeplanneruniversal.com
                    <p style="text-align:justify;">Your privacy is important to us. It is Life Planner Studies & opportunities' policy to respect your privacy regarding any information we may collect from you across our website, http://www.lifeplanneruniversal.com/, and other sites we own and operate.</p>
                    
                    <p style="text-align:justify;">We only ask for personal information when we truly need it to provide a service to you. We collect it by fair and lawful means, with your knowledge and consent. We also let you know why we’re collecting it and how it will be used.</p>

                    <p style="text-align:justify;">We only retain collected information for as long as necessary to provide you with your requested service. What data we store, we’ll protect within commercially acceptable means to prevent loss and theft, as well as unauthorized access, disclosure, copying, use or modification.</p>

                    <p style="text-align:justify;">We don’t share any personally identifying information publicly or with third-parties, except when required to by law.</p>

                    <p style="text-align:justify;">Our website may link to external sites that are not operated by us. Please be aware that we have no control over the content and practices of these sites, and cannot accept responsibility or liability for their respective privacy policies.</p>

                    <p style="text-align:justify;">You are free to refuse our request for your personal information, with the understanding that we may be unable to provide you with some of your desired services.</p>
                </div>
              </div> 
              <div class="row"> 
                <div class="content-content">
                
                    <p style="text-align:justify;">Your continued use of our website will be regarded as acceptance of our practices around privacy and personal information. If you have any questions about how we handle user data and personal information, feel free to contact us.</p> 
                    <strong>This policy is effective as of  24 March 2015.</strong>
                    
                   
                </div> 
              </div> 
            </div> 
          </div> 
        </div>
    </div>
</section>


<style type="text/css">
  .tab-title, .tab-form-title {
    font-style: normal;
    font-size: 18px;
    color: #656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 15px;
    margin-bottom: 10px;
}
</style>