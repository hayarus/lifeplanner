<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Events</h2>
                        </div>
                        <div class="breadcrumbs">

                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

<section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div class="member-single">
                        <?php foreach($events as $event){ ?>
                        <div class="col-md-12">
                            <div class="member-single-post">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="item-thumbnail">
                                            <?php if (!empty($event['Event']['image'])){ ?>
                                              <a href="#"><img src="<?php echo $this->webroot .$eventImagePath.$event['Event']['image']; ?>" alt="image" style = "width: 100%"></a>
                                            <?php }else { ?>
                                             <a href="#"><img src="<?php echo $this->webroot; ?>image_not_available.pNg" alt="image" style = "width: 100%"></a>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <div class="content-pad">
                                            <div class="item-content">
                                                <h3 class="item-title"><?php echo $event['Event']['title'] ?></h3>
                                                
                                                <div class="member-tax small-text">
                                                    <!-- <a href="#" class="cat-link"></a> -->
                                                    
                                                </div>
                                                
                                                <div class="details"> 
                                                <strong>Event Date : </strong> <?php  echo date("d-F-Y", strtotime($event['Event']['eventdate'])) ;  ?>
                                                </div>
                                                <div class="details"> 
                                                <strong>Venue : </strong> <?php echo $event['Event']['venue']; ?>
                                                </div>    
                                                <p><?php echo $event['Event']['description']; ?></p>

                                                <!-- <ul class="list-inline social-light">
                                                    <li><a class="btn btn-default social-icon" href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a class="btn btn-default social-icon" href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a class="btn btn-default social-icon" href="#"><i class="fa fa-tumblr"></i></a></li>
                                                </ul> -->
                                            </div>
                                        </div><!--/content-pad-->
                                    </div><!--/col-md-8-->
                                </div><!--/row-->
                            </div><!--/member-single-post-->

                            <div class="pure-content">
                                <!-- <div class="content-pad">
                                    <p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure.</p>
                                    <p>To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure.</p>
                                </div> -->
                            </div>

                            
                        </div><!-- /col-md-9 -->
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>


<style type="text/css">
  .details{
    float: left;
    width: 50%;
    margin-bottom: 15px;
  }
</style>

                    