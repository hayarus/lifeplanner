
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Gallery</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="<?php echo $this->webroot; ?>">Home </a></li>
                                <li>\ Gallery</li>
                            </ul>                   
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-small-v1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        
                        
                        <div class="row">
                            <?php if(!empty($galleries)) { foreach($galleries as $gallery) { ?>
                              <div class="column">
                                  <div class="card">
                                  <div style = "float: left; width:60%; margin:2%;">
                                    <img src="<?php echo $this->webroot .$galleryImagePath.$gallery['Gallery']['image']; ?>" alt="Jane" style="width:100%;">
                                  </div>
                                  <div style = "float: left; width:25%; margin:2%;">
                                    <h2 style="font-size: 15px;"><?php echo $gallery['Gallery']['titile']; ?></h2>
                                    <p><?php echo $countries[$gallery['Gallery']['country_id']]; ?></p>
                                  </div>
                                    

                                      <div style="padding-left:10px; float: left;">
                                        
                                        <p class="title"><?php echo $gallery['Gallery']['university']; ?></p>
                                        <p><?php echo $gallery['Gallery']['course']; ?></p>
                                        
                                        
                                      </div>
                                  </div>
                              </div> 
                            <?php } }  ?>
                        </div> 
                    </div>

                    <div class="dataTables_paginate paging_bootstrap pull-right">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                          <?php }?>
    						        </div>
                      </div>
                </div>

                
          </div>
        </section>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<style>
html {
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

.column {
  float: left;
  width: 25%;
  margin-bottom: 16px;
  padding: 0 11px;
}

@media screen and (max-width: 650px) {
  .column {
    width: 100%;
    display: block;
  }
}

.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  max-height: 275px;
  min-height: 275px;
  float: left;
}

.container {
  padding: 0 16px;
}

.container::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title {
  color: grey;
}

.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
.section-header {
    text-align: center!important;
}
.section-header .title {
  font-family: "Bitter", sans-serif;
  font-size: 28px;
  color: #253b80;
  margin-bottom: 35px;
  text-transform: uppercase;
}

</style>



