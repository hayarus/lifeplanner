<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>css/style-tab.css">
<script type="text/javascript">

$(document).ready(function(){
var error1 = $('.alert-danger');
$('#eligibilityform').validate({
errorElement: 'span', //default input error message container
errorClass: 'help-block', // default input error message class
focusInvalid: false, // do not focus the last invalid input
ignore: "",
rules:{
"data[name]" : {required : true},
"data[phone]" : {required : true, minlength:10, maxlength:10,},
"data[email]" : {required : true,email:true},
"data[qualification]" : {required : true},
"data[percent]" : {required : true},
"data[yearofpassing]" : {required : true},
  
},
messages:{
"data[name]" : {required :"Please enter name."},
"data[phone]" : {required : "Please enter phone no."},
"data[email]" : {required :"Please enter email.",email:"Please enter valid email."},
"data[qualification]" : {required :"Please enter qualification."},
"data[percent]" : {required :"Please enter last CGPA."},
"data[yearofpassing]" : {required :"Please enter year of passing."},
},

invalidHandler: function (event, validator) { //display error alert on form submit              
//success1.hide();
error1.show();
//App.scrollTo(error1, -200);
},

highlight: function (element) { // hightlight error inputs
$(element)
.closest('.form-group').addClass('has-error'); // set error class to the control group
},

unhighlight: function (element) { // revert the change done by hightlight
$(element)
.closest('.form-group').removeClass('has-error'); // set error class to the control group
},

success: function (label) {
label
.closest('.form-group').removeClass('has-error'); // set success class to the control group
label
.closest('.form-group').removeClass('error');
},
});
});
</script>
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">European Countries</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / European Countries
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                        <!--  -->
                        <div class="tabs">
                            <ul class="tab-links">
                                <li class="active"><a href="#tab1">GERMANY</a></li>
                                <li><a href="#tab2">POLAND</a></li>
                                <li><a href="#tab3">ITALY</a></li>
                                <li><a href="#tab4">SWEDEN</a></li>
                                <li><a href="#tab5">FRANCE</a></li>
                                <li><a href="#tab6">SPAIN</a></li>
                                <li><a href="#tab7">MOLDOVA</a></li> 
                            </ul>
                            <br>
                            <div class="tab-content"> 
                                <div id="tab1" class="tab active"> 
                                    <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                            <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/germany.png" alt="image"> 
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-content">
                                                            <h4 class="item-title"> GERMANY </h4>
                                                            <p>Germany is a country in Central and Western Europe. It has EU’s largest economy and its capital city ,Berlin is the epitome of this reputation. Germany is the  seventh  largest country in Europe  with an area of  357,386 square kilometers. As a developed country with a high standard of living, it upholds a social security , universal health care system and a tuition free education. Germany is one of the best cultural places to visit in Europe. In 1961 , East Germany started building the Berlin wall between the two parts of Berlin. West Germany was one of the countries that started the European Union.</p> 
                                                            
                                                            <div class="price main-color-1"></div> 
                                                            
                                                            <h3><strong>HIGHER EDUCATION SYSTEM IN GERMANY</strong></h3> 
                                                            <div class="price main-color-1"></div>
                                                            <p>In Germany there are hundreds of institutions that allow the opportunity for advanced degrees.</p>
                                                            <p>Germany’s position as a European powerhouse means that many of its universities offer great job prospects. </p>
                                                            <p>Germany can be relatively inexpensive country to study in as a lot of undergraduate courses do not command any tuition fees not just domestic but also international.</p>
                                                            <div class="event-time"> 
                                                                <h3><strong>WHY STUDY IN GERMANY?</strong></h3> 
                                                                <div class="price main-color-1"></div> 
                                                                <p>German universities provide excellent teaching and research ranking among the best in the world.<p>
                                                                <p> German universities offer outstanding academic programmes with a range of attractive practice oriented options.</p>
                                                                <p>Many study programmes combine theory and practice which facilitate career.</p>
                                                                <p>Universities here do not charge any tuition fees and even if they do, the fees tend to be very low.</p>
                                                                <p> The German government provides considerable funding to the universities.</p>
                                                                <p>Germany offers economic and political stability , which makes it an ideal place for you to study.</p> 
                                                            </div>
                                                            <div class="event-address"> 
                                                                
                                                            </div> <!--- event-address ---->
                                                        </div>
                                                        </div>  
                                                    </div>
                                            </article>
                                        </div><!--- eventlisting --->
                                        <div class="event-listing event-listing-classic">
                                            <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5">
                                                    <div class="content-pad ">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                    <div class="content-pad">
                                                        <div class="item-content"></div>
                                                        <div class="item-meta">
                                                            <a class="flat-button"  href="<?php echo $this->webroot;?>universities/medicallist">MBBS IN GERMANY <i class="fa fa-angle-right"></i></a>

                                                            <a class="flat-button"  href="<?php echo $this->webroot;?>universities/nursingingermany">NURSING IN GERMANY <i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div><!--/event-listing-->
                                    </div> 
                                </div> 
                                <!-- //tab1 -->
                                 <div id="tab2" class="tab"> 
                                     <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                            <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/poland.png" alt="image">
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-content">
                                                            <h4 class="item-title"> POLAND </h4> 
                                                            <p>The Republic of Poland is located  in Central Europe. Poland has a population of  38.5 million people and the capital & largest metropolis is Warsaw. Poland is the 21st largest economy in the world and the ninth largest in Europe. Since December 2007 Poland is a member of the Schengen  area  and has the lowest living expenses in  Schengen countries. The Polish climate is moderate continental, with relatively cold winters  and hot summer.</p>
                                                            <div class="price main-color-1"></div>

                                                            <h3><strong>HIGHER EDUCATION IN POLAND</strong></h3>
                                                            <div class="price main-color-1"></div>
                                                            <p>Poland is the educational Hub of Europe with attractions such as the  criteria of NO IELTS .</p>
                                                            <p>Polish higher education system is developing most dynamically.</p>
                                                            <p>The quality of the education provided is monitored and regularly evaluated.</p>
                                                            <p>Poland holds fourth place in Europe after the United Kingdom, Germany and France in terms of the number of people enrolled in higher education.</p>
                                                            <p>Each year almost half a million young people begin their education at universities and colleges.</p>
                                                            <p>The Polish university level schools offer over high quality 200 lines of study as an integral part of the European Higher Education Area.</p>

                                                            <div class="event-time">
                                                                <h3><strong>WHY STUDY  IN  POLAND?</strong></h3>
                                                                <div class="price main-color-1"></div>
                                                                <p>Poland has more than 400 universities and institutions, both public and private, which cover over 200 fields of study, they include</p>
                                                                <p>Studying in Poland will provide a solid education which will thoroughly prepare you for work in the most advanced labor markets of the world, at the same time stimulating your own personal development.</p>
                                                                <p>You will also have the unique opportunity of meeting outstanding specialists and renowned intellectuals in your chosen field.</p>
                                                                <p>Pursuing your studies in Poland, a country of great historical significance and continued aspiration, will undoubtedly be a fascinating, even life-changing, adventure.</p>
                                                                <p>The reform of science and the higher education sector launched in the years 2010-2011 introduces a new model of financing, based on the principles of competitiveness, quality and transparency of procedures.</p>
                                                                <p>The reform is followed by increased spending which will allow the best among Polish academic institutions to compete with the world’s recognized educational centers.</p>

                                                            </div>
                                                            <div class="event-address">
                                                                
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div><!--- eventlisting --->
                                        <div class="event-listing event-listing-classic">
                                            <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5">
                                                    <div class="content-pad ">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                    <div class="content-pad">
                                                        <div class="item-content"></div>
                                                        <div class="item-meta">
                                                            <a class="flat-button"  href="<?php echo $this->webroot;?>universities/medicallist">MBBS IN POLAND <i class="fa fa-angle-right"></i></a>

                                                            <a class="flat-button"  href="<?php echo $this->webroot;?>universities/studyinpoland">UNIVERSITY OF LODZ <i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div><!--/event-listing-->
                                    </div> 
                                </div> 
                                <!-- //tab2 -->
                                 <div id="tab3" class="tab"> 
                                     <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                             <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/italy.png" alt="image">
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                     <div class="content-pad">
                                                        <div class="item-content">
                                                            <h4 class="item-title"> ITALY </h4>
                                                            <p>The Italian Republicis located at southern Europe and is a Schengen Visa zone.The Italians call their country “Bella Italy” meaning beautiful Italy and the country is worth that description. Italy has a population of 61 million people and the capital city is Rome. Other major cities in Italy are Florence, Milan, Naples and Venice. The country has a largely temperate seasonal and Mediterranean climate and the spoken language is Italian.Italy is popular for its rich cultural background and is one of the most popular tourist destinations in the world. The country is has achieved notable economic advancement since its formation as a Republic in the year 1946.</p>
                                                            <div class="price main-color-1"></div>
                                                            <h3><strong>HIGHER EDUCATION IN ITALY</strong></h3>
                                                            <div class="price main-color-1"></div>
                                                            <p>Italy is home to many prestigious institutions that has established a set of standards for excellence in higher education in Europe.</p>
                                                            <p> Education in Italy is free and mandatory from the age of six to sixteen.</p>
                                                            <p>A large number of reputed private and public universities provide higher education in Italy, which are some of the oldest in the world. Most of the universities are ran by the Ministry of Education.</p>
                                                            <p> Universities in Italy are increasingly welcoming international students and offer degree programs specially designed for them.</p>

                                                            <div class="event-time">
                                                            <h3><strong>WHY STUDY IN ITALY?</strong></h3>
                                                            <div class="price main-color-1"></div>
                                                            <p>Living expenses are budget friendly in smaller cities and Italian universities have lesser fee than most of the universities in Europe.</p>
                                                            <p> Italy offers a wide range of courses where students have the option of pursuing degree programs or certificate programs that can be studied online, through distance learning or facilitated through classrooms at universities.</p>
                                                            <p> The country has a very high level of human development index and excellent infrastructure development which makes it one of the best countries in Europe to live and pursue studies.</p>
                                                            <p> Italy has top universities with internationally reputed professors and facilities that enables students to find professional success and to settle in Europe. Due to these and more reasons students are opting to study in Italy.</p>
                                                            <p> English language can be used inside academic institutions and circles for communication. However it is advisable to learn Italian if you wish to stay back and work in Italy.</p>
                                                            
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                             </article>
                                        </div>
                                    </div>
                                </div>
                                <!-- //tab3 -->

                                 <div id="tab4" class="tab"> 
                                     <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                             <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/sweden.png" alt="image">
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                     <div class="content-pad">
                                                        <div class="item-content">
                                                            <h4 class="item-title"> SWEDEN </h4>
                                                            <p>Sweden is a Scandinavian nation with exotic landscapes and a unique culture.The capital, Stockholm and other important cities such as Gothenburg and Malmö are all located near the ocean.Although a member of European Union, its currency is Swedish Krona and not Euro. The large number of top universities in Sweden and relatively easy emigration process has made the country an attractive place for international students. English is a common medium for communication here as a result of which you can study without having to learn the Swedish language. </p>
                                                            <div class="price main-color-1"></div>
                                                            
                                                            <h3><strong>HIGHER EDUCATION IN SWEDEN</strong></h3>
                                                            <div class="price main-color-1"></div>
                                                            <p>Sweden presents a package of high quality education at low cost in some of its oldest universities in the world.</p> 
                                                            <p>Swedish universities offer educational programs according to European standard.</p>
                                                            <p> The Swedish academic calendar is split into two semesters, Autumn Semester starting form end of August till mid-January and Spring Semester starting from mid- January to June.</p>
                                                            <p> Swedish degrees are of high value in international job market, especially in fields of business and technology. IT sector in Sweden is estimated to be one of the best because of the innovative courses the country provides.</p>
                                                            <p> This in turn gives graduates from the country an easy access to work in Europe.  Majority degree programs in Sweden include internships, which gives youa real-world experience in tune with European standards while building your career.</p>
                                                        

                                                            <div class="event-time">
                                                            <h3><strong>WHY STUDY IN SWEDEN?</strong></h3>
                                                            <div class="price main-color-1"></div>
                                                            <p>Sweden ranks first in Global Creativity Index, second in the area of innovation and second position globally in terms of competitiveness.</p>
                                                            <p> Students are required to take a Residence Permit during their stay and can work for any number of hours a week.The Student Residence Permit can be converted into work permit without having to leave the country.</p>
                                                            <p>A large number of international students are granted work permits after completion of study in Sweden. At the successful completion of your studies, you can apply to extend your residence permit to look for work for up to six months.</p>
                                                            <p>A large number of courses offered in universities and university colleges are taught in English and the natives are friendly towards immigrant students. The excellent public transport system, infrastructure developments and the high standard of living ensures you a comfortable life during your stay at Sweden.</p>
                                                                
                                                            </div>
                                                            <div class="event-address">
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                             </article>
                                        </div>
                                    </div>
                                </div>
                                <!-- //tab4 -->
                                <div id="tab5" class="tab"> 
                                     <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                             <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/france.png" alt="image">
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                     <div class="content-pad">
                                                        <div class="item-content">
                                                            <h4 class="item-title"> FRANCE </h4>
                                                                <p>France is a developed country with the world's seventh-largest economy and  has long been a global center of art, science, and philosophy. The museums in France are enough to make you learn about history, art and science, being the motherland of many revolutionary schools of thought and innovations. Studying in France is an excellent opportunity to learn about its rich culture in a society where international students form all around the globe are welcomed. The country is bordered by Belgium, Luxembourg and Germany to the northeast, Switzerland and Italy to the east, and Andorra and Spain to the south.France has one of the strongest economies in European Union with excellent labor laws to protect its employees.</p>
                                                                <div class="price main-color-1"></div>

                                                                <h3><strong>HIGHER EDUCATION IN FRANCE</strong></h3>
                                                                <div class="price main-color-1"></div>
                                                                <p>France has been ranked to be the second most attractive destination for international students, with Canada in first position.</p>
                                                                <p>France offers various visa options for international students like short stay, temporary long stay, and long stay student visas. The education system has been developed in a manner that the students are expected to enjoy their life along with refining their knowledge and work skills.</p>
                                                                <p>The French educational system is highly organized and is divided into the three stages of primary education, secondary education, and higher education.Various surveys among international students pursuing their study in France states that factors like quality of programs, international experience and getting an internationally accepted degree were their reasons behind choosing France as a destination in Europe for higher studies.</p>
                                                                <p>However the possibility of finding a job in France will be dependent upon your academic performance and French language skills. </p>

                                                                <div class="event-time">
                                                                   
                                                                <h3><strong>WHY STUDY IN FRANCE?</strong></h3>
                                                                <div class="price main-color-1"></div>
                                                                <p>The French government invests a lot of education and research, which makes sure that excellent quality of education is available.</p>
                                                                <p> This is reflected in the fact that 39 French universities are listed in the QS World University Ranking List of 2018, with some of them ranking inside top 50 globally.</p>
                                                                <p> Most of these universities offer affordable fee options which are beneficial for students from Third World Countries.</p>
                                                                <p> Recent changes in migration policy allows students who have completed Masters Course to apply for a temporary residence permit for a period up to 24 months.</p>
                                                                <p> Any international student who has joined to study in France is allowed to do part time work for a period of maximum of 964 hours a year. </p>

                                                                </div>
                                                                <div class="event-address">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                             </article>
                                        </div>
                                    </div>
                                </div>
                                <!-- //tab5 -->
                                <div id="tab6" class="tab"> 
                                     <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                             <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/spain.png" alt="image">
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                     <div class="content-pad">
                                                        <div class="item-content">
                                                            <h4 class="item-title"> SPAIN</h4>
                                                                <p>Spain is one of the most culturally unique countries in Europe which has made important contributions to world history. The country is officially known as the Kingdom of Spain and the capital is Madrid. It has a population of about 47 million and Euro is the currency used. Spain is the largest country in Southern Europe, the second largest country in Western Europe and the European Union, and the fourth largest country in the European continent. By population, Spain is the sixth largest in Europe and the fifth in the European Union. Major urban areas include Barcelona, Valencia, Seville, Malaga and Bilbao, which are all world famous tourist hotspots. Spain is renowned for its bullfights, food, architecture, islands, beaches and Flamenco music.</p>
                                                                <div class="price main-color-1"></div>
                                                                


                                                                <h3><strong>HIGHER EDUCATION SYSTEM IN SPAIN</strong></h3>
                                                                    <div class="price main-color-1"></div>
                                                                    <p>The Spanish education system is divided into four stages, Nursery, Preschool, Primary and Compulsory Secondary Education.</p>
                                                                    <p>The Kingdom of Spain has 76 state funded universities that offer higher education to both native and international students.</p>
                                                                    <p>Apart from these there are 24 private universities and 7 affiliated to the Catholic Church.</p>
                                                                    <p>Most of these universities have secured their position as one of the best universities in the world, according to trusted international ranking indexes. </p>


                                                                    <div class="event-time">
                                                                    <h3><strong>WHY STUDY IN SPAIN?</strong></h3>
                                                                    <div class="price main-color-1"></div>
                                                                    <p>Spain is home to some of the oldest universities in the world, which makes studying there an unforgettable learning experience.</p>
                                                                    <p>We provide admission to certain reputed universities and our counselors will guide you through the process of selecting a suitable one with a course that is best suited for you.</p>
                                                                    <p>Spain joined the European Union in the tear 1986 and hence is a Schengen Visa country and an open door for international students into Europe.</p> 
                                                                    <p>Spain is an integral part of the European Union with regard to quality education and provides high standard higher education to international students.</p> 
                                                                    <p>Spanish universities are amongst the top ranked in the world and provide many courses and another factor that make this studying in this Schengen country easier is that you can choose to study without IELTS.</p>
                                                                    <p>The knowledge of Spanish language is necessary if the students wish to achieve part time work along with studies.</p><p>However international students from all around the world has found Spain as a admirable country to pursue higher education.</p>
                                                                    <p> As a result of rising number of international students in Spain, most universities have started offering courses in English along with Spanish.</p>
                                                                    <p> Low cost of living is a rare requirement to find in Europe but if you are looking to pursue European quality education at a cheap rate, that too in a Schengen country, then Spain will be an apt choice.</p>
                                                                    
                                                                    </div>
                                                                    <div class="event-address">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                             </article>
                                        </div>
                                    </div>
                                </div>
                                <!-- //tab6 -->
                                 <div id="tab7" class="tab"> 
                                     <div id="lipsum"> 
                                        <div class="event-listing event-listing-classic"> 
                                             <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5"> 
                                                    <div class="content-pad"> 
                                                        <div class="item-thumbnail"> 
                                                            <img src="<?php echo $this->webroot; ?>images/flags/moldova.png" alt="image">
                                                        </div> 
                                                    </div> 
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                     <div class="content-pad">
                                                        <div class="item-content">
                                                            <h4 class="item-title"> MOLDOVA </h4>
                                                                <p>Moldova is a landlocked country in Eastern Europe, which emerged after the collapse of Soviet Union in 1991. The domestic economy largely relies on agriculture and is a member of the Central European Free Trade Agreement (CEFTA). The country has a population of 3.5 million and the major languages spoken are Romanian and Russian. Moldova is a parliamentary republic and the capital city is called Chisinau. The country does not offer citizenship by birth, instead grants citizenship by descent only. 10 years of residency is required for naturalization and the neighboring countries are Romania and Ukraine. It is one of the poorest countries in Europe, which makes living expenses considerably lower in comparison to rest of Europe. The climate is mildly cold in the autumn and winter and relatively cool in the spring and summer.</p>
                                                                <div class="price main-color-1"></div>
                                                            <h3><strong>HIGHER EDUCATION IN MOLDOVA</strong></h3>
                                                            <div class="price main-color-1"></div>
                                                            <p>Higher education in Moldova is offered by universities, academies and institutes which are autonomous in nature.</p>
                                                            <p>The country is expecting to have a major leap in its educational system with the completion of its “Education 2020’ Strategy.</p>
                                                            <p>There are both private and public universities, academies and institutes offering full time courses.</p>

                                                            <div class="event-time">

                                                                <h3><strong> WHY STUDY IN MOLDOVA?</strong></h3>
                                                                <div class="price main-color-1"></div>
                                                                <p>Higher education in Moldova is focused on producing highly qualified professionals who are able to work effectively in changing conditions of life and economy. Low Tuition Fee is what attracts international students from across the world to this landlocked country. Average monthly living expenses in Moldova is estimated to be in between 500-600 Euro per month and part-time jobs allowed 20 hours per week. World class education is provided in fields like Engineering, Business, Medicine, Humanities and Language, Law and International Relations. International students are allowed to stay for one year after the completion of the study program. Choosing to study in Moldova is an exciting opportunity to understand the history of Europe and its culture. </p>
                                                            </div>
                                                            <div class="event-address">
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                             </article>
                                        </div>
                                        <div class="event-listing event-listing-classic">
                                            <article class="post-item row event-classic-item">
                                                <div class="col-md-4 col-sm-5">
                                                    <div class="content-pad ">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-7">
                                                    <div class="content-pad">
                                                        <div class="item-content"></div>
                                                        <div class="item-meta">
                                                            <a class="flat-button"  href="<?php echo $this->webroot;?>universities/medicallist">MBBS IN MOLDOVA <i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div><!--/event-listing-->
                                    </div>
                                </div>
                                <!-- //tab7 -->
                        </div><!--- tab-content  ---->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row">
                        <div class="eligible">
                            <h4 class="title">Free Assessment </h4>
                            <div class="contact-form">
                                <div class="line-box"></div>
                                <form action="<?php echo $this->webroot;?>countries/europeancountries" method="post" id="eligibilityform" class="comment-form" novalidate="">                         
                                    <fieldset class="style-1 full-name"> 
                                        <input type="text" id="name" placeholder="Your name" class="form-control" name="data[name]" tabindex="1" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                    <fieldset class="style-1 phone">
                                        <input type="text" id="phone" placeholder="Your phone number" class="form-control" name="data[phone]" tabindex="2" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                    <fieldset class="style-1 email-address">
                                        <input type="email" id="email" placeholder="Your email" class="form-control" name="data[email]" tabindex="3" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset> 

                                    <fieldset class="style-1 subject">
                                        <input type="text" id="qualification" placeholder="Qualification" class="form-control" name="data[qualification]" tabindex="4" value="" size="32" aria-required="true"style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset> 

                                    <fieldset class="style-1 percent">
                                        <input type="text" id="percent" placeholder="CGPA" class="form-control" name="data[percent]" tabindex="5" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                    <fieldset class="style-1 yearofpassing">
                                        <input type="text" id="yearofpassing" placeholder="Year of passing dd/mm/yyyy" class="form-control" name="data[yearofpassing]" tabindex="6" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                    <div class="submit-wrap">
                                        <button class="flat-button button-style style-v1">Check  Eligibility  <i class="fa fa-angle-right"></i></button>
                                    </div>             
                               </form>
                            </div><!-- contact-form -->
                        </div>
                    </div>
                </div>
           </div>
        </div> 
    </section>
                        
<style type="text/css">
    .help-block{
        color:#b20828!important;
        margin-top: -13px!important;
    }
    .title{
    margin-bottom: 20px;
    font-size: 21px;
    }
    .contact-form .submit-wrap button.flat-button.style-v1 {
    padding: 12px 20px;
    height: 42px;
    line-height: 0;
    background-color: #872b8e;
    border-top: #872b8e;
    border-left: #872b8e;
    border-right: #872b8e;
    border-bottom: rgba(0,0,0,.25);
    box-shadow: inset 0 -3px 0 rgba(0,0,0,.25);
    text-transform: none;
}
.contact-form .submit-wrap button.flat-button.style-v1:hover {
    background-color: #81c873;
    border-top: #81c873;
    border-left: #81c873;
    border-right: #81c873;
}
.eligible{
    border:1px solid #eaeaea;
    padding-top: 5px;
    padding-left:10px;
    padding-right: 10px;
    padding-bottom: 10px;
}
.post-item .content-pad a{
    margin-bottom: 10px;
}
.post-item .content-pad .flat-button {
    background-color: #872b8e;
    color: #FFF;
    border: #872b8e;

}
.post-item .content-pad .flat-button:hover {
    background-color: #81c873;
    color: #FFF;
    border: #81c873;

}
</style>
<script type="text/javascript">

jQuery(document).ready(function() {

jQuery('.tabs .tab-links a').on('click', function(e)  {
var currentAttrValue = jQuery(this).attr('href');

// Show/Hide Tabs
jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400); 

console.log(currentAttrValue);
jQuery(window).scrollTop(jQuery(".tabs").offset().top-100);
jQuery(".tab-links li").removeClass("active");
jQuery('a[href="'+currentAttrValue+'"]').parent('li').addClass('active');

e.preventDefault();
});
});


</script>
