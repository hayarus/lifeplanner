
     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Australia</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Australia
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                   <div id="content" class="col-md-9">
                        <div class="row">
                          <div class="csoon">
                            <img  src="<?php echo $this->webroot; ?>images/comingsoon.gif">
                          </div>
                            <div class="col-md-12 col-sm-12">
                              <div class="row">
                                  <div class="content-pad single-course-detail">
                                      <div class="course-detail">
                                          <div class="content-content">
                                              <div class="content-pad calendar-import">
                                                    <div style="float: left;">
                                                        <a href="<?php echo $this->webroot;?>countries/englishspeakingcountries" class="flat-button"> Study In Australia <i class="fa fa-angle-right"></i></a>
                                                    </div>
                                                </div>          
                                          </div><!--/content-content-->
                                      </div><!--/course-detail-->
                                  </div><!--/single-content-detail-->   
                                </div>
                              <div class="clear"></div>      
                          </div>
                        </div>
                    </div>
<style type="text/css">
.clear{
  margin-bottom: 10px;
  padding-top: 10px;
}
.content-content{
  border: none!important;
}
.nav a{
  font-weight: bold;
  font-size: 16px;
}
.tab-pane{
  padding: 20px;
}
.tab-title {
  font-style: normal;
  font-size: 16px;
  color:#656565;
  font-family: 'Oswald', sans-serif;
  letter-spacing: 1px;
  text-transform: uppercase;
  margin-top: 20px;
  margin-bottom: 10px;
}
.csoon{
 width: 100%;
 height: 100%;
  padding: 40px;
}
.csoon img{
 padding: 40px;
}
.single-course-detail .content-content .content-pad .flat-button {
    background-color: #872b8e;
    color: #FFF;
    border: #872b8e;

}
.single-course-detail .content-content .content-pad .flat-button:hover {
      background-color: #81c873;
      border-color: #81c873;
      color: #fff;

    }
</style>