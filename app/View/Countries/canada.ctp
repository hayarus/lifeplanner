
     <div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">Canada</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / Canada
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->

        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                   <div id="content" class="col-md-9">

                        <div class="col-md-12">
                            <div class="row">
                              <h4 class="tab-title">WHY CANADA IMMIGRATION</h4>
                                <p>Canada’s goal last year was 290,000 newcomers, Future Goal by 2020 is 340,000 intakes.</p>
                                <p>Life Planner opens a safe door to migrate and settle in Canada!</p>
                                <br>

                                <div class="col-md-12 col-sm-12">
                                  <div class="row">
                                      <div class="flat-accordion">
                                          <div class="name-toggle">
                                              
                                          </div>
                                          <div class="flat-toggle">
                                              <div class="toggle-title">Why Canada? </div>
                                              <div class="toggle-content">
                                                  <ul class="list-ul">
                                                    <li>Second largest country in the world</li>
                                                    <li>Tenth biggest economy in the world</li>
                                                    <li>Developing country</li>
                                                    <li>Federal governance system instead of Unitary</li>
                                                    <li>75% of unutilized landmass</li>
                                                    <li>Unexploited natural resources</li>
                                                    <li>Low population of three and a half crore</li>
                                                 </ul>                                
                                              </div>
                                          </div><!-- /toggle -->
                                          <div class="flat-toggle">
                                              <div class="toggle-title">Benefits of choosing Canada</div>
                                              <div class="toggle-content">
                                                  <div class="info"> 
                                                  <ul class="list-ul">                                   
                                                      <li>Immense chance for immigration being the second largest country in the world with a population size similar to that of Kerala.</li>
                                                      <li>Steady economic growth and abundance of natural resources that assures a sustainable future for Canada.</li>
                                                      <li>Immigrant labour force required for infrastructure developed in its fast growing economy. This proves why Canada has an immigrant population of 90% immigrants.</li>
                                                    </ul>
                                                  </div>
                                              </div>
                                          </div><!-- /.toggle -->
                                          <div class="flat-toggle">
                                              <div class="toggle-title">Why LIFE PLANNER? </div>
                                              <div class="toggle-content">
                                                  <div class="info">                                    
                                                      <ul class="list-ul">                                   
                                                      <li>7 years of experience in the field</li>
                                                      <li>Many happy students and a large number of them becameCanadian citizens after studies.</li>
                                                      <li>Transparent processing without any hidden unnecessary charges.</li>
                                                      <li>A friendly team of experts to assist you.</li>
                                                      <li>Detailed counselling conducted throughout admission and visa processing after understanding the talent, interest and study skills of individual students.</li>
                                                      <li>Being a Federal governance system, all 10 provinces of Canada have different immigration laws. We help you choose Provinces with lesser immigration restrictions so you can become a permanent resident soon.</li>
                                                      <li>Expert training through Gurukulam mode of education at our own Academy to ensure that you get admission with excellent IELTS score. Free training for selected candidates.</li>
                                                      <li>Dedicated service provided in admission and visa processing, airport pick up, and even arranging a place for you to stay in a new country.</li>
                                                      <li>Find a job in Canada after studies with the help of our Immigration Consultant Office.</li>
                                                    </ul>
                                                  </div>
                                              </div>
                                          </div><!-- /.toggle --> 
                                          <div class="flat-toggle">
                                              <div class="toggle-title">Legal Authority </div>
                                              <div class="toggle-content">
                                                  <div class="info">                                    
                                                      <div class="col-md-12 col-sm-12">
                                                      <div class="row">
                                                        <div class="info"> 
                                                            <h4 class="info-title">Legal Authority</h4>
                                                              <div class="col-md-12"> 
                                                                <div class="row"> 
                                                                  <div class="col-md-3">
                                                                    <div class="contact-form">
                                                                      <div class="row">
                                                                        <div class="col-md-12">
                                                                          <img src="<?php echo $this->webroot ; ?>images/immigration/MaryAnnGo.jpg">
                                                                        </div>
                                                                          <div class="col-md-12">
                                                                            <strong>Mary Ann Go</strong>
                                                                            <p>(Paralegal Regulated Canadian Immigration Consultant)</p>
                                                                            <p>ICCRC ID : R508947 </p>
                                                                            <p><strong>Office At : </strong>505 34th Ave NE </p>
                                                                            <p>Calgary, AB T2E 2J9 </p>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <div class="contact-form">
                                                                      <div class="row">
                                                                          <div class="col-md-12">
                                                                            <p>To make Canadian Immigration easy to access for everyone and to deliver sound, reliable and thorough immigration advice and assistance by simplifying the immigration process for qualified clients and their families.<p>
                                                                            <p>We will continually check your application until you obtain your residence status in Canada.We will represent and support you up to the end of your immigration process.</p>
                                                                            <p>We are not only here to help you come to Canada, New Beginnings Immigration Services will also provide you advice on better opportunities that you can possibly embark as you start your New Beginnings with us.</p>
                                                                            <p>It is our duty to deliver a holistic, professional approach and continually improve our services to meet the highest standards and fully satisfy our clients.</p>
                                                                          </div>
                                                                        </div>
                                                                      </div>
                                                                    </div> 
                                                                  </div> 
                                                                </div>
                                                            </div>  
                                                          </div>
                                                      <div class="clear"></div>      
                                                    </div>
                                                  </div>
                                              </div>
                                          </div><!-- /.toggle --> 
                                      </div><!-- /.accordion -->
                                    </div>
                                    <div class="clear"></div>
                                </div><!--/col-md-6 col-sm-6 -->
                                 <div class="content-pad single-course-detail">
                                    <div class="course-detail">
                                        <div class="content-content">
                                            <div class="content-pad calendar-import">
                                                  <div style="float: left;">
                                                      <a href="<?php echo $this->webroot;?>countries/englishspeakingcountries" class="flat-button"> STUDY IN CANADA <i class="fa fa-angle-right"></i></a>
                                                  </div>
                                              </div>          
                                        </div><!--/content-content-->
                                    </div><!--/course-detail-->
                                </div><!--/single-content-detail--> 
                            </div>
                        </div>
                      </div> <!-- col-md-9  -->
                      
                      <div class="col-md-3">
                        <div class="row">
                        <div class="eligible">
                            <h4 class="title">Free Assessment </h4>
                            <div class="contact-form">
                                <div class="line-box"></div>
                                <form action="<?php echo $this->webroot;?>countries/canada" method="post" id="eligibilityform" class="comment-form" novalidate="">                         
                                    <fieldset class="style-1 full-name"> 
                                        <input type="text" id="name" placeholder="Your name" class="form-control" name="data[name]" tabindex="1" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                    <fieldset class="style-1 phone">
                                        <input type="text" id="phone" placeholder="Your phone number" class="form-control" name="data[phone]" tabindex="2" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                    <fieldset class="style-1 email-address">
                                        <input type="email" id="email" placeholder="Your email" class="form-control" name="data[emailid]" tabindex="3" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset> 

                                    <fieldset class="style-1 subject">
                                        <input type="text" id="qualification" placeholder="Job title" class="form-control" name="data[jobtitle]" tabindex="4" value="" size="32" aria-required="true"style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset> 

                                    <fieldset class="style-1 percent">
                                        <input type="text" id="percent" placeholder="Years of experience" class="form-control" name="data[yearofexp]" tabindex="5" value="" size="32" aria-required="true" style="border: 1px solid #eaeaea; background-color: #eaeaea;">
                                    </fieldset>

                                   

                                    <div class="submit-wrap">
                                        <button class="flat-button button-style style-v1">Check  Eligibility  <i class="fa fa-angle-right"></i></button>
                                    </div>             
                               </form>
                            </div><!-- contact-form -->
                        </div>
                    </div>
                </div>
           </div>
        </div> 
    </section>
<style type="text/css">
  .flat-accordion .toggle-content {
        font-family:inherit!important;
  }
  .content-content{
    border:none!important;
  }
  .clear{
    margin-bottom: 10px;
    padding-top: 10px;
}
  .nav > li > a {
    padding: 10px 8px;
}
  .nav a{
    font-weight: bold;
    font-size: 15px;
  }
  .tab-pane{
    padding: 20px;
  }
  .tab-title , .tab-form-title, .info-title{
    font-style: normal;
    font-size: 18px;
    color:#656565;
    font-weight: 500;
    font-family: 'Bitter', sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-top: 10px;
    margin-bottom: 5px;
}
.info-title{
  margin-bottom: 20px;
}
.required{
  color:red;
}
.contact-form .submit-wrap button.flat-button.style-v1 {
    padding: 12px 20px;
    height: 42px;
    line-height: 0;
    background-color: #179bd7;
    border-top: #179bd7;
    border-left: #179bd7;
    border-right: #179bd7;
    border-bottom: rgba(0,0,0,.25);
    box-shadow: inset 0 -3px 0 rgba(0,0,0,.25);
    text-transform: none;
}
.comment-form select {
    padding: 10px 10px;
    width: 100%;
}
.assesment-form{
background-color: #9400D3;
margin-bottom: 2%;
border-radius: 5px;
}
.assesment-form .tab-form-title{
  color:#fff;
}

.list-ul{
        margin-top: 10px;
        margin-left: 10px;
        margin-bottom: 20px;
    }
.list-ul li {
      list-style-type: square;
      margin-bottom: 5px;
      margin-left: 10px;
  }
  .eligible{
    border:1px solid #eaeaea;
    padding-top: 5px;
    padding-left:10px;
    padding-right: 10px;
    padding-bottom: 10px;
}
.title{
    margin-bottom: 20px;
    font-size: 21px;
    }
.help-block{
    color:#b20828!important;
    margin-top: -13px!important;
    }

.single-course-detail .content-content .content-pad .flat-button {
    background-color: #872b8e;
    color: #FFF;
    border: #872b8e;

}
.single-course-detail .content-content .content-pad .flat-button:hover {
      background-color: #81c873;
      border-color: #81c873;
      color: #fff;

    }
.contact-form .submit-wrap button.flat-button.style-v1 {
    padding: 12px 20px;
    height: 42px;
    line-height: 0;
    background-color: #872b8e;
    border-top: #872b8e;
    border-left: #872b8e;
    border-right: #872b8e;
    border-bottom: rgba(0,0,0,.25);
    box-shadow: inset 0 -3px 0 rgba(0,0,0,.25);
    text-transform: none;
}
.contact-form .submit-wrap button.flat-button.style-v1:hover {
    background-color: #81c873;
    border-top: #81c873;
    border-left: #81c873;
    border-right: #81c873;
}
/* ---// meadia screen //--- */
@media (min-width: 1200px){
  .tab-image img {
    /*width: 70%;*/
  }
}
</style>

<script type="text/javascript">
     
    $(document).ready(function(){

    var error1 = $('.alert-danger');
    $('#assesmentform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "data[firstname]" : {required : true},
    "data[lastname]" : {required : true},
    "data[dob]" : {required : true},
    "data[mobile]" : {required : true, minlength:10, maxlength:10 },
    "data[email]" : {required : true,email:true},
    "data[qualific]" : {required : true},
    "data[completion]" : {required : true},
    "data[percentage]" : {required : true},
    "data[residential]" :{required:true}, 
    "data[city]" :{required:true},
    "data[street]" :{required:true},
    "data[pin]" :{required:true, minlength:6, maxlength:6 },
    "data[country]" :{required:true},
    "data[state]" :{required:true},
    },
    messages:{
    "data[firstname]" : {required : "Please enter first name"},
    "data[lastname]" : {required : "Please enter last name"},
    "data[dob]" : {required : "Please enter date of birth."},
    "data[mobile]" : {required : "Please enter mobile number."},
    "data[email]" : {required : "Please enter  email id.",email:"Please enter valid email id."},
    "data[qualific]" : {required : "Please enter qualification."},
    "data[completion]" : {required : "Please enter year of completion."},
    "data[percentage]" : {required : "Please enter mark percentage."},
    "data[residential]"  :{required : "Please enter residential name or residential number"},
    "data[city]"  :{required : "Please enter city"},
    "data[street]"  :{required : "Please enter street name"},
    "data[pin]"  :{required : "Please enter pin"},
    "data[country]"  :{required : "Please enter country name"},
    "data[state]"  :{required : "Please enter state name"},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
    //success1.hide();
    error1.show();
    //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
    $(element)
    .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
    $(element)
    .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
    label
    .closest('.form-group').removeClass('has-error'); // set success class to the control group
    label
    .closest('.form-group').removeClass('error');
    },
  });
     var error1 = $('.alert-danger');
    $('#eligibilityform').validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",
    rules:{
    "data[name]" : {required : true},
    "data[phone]" : {required : true, minlength:10, maxlength:10,},
    "data[emailid]" : {required : true,email:true},
    "data[jobtitle]" : {required : true},
    "data[yearofexp]" : {required : true},

    },
    messages:{
    "data[name]" : {required :"Please enter name."},
    "data[phone]" : {required : "Please enter phone no."},
    "data[emailid]" : {required :"Please enter email.",email:"Please enter valid email."},
    "data[jobtitle]" : {required :"Please enter job title."},
    "data[yearofexp]" : {required :"Please enter year of experience."},
    },

    invalidHandler: function (event, validator) { //display error alert on form submit              
    //success1.hide();
    error1.show();
    //App.scrollTo(error1, -200);
    },

    highlight: function (element) { // hightlight error inputs
    $(element)
    .closest('.form-group').addClass('has-error'); // set error class to the control group
    },

    unhighlight: function (element) { // revert the change done by hightlight
    $(element)
    .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },

    success: function (label) {
    label
    .closest('.form-group').removeClass('has-error'); // set success class to the control group
    label
    .closest('.form-group').removeClass('error');
    },
  });
});
</script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>