<link rel="stylesheet" type="text/css" href="<?php echo $this->webroot;?>css/style-tab.css">
<div class="page-title full-color">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">                    
                        <div class="page-title-heading">
                            <h2 class="title">English Speaking Countries</h2>
                        </div>
                        <div class="breadcrumbs"><?php echo $this->Html->getCrumbs(' > ','Home'); ?> / English Speaking Countries
                                              
                        </div>                  
                    </div><!-- /.col-md-12 -->  
                </div><!-- /.row -->  
            </div><!-- /.container -->                      
        </div><!-- /page-title -->
        <section class="flat-row padding-v1" id="main">
            <div class="container">
                <div class="row">
                    <div id="content" class="col-md-9">
                        <div class="tabs">
                            <ul class="tab-links">
                                <li class="active"><a href="#tab1">CANADA</a></li>
                                <li><a href="#tab2">AUSTRALIA</a></li>
                                <li><a href="#tab3">NEW ZEALAND</a></li>
                                <li><a href="#tab4">USA</a></li>
                                <li><a href="#tab5">UK</a></li>
                                <li><a href="#tab6">IRELAND</a></li>
                            </ul>
                            <br>
                            <div class="tab-content">
                                <div id="tab1" class="tab active">
                                    <div id="lipsum">
                            <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                          <img src="<?php echo $this->webroot; ?>images/flags/canada.png" alt="image">  
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h4 class="item-title">
                                                CANADA
                                            </h4>
                                            <p>Canada has over the years shown steady fast growth in the fields of economic development and technological innovation. It is the second largest country in the world with a comparatively small population which has resulted in only 75% of the landmass being utilized. The most populated Provinces in the country are Ontario, Quebec and British Columbia. Canada has some of the most diverse population in the world because of its pro emigration attitude throughout history. Majority of the native Canadians identify themselves as being part of more than one ethnic origin, with a considerable number of Indians residing in the country. English is the official language and the country follows a federal governance system. Canada has a steady growing economy and offers high standard of living to its natives. Its large natural gas and crude oil reserves along with lots of other unexploited resources makes the country a place of high potential to settle down after higher education in Canada. </p>
                                            <div class="price main-color-1"></div>
                                            <h3><strong>HIGHER EDUCATION IN CANADA </strong></h3>
                                            <div class="price main-color-1"></div>

                                            <p>Canada is a very popular destination for students looking to gain a degree overseas because of its respected universities and its reputation as a safe and welcoming country.</p>
                                            <p>With consistently high levels of education spending, it boasts high standards of higher education , with several institutions regularly featuring among the top 100 globally.</p>
                                            <p>With tuition fees lower than many Anglophone countries, and health insurance cheaper than across the border in the US, it is no wonder that an increasing number of English-speaking students are starting to consider courses there.</p>
                                            <p>The higher education system in Canada is not very different from the American education system, with a little influence from Britain.</p>
                                            <strong>There are mainly three types of institutions:</strong>
                                            <p><strong>Public Universities:</strong> Public universities in Canada are run by the provincial, territorial or federal government funding besides receiving the tuition fees from students.</p>
                                            <p><strong>Private Universities:</strong> Private universities in Canada are mostly funded by donations and research grants. These universities do not receive funding from the government bodies.</p>
                                            <p><strong>Liberal Arts Colleges:</strong> Liberal arts colleges in Canada are pretty similar to that in the USA. These are either public or private colleges, with a primary emphasis on undergraduate courses in liberal arts.</p>

                                            <div class="event-time">
                                             <h3><strong>WHY STUDY IN CANADA?</strong></h3>
                                              <div class="price main-color-1"></div>
                                                <p>International students are largely attracted to Canada because of its emigration friendly policies and accessibility to high quality education with a low tuition fee.</p>
                                                <p> A degree acquired from a Canadian university is considered with due respect in the United States and all around Europe.</p>
                                                <p> Almost all major universities offer excellent scholarship programs and your chances of getting a job in the same country are high after the completion of course.</p>
                                                <p>Students will find lots of part time work opportunities and no separate work permit is needed to take up jobs for up to 20 hours a week.</p> 
                                                <p>Immigrant labour force is required for infrastructure developed in its fast growing economy, considering its low population density.</p> 
                                                <p>Students who have completed their course can get work permit up to two years and are allowed to apply for permanent residence after the completion of one year work permit.</p>
                                                <p> As the country is in favour of accepting international students, the visa processing is transparent and less complicated compared to that of European countries of US.</p>
                                                <p> Canada is also known for its tolerance to foreigners and your individual values will be respected. </p>  
        
                                            </div>
                                            <div class="event-address">
                                                
                                            </div>
                                        </div>
                                        <div class="item-meta">
                                            
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad ">
                                        
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content"></div>
                                        <div class="item-meta">
                                            <a class="flat-button"  href="<?php echo $this->webroot;?>countries/canada">IMMIGRATION TO CANADA <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
                    </div>
        </div>
        <div id="tab2" class="tab">
            <div id="lipsum">
                <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                            <img src="<?php echo $this->webroot; ?>images/flags/australia.png" alt="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h4 class="item-title">
                                               AUSTRALIA
                                            </h4>
                                            <p>Australia, officially known as the Commonwealth of Australia is one of the best countries to live in. It is the sixth largest country in the world with low population density. Australia is a developed country with the world's 13th-largest economy. The country provides high standard of living, health benefits, quality education, economic freedom, civil liberties and political rights. Majority of the present population have reached the continent through immigration during the past few centuries.  The country has been witnessing large immigration flows in the recent past, mostly high skilled workers and has resulted in the formation of a tolerant multicultural society. </p>
                                            
                                            <div class="price main-color-1"></div>
                                            <div class="event-time">
                                                <h3><strong>HIGHER EDUCATION IN AUSTRALIA</strong></h3>
                                                <div class="price main-color-1"></div>
                                                    <p>Overseas study is generally expensive but Australia provides affordable education with excellent standard of living.</p>
                                                    <p> The education curriculum is designed in a way as to fulfil the needs of current employment opportunities.</p>
                                                    <p> Education in Australia comprises of early childhood education (preschool) and primary education, followed by secondary education, tertiary education and adult education. There is an integrated system for assessing qualifications, known as Australian Qualification Framework (AQF) and 39 universities for higher education in Australia.</p>
                                                    <p> Australia is growing as a destination among international students wishing for its high quality scientific research, which makes the country at the forefront of new technology and innovations.</p>
                                                    <p> The academic year usually starts at September and November every year while some universities recruit around the year. We advise the aspirants to start the processing several months before itself as it takes a while.</p>
                                            </div>
                                            <div class="event-address"> 
                                                <h3><strong>WHY STUDY IN AUSTRALIA?</strong></h3> 
                                                <div class="price main-color-1"></div>
                                                    <p>Educational institutions in Australia offer a wide variety of courses and degrees and the students can select a course that is suitable for them through counseling with our experts. </p>
                                                    <p>Many international students choose to study in Australia because of the cultural diversity, immigrant friendly policies and high quality of education. </p>
                                                    <p>There is a huge Indian student population in Australia and a bigger population of Australian citizens who are of Indian origin and have attained permanent residence over the years.</p>
                                                    <p> The visa process to Australia is transparent and straight forward. </p>
                                                    <p>It is the third most popular destination for international students among English-speaking countries. </p>
                                                    <p>Living expenses and tuition costs are considerably lower in Australia than they are in other English speaking countries. </p>
                                                    <p>Work permit allowed for international students is another reason for aspirants to prefer Australia for higher education along with the availability of attractive scholarships based on academic excellence and merit. </p>
                                                    <p>Students with a valid Australian Student Visa are allowed to work up to 20 hours per week. </p>
                                                    <p>This will help the students to earn money pay for living expenses and is an excellent opportunity to gain work experience in their field of study. </p>
                                                    <p>Students can choose between universities, vocational education, and English language training according to their preferences and we are here to suggest excellent choices suited for you.</p>

                                            </div>
                                        </div>
                                        <div class="item-meta">
                                           
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
                        <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad ">
                                        
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content"></div>
                                        <div class="item-meta">
                                            <a class="flat-button"  href="<?php echo $this->webroot;?>countries/australia">IMMIGRATION TO AUSTRALIA <i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
            </div>
        </div> 
        <div id="tab3" class="tab">
            <div id="lipsum">
                <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                           <img src="<?php echo $this->webroot; ?>images/flags/newzealand.png" alt="image">
                                        </div>
                                    </div>
                                </div>
                               <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h4 class="item-title">
                                                NEWZEALAND
                                            </h4>
                                             <div class="price main-color-1"></div>
                                             New Zealand is a developed country that provides an international quality of life, health, education, protection of civil liberties, and economic freedom. The country is situated in the Pacific Ocean and comprises mostly of two main islands called the North Island and the South Island. The languages spoken are English and Maori and the country has a very high Human Development Index. New Zealand has a rich culture that is diverse, which makes it tolerant towards immigrants. The country is largely dependent upon agricultural products, manufacturing and tourism. Is rich with unique plants and animals that are not found in any other parts of the world. New Zealand had major structural changes in its economy during the 1980s from a protectionist to a liberal free-trade economy. 
                                            <div class="price main-color-1"></div>

                                            
                                            
                                            <div class="event-time">
                                                <h3><strong>HIGHER EDUCATION IN NEW ZEALAND</strong></h3>
                                                <div class="price main-color-1"></div>
                                                    <p>The educational system in New Zealand is extremely varied and is one of the best in the world. It has a high level of literacy and the public educational system is one of the best in the world. </p>
                                                    <p>New Zealand has 8 Universities in total that are located at various geographic regions. The country also hosts 18 Institutes of Technology and Polytechnics (ITPs) which offer courses in English.</p>
                                                    <p> The government spends the highest percentage of public funding in education in the world, which signifies why this small country is becoming increasingly popular among high quality education aspirants.</p>
                                                    <p> The country offers certificates, diplomas, graduate and post graduate degrees, which are all ranked on the basis of New Zealand Qualifications Framework.</p> 
                                                    <p>The institutions are allowed to decide their own fees hence it will be different for each. This is where you need an agency that is familiar with various institutions in New Zealand to help you choose the ideal course based on your budget.</p>
                                            </div>
                                            <div class="event-address">
                                                <h3><strong>WHY STUDY IN NEW ZEALAND?</strong></h3>
                                                <div class="price main-color-1"></div>
                                                    <p>What makes this country a favorite destination international students is that the New Zealand natives, permanent residents and foreign workers who are on work visa are all covered by social security, without the need to make social security contributions. </p>
                                                    <p>The country is popular for its educational system and standard of living which is why it should be your number one reason for choosing New Zealand for higher education. </p>
                                                    <p>First, the tuition fee is comparatively lowest in the world. </p>
                                                    <p>You will be getting European standard of education at a comparatively lower price than that of a Schengen visa country. </p>
                                                    <p>The support services for international students are the best in the world. </p>
                                                    <p>The degrees offered here are acknowledged all around the world with which you can apply for a job anywhere in the world. </p>
                                                    <p>You will get a high quality education that is in tune with recent global standards and demand. </p>
                                                    <p>The government has made it mandatory for institutions to revise the syllabus at regular intervals to ensure high quality education.</p>
                                            </div>
                                        </div>
                                        <div class="item-meta">

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
            </div>
        </div> 

        <div id="tab4" class="tab">
            <div id="lipsum">
                 <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                           <img src="<?php echo $this->webroot; ?>images/flags/usa.png" alt="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h4 class="item-title">
                                                UNITED STATES OF AMERICA 
                                            </h4>
                                             <div class="price main-color-1"></div>
                                             The United States of America has the most technologically advanced economy in the world with unmatched standards. The U.S. economy is characterized by the dominance of service sector and technology with the manufacturing sector remaining as the second-largest in the world.  Although its population is only 4.3% of the world, the U.S. holds 31% of the total wealth in the world. Even though wide income and wealth inequalities exist in the United States, the country ranks very high in measures of socio-economic performance including average wage, human development, per capita GDP, and worker productivity. 
                                            <div class="price main-color-1"></div>
                                            <div class="event-time">
                                                <h3><strong>HIGHER EDUCATION IN USA</strong></h3>
                                                <div class="price main-color-1"></div>
                                                <p>Compared to the system of higher education in other countries, higher education in the United States is largely private from government control and is highly decentralized. </p>
                                                <p>International students have a wide-variety of courses to choose from and can also select to study in public or private universities, universities with different student populations, or even religiously affiliated universities. Whether you prefer urban, suburban, or rural location to study, there will be a perfect institution at every point that fits your needs. </p>
                                                <p>American higher education gives a large emphasis on independent research, quality, diversity, and accessibility. More than 16 million students are enrolled in university programs for getting an associate’s, bachelors, master’s, or doctorate degree.</p>
                                            </div>
                                            <div class="event-address">
                                                <h3><strong>WHY STUDY IN USA?</strong></h3>
                                                <div class="price main-color-1"></div>
                                                    <p>Majority international students prefer the USA than any other country in the world for many reasons. Check out any ranking system of universities and you will find American universities on the top list.</p>
                                                    <p> Apart from top ranked universities, there are almost 4,000 other world-class universities in US that offer high quality education that can lead to excellent career opportunities in the US and all around the world.</p>
                                                    <p> The US produces most of the world’s ground-breaking innovations in technology, business, arts, and every other field imaginable.</p>
                                                    <p>Getting a Green Card and becoming a permanent resident in the US is a dream come true for anyone migrating from their country and studying in the US brings you closer to this dream.</p>
                                                    <p> International students in the US can do part time work in campus up to 20 hours a week.</p>
                                                    <p> Most of the students utilize time to be part of internships, or Curricular Practical Training, which allows students to work for university credit and gain expertise in a job that is related to their field of study. Nearly every university, even those not located in big cities, has employees dedicated to helping students secure internships.</p>
                                                    <p> The US has welcomed millions of international students in the past so you will find networks of international students available at all universities to make you feel, welcomed. </p>
                                            </div>
                                        </div>
                                        <div class="item-meta">

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
            </div>
        </div> 

        <div id="tab5" class="tab">
            <div id="lipsum">
                 <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                           <img src="<?php echo $this->webroot; ?>images/flags/uk.png" alt="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h4 class="item-title">
                                                UNITED KINGDOM
                                            </h4>
                                             <div class="price main-color-1"></div>
                                                The United Kingdom consists of four constituent countries: England, Scotland, Wales, and Northern Ireland. The country does not need much introduction among Indians because of our colonial past. London is the capital city and Pond Sterling is the currency.  It has a high-income economy and has a very high Human Development Index rating, ranking 14th in the world. It was the first industrialised country and colonial super power during the 18th, 19th and early 20th centuries. The British Empire was described to be the one where “the sun never sets”. The Empires glory faded with the First and Second World Wars which resulted in the independence of its colonies into nation states. However the UK still remains relatively powerful with considerable economic, cultural, military, scientific and political influence internationally. It has been a leading member state of the European Union (EU) and its predecessor, the European Economic Community (EEC) since 1973. A public referendum in 2016 resulted in 51.9% of UK voters favoring to leave the European Union and the country's controversial exit is still being negotiated. 

                                            <div class="price main-color-1"></div>
                                            <div class="event-time">
                                                <h3><strong>HIGHER EDUCATION IN UNITED KINGDOM</strong></h3>
                                                <div class="price main-color-1"></div>
                                                <p>The UK education system is worldwide reputed for its high quality and standards. In general, the British higher education system has five stages of education: early years, primary years, secondary education, Further Education (FE) and Higher Education (HE).</p>
                                                <p> Britons enter the education system at the age of three and up to 16 are obliged to attend school (compulsory education), while afterward is upon their choice. Besides sharing many similarities, the UK education system at different levels at each zone of administration (England, Scotland, and Wales) differs a bit. </p>
                                                <p>Generally spoken these differences are not so meaningful that we can talk about the UK higher education as being one.</p>
                                                <div class="price main-color-1"></div>
                                            </div>
                                            <div class="event-address">
                                                <h3><strong>WHY STUDY IN UK?</strong></h3>
                                                <div class="price main-color-1"></div>
                                                    <p>British higher education and qualifications have an impressive international reputation.</p>
                                                    <p>The UK has a long history of welcoming international students to study in its universities and colleges.</p> 
                                                    <p>In Britain last year there were 1.8 million full-time undergraduate students in higher education, which included over 104,000 international students. </p>
                                                    <p>Undergraduate and postgraduate courses in the UK tend to be shorter than in other countries which can help to keep the cost of tuition fees and living expenses down.</p>
                                            </div>
                                        </div>
                                        <div class="item-meta">

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing--> 
            </div>
        </div> 

        <div id="tab6" class="tab">
            <div id="lipsum">
                <div class="event-listing event-listing-classic">
                            <article class="post-item row event-classic-item">
                                <div class="col-md-4 col-sm-5">
                                    <div class="content-pad">
                                        <div class="item-thumbnail">
                                           <img src="<?php echo $this->webroot; ?>images/flags/ireland.png" alt="image">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-7">
                                    <div class="content-pad">
                                        <div class="item-content">
                                            <h4 class="item-title">
                                                IRELAND
                                            </h4>
                                             <div class="price main-color-1"></div>
                                             Ireland is officially known as the Republic of Ireland with a population of almost 5 million and is bordered by Northern Ireland and United Kingdom.  Irish education is often considered as equivalent to UK education and is a member of European Union. Indian students are required to get an acceptance letter from an authorized university before starting the visa process. Each college and university will be having different rules and regulations for admission which can be provided by expert counselors. 
                                            <div class="price main-color-1"></div>
                                            <div class="event-time">
                                                <h3><strong>HIGHER EDUCATION IN IRELAND </strong></h3>
                                                <div class="price main-color-1"></div>
                                                <p>STUDY in one of the best education systems in the world for higher education! Ireland has held its position in international ranking systems of higher education since a while now.</p> 
                                                <p>The country maintains 19th position among 50 countries in the filed of higher education according to the 2018 World Universities Ranking.</p> 
                                                <p>Irish universities excel in the field of natural sciences, social sciences and humanities as undergraduate and postgraduate courses.</p>
                                                <p> Ireland is also where some of the world’s biggest and best companies have located key strategic research facilities. And in Ireland, you’ll find a unique ecosystem that sees academic researchers working hand-in-hand with small home-grown and start-up companies in partnership with some of the most powerful multinational companies.</p>
                                            </div>
                                            <div class="event-address">
                                                 <h3><strong>WHY STUDY IN IRELAND?</strong></h3>
                                                <div class="price main-color-1"></div>
                                                <p>Studying from Ireland helps you benefit from its investment in the education system. </p>
                                                <p>International students can choose from over 5000 internationally recognised courses. </p>
                                                <p>Students can gain access to world-class research opportunities in highly innovative programs and connect with career opportunities with leading global companies located in Ireland.</p>
                                                <p>Fulfil your dream of studying in Europe with world class facilities and courses that expose you to recent trends and skill development.</p> 
                                                <p>Ireland is one of the friendliest and safest countries in the world. </p>
                                                <p>Ireland has around 35,000 international students from 161 countries studying in harmony with natives. </p>
                                                <p>The Irish Government invests millions on research and innovation of its higher education institutions.</p> 
                                                <p>The impact of this funding is seen in the kind of courses that Irish universities provides, which are some of the most sought after courses in the world.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="item-meta">

                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div><!--/event-listing-->
            </div>
        </div> 
    </div>
    
    <!-- <div class="tabs">
    <ul class="tab-links">
        <li class="active"><a href="#tab1">Tab #1</a></li>
        <li><a href="#tab2">Tab #2</a></li>
    </ul>
</div> -->
</div>

</div><!-- /col-md-9 -->
<style type="text/css">
.post-item .content-pad a{
    margin-bottom: 10px;
}
.post-item .content-pad .flat-button {
    background-color: #872b8e;
    color: #FFF;
    border: #872b8e;

}
.post-item .content-pad .flat-button:hover {
    background-color: #81c873;
    color: #FFF;
    border: #81c873;

}

</style>
<script type="text/javascript">

    jQuery(document).ready(function() {

    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
       jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400); 

       console.log(currentAttrValue);
        jQuery(window).scrollTop(jQuery(".tabs").offset().top-100);
        jQuery(".tab-links li").removeClass("active");
        jQuery('a[href="'+currentAttrValue+'"]').parent('li').addClass('active');

       e.preventDefault();
    });
});


</script>