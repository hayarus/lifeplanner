<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/ckeditor/ckeditor.js"></script>
<?php $this->Html->addCrumb('Events', '/admin/events'); ?>
<?php $this->Html->addCrumb('Add', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	CKEDITOR.config.contentsCss = '<?php echo $this->webroot; ?>css/style.css';
	var error1 = $('.alert-danger');
	$('#EventAdminAddForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Event][title]" : {required : true},
        "data[Event][eventdate]" : {required : true},
        "data[Event][venue]" : {required : true},
        "data[Event][description]" : {required: function() 
        		                    {
        		                    CKEDITOR.instances.StaticpageContent.updateElement();
        		                    }},
        "data[Event][image]" : {required : true, accept : "png|jpg|jpeg|gif", filesize: 1000000},
        "data[Event][status]" : {required : true},
		},
		messages:{
		"data[Event][title]" : {required :"Please enter heading."},
        "data[Event][eventdate]" : {required :"Please enter event date."},
        "data[Event][venue]" : {required :"Please enter venue."},
        "data[Event][description]" : {required :"Please enter description."},
        "data[Event][image]" : {required :"Please upload image.", accept : "Please upload an image in png, jpg, jpeg or gif format.", filesize: "File size should be less than 1MB."},
        "data[Event][status]" : {required :"Please enter status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "data[Event][description]"){
				$("span.err1").append(error);
			}else {
				error.insertAfter(element);
			}
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Add Event'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form events">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Event', array('class' => 'form-horizontal','enctype'=>'multipart/form-data','method'=>'post')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('heading<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
                        <div class="form-group">
                             <?php echo $this->Form->label('event date<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
                            <div class='col-md-4'>
                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy" data-link-field="CallDateOpened">
                                    <input type="text" size="16"  class="form-control" name="data[Event][eventdate]"  readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div> 
                            </div>
                        </div>
                        <div class="form-group">
                             <?php echo $this->Form->label('venue<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
                            <div class='col-md-4'>
                                <?php echo $this->Form->input('venue', array('class' => 'form-control', 'label' => false, 'required' => false));?>
                            </div>
                        </div>
                        <div class="form-group">
							 <?php echo $this->Form->label('description<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-8'>
								<?php echo $this->Form->input('description', array('class' => 'ckeditor form-control', 'label' => false, 'required' => false, 'id' => 'StaticpageContent'));?>
								<span class="err1"></span>
							</div>
						</div>
                        <div class="form-group">
							 <?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('image', array('class' => 'form-control', 'label' => false, 'type'=> 'file', 'required' => false));?>
								<span class="required" style="color:#F00;">Choose image diamention 359 X 250 </span>
							</div>
						</div>
                        <div class="form-group">
							 <?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$boolean_values_status));?>
							</div>
                        </div>
                        <div class="form-group">
							 <?php echo $this->Form->label('event_status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('event_status', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$boolean_values_event_status));?>
							</div>
						</div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/events'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->Html->addCrumb('Events', '/admin/events'); $paginationVariables = $this->Paginator->params();?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<div class="row">
    <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
	                <i class="fa fa-list-alt"></i><?php echo __('Events'); ?>                </div>
                <!-- <div class="tools">
    		        <a href="javascript:;" class="collapse"></a>
            	</div> -->
            </div>
            <div class="portlet-body"><br />
                <div class="table-toolbar">
                    <div class="btn-group"> 
                        <?php echo $this->Form->create('', array('type' => 'get','role' => 'form', 'url' => array('controller' => strtolower('Events'), 'action' => 'index', 'admin' => true))); ?>
                            <div class="row">
                            <!-- /.col-md-6 -->
                                <div class="col-md-6">
                                    <!-- <div class="input-group input-medium" >
                                        <input type="text" class="form-control" placeholder="Search here" name="search">
                                        <span class="input-group-btn">
	                                        <button class="btn green" type="button" onclick="this.form.submit()">Search <i class="fa fa-search"></i></button>
                                        </span>
                                    </div> -->
                                <!-- /input-group -->
                                </div>
                            <!-- /.col-md-6 -->
                            </div>
                        <!-- /.row -->
                        </form>
                    </div>
                    <div class="btn-group pull-right">
                        <div class="btn-group" style="padding-right:15px;">
	                        <!-- <?php echo $this->Html->link(__('New Event <i class="fa fa-plus"></i>'), array('action' => 'add'), array('class' => 'btn green','escape' => FALSE)); ?>  -->
                        </div>
                        <!-- <button class="btn default"  onclick="window.location.href='<?php echo $this->webroot.'admin/Events/export'; ?>'">Export to CSV <i class="fa fa-download"></i> </button> -->
                    </div>
                </div>
                <br />
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover " id="sample_1">
                  <thead>
                      <tr>
                        <th>#</th>  
                        <th><?php echo $this->Paginator->sort('title'); ?></th>
                        <!-- <th><?php echo $this->Paginator->sort('content'); ?></th> -->
                        <th>Preview</th>
                        <th><?php echo $this->Paginator->sort('status'); ?></th>
                        <th>Event Status</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                  </thead>
                  <?php  if(isset($events) && sizeof($events)>0) {?>
                  <tbody>
                  <?php $slno=0; foreach ($events as $event): $slno++?>
                  <tr>
                     <td><?php echo $slno+$paginationVariables['limit']*($paginationVariables['page']-1); ?></td>
        							<td><?php echo h($event['Event']['title']); ?>&nbsp;</td>
                  		<!-- <td><?php echo h($event['Event']['content']); ?>&nbsp;</td> -->
                  		<td><?php if(!empty($event['Event']['image'])){ ?><img src="<?php echo $this->webroot .$eventImagePath.$event['Event']['image']; ?>" alt="" width="150" height="150"/> <?php } else{?> No Video Available <?php } ?>
                          <td><?php echo h($boolean_values_status[$event['Event']['status']]); ?>&nbsp;</td>
                          <td><?php echo h($boolean_values_event_status[$event['Event']['event_status']]); ?>&nbsp;</td>
											<td>
													<?php echo $this->Html->link(__('<i class="fa fa-edit"></i> Edit'), array('action' => 'edit', $event['Event']['id']), array('class' => 'btn default btn-xs purple','escape' => FALSE)); ?>
											</td>
											<td>
													<?php echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i> Delete'), array('action' => 'delete', $event['Event']['id']), array('confirm' => 'Are you sure you want to delete this?','class' => 'btn red btn-xs black','escape' => FALSE)); ?>
											</td>
										</tr>
									<?php endforeach; ?>
                   <?php } else {?>
                      <tr><td colspan='7' align='center'>No Records Found.</td></tr>
                  <?php }?>
                  </tbody>
                </table>
              </div>
                <div class="row">
    	            <div class="col-md-5 col-sm-12" style="margin-top:10px;">
                      <div id="sample_1_length" class="dataTables_length">
                        <?php echo $this->Form->create('', array( 'url' => array('controller' => strtolower('Events'), 'action' => 'index'))); ?>
                            <label>Show 
                          			<?php echo $this->Form->input('limit', array('name' => 'data[Event][limit]','type' => 'select', 'class' => 'form-control input-xsmall', 'label' => false,  'required' => false,'hiddenField' => false, 'style' => 'display:inline-block !important; padding-right:0px', 'onchange' => 'this.form.submit()', 'options' =>$default_limit_dropdown, 'default' => $limit, 'div' => false));?>
                            records
                            </label>
                            <?php echo $this->Form->end(); ?>
                					</div>
                				</div>
                        <!-- <div class="col-md-7 col-sm-12">
                          <div class="dataTables_paginate paging_bootstrap">
                            <?php $totalItem = $this->Paginator->counter('{:count}')?><?php $currentItem = $this->Paginator->counter('{:current}')?><?php if($totalItem>$currentItem) {?>
                            <ul class="pagination" style="visibility: visible;">
                                <li class="prev disabled">
                                			<?php echo $this->Paginator->prev('< ' . __(''), array(), null, array('class' => 'prev disabled'));?>
	
                                </li>
                                <li>
                                			<?php		echo $this->Paginator->numbers(array('separator' => ''));	?>
                                </li>
                                <li class="next disabled">
                                			<?php		echo $this->Paginator->next(__('') . ' >', array(), null, array('class' => 'next'));	?>
                                </li>
                            </ul>
                      <?php }?>
    						        </div>
                      </div> -->
    				    </div>
    			</div>
    	</div>
    <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
