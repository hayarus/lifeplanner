<script type="text/javascript" src="<?php echo $this->webroot; ?>assets/plugins/ckeditor/ckeditor.js"></script>
<?php $this->Html->addCrumb('Events', '/admin/events'); ?>
<?php $this->Html->addCrumb('Edit', ''); ?>
<div style='padding-bottom:10px;'><?php echo $this->Session->flash(); ?></div>
<script type="text/javascript">
$(document).ready(function(){
	CKEDITOR.config.contentsCss = '<?php echo $this->webroot; ?>css/style.css';
	var error1 = $('.alert-danger');
	$('#EventAdminEditForm').validate({
		errorElement: 'span', //default input error message container
		errorClass: 'help-block', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules:{
		"data[Event][id]" : {required : true},
		"data[Event][title]" : {required : true},
		"data[Event][eventdate]" : {required : true},
        "data[Event][venue]" : {required : true},
		"data[Event][description]" : {required: function() 
		                    {
		                    CKEDITOR.instances.StaticpageContent.updateElement();
		                    }},
		"data[Event][image]" : {accept : "png|jpg|jpeg|gif", filesize: 1000000},
		"data[Event][status]" : {required : true},
		},
		messages:{
		"data[Event][id]" : {required :"Please enter id."},
		"data[Event][title]" : {required :"Please enter heading."},
		"data[Event][eventdate]" : {required :"Please enter event date."},
        "data[Event][venue]" : {required :"Please enter venue."},
		"data[Event][description]" : {required :"Please enter description."},
		"data[Event][image]" : { accept : "Please upload an image in png, jpg, jpeg or gif format.", filesize: "File size should be less than 1MB."},
		"data[Event][status]" : {required :"Please enter status."},
		},

		invalidHandler: function (event, validator) { //display error alert on form submit              
			//success1.hide();
			error1.show();
			//App.scrollTo(error1, -200);
		},

		highlight: function (element) { // hightlight error inputs
			$(element)
				.closest('.form-group').addClass('has-error'); // set error class to the control group
		},

		unhighlight: function (element) { // revert the change done by hightlight
			$(element)
				.closest('.form-group').removeClass('has-error'); // set error class to the control group
		},

		success: function (label) {
			label
				.closest('.form-group').removeClass('has-error'); // set success class to the control group
			label
				.closest('.form-group').removeClass('error');
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "data[Event][description]"){
				$("span.err1").append(error);
			}else {
				error.insertAfter(element);
			}
		},
	});
});

</script>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                 <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        	<i class="fa fa-reorder"></i><?php echo __('Edit Event'); ?>                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form events">
                    <div class="alert alert-danger display-hide">
                        <button data-close="alert" class="close"></button>
                        You have some form errors. Please check below.
                    </div>
                    <!-- BEGIN FORM-->
                    <?php echo $this->Form->create('Event', array('class' => 'form-horizontal','enctype'=>'multipart/form-data','method'=>'post')); ?>
                    <div class="form-body">  
                    	<div class="form-group">
	                        	<label class="col-md-3 control-label" for="">&nbsp;</label>
	                            <div class="col-md-4">
	                                <div class="input text">
										<span class="required" style="color:#F00"> *</span>= Required<br>
									</div>
	                            </div>
	                        </div>                                
                        <div class="form-group">
							 <?php echo $this->Form->label('<span class=required>  </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<span class="required" style="color:#F00;">Choose image diamention 359 X 250 </span>
								<?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('heading<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => false, 'required' => false));?>
							</div>
						</div>
						<div class="form-group">
                             <?php echo $this->Form->label('event date<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
                            <div class='col-md-4'>
                                <div class="input-group date date-picker" data-date-format="dd-mm-yyyy" data-link-field="CallDateOpened">
                                    <input type="text" size="16"  class="form-control" name="data[Event][eventdate]" value="<?php echo date("d-m-Y", strtotime($this->request->data['Event']['eventdate']));?>"  readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div> 
                            </div>

                        </div>
                        <div class="form-group">
                             <?php echo $this->Form->label('venue<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
                            <div class='col-md-4'>
                                <?php echo $this->Form->input('venue', array('class' => 'form-control', 'label' => false, 'required' => false));?>
                            </div>
                        </div>
						<div class="form-group">
							 <?php echo $this->Form->label('description<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-8'>
								<?php echo $this->Form->input('description', array('class' => 'ckeditor form-control', 'label' => false, 'required' => false, 'id' => 'StaticpageContent'));?>
								<span class="err1"></span>
							</div>
						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('image<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('image', array('class' => 'form-control', 'label' => false, 'type'=> 'file', 'required' => false));?>
								
								<span class="imageErrCont"></span>
                      		</div>
                      		<?php if(!empty($event['Event']['image'])) { ?>
							<div class="bs-example">
							<a href="#myModal" class="" data-toggle="modal"><i class="fa fa-search" style="margin-top: 12px;"></i></a>
							</div>
                            <?php } ?>

						</div>
						<div class="form-group">
							 <?php echo $this->Form->label('status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('status', array('class' => 'form-control', 'label' => false, 'required' => false ,'options' => $boolean_values_status));?>
							</div>
						</div>

						<div class="form-group">
							 <?php echo $this->Form->label('event_status<span class=required> * </span>' ,null, array('class' => 'col-md-3 control-label')); ?>
							<div class='col-md-4'>
								<?php echo $this->Form->input('event_status', array('class' => 'form-control', 'label' => false, 'required' => false,'options'=>$boolean_values_event_status));?>
							</div>
						</div>
						
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="button" class="btn default" onclick="window.location='<?php echo $this->webroot.'admin/events'; ?>'">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"> Image</h4>
                </div>
                <div class="modal-body">
                	<?php  if(isset($event['Event']['image'])){?>
							<img src="<?php echo $this->webroot .$eventImagePath.$event['Event']['image']; ?>" alt="" />
							<?php } ?>
                    
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
	 .modal-content iframe{
        margin: 0 auto;
        display: block;
    }
</style>