
    var locations = [
    ['Life Planner Studies & Opportunities Ernakulam','1st floor,saniya Plaza, Mahakavi Bharathiyar Rd, near south K.S, R.T.C, Shenoys, Ernakulam, Kerala 682035','https://www.google.com/maps/place/Life+Planner+Studies+%26+Opportunities+(P)+Ltd,Kochi/@9.9776213,76.2866544,19z/data=!4m5!3m4!1s0x3b080d7a4bc19b43:0x1d4126320c8d21bc!8m2!3d9.9777547!4d76.2865176'],
    ['Life Planner Studies & Opportunities Kottayam','2nd Floor,Thevarolil Building, Lal Bahadur Shastri Rd, Kottayam, Kerala 686001','https://www.google.com/maps/place/Life+Planner+Studies+%26+Opportunities+(P)+Ltd./@9.5924073,76.5270647,20z/data=!4m5!3m4!1s0x0:0xa874c155759a8734!8m2!3d9.5924609!4d76.5271465'],
   
];

var geocoder;
var map;
var bounds = new google.maps.LatLngBounds();

function initialize() {
    map = new google.maps.Map(
    document.getElementById("map"), {
        center: new google.maps.LatLng(),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    geocoder = new google.maps.Geocoder();

    for (i = 0; i < locations.length; i++) {


        geocodeAddress(locations, i);
    }
}
google.maps.event.addDomListener(window, "load", initialize);

function geocodeAddress(locations, i) {
    var title = locations[i][0];
    var address = locations[i][1];
    var url = locations[i][2];
    geocoder.geocode({
        'address': locations[i][1]
    },

    function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var marker = new google.maps.Marker({
                // icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                map: map,
                position: results[0].geometry.location,
                title: title,
                animation: google.maps.Animation.DROP,
                address: address,
                url: url
            })
            infoWindow(marker, map, title, address, url);
            bounds.extend(marker.getPosition());
            map.fitBounds(bounds);
        } else {
            alert("geocode of " + address + " failed:" + status);
        }
    });
}

function infoWindow(marker, map, title, address, url) {
    google.maps.event.addListener(marker, 'click', function () {
        var html = "<div><h3>" + title + "</h3><p>" + address + "<br></div><a href='" + url + "' target='_blank'>View location</a></p></div>";
        iw = new google.maps.InfoWindow({
            content: html,
            maxWidth: 350
        });
        iw.open(map, marker);
    });
}

function createMarker(results) {
    var marker = new google.maps.Marker({
        // icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
        map: map,
        position: results[0].geometry.location,
        title: title,
        animation: google.maps.Animation.DROP,
        address: address,
        url: url
    })
    bounds.extend(marker.getPosition());
    map.fitBounds(bounds);
    infoWindow(marker, map, title, address, url);
    return marker;
}


