-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2019 at 03:31 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lifeplanner_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `lp_blogs`
--

CREATE TABLE `lp_blogs` (
  `id` int(11) NOT NULL,
  `author` varchar(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `subtitle` varchar(200) NOT NULL,
  `blogcategory_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_blogs`
--

INSERT INTO `lp_blogs` (`id`, `author`, `title`, `subtitle`, `blogcategory_id`, `content`, `status`, `created`, `modified`) VALUES
(2, 'admin', 'testing', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it', 1, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; text-align: justify; background-color: rgb(255, 255, 255);\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at ligula sapien. Nam at urna at metus mattis eleifend. Proin quis leo risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer ut pretium sem. Phasellus nec vehicula nisi. Sed eget dolor leo. Aenean iaculis urna non lectus blandit auctor. Aliquam eu orci porttitor, dignissim dolor vitae, varius dolor. Aliquam accumsan varius nunc, feugiat venenatis elit. Integer commodo nibh metus, vitae maximus purus auctor at. Maecenas euismod libero orci, non aliquam magna elementum ut. Donec viverra lacus et quam feugiat, id bibendum nisl viverra.</span></p>\r\n', 1, '2019-02-05 11:29:41', '2019-03-08 06:59:21'),
(3, 'admin', 'Testing again simply dummy text of the printing ', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled ', 1, '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; text-align: justify; background-color: rgb(255, 255, 255);\">&nbsp;simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"margin-bottom: 15px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255);\">Integer pretium, diam ut cursus tempor, quam enim hendrerit arcu, eget efficitur orci felis hendrerit dolor. Cras in mauris nec quam posuere vulputate at nec quam. Nullam sed luctus ex, vitae posuere nibh. Suspendisse sit amet elementum nisi. Morbi tempor vestibulum dolor, porttitor sollicitudin diam tincidunt ut. Donec eu quam id magna ultricies tristique lobortis ut diam. Pellentesque fringilla metus eget tortor dignissim, sit amet venenatis sem volutpat. Vivamus non tortor diam.</p>\r\n', 1, '2019-02-12 06:50:24', '2019-03-08 10:24:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lp_blogs`
--
ALTER TABLE `lp_blogs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lp_blogs`
--
ALTER TABLE `lp_blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
