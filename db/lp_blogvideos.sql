-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2019 at 02:26 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lifeplanner_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `lp_blogvideos`
--

CREATE TABLE `lp_blogvideos` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `youtube_link` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_blogvideos`
--

INSERT INTO `lp_blogvideos` (`id`, `blog_id`, `youtube_link`, `status`, `created`, `modified`) VALUES
(2, 2, 'SiwxNBM_UXc', 1, '2019-02-05 12:04:24', '2019-02-05 12:04:24'),
(3, 2, 'CKcai99Eb7c', 1, '2019-02-05 12:05:52', '2019-02-05 12:05:52'),
(4, 2, 'HgDGUb2TjyM', 1, '2019-02-05 12:05:52', '2019-02-05 12:05:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lp_blogvideos`
--
ALTER TABLE `lp_blogvideos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_id` (`blog_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lp_blogvideos`
--
ALTER TABLE `lp_blogvideos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lp_blogvideos`
--
ALTER TABLE `lp_blogvideos`
  ADD CONSTRAINT `lp_blogvideos_ibfk_1` FOREIGN KEY (`blog_id`) REFERENCES `lp_blogs` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
