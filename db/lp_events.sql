-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 11:57 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lifeplanner_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `lp_events`
--

CREATE TABLE `lp_events` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lp_events`
--

INSERT INTO `lp_events` (`id`, `title`, `content`, `image`, `status`, `created`, `modified`) VALUES
(1, 'Prof. Hab Dr.Arkadiusz Jawien Director, Center for Medicine education in English Visits India', '<p><span style=\"font-family: Roboto, sans-serif; font-size: 13px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255);\">Study MBBS where you can assure your seats for PG and PhD thereafter Attend the Seminar and Assure your seats.</span></p>\r\n', 'img-5544738-nico.jpg', 0, '2019-03-01 04:50:31', '2019-03-01 11:54:11'),
(2, 'The biggest University in Central Europe for Arts, Science & Business Attend the Seminar and Assure ', '<p><span style=\"font-family: Roboto, sans-serif; font-size: 13px; font-style: normal; font-weight: 400; background-color: rgb(255, 255, 255);\">The biggest University in Central Europe for Arts, Science &amp; Business Attend the Seminar and Assure your seats .</span></p>\r\n', 'img-5487670-lodz.jpg', 1, '2019-03-01 06:50:31', '2019-03-01 06:50:31');
--
-- Indexes for dumped tables
--

--
-- Indexes for table `lp_events`
--
ALTER TABLE `lp_events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lp_events`
--
ALTER TABLE `lp_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
